<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 31/07/2017
 * Time: 01:05
 */


return [
    'protected_users' => [
        1,4
    ],
    'iata' => [
        'key' => '62dfee7f-844f-48c3-b751-cb6b1f2a6b9c',
        'airports' => 'https://iatacodes.org/api/v6/airports?api_key=',
        'cities' => 'https://iatacodes.org/api/v6/cities?api_key=',
        'airlines' => 'https://iatacodes.org/api/v6/airlines?api_key=',
        'aircraft' => 'https://iatacodes.org/api/v6/aircrafts?api_key=',
        'airplane' => 'https://iatacodes.org/api/v6/aircrafts?api_key=',
        'routes' => 'https://iatacodes.org/api/v6/routes?api_key=',
    ],
    'upload_folders' => [
        'profile' => 'uploads/profile',
        'logos' => 'uploads/logos',
        'docs' => 'uploads/docs',
        'posters' => 'uploads/posters',
        'pdfs' => 'uploads/pdfs',
    ],
    'yahoo_url'         => 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDKES%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=',
];

