<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::auth();
//Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('/login', 'Auth\LoginController@login')->name('login_auth');
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);

    return redirect()->back();
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('wyswg',function(){
    return view('test.editor-demo');
});

Route::get('/', function () {
    if (Auth::check()) {
        if (Auth::user()->isRole('Admin')) {
            return redirect()->route('org.index');
        }
        else {
            if (isset(Auth::user()->code)) {
                $code = Auth::user()->code;
                $inv = \App\Models\Invitations::where('code','=',$code)
                    ->with('event')->first();
                $event = $inv->event;
            }
            else {
                $user =  \App\Models\DelegateProfile::where('user_id','=',Auth::user()->id)
                    ->with('invitations','invitations.event')->first();
                $inv = $user->invitations->last();
                $event = $inv->event;
                $code = $inv->get('code');
            }
            if ($code && $event) {
                return redirect()->route('del.view.invitation',[$code,$event->slug]);
            }
            else {
                return redirect()->route('logout');
            }
        }
    }
    redirect()->route('logout');
})->middleware('auth');

Route::get('/home', function () {
    if (Auth::check()) {
        if (Auth::user()->isRole('Admin')) {
            return redirect()->route('org.index');
        }
        else {
            if (isset(Auth::user()->code)) {
                $code = Auth::user()->code;
                $inv = \App\Models\Invitations::where('code','=',$code)
                ->with('event')->first();
                $event = $inv->event;
            }
            else {
                $user =  \App\Models\DelegateProfile::where('user_id','=',Auth::user()->id)
                ->with('invitations','invitations.event')->first();
                $inv = $user->invitations->last();
                $event = $inv->event;
                $code = $inv->get('code');
            }
            if ($code && $event) {
                return redirect()->route('del.view.invitation',[$code,$event->slug]);
            }
            else {
                return redirect()->route('logout');
            }
        }
    }
    redirect()->route('logout');
})->middleware('auth')->name('redirect.login');

 Auth::routes();


Route::any('logout/{id?}','Auth\LoginController@logout')->name('logout');
//Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
//Route::get('/event/{id}', 'HomeController@ViewEvent')->name('org.view.event');
Route::get('/password/reset','FrontController@forgot')->name('forgot');
Route::any('/forgot/password', 'FrontController@ForgotPassword')->name('forgot.password');
Route::any('/reset/password/{token}', 'FrontController@ResetPassword')->name('reset.password');

Route::any('/event/{eventid}', 'FrontController@index')->name('org.view.event');
Route::any('/event/{eventid}/about', 'FrontController@Aboutindex')->name('org.view.event.about');
Route::any('/event/{eventid}/sponsors', 'FrontController@ViewEventSponsors')->name('org.view.event.spo');
Route::any('/event/{eventid}/essential/info', 'FrontController@ViewEventEssentialInfo')->name('org.view.event.info');
Route::any('/event/{eventid}/delegate/verify', 'FrontController@DelegateVerifyCode')->name('del.validate.code');
Route::any('/event/{code}/{eventid}/delegate/login', 'FrontController@DelegateLoginCode')->name('delegate.login');
Route::any('/event/{code}/{eventid}/delegate/renew', 'FrontController@DelegateLoginRenew')->name('delegate.login.renew.code');
Route::any('/event/{code}/{eventid}/delegate/reset/{token}', 'FrontController@DelegateLoginReset')->name('delegate.login.reset.code');
Route::any('/programme/download/{id}', 'FrontController@getDownload')->name('get.download');


Route::any('see/files/{filename}', function ($filename)
{
    $filename = str_replace('~','/',$filename);
    $path = storage_path('/public/'.$filename);
    if(! Storage::disk('public')->exists($filename) ) abort(404);
    $file = Storage::disk('public')->get($filename);//File::get($path);
    $type = Storage::disk('public')->mimeType($filename);//File::mimeType($path);
    //$response = Response::make($file, 200);
    //$response->header("Content-Type", $type);
    //return $response;
    return \Image::make($file)->response();
})->name('org.files');

//'has.role:admin',
Route::group(['prefix' => 'organizer','middleware' => ['auth','admin']], function()
{
    Route::get('/home', 'OrganizerController@index')->name('org.index');
    Route::get('/', 'OrganizerController@index')->name('org.index');

    Route::get('/events', 'OrganizerController@EventList')->name('org.events');
    Route::get('/payments', 'OrganizerController@SystemPayments')->name('org.payments');
    Route::get('/refunds', 'OrganizerController@SystemRefunds')->name('org.refunds');
    Route::get('/sales/stats', 'OrganizerController@SystemStats')->name('org.sales.stats');
    Route::get('/notices', 'OrganizerController@EventNotifications')->name('org.notices');
    Route::any('/events/new', 'OrganizerController@EventListNew')->name('org.new.event');

    Route::any('/event/dispatch/view/delegation/{id}', 'OrganizerController@EventDelegationView')->name('org.delegation.send');
    Route::any('/event/dispatch/view/delegate/{id}', 'OrganizerController@EventDelegateSend')->name('org.delegate.send');
    Route::get('/event/confirmation/{id}/{code}', 'OrganizerController@EventDelegationConfirmation')->name('org.event.confirmation');
    Route::any('/event/delegates/down/xls/{id}', 'OrganizerController@DelegateDbDownload')->name('download.inv.db');
    Route::any('/event/delegates/down/custom/{id}', 'OrganizerController@DelegateDbCustomDownload')->name('download.inv.custom.db');

    Route::any('/events/edit/meta/{id}', 'OrganizerController@EventListManageMeta')->name('org.edit.event.meta');
    Route::post('/events/edit/meta/{id}/{event}', 'OrganizerController@EventMetaEdit')->name('org.edit.meta');
    Route::post('/events/delete/meta/{id}/{event}', ['nocsrf' => TRUE,'uses' => 'OrganizerController@EventMetaDelete'])->name('org.del.meta');
    Route::post('/events/new/meta/{id}', 'OrganizerController@EventMetaNew')->name('org.new.meta');

    Route::any('/events/edit/this/{id}', 'OrganizerController@EventListEdit')->name('org.edit.event');
    Route::any('/events/overview/{id}', 'OrganizerController@EventListOverview')->name('org.view.overview');
    Route::post('/events/delete/event/{event}', ['nocsrf' => TRUE,'uses' => 'OrganizerController@EventDelete'])->name('org.edit.delete.event');
    Route::post('/events/publish/event/{event}', ['nocsrf' => TRUE,'uses' => 'OrganizerController@EventPublish'])->name('org.edit.publish.event');

    Route::post('/events/profile/list', ['nocsrf' => TRUE,'uses' => 'OrganizerController@ProfilesList'])->name('org.dt.list.events.profiles.list');
    Route::post('/events/user/log/list', ['nocsrf' => TRUE,'uses' => 'OrganizerController@UserActivity'])->name('org.dt.list.user.log.list');

    Route::post('/events/send/email', ['nocsrf' => TRUE,'uses' => 'OrganizerController@SendEmail'])->name('org.email.profile');
    Route::post('/events/send/sms', ['nocsrf' => TRUE,'uses' => 'OrganizerController@SendSms'])->name('org.sms.profile');
//    Route::any('/events/profile/d/id/{id}', 'OrganizerController@EventManageProgram')->name('org.d.id.profile');
//    Route::any('/events/profile/d/prof/{id}', 'OrganizerController@EventManageProgram')->name('org.d.pic.profile');

    Route::any('/events/manage/programme/{id}', 'OrganizerController@EventManageProgram')->name('org.manage.event.prog');
    Route::any('/events/edit/programme/{id}/{eventid}', ['nocsrf' => TRUE,'uses' => 'OrganizerController@EventEditProgram'])->name('org.edit.event.prog');
    Route::any('/events/delete/programme/{id}/{eventid}', ['nocsrf' => TRUE,'uses' => 'OrganizerController@EventDeleteProgram'])->name('org.delete.event.prog');

    Route::any('/events/manage/tickets/{id}', 'OrganizerController@EventTicketManage')->name('org.manage.event.tickets');
    Route::any('/events/edit/tickets/{id}/{event}', 'OrganizerController@EventTicketEdit')->name('org.edit.tickets.event');
    Route::any('/events/delete/tickets/{id}/{event}', 'OrganizerController@EventTicketDelete')->name('org.delete.tickets.event');

    Route::any('/events/manage/images/{id}', 'OrganizerController@EventImagesManage')->name('org.manage.event.images');
    Route::any('/events/edit/images/{id}/{event}', [ 'nocsrf' => TRUE,'uses' => 'OrganizerController@EventsPostersEdit'])->name('org.edit.poster.event');
    Route::any('/events/delete/images/{id}/{event}', [ 'nocsrf' => TRUE,'uses' =>'OrganizerController@EventImagesDelete'])->name('org.delete.poster.event');

    Route::any('/events/manage/hotel/{id}', 'OrganizerController@EventHotelsManage')->name('org.manage.event.hotels');
    Route::any('/events/edit/hotel/{id}/{event}', 'OrganizerController@EventHotelsEdit')->name('org.edit.event.hotel');
    Route::any('/events/delete/hotel/{id}/{event}', 'OrganizerController@EventHotelsDelete')->name('org.delete.event.hotel');

    Route::any('/events/manage/activities/{id}', 'OrganizerController@EventActivitiesManage')->name('org.manage.event.activities');

    Route::any('/events/manage/transfer/{id}', 'OrganizerController@EventTransferManage')->name('org.manage.event.transfer');

    Route::any('/events/manage/sponsors/{id}', 'OrganizerController@EventSponsorsManage')->name('org.manage.event.sponsors');

    Route::any('/events/manage/organizers/{id}', 'OrganizerController@EventOrganizersManage')->name('org.manage.event.organizers');

    Route::any('/events/manage/news/{id}', 'OrganizerController@EventNewsManage')->name('org.manage.event.news');

    Route::post('/events/new/ticket/{id}', 'OrganizerController@EventTicketNew')->name('org.new.ticket');
    Route::any('/events/new/bulk/ticket/{id}', 'OrganizerController@EventTicketBulkNew')->name('org.new.ticket.bulk');
    Route::post('/events/new/hotel/{id}', 'OrganizerController@EventHotelNew')->name('org.new.hotel');

    Route::post('/events/new/act/{id}', 'OrganizerController@EventActNew')->name('org.new.act');
    Route::post('/events/edit/act/{id}/{event}', ['nocsrf' => TRUE,'uses'=>'OrganizerController@EventActEdit'])->name('org.edit.act');

    Route::post('/events/new/t/packages/{id}', 'OrganizerController@EventTransNew')->name('org.new.trans');
    Route::post('/events/edit/t/packages/{id}/{event}',  ['nocsrf' => TRUE,'uses'=>'OrganizerController@EventTransEdit'])->name('org.edit.trans');
    Route::any('/events/en/de/package/{id}/{event}',['nocsrf' => TRUE, 'as' => 'org.event.dis.package', 'uses' => 'OrganizerController@EnDisEventPackages']);


    Route::post('/events/new/map/{id}', 'OrganizerController@EventMapNew')->name('org.new.map');
    Route::post('/events/new/programme/{id}', 'OrganizerController@EventProgNew')->name('org.new.prog');

    Route::post('/events/new/news/{id}', 'OrganizerController@EventNewsNew')->name('org.new.news');
    Route::post('/events/edit/news/{id}', 'OrganizerController@EventNewsEdit')->name('org.edit.news.event');
    Route::post('/events/del/news/{id}', 'OrganizerController@EventNewsDel')->name('org.delete.news.event');

    Route::post('/events/new/sponsors/{id}', 'OrganizerController@EventSponsorNew')->name('org.new.sponsor');
    Route::post('/events/edit/sponsors/{id}/{event}', 'OrganizerController@EventSponsorEdit')->name('org.edit.sponsor');
    Route::post('/events/delete/sponsors/{id}/{event}', ['nocsrf' => TRUE,'uses' =>'OrganizerController@EventSponsorDelete'])->name('org.del.sponsor');

    Route::post('/events/new/organizer/{id}', 'OrganizerController@EventOrganizerNew')->name('org.new.organizer');
    Route::post('/events/edit/organizer/{id}/{event}', 'OrganizerController@EventOrganizerEdit')->name('org.edit.organizer');
    Route::post('/events/del/organizer/{id}/{event}', ['nocsrf' => TRUE,'uses' =>'OrganizerController@EventOrganizerDelete'])->name('org.delete.organizer');

    Route::post('/events/new/upload/imgs/{id}', 'OrganizerController@uploadEventImg')->name('org.new.event.img');

    Route::any('/events/new/bulk/upload/imgs/{event}/{id}/{type}', 'OrganizerController@uploadEventBulkImg')->name('org.new.event.bulk.img');
    Route::any('/events/new/bulk/posters/{event}', 'OrganizerController@uploadEventBulkPosters')->name('org.new.event.bulk.poster');

    Route::any('/events/new/upload/poster/{id}', 'OrganizerController@uploadEventImg')->name('org.new.event.posters');
    Route::any('/events/new/upload/sponsors/{id}', 'OrganizerController@uploadEventImg')->name('org.new.event.sponsors');
    Route::any('/events/new/upload/organizers/{id}', 'OrganizerController@uploadEventImg')->name('org.new.event.organizers');
    Route::any('/events/new/upload/tickets/{id}', 'OrganizerController@uploadEventImg')->name('org.new.event.tickets');

    Route::any('/events/new/delegation/deposit/{delegation_id}/{event_id}', 'OrganizerController@newDelegationDeposit')->name('org.add.overpayment');

    Route::any('/events/delegate/types/{event}', 'OrganizerController@Delegateindex')->name('org.delegate.types');

    Route::any('/events/delegations/{event}', 'OrganizerController@newEventDelegations')->name('org.events.delegations');
    Route::any('/events/custom/fields/{event}', 'OrganizerController@newEventFields')->name('org.events.custom.fields');
    Route::any('/events/delegations/status/{id}/{event}', 'OrganizerController@statusEventDelegations')->name('org.events.delegations.status');
    Route::any('/events/delegations/edit/{id}/{event}', 'OrganizerController@editEventDelegations')->name('org.events.delegations.edit');
    Route::any('/events/delegations/uploads/{event}', 'OrganizerController@newEventDelegationsUploads')->name('org.events.delegations.uploads');
    Route::any('/events/export/database/{event}', 'OrganizerController@exportEventDatabase')->name('org.events.export.database');
    Route::any('/edit/invite/{id}', 'OrganizerController@editInvite')->name('org.edit.invite');

    //Route::any('/events/inv/template/{event}', 'OrganizerController@newEventInvitationTemplate')->name('org.events.inv.template');
    Route::any('/events/invitations/make/{delegation?}/{event?}', 'OrganizerController@newEventInvitationMake')->name('org.events.inv.send');
    Route::any('/events/invitations/payment/{delegation?}/{event?}', 'OrganizerController@newEventInvitationPayment')->name('org.events.inv.payment');
    Route::any('/events/invitations/resend/{delegation?}/{event?}', 'OrganizerController@EventInvitationResend')->name('org.events.inv.resend');
    Route::any('/events/invitations/print/card/{delegation?}/{event?}', 'OrganizerController@EventInvitationPrintCard')->name('org.events.inv.print.card');
    Route::any('/events/invitations/print/all/{delegation?}/{event?}', 'OrganizerController@EventInvitationPrintAll')->name('org.events.inv.print.all');
    Route::any('/events/invitations/print/statement/{delegation?}/{event?}', 'OrganizerController@EventInvitationPrintStatement')->name('org.events.statement.print.all');
    Route::any('/events/invitations/manage/all/{delegation?}/{event?}', 'OrganizerController@newEventInvitationMake')->name('org.events.inv.manage.all');
    Route::any('/events/delegations/print/{delegation?}/{event?}', 'OrganizerController@delegationPrint')->name('org.events.inv.print');
    Route::any('/events/invitations/status/{delegation?}/{event?}',['nocsrf' => TRUE,'uses' =>'OrganizerController@invitationStatusChange'] )->name('org.events.inv.status');
    Route::any('/events/invitations/edit/p/{delegation?}/{event?}', 'OrganizerController@invitationEditProfile')->name('org.events.inv.edit.profile');
    Route::any('/events/invitations/edit/s/{delegation?}/{event?}', 'OrganizerController@invitationEditService')->name('org.events.inv.edit.service');


    Route::any('/login/as/delegate/{event}/{type}',['nocsrf' => TRUE, 'as' => 'org.events.login.as.delegate', 'uses' => 'OrganizerController@loginAsDelegate']);
    Route::any('/events/see/delegates/list/all/{event}/{type}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.delegates.all', 'uses' => 'OrganizerController@listEventExportInvitations']);
    Route::any('/events/see/fields/list/all',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.fields', 'uses' => 'OrganizerController@ListEventsFields']);
    Route::any('/events/see/fields/list/status/{id}/{event}',['nocsrf' => TRUE, 'as' => 'org.event.dis.field', 'uses' => 'OrganizerController@ListEventsStatusFields']);
    Route::any('/events/see/fields/list/edit/{id}/{event}',['nocsrf' => TRUE, 'as' => 'org.edit.field', 'uses' => 'OrganizerController@ListEventsEditFields']);
    Route::any('/events/see/list/{status}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events', 'uses' => 'OrganizerController@ListEvents']);
    Route::any('/events/see/delegation/payment/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegation.payment', 'uses' => 'OrganizerController@ListDelegationPayments']);
    Route::any('/events/see/delegation/refund/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegation.refund', 'uses' => 'OrganizerController@ListDelegationRefunds']);
    Route::any('/events/see/delegation/overpayment/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegation.overpayment', 'uses' => 'OrganizerController@ListDelegationOverPayments']);
    Route::any('/events/see/delegation/list/{id?}',['nocsrf' => TRUE, 'as' => 'org.events.delegations.list', 'uses' => 'OrganizerController@listEventDelegation']);
    Route::any('/events/uploads/delegation/list/{id?}',['nocsrf' => TRUE, 'as' => 'org.events.upload.delegations.list', 'uses' => 'OrganizerController@listEventDelegationUploads']);
    Route::any('/events/uploads/delegation/edit/{id?}',['nocsrf' => TRUE, 'as' => 'org.edit.upload.delegation.event', 'uses' => 'OrganizerController@EditEventDelegationUploads']);
    Route::any('/events/uploads/delegation/delete/{id?}',['nocsrf' => TRUE, 'as' => 'org.delete.upload.delegation.event', 'uses' => 'OrganizerController@DeleteEventDelegationUploads']);
    Route::any('/events/see/delegation/invitations/list/{id?}',['nocsrf' => TRUE, 'as' => 'org.events.delegations.invitations.list', 'uses' => 'OrganizerController@listEventDelegationInvitations']);
    Route::any('/events/see/news/{status}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.news', 'uses' => 'OrganizerController@ListEventsNews']);
    Route::any('/events/see/prog/list/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.prog', 'uses' => 'OrganizerController@ListEventsProg']);
    Route::any('/events/see/posters/list/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.posters', 'uses' => 'OrganizerController@ListEventsPosters']);
    Route::any('/events/see/org/list/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.org', 'uses' => 'OrganizerController@ListEventsOrganizers']);
    Route::any('/events/see/spo/list/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.spo', 'uses' => 'OrganizerController@ListEventsSponsors']);
    Route::any('/events/delete/invitation/{inv}/{event}',['nocsrf' => TRUE, 'as' => 'org.events.inv.delete', 'uses' => 'OrganizerController@DeleteEventInvitation']);

    Route::any('/events/see/o/imgs/list/{event}/{rid}/{type}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.ot.images', 'uses' => 'OrganizerController@ListOtherImages']);
    Route::any('/events/edit/o/imgs/list/{id}/{event}/{rid}/{type}',['nocsrf' => TRUE, 'as' => 'org.edit.image.sys', 'uses' => 'OrganizerController@EditOtherImages']);
    Route::any('/events/dele/o/imgs/list/{id}/{event}/{rid}/{type}',['nocsrf' => TRUE, 'as' => 'org.delete.image.sys', 'uses' => 'OrganizerController@DelOtherImages']);
    Route::any('/events/en/de/activity/{id}/{event}',['nocsrf' => TRUE, 'as' => 'org.event.dis.act', 'uses' => 'OrganizerController@EnDisEventActivities']);
    Route::any('/events/delete/activity/{id}/{event}',['nocsrf' => TRUE, 'as' => 'org.event.delete.act', 'uses' => 'OrganizerController@DeleteEventActivities']);

    Route::any('/events/see/hotels/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.hotels', 'uses' => 'OrganizerController@ListEventsHotels']);
    Route::any('/events/see/act/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.act', 'uses' => 'OrganizerController@ListEventsAct']);
    Route::any('/events/see/trans/{id}',['nocsrf' => TRUE, 'as' => 'org.dt.list.trans', 'uses' => 'OrganizerController@ListEventsTrans']);
    Route::any('/events/see/meta/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.meta', 'uses' => 'OrganizerController@ListEventsMeta']);
    Route::any('/events/see/tickets/{event}',['nocsrf' => TRUE, 'as' => 'org.dt.list.events.tickets', 'uses' => 'OrganizerController@ListEventsTickets']);

    Route::any('/events/see/user/log/{user?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.user.logs', 'uses' => 'OrganizerController@UsersLogindexlist']);
    Route::any('/events/see/user/lsit/{user?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.user.list', 'uses' => 'OrganizerController@UsersListindexlist']);
    Route::any('/events/see/delegate/lsit/{id?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegate.list', 'uses' => 'OrganizerController@DelegateListindexlist']);
    Route::any('/events/see/delegate/all',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegate.list.all', 'uses' => 'OrganizerController@DelegateListindexall']);
    Route::any('/events/see/delegation/lsit/{user?}',['nocsrf' => TRUE, 'as' => 'org.dt.list.delegation.list', 'uses' => 'OrganizerController@DelegationListindexlist']);
    Route::any('/system/see/countries',['nocsrf' => TRUE, 'as' => 'org.dt.list.countries.list', 'uses' => 'OrganizerController@CountriesListindexlist']);
    Route::any('/system/see/title',['nocsrf' => TRUE, 'as' => 'org.dt.list.titles.list', 'uses' => 'OrganizerController@TitlesListindexlist']);
    Route::any('/system/see/news/cat',['nocsrf' => TRUE, 'as' => 'org.dt.list.newscat.list', 'uses' => 'OrganizerController@NewsCatListindexlist']);

    Route::any('/system/status/news/cat/{id}',['nocsrf' => TRUE, 'as' => 'org.newscat.status', 'uses' => 'OrganizerController@NewsCatStatus']);
    Route::any('/system/edit/news/cat/{id}',['nocsrf' => TRUE, 'as' => 'org.newscat.edit', 'uses' => 'OrganizerController@NewsCatEdit']);

    Route::any('/system/status/titles/{id}',['nocsrf' => TRUE, 'as' => 'org.title.status', 'uses' => 'OrganizerController@TitlesStatus']);
    Route::any('/system/edit/titles/{id}',['nocsrf' => TRUE, 'as' => 'org.titles.edit', 'uses' => 'OrganizerController@TitlesEdit']);

    Route::any('/system/see/flight/Airports',['nocsrf' => TRUE, 'as' => 'org.dt.list.flight.airports', 'uses' => 'OrganizerController@FlightAirportsindexlist']);
    Route::any('/system/see/flight/Airlines',['nocsrf' => TRUE, 'as' => 'org.dt.list.flight.Airlines', 'uses' => 'OrganizerController@FlightAirlinesindexlist']);
    Route::any('/system/see/flight/Aircrafts',['nocsrf' => TRUE, 'as' => 'org.dt.list.flight.Aircrafts', 'uses' => 'OrganizerController@FlightAircraftsindexlist']);
    Route::any('/system/see/flight/Routes',['nocsrf' => TRUE, 'as' => 'org.dt.list.flight.Routes', 'uses' => 'OrganizerController@FlightRoutesindexlist']);

    Route::get('/profile', 'OrganizerController@index')->name('org.profile');
    Route::get('/activities', 'OrganizerController@index')->name('org.activities');
    Route::get('/account', 'OrganizerController@index')->name('org.account');
    Route::get('/sales/account', 'OrganizerController@SalesAccounts')->name('org.accounts');
    Route::get('/system/reports', 'OrganizerController@index')->name('org.reports');
    // Route::any('/settings/delegate/types', 'OrganizerController@Delegateindex')->name('org.delegate.types');
    Route::any('/settings/delegation/types', 'OrganizerController@Delegationindex')->name('org.delegation.types');
    Route::any('/settings/flight/info', 'OrganizerController@Flightindex')->name('org.flight.info');
    Route::any('/settings/country/info', 'OrganizerController@Countryindex')->name('org.country.info');
    Route::any('/settings/titles/info', 'OrganizerController@Titlesindex')->name('org.titles.info');
    Route::any('/settings/news/category/info', 'OrganizerController@NewsCategoryindex')->name('org.newcategory.info');
    Route::any('/settings/event/activity', 'OrganizerController@EventActivityindex')->name('org.activities.info');

    Route::any('/settings/emailtemplates/', 'OrganizerController@EmailTemplatesIndex')->name('org.email.template');
    Route::post('/settings/emailtemplates/new/', 'OrganizerController@EmailTemplatesNew')->name('org.email.template.new');
    Route::any('/settings/emailtemplates/edit/{id}/', 'OrganizerController@EmailTemplatesEdit')->name('org.email.template.edit');
    Route::any('/settings/emailtemplates/delete/{id}/', 'OrganizerController@EmailTemplatesDelete')->name('org.email.template.delete');

    Route::any('/settings/event/hotels', 'OrganizerController@EventHotelsindex')->name('org.hotels.info');
    Route::any('/settings/event/flights', 'OrganizerController@EventDelegateFlightindex')->name('org.transport.info');
    Route::any('/settings/event/invitations', 'OrganizerController@EventDelegateInvitationsindex')->name('org.invitations.info');
    Route::any('/settings/event/delegation', 'OrganizerController@EventDelegationtindex')->name('org.delegations.info');
    Route::any('/settings/event/delegate', 'OrganizerController@EventDelegatetindex')->name('org.delegate.info');
    Route::any('/settings/event/transfers', 'OrganizerController@EventTransferstindex')->name('org.logistics.info');

    //Route::get('/settings/event/transfers', 'OrganizerController@Transferstindex')->name('org.logistics.info');
    Route::any('/settings/sales/ac/new/pay/{id}/{event}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsNewPayment'])->name('org.sales.accounts.new.payment');
    Route::any('/settings/sales/ac/edit/pay/{id}/{event}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsEditPayment'])->name('org.sales.accounts.edit.payment');
    Route::any('/settings/sales/ac/edit/ref/{id}/{event}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsEditRefund'])->name('org.sales.accounts.edit.refund');

    Route::any('/settings/sales/ac/new/ref/{id?}/{inv?}/{event?}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsNewRefund'])->name('org.sales.accounts.new.refund');
    Route::any('/settings/sales/ac/print/rec/{id?}/{inv?}/{event?}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsPrintReceipt'])->name('org.sales.accounts.print.receipt');
    Route::any('/settings/sales/ac/send/rec/{id?}/{inv?}/{event?}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsSendReceipt'])->name('org.sales.accounts.send.receipt');

    Route::any('/settings/sales/ac/list', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@SalesAccountsLists'])->name('org.sales.accounts.list');
    Route::any('/settings/delegate/status/{id}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@EditdelegateStatus'])->name('org.fix.delegate.status');
    Route::any('/settings/delegate/edit/{id}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@EditDelegate'])->name('org.fix.delegate.edit');
    Route::any('/settings/delegation/status/{id}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@EditdelegationStatus'])->name('org.fix.delegation.status');
    Route::any('/settings/delegation/edit/{id}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@EditDelegation'])->name('org.fix.delegation.edit');

    Route::any('/settings/list/del/t/packages/{type}/{event}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@ListTransferPackages'])->name('org.events.del.tpackage.list');
    Route::any('/settings/list/flight/t/packages/{type}', ['nocsrf' => TRUE, 'uses'=>'OrganizerController@ListFlightPackages'])->name('org.dt.list.flight.list');


    //setting
    Route::any('/settings/users', 'OrganizerController@Usersindex')->name('org.users.mgr');
    Route::get('/settings/users/logs', 'OrganizerController@UsersLogindex')->name('org.users.logs');
    Route::post('/settings/fix/role/{id}', ['nocsrf' => TRUE, 'uses'=> 'OrganizerController@UsersFixrole'])->name('org.fix.user.role');
    Route::post('/settings/fix/status/{id}', ['nocsrf' => TRUE, 'uses'=> 'OrganizerController@UsersFixstatus'])->name('org.fix.user.status');
    Route::any('/settings/fix/password/{id}',['nocsrf' => TRUE, 'uses'=>  'OrganizerController@UsersFixpassword'])->name('org.fix.user.password');
    Route::post('/settings/login/as/{id}', ['nocsrf' => TRUE, 'uses'=> 'OrganizerController@UsersLoginAs'])->name('org.fix.user.loginas');


    Route::post('/events/new/tent/{id}', 'OrganizerController@EventTentNew')->name('org.new.tent');
    Route::any('/events/manage/tent/{id}', 'OrganizerController@EventTentsManage')->name('org.manage.event.tents');
    Route::any('/events/edit/tent/{id}/{event}', 'OrganizerController@EventTentsEdit')->name('org.edit.event.tent');
    Route::any('/events/delete/tent/{id}/{event}', 'OrganizerController@EventTentsDelete')->name('org.delete.event.tent');

    Route::post('/events/new/term/{id}', 'OrganizerController@EventTermNew')->name('org.new.term');
    Route::any('/events/manage/term/{id}', 'OrganizerController@EventTermsManage')->name('org.manage.event.terms');
    Route::any('/events/edit/term/{id}/{event}', 'OrganizerController@EventTermsEdit')->name('org.edit.event.term');
    Route::any('/events/delete/term/{id}/{event}', 'OrganizerController@EventTermsDelete')->name('org.delete.event.term');

    Route::post('/events/new/disclaimer/{id}', 'OrganizerController@EventDisclaimerNew')->name('org.new.disclaimer');
    Route::any('/events/manage/disclaimer/{id}', 'OrganizerController@EventDisclaimersManage')->name('org.manage.event.disclaimers');
    Route::any('/events/edit/disclaimer/{id}/{event}', 'OrganizerController@EventDisclaimersEdit')->name('org.edit.event.disclaimer');
    Route::any('/events/delete/disclaimer/{id}/{event}', 'OrganizerController@EventDisclaimersDelete')->name('org.delete.event.disclaimer');

    Route::post('/events/new/media/{id}', 'OrganizerController@EventMediaNew')->name('org.new.media');
    Route::any('/events/manage/media/{id}', 'OrganizerController@EventMediasManage')->name('org.manage.event.medias');
    Route::any('/events/edit/media/{id}/{event}', 'OrganizerController@EventMediasEdit')->name('org.edit.event.media');
    Route::any('/events/delete/media/{id}/{event}', 'OrganizerController@EventMediasDelete')->name('org.delete.event.media');

    Route::post('/events/new/bookingForm/{id}', 'OrganizerController@EventBookingFormNew')->name('org.new.bookingForm');
    Route::any('/events/manage/bookingForm/{id}', 'OrganizerController@EventBookingFormsManage')->name('org.manage.event.bookingForms');
    Route::any('/events/edit/bookingForm/{id}/{event}', 'OrganizerController@EventBookingFormsEdit')->name('org.edit.event.bookingForm');
    Route::any('/events/delete/bookingForm/{id}/{event}', 'OrganizerController@EventBookingFormsDelete')->name('org.delete.event.bookingForm');



});

Route::group(['prefix' => 'delegation','middleware' => ['auth','delegate']], function()
{
    // verify a few user details
    Route::any('/event/{code}/{eventid}/delegate/change', 'FrontController@DelegateLoginChangePassword')->name('delegate.login.change.code');
    Route::any('/event/{code}/{eventid}/delegate/email', 'FrontController@DelegateLoginConfirmEmail')->name('delegate.login.confirm.email');
    //basic routes
    Route::get('/home', 'DelegateController@index')->name('del.index');
    Route::get('/delegate/logout/{code?}/{event}', 'DelegateController@logout')->name('del.logout');
    //frontend on login
    Route::any('/event/{eventid}/{code?}', 'DelegateController@Eventindex')->name('del.view.event');
    Route::any('/event/{eventid}/{code?}/about', 'DelegateController@AboutEventindex')->name('del.view.event.about');
    Route::any('/event/{eventid}/sponsors/{code?}', 'DelegateController@ViewEventSponsors')->name('del.view.event.spo');
    Route::any('/event/{eventid}/essential/info/{code?}', 'DelegateController@ViewEventEssentialInfo')->name('del.view.event.info');
    //edit invitation
    Route::any('/edit/invitation/{invitation}/{event}', ['nocsrf' => TRUE,'uses' =>'DelegateController@ManageInvitation'])->name('del.manage.invitation');
    Route::any('/edit/invitation/hotel/{invitation}/{event}', ['nocsrf' => TRUE,'uses' =>'DelegateController@ManageHotelInvitation'])->name('del.manage.hotel.invitation');
    Route::any('/edit/prev/invitation/{invitation}/{event}', ['nocsrf' => TRUE,'uses' =>'DelegateController@ManageInvitation'])->name('del.manage.prev.invitation');
    Route::any('/view/invitation/{invitation}/{event}', 'DelegateController@ViewInvitation')->name('del.view.invitation');
    //ajax
    Route::get('/search/airport/',array('as'=>'search.airport','uses'=>'DelegateController@autocompleteAirports'));
    Route::get('/search/airline/',array('as'=>'search.airline','uses'=>'DelegateController@autocompleteAirlines'));
    Route::get('/search/aircraft/',array('as'=>'search.aircraft','uses'=>'DelegateController@autocompleteAircrafts'));
    Route::get('/search/flight/number/',array('as'=>'search.flight.number','uses'=>'DelegateController@autocompleteFlightNumber'));
    //print
    Route::any('/events/invitation/print/all/{inv_id?}/{event?}', 'DelegateController@EventInvitationPrintAll')->name('del.events.inv.print.all');
    Route::any('/settings/sales/ac/print/rec/{id?}', ['nocsrf' => TRUE, 'uses'=>'DelegateController@SalesAccountsPrintReceipt'])
        ->name('del.sales.accounts.print.receipt');
    Route::any('/terms/download', 'DelegateController@getTerms')->name('get.terms');
});

/**
 * Teamwork routes
 */

Route::group(['prefix' => 'teams', 'namespace' => 'Teamwork'], function()
{
    Route::get('/', 'TeamController@index')->name('teams.index');
    Route::get('create', 'TeamController@create')->name('teams.create');
    Route::post('teams', 'TeamController@store')->name('teams.store');
    Route::get('edit/{id}', 'TeamController@edit')->name('teams.edit');
    Route::put('edit/{id}', 'TeamController@update')->name('teams.update');
    Route::delete('destroy/{id}', 'TeamController@destroy')->name('teams.destroy');
    Route::get('switch/{id}', 'TeamController@switchTeam')->name('teams.switch');

    Route::get('members/{id}', 'TeamMemberController@show')->name('teams.members.show');
    Route::get('members/resend/{invite_id}', 'TeamMemberController@resendInvite')->name('teams.members.resend_invite');
    Route::post('members/{id}', 'TeamMemberController@invite')->name('teams.members.invite');
    Route::delete('members/{id}/{user_id}', 'TeamMemberController@destroy')->name('teams.members.destroy');

    Route::get('accept/{token}', 'AuthController@acceptInvite')->name('teams.accept_invite');
});


Route::get('api/get-country-callcode','DelegateController@getCallingCodeList');
