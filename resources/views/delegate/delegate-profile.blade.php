<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/07/2017
 * Time: 23:39
 */
?>

@extends('layouts.front-v-iii')

@section('content')

    <style>
        h4 { color: #84754e !important; }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            background: transparent;
            color: #84754e;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            background: transparent;
            color: #84754e;
            font-weight: bold;
            padding: 5px 15px 4px;
        }

        .nav-tabs > li > a, .nav-tabs > li > a:hover, .nav-tabs > li > a:focus {
            background: transparent;
            color: #84754e;
            border-radius: 0;
            padding: 5px 15px 4px;
            border: none !important;
        }

        .tab-content {
            background:transparent;
            padding: 0px;
        }

        .nav-tabs {
            border-bottom:none;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
        {
            border-top: none;
        }

        .roundimage {
            position: relative;
            overflow: hidden;
            -webkit-border-radius: 50% !important;
            -moz-border-radius: 50% !important;
            -ms-border-radius: 50% !important;
            -o-border-radius: 50% !important;
            border-radius: 50% !important;
            width: 150px;
            height: 150px;
            max-width: 150px;
            max-height: 150px;
            padding: 0px !important;
        }

        .roundimage img {
            display: inline;
            margin: 0 auto !important;
            height: 100%;
            width: auto;
        }
        .badge-circle {
            border-radius: 50%!important;
            display: inline-block;
            /*margin-right: 20px;*/
        }
    </style>

    <div class="container">
        {{--{{ dd($invitation->delegation->countryname->details->demonym) }}--}}
        {{--{{ dd($countries->first()) }}--}}
      <!-- <div class="row" style="text-align:center">
       <p class="impact-normal" style="font-family:Lato-Black;"><b>{{$event->name}}</b></p>
       <p style="font-size: 18pt !important; font-family: 'Lato-Light' !important;"> {{ $event->venue }}  </p>
       <p style="font-size: 18pt !important; font-family: 'Lato-Light' !important;">{{ date('d',strtotime($event->start)) }} {{ date('F',strtotime($event->start)) }} - {{ date('d',strtotime($event->end)) }} {{ date('F',strtotime($event->end)) }} {{ date('Y',strtotime($event->start)) }} </p>
      </div> -->
        <form action="{{ route ('del.manage.invitation',[$code,$eventid]) }}" method="post" id="profile-form" class="form form-horizontal" enctype="multipart/form-data" onsubmit="return validate(this);">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12 {{ $errors->has('my_profile_pic') ? ' has-error' : '' }}">
                    {{--<H4> PERSONAL DETAILS</H4>--}}

                    {{--<div class="fileinput fileinput-new" data-provides="fileinput" data-toggle="tooltip" title="upload jpg or png 600x600" >--}}
                        {{--<div class="fileinput-new thumbnail roundimage" >--}}
                            {{--<img src="{{ isset($invitation->profile->passport_image) ? asset($invitation->profile->passport_image) : asset('assets/img/image-600.png') }}">--}}
                        {{--</div>--}}
                        {{--<div class="fileinput-preview fileinput-exists thumbnail roundimage"  ></div>--}}
                        {{--<div>--}}
                            {{--<span class="btn btn-default btn-file">--}}
                                {{--<span class="fileinput-new">Select Identification Photo </span><span class="fileinput-exists">Change</span>--}}
                                {{--,custom[validateMIME[image/jpeg|image/png]]--}}
                                {{--<input type="file" accept="image/*" name="my_profile_pic" id="my_profile_pic" class="validate[ optional,checkFileType[jpg|jpeg|JPG|png|PNG] ]"></span>--}}
                            {{--<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>--}}
                            {{--<div class="upload-error"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<br>--}}
                    <!-- position: relative;
          background: {{ $invitation->delegateType->color_hexcode }};
                    width: 80px;
                    height: 80px;
                    clear: both;
                    /* position: absolute; */
                    vertical-align: middle;
                    text-align: center;
                    display: inline-block;
                    vertical-align: middle;
                    line-height: normal; -->

                    <h4 style="text-transform:uppercase;"> {{__('trans.Attendance Category')}} </h4>
                     <div style="position: relative;    display: block;    /* margin: 2em 0; */
                     background-color: transparent;    color: #ddd;    text-align: left;" id="color_code">
                     <span class="badge-circle " style="

                             ">
                          <img src="{{ url($invitation->delegateType->image_path) }}" width="80">
                     </span>
                     </div>
                     <br>
                     <!-- Group Name: -->
                       <h4 class="text-uppercase side-heading"> {{ (ucfirst($invitation->delegateType->name))}}</h4>
                       @if($invitation->delegation->type == 2)
                       @else<h4 class="text-uppercase side-heading" >{{__('trans.Team Leader')}}: {{ucfirst($invitation->delegation->contact_name)}}</h4>@endif
                     <div>

                     </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">

                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label> {{__('trans.Title')}} </label>
                        <select class="form-control" name="title">
                          <option value=""> {{__('trans.Select Title')}} </option>
                          @foreach($titles as $title)
                           <option value="{{$title}}" @if(isset($invitation->profile->title) && $invitation->profile->title == $title) selected @endif>{{$title}}</option>
                          @endforeach


                        </select>
                        <!-- <input type="text" placeholder="Title" name="title" class="form-control tag-this"
                               value="{{ ($invitation->profile->title? $invitation->profile->title : Request::old('title')) }}"> -->
                        @if ($errors->has('title'))
                            <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('fname') ? ' has-error' : '' }}">
                        <label>{{__('trans.First Name')}} </label>
                        <input type="text" placeholder="{{__('trans.First Name')}}" name="fname" class="form-control "
                               value="{{ ($invitation->profile->fname ? $invitation->profile->fname : Request::old('fname')) }}">
                        @if ($errors->has('fname'))
                            <span class="help-block">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('surname') ? ' has-error' : '' }}">
                        <label>{{__('trans.Surname')}} </label>
                        <input type="text" placeholder="{{__('trans.Surname')}}" name="surname" class="form-control "
                               value="{{ ($invitation->profile->surname? $invitation->profile->surname :  Request::old('surname')) }}">
                        @if ($errors->has('surname'))
                            <span class="help-block">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>



                    <div class="form-group {{ $errors->has('organisation') ? ' has-error' : '' }}">
                        <label> {{__('trans.Organisation')}} </label>
                        <input type="text" placeholder="{{__('trans.Organisation')}}" name="organisation" class="form-control"
                               value="{{ ($invitation->profile->organisation?$invitation->profile->organisation :Request::old('organisation')) }}">
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                        <strong>{{ $errors->first('organisation') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}">
                        <label> {{__('trans.Position')}} </label>
                        <input type="text" placeholder="{{__('trans.Position')}}" name="position" class="form-control"
                               value="{{ ($invitation->profile->position? $invitation->profile->position:Request::old('position')) }}">
                        @if ($errors->has('position'))
                            <span class="help-block">
                        <strong>{{ $errors->first('position') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('residential') ? ' has-error' : '' }}">
                        <label> {{__('trans.Residential Address')}} </label>
                        <input type="text" placeholder="{{__('trans.Residential Address')}}" name="residential" class="form-control"
                               value="{{ ($invitation->profile->residential? $invitation->profile->residential:Request::old('residential')) }}">
                        @if ($errors->has('residential'))
                            <span class="help-block">
                        <strong>{{ $errors->first('residential') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('nationality') ? ' has-error' : '' }}">
                        <label> {{__('trans.Nationality')}} </label>
                        {{--<input type="text" placeholder="{{__('trans.Nationality')}}"  name="nationality" class="form-control "--}}
                        {{--value="{{ ($invitation->profile->nationality ? $invitation->profile->nationality:  Request::old('nationality')) }}">--}}
                        <?php
                        $t = ( isset($invitation->profile->nationality) ? $invitation->profile->nationality : $invitation->delegation->countryname->details->demonym )
                        ?>
                        {{--{{ dd($t)}}--}}
                        <select class="form-control" name="nationality" id="country">
                            <option></option>
                            @foreach($countries as $lst)
                                @if ($lst->details)
                                    <option value="{{$lst->details->demonym}}" {{ ($t == $lst->details->demonym ? 'selected' :'') }} > {{$lst->details->demonym}} </option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('nationality'))
                            <span class="help-block">
                        <strong>{{ $errors->first('nationality') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('national_id_type') ? ' has-error' : '' }}">
                        <label> {{__('trans.Identification Type')}} </label>
                        <select class="form-control" placeholder="Identification type" name="national_id_type">
                            <option value="" > {{__('trans.Select Identification Type')}} </option>
                            @if ($invitation->profile->national_id_type)
                                <option value="Passport" @if ($invitation->profile->national_id_type =='Passport') selected @endif >{{__('trans.Passport')}} </option>
                                <option value="National ID" @if ($invitation->profile->national_id_type =='National ID') selected @endif > {{__('trans.National ID')}} </option>
                            @elseif( Request::has('national_id_type'))
                                <option value="Passport" @if (Request::old('national_id_type') =='Passport') selected @endif > {{__('trans.Passport')}} </option>
                                <option value="National ID" @if (Request::old('national_id_type') =='National ID') selected @endif > {{__('trans.National ID')}} </option>
                            @else
                                <option value="Passport"> {{__('trans.Passport')}} </option>
                                <option value="National ID"> {{__('trans.National ID')}} </option>
                            @endif
                        </select>
                        @if ($errors->has('national_id_type'))
                            <span class="help-block">
                        <strong>{{ $errors->first('national_id_type') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('national_id') ? ' has-error' : '' }}">
                        <label> {{__('trans.Identification Number')}} </label>
                        <input type="text" placeholder="{{__('trans.Passport or National ID')}}" name="national_id"
                               class="form-control " value="{{ ($invitation->profile->national_id ? $invitation->profile->national_id : Request::old('national_id')) }}">
                        @if ($errors->has('national_id'))
                            <span class="help-block">
                        <strong>{{ $errors->first('national_id') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('national_id_image') ? ' has-error' : '' }}">
                        <label> {{__('trans.Upload passport details (Please note the details will be used for your accreditation)')}}  </label>
                        {{--<input type="file" placeholder="{{__('trans.upload picture passport or national id')}}" name="national_id_image" class="form-control " >--}}
                        <div class="fileinput fileinput-new" data-provides="fileinput" data-toggle="tooltip" title="{{__('trans.upload the following file types jpg, jpeg, JPG,JPEG, png, PNG, pdf')}}" >
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="@if($invitation->profile->national_id_image) {{ asset($invitation->profile->national_id_image)}} @else '' @endif" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new" > {{$bookingForm->upload_section}} </span>
                                    <span class="fileinput-exists"> Change </span>
                                    {{--validate[ optional,checkFileType[jpg|jpeg|JPG|png|PNG|pdf|doc|docx] ]accept=".doc,.docx,.pdf,image/*"--}}
                                    <input type="file" class="validate[ optional,checkFileType[jpg|jpeg|JPG|png|PNG|JPEG|pdf]]" name="national_id_image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{__('trans.Remove')}} </a>
                            </div>
                            <div class="upload-error"></div>
                        </div>
                        @if ($errors->has('national_id_image'))
                            <span class="help-block">
                        <strong>{{ $errors->first('national_id_image') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('date_issue') ? ' has-error' : '' }}">
                        <label> {{__('trans.Date of issue')}} </label>
                        <input type="text" placeholder="{{__('trans.Date of issue')}}" name="date_issue" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="@if (isset($invitation->profile->date_issue)) {{ date('d-m-Y',strtotime($invitation->profile->date_issue))}} @endif">
                        @if ($errors->has('date_issue'))
                            <span class="help-block">
                        <strong>{{ $errors->first('date_issue') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="form-group {{ $errors->has('date_expire') ? ' has-error' : '' }}">
                        <label> {{__('trans.Date of expiration')}} </label>
                        <input type="text" placeholder="{{__('trans.Date of expiration')}}" name="date_expire" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="@if (isset($invitation->profile->date_expire)) {{ date('d-m-Y',strtotime($invitation->profile->date_expire))}} @endif">
                        @if ($errors->has('date_expire'))
                            <span class="help-block">
                        <strong>{{ $errors->first('date_expire') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label> {{__('trans.Email')}} </label>
                        <input type="email" placeholder="{{__('trans.Email')}}" name="email" readonly=""
                               class="form-control validate[custom[email]]" value="{{ ($invitation->profile->email ? $invitation->profile->email : Request::old('email')) }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('cell_phone') ? ' has-error' : '' }}">

                        <div class="row">
                            <div class="col-md-3"  style="padding-right: 1px;"  >
                                <label> {{__('trans.Country Code')}} </label>
                                <!-- <select name="cell_phone_code" id="cell" class="form-control" >
                                <option>{{ ($invitation->profile->cell_phone_code ? $invitation->profile->cell_phone_code : '+'.str_replace('"]','', str_replace('["','',$invitation->delegation->countryname->details->callingcodes)))  }}</option>
                                </select> -->


                                <select name="cell_phone_code" class="form-control selectpicker">
                                @foreach($codes as $list)

                                @php $country = DB::table("countries_advanced")->where('id','=',$list)->first(); @endphp
                                @php  $c = '+'.str_replace('"]','', str_replace('["','',$country->callingcodes));@endphp
                                <option data-content="<img src='{{$country->flag}}' width='30px'> {{$country->name}}" value="{{$c}}"   @if(isset($invitation->profile->cell_phone_code) && $invitation->profile->cell_phone_code == $c) selected @endif>{{$country->name}}</option>
                                  @endforeach
                                </select>
                                <!-- <input type="tel" placeholder="Cell phone country code" name="cell_phone_code" class="form-control "
                                       value="{{ ($invitation->profile->cell_phone_code ? $invitation->profile->cell_phone_code : '+'.str_replace('"]','', str_replace('["','',$invitation->delegation->countryname->details->callingcodes)))  }}"> -->
                            </div>
                            <div class="col-md-9"  >
                                <label> {{__('trans.Cell Phone')}} </label>
                                <input type="tel" placeholder="{{__('trans.Cell phone (Please leave out the country code)')}}" name="cell_phone" class="form-control "
                                       value="{{ ($invitation->profile->cell_phone ? $invitation->profile->cell_phone : Request::old('cell_phone') ) }}">
                            </div>
                        </div>
                        @if ($errors->has('cell_phone'))
                            <span class="help-block">
                        <strong>{{ $errors->first('cell_phone') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('office_phone') ? ' has-error' : '' }}">

                        <div class="row">
                            <div class="col-md-3"  style="padding-right: 1px;" >
                                <label> {{__('trans.Country Code')}}  </label>

                                <select name="office_phone_code" class="form-control selectpicker">
                                @foreach($codes as $list)
                                @php $country = DB::table("countries_advanced")->where('id','=',$list)->first(); @endphp
                                @php  $c = '+'.str_replace('"]','', str_replace('["','',$country->callingcodes));@endphp
                                <option data-content="<img src='{{$country->flag}}' width='30px'> {{$country->name}}" value="{{$c}}"   @if(isset($invitation->profile->office_phone_code) && $invitation->profile->office_phone_code == $c) selected @endif>{{$country->name}}</option>
                                  @endforeach
                                </select>
                                <!-- <input type="tel" placeholder="Office phone country ode" name="office_phone_code" class="form-control "
                                       value="{{ ($invitation->profile->office_phone_code ? $invitation->profile->office_phone_code : '+'.str_replace('"]','', str_replace('["','',$invitation->delegation->countryname->details->callingcodes)))  }}"> -->
                            </div>
                            <div class="col-md-9" >
                                <label> {{__('trans.Office Phone')}} </label>
                                <input type="tel" placeholder="{{__('trans.Office phone (Please leave out the country code)')}} " name="office_phone" class="form-control "
                                       value="{{ ($invitation->profile->cell_phone? $invitation->profile->cell_phone : Request::old('office_phone') ) }}">
                            </div>
                        </div>
                        @if ($errors->has('office_phone'))
                            <span class="help-block">
                        <strong>{{ $errors->first('office_phone') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="col-md-3 col-sm-12"> </div>

            </div>
           @if($bookingForm->health_insurance == 1)
            <br><br>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12" align="center">
                    <h4 class="text-uppercase side-heading">{{__('trans.Health Insurance')}} </h4>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <!-- <label >{{__('trans.Health Insurance Service Provider')}}</label> -->
                    <div class="form-group {{ $errors->has('insurance_provider') ? ' has-error' : '' }}">
                        <label>{{__('trans.Insurance Service Provider')}} </label>
                        <input type="text" placeholder="{{__('trans.Insurance Provider')}}" name="insurance_provider" class="form-control "
                               value="{{ ($invitation->profile->insurance_provider? $invitation->profile->insurance_provider :  Request::old('insurance_provider')) }}">
                        @if ($errors->has('insurance_provider'))
                            <span class="help-block">
                        <strong>{{ $errors->first('insurance_provider') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>
                    <div class="form-group {{ $errors->has('membership_number') ? ' has-error' : '' }}">
                        <label>{{__('trans.Membership Number')}} </label>
                        <input type="text" placeholder="{{__('trans.Membership Number')}}" name="membership_number" class="form-control "
                               value="{{ ($invitation->profile->membership_number? $invitation->profile->membership_number :  Request::old('membership_number')) }}">
                        @if ($errors->has('membership_number'))
                            <span class="help-block">
                        <strong>{{ $errors->first('membership_number') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>
                    <div class="form-group {{ $errors->has('membership_email') ? ' has-error' : '' }}">
                        <label>{{__('trans.Email')}}</label>
                        <input type="text" placeholder="{{__('trans.Email')}}" name="membership_email" class="form-control "
                               value="{{ ($invitation->profile->membership_email? $invitation->profile->membership_email :  Request::old('membership_email')) }}">
                        @if ($errors->has('membership_email'))
                            <span class="help-block">
                        <strong>{{ $errors->first('membership_email') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>
                    <div class="form-group {{ $errors->has('membership_telephone') ? ' has-error' : '' }}">

                        <div class="col-md-3"  style="padding-right: 1px; padding-left: 0px;">
                          <label>{{__('trans.Country Code')}} </label><br>
                          @foreach($codes as $list)
                          @php $country = DB::table("countries_advanced")->where('id','=',$list)->first(); @endphp
                          @php  $c = '+'.str_replace('"]','', str_replace('["','',$country->callingcodes));@endphp

                              @endforeach
                        <select name="membership_phone_code" class="form-control selectpicker">
                        @foreach($codes as $list)
                        @php $country = DB::table("countries_advanced")->where('id','=',$list)->first(); @endphp
                        @php  $c = '+'.str_replace('"]','', str_replace('["','',$country->callingcodes));@endphp
                        <option data-content="<img src='{{$country->flag}}' width='30px'> {{$country->name}}" value="{{$c}}"   @if(isset($invitation->profile->membership_phone_code) && $invitation->profile->membership_phone_code == $c) selected @endif>{{$country->name}}</option>
                          @endforeach
                        </select>
                      </div>
                        <div class="col-md-9" style="padding-left: 1px; padding-right:0px">
                        <label>{{__('trans.Telephone')}} </label>
                        <input type="text" placeholder="{{__('trans.Telephone (Please leave out the country code)')}} " name="membership_telephone" class="form-control "
                               value="{{ ($invitation->profile->membership_telephone? $invitation->profile->membership_telephone :  Request::old('membership_telephone')) }}">
                        </div>
                        @if ($errors->has('membership_telephone'))
                            <span class="help-block">
                        <strong>{{ $errors->first('membership_telephone') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>
                    <div class="form-group {{ $errors->has('medical_conditions') ? ' has-error' : '' }}">
                        <label>{{__('trans.Medical Conditions')}} </label>
                          <textarea placeholder="{{__('trans.Medical Conditions')}}" name="medical_conditions" class="form-control " >{{ ($invitation->profile->medical_conditions? $invitation->profile->medical_conditions :  Request::old('medical_conditions')) }}</textarea>
                        @if ($errors->has('medical_conditions'))
                            <span class="help-block">
                        <strong>{{ $errors->first('medical_conditions') }}</strong>
                    </span>
                        @endif
                        <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                    </div>

                      <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                  </div>
                <div class="col-md-3 col-sm-12 col-xs-12"> </div>
            </div>
             @else
             @endif

           @if($bookingForm->emergency_contacts == 1)
           <br><br>
           <div class="row">
               <div class="col-md-3 col-sm-12 col-xs-12" align="center">
                   <h4 class="text-uppercase side-heading"> {{__('trans.EMERGENCY CONTACTS')}}</h4>
               </div>
               <div class="col-md-6 col-sm-12 col-xs-12">
                 <label>{{__('trans.Emergency Contact')}}</label>
                 <br>
                 <small style="color: #84754e ;">
                  {{__('trans.Note: Person listed must not be someone also participating in this event')}}.
                 </small>
                 <div class="form-group {{ $errors->has('a_contact_name') ? ' has-error' : '' }}">
                     <label>{{__('trans.Name')}} </label>
                     <input type="text" placeholder="{{__('trans.Name')}} " name="a_contact_name" class="form-control "
                            value="{{ ($invitation->profile->a_contact_name? $invitation->profile->a_contact_name :  Request::old('a_contact_name')) }}">
                     @if ($errors->has('a_contact_name'))
                         <span class="help-block">
                     <strong>{{ $errors->first('a_contact_name') }}</strong>
                 </span>
                     @endif
                     <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                 </div>
                 <div class="form-group {{ $errors->has('a_contact_email') ? ' has-error' : '' }}">
                     <label>{{__('trans.Email')}}</label>
                     <input type="text" placeholder="{{__('trans.Email')}}" name="a_contact_email" class="form-control "
                            value="{{ ($invitation->profile->a_contact_email? $invitation->profile->a_contact_email :  Request::old('a_contact_email')) }}">
                     @if ($errors->has('a_contact_email'))
                         <span class="help-block">
                     <strong>{{ $errors->first('a_contact_email') }}</strong>
                 </span>
                     @endif
                     <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                 </div>
                 <div class="form-group {{ $errors->has('a_contact_phone') ? ' has-error' : '' }}">

                     <div class="col-md-3" style="padding-right: 1px; padding-left: 0px;">
                       <label>{{__('trans.Country Code')}} </label><br>

                     <select name="a_contact_phone_code" class="form-control selectpicker">
                     @foreach($codes as $list)
                     @php $country = DB::table("countries_advanced")->where('id','=',$list)->first(); @endphp
                     @php  $c = '+'.str_replace('"]','', str_replace('["','',$country->callingcodes));@endphp
                     <option data-content="<img src='{{$country->flag}}' width='30px'> {{$country->name}}" value="{{$c}}"   @if(isset($invitation->profile->a_contact_phone_code) && $invitation->profile->a_contact_phone_code == $c) selected @endif>{{$country->name}}</option>
                       @endforeach
                     </select>
                   </div>
                   <div class="col-md-9" style="padding-left: 1px; padding-right:0px">
                       <label>{{__('trans.Telephone')}} </label>
                     <input type="tel" placeholder="{{__('trans.Telephone (Please leave out the country code)')}}'" name="a_contact_phone" class="form-control"
                            value="{{ ($invitation->profile->a_contact_phone? $invitation->profile->a_contact_phone :  Request::old('a_contact_phone')) }}">
                   </div>
                     @if ($errors->has('a_contact_phone'))
                         <span class="help-block">
                     <strong>{{ $errors->first('a_contact_phone') }}</strong>
                 </span>
                     @endif
                     <!-- <small style="color: #84754e ;"> * Please note that the name and title you provide will be printed on your badge  </small> -->
                 </div>
               <div class="col-md-3 col-sm-12 col-xs-12"> </div>
           </div>
        </div>
        @else
        @endif
        @if($bookingForm->flights == 1)
           <br><br>
           <div class="row">
               <div class="col-md-3 col-sm-12" align="center">
                   <h4 class="text-uppercase side-heading">{{__('trans.Travel Details')}}</h4>
               </div>
               <div id="flight-info" class="col-md-6 col-sm-12 {{ $errors->has('flight-type') ? ' has-error' : '' }} ">

                   <ul class="nav nav-tabs" role="tablist" id="listnav">
                       <li role="presentation" class="
                               @if ($invitation->flight_type == 'Scheduled Flight') active @else @endif
                               @if ($invitation->flight_type != 'Scheduled Flight' && $invitation->flight_type != 'Charter Flight') active @else @endif
                               ">
                           <a href="#ScheduledFlight" aria-controls="ScheduledFlight" role="tab" data-toggle="tab">
                               <input id="flight1" name="flight-type" value="Scheduled Flight" type="radio"
                                      @if ($invitation->flight_type == 'Scheduled Flight') checked @else checked @endif />
                               &nbsp; {{__('trans.Scheduled Flight')}}
                           </a>
                       </li>
                       <li role="presentation" class="@if ($invitation->flight_type == 'Charter Flight') active @else  @endif">
                           <a href="#CharterFlight" aria-controls="CharterFlight" role="tab" data-toggle="tab">
                               <input id="flight2" name="flight-type" value="Charter Flight" type="radio"
                                      @if ($invitation->flight_type == 'Charter Flight') checked @else  @endif
                               />
                               &nbsp; {{__('trans.Charter Flight')}}
                           </a>
                       </li>
                   </ul>

                   <small style="color: #84754e ;">
                       {{__('trans.Kindly provide your flight details.Please provide details of your travel for this event. Please note that transfers (see below) can only be provided if travel details are complete.')}}
                   </small>

                   <div class="tab-content">
                       <div role="tabpanel" class="tab-pane
                       @if ($invitation->flight_type == 'Scheduled Flight') active @else @endif
                       @if ($invitation->flight_type != 'Scheduled Flight' && $invitation->flight_type != 'Charter Flight') active @else @endif
                               " id="ScheduledFlight">

                           {{--ScheduledFlight--}}

                           <table width="100%" class="table">
                               <tr>
                                   <td align=""> {{__('trans.Inbound - Ticket details to arrival point')}}</td>
                                   <td align=""> {{__('trans.Outbound - Ticket details from departure point')}}</td>
                               </tr>
                               <tr>
                                   <td align="center">
                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Number')}} e.g. UA5178" name="i_f_number" class="form-control search_number"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->flight_number}}@else{{Request::old('i_f_number',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Carrier')}}" name="i_f_carrier" class="form-control search_airline"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->flight_carrier}}@else{{Request::old('i_f_carrier',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Airport Name')}}" name="i_f_airport" class="search_airport form-control "
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->airport_name}}@else{{Request::old('i_f_airport',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Terminal')}}" name="i_f_terminal" class="form-control "
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->terminal}}@else{{Request::old('i_f_terminal',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Arrival Date')}}" name="i_f_date" class="form-control date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->arrival_date}}@else{{Request::old('i_f_date',null) }}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Arrival Time')}}" name="i_f_time" class="form-control timepicker"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight)){{$invitation->inflight->arrival_time}}@else{{Request::old('i_f_time',null) }}@endif">

                                       <label class="pull-left">{{__('trans.Transfer Required')}} </label>
                                       <input type="radio" name="i_f_transfer_req" value="Yes" style="padding: 6px;"
                                              @if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight) && $invitation->inflight->transfer_req == 'Yes' ) checked @else{{Request::old('i_f_transfer_req',null)}}@endif
                                       > {{__('trans.Yes')}}
                                       <input type="radio" name="i_f_transfer_req" value="No" style="padding: 6px;"
                                              @if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->inflight) && $invitation->inflight->transfer_req == 'No' ) checked @else{{Request::old('i_f_transfer_req',null)}}@endif
                                       > {{__('trans.No')}}

                                       <br><br>


                                   </td>
                                   <td align="center">
                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Number')}} e.g. UA5178" name="o_f_number" class="form-control search_number"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->inflight->flight_number}}@else{{Request::old('o_f_number',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Carrier')}}" name="o_f_carrier" class="form-control search_airline"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->outflight->flight_carrier}}@else{{Request::old('o_f_carrier',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Airport Name')}}" name="o_f_airport" class="form-control search_airport"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->outflight->airport_name}}@else{{Request::old('o_f_airport',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Terminal')}}" name="o_f_terminal" class="form-control "
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->outflight->terminal}}@else{{Request::old('o_f_terminal',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Departure Date')}}" name="o_f_date" class="form-control date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->outflight->departure_date}}@else{{Request::old('o_f_date',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Departure Time')}}" name="o_f_time" class="form-control timepicker"
                                              value="@if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight)){{$invitation->outflight->departure_time}}@else{{Request::old('o_f_time',null)}}@endif">

                                       <label class="pull-left"> {{__('trans.Transfer Required')}} </label>
                                       <input type="radio" name="o_f_transfer_req" value="Yes" style="padding: 6px;" @if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight) && $invitation->outflight->transfer_req == 'Yes' ) checked @else{{Request::old('o_f_transfer_req',null)}}@endif
                                       > {{__('trans.Yes')}}
                                       <input type="radio" name="o_f_transfer_req" value="No" style="padding: 6px;" @if(($invitation->flight_type == 'Scheduled Flight') && ($invitation->outflight) && $invitation->outflight->transfer_req == 'No' ) checked @else{{Request::old('o_f_transfer_req','No')}}@endif
                                       > {{__('trans.No')}}

                                       <br><br>

                                   </td>
                               </tr>
                           </table>

                       </div>
                       <div role="tabpanel" class="tab-pane
                           @if ($invitation->flight_type == 'Charter Flight') active @else  @endif
                           " id="CharterFlight">

                           {{--CharterFlight--}}

                           <table width="100%" class="table">
                               <tr>
                                 <td align=""> {{__('trans.Inbound - Ticket details to arrival point')}}</td>
                                 <td align=""> {{__('trans.Outbound - Ticket details from departure point')}}</td>
                               </tr>
                               <tr>
                                   <td align="center">
                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Number')}} e.g. UA5178" name="c_i_f_number" class="form-control search_number"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->flight_number}}@else{{Request::old('c_i_f_number',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Carrier')}}" name="c_i_f_carrier" class="form-control search_airline"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->flight_carrier}}@else{{Request::old('c_i_f_carrier',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Airport Name')}}" name="c_i_f_airport" class="search_airport form-control "
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->airport_name}}@else{{Request::old('c_i_f_airport',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Terminal')}}" name="c_i_f_terminal" class="form-control "
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->terminal}}@else{{Request::old('c_i_f_terminal',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Arrival Date')}}" name="c_i_f_date" class="form-control date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->arrival_date}}@else{{Request::old('c_i_f_date',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Arrival Time')}}" name="c_i_f_time" class="form-control timepicker"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight)){{$invitation->inflight->arrival_time}}@else{{Request::old('c_i_f_time',null)}}@endif">

                                       <label class="pull-left">{{__('trans.Transfer Required')}} </label>
                                       <input type="radio" name="c_i_f_transfer_req" value="Yes" style="padding: 6px;"
                                              @if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight) && $invitation->inflight->transfer_req == 'Yes' ) checked @else {{Request::old('c_i_f_transfer_req',null)}}@endif
                                       > {{__('trans.Yes')}}
                                       <input type="radio" name="c_i_f_transfer_req" value="No" style="padding: 6px;"
                                              @if(($invitation->flight_type == 'Charter Flight') && ($invitation->inflight) && $invitation->inflight->transfer_req == 'No' ) checked @else {{Request::old('c_i_f_transfer_req',null)}}@endif
                                       > {{__('trans.No')}}

                                       <br><br>


                                   </td>
                                   <td align="center">
                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Number')}} e.g. UA5178" name="c_o_f_number" class="form-control search_number"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->inflight->flight_number}}@else{{Request::old('c_o_f_number',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Flight Carrier')}}" name="c_o_f_carrier" class="form-control search_airline"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->outflight->flight_carrier}}@else{{Request::old('c_o_f_carrier',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Airport Name')}}" name="c_o_f_airport" class="form-control search_airport"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->outflight->airport_name}}@else{{Request::old('c_o_f_airport',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Terminal')}}" name="c_o_f_terminal" class="form-control "
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->outflight->terminal}}@else{{Request::old('c_o_f_terminal',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Departure Date')}}" name="c_o_f_date" class="form-control date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->outflight->departure_date}}@else{{Request::old('c_o_f_date',null)}}@endif">

                                       <input type="text" style="margin-bottom: 5px;" placeholder="{{__('trans.Departure Time')}}" name="c_o_f_time" class="form-control timepicker"
                                              value="@if(($invitation->flight_type == 'Charter Flight') && ($invitation->outflight)){{$invitation->outflight->departure_time}}@else{{Request::old('c_o_f_time')}}@endif">

                                       <label class="pull-left"> {{__('trans.Transfer Required')}} </label>
                                       <input type="radio" name="c_o_f_transfer_req" value="Yes" style="padding: 6px;" @if (($invitation->flight_type == 'Charter Flight') && ($invitation->outflight) && $invitation->outflight->transfer_req == 'Yes' ) checked @else{{Request::old('c_o_f_transfer_req',null)}}@endif
                                       > {{__('trans.Yes')}}
                                       <input type="radio" name="c_o_f_transfer_req" value="No" style="padding: 6px;" @if (($invitation->flight_type == 'Charter Flight') && ($invitation->outflight) && $invitation->outflight->transfer_req == 'No' ) checked @else{{Request::old('c_o_f_transfer_req','No')}}@endif
                                       > {{__('trans.No')}}
                                       <br><br>

                                   </td>
                               </tr>
                           </table>

                       </div>
                   </div>

               </div>
               <div class="col-md-3 col-sm-12">
                 <?php
                  $t = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Travel')->first();
                 ?>
                 @if(!empty($t))
                 <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$t->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on travel details')}}</a>
                @else
                @endif
               </div>

           </div>
           @else
           @endif
           @if($bookingForm->transfers == 1)
           <br>
           <br>
           <div class="row">
               <div class="col-md-3 col-sm-12" align="center">
                   <h4 class="text-uppercase side-heading"> {{__('trans.Transfer Packages')}}</h4>

               </div>
               <div class="col-md-6 col-sm-12">

                 <label> {{__('trans.Arrival Transfer')}} </label><br>
                 <small style="color: #84754e ;">{{__('trans.Please select Arrival transfer package required (see Essential Information for details)')}}':</small>
                   <div class="form-group {{ $errors->has('transfer_packages') ? ' has-error' : '' }}">
                       <select name="transfer_packages" placeholder="{{__('trans.Select Transfer Package')}} "  class="select23 form-control" id="transfer_to">
                           <option value=""> {{__('trans.Select Transfer Package')}} </option>
                           @foreach( $tpackages as $ls)
                            @if($ls->t_mode == 1)
                               <option  value="{{ $ls->id }}" @if (isset($invitation->transfer) && $invitation->transfer->tpackage_id == $ls->id) selected @endif >
                                   {{ $ls->name }} - {{ $ls->t_type }} - {{ "$ ".$ls->cost }}</option>
                            @endif
                           @endforeach
                       </select>
                       @if ($errors->has('transfer_packages'))
                           <span class="help-block">
                       <strong>{{ $errors->first('transfer_packages') }}</strong>
                   </span>
                       @endif
                   </div>

                   {{--<div class="form-group">--}}
                   {{--<label> {{__('trans.Departure Transfer')}} </label>--}}
                   {{--<input type="text" placeholder="Pickup Location" name="t_pickup_loc" class="form-control " value="">--}}
                   {{--</div>--}}

               </div>

               <div class="col-md-3 col-sm-12">
                 <?php
                  $t = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Travel')->first();
                 ?>
                 @if(!empty($t))
                 <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$t->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on transfer packages')}}</a>
                 @else
                 @endif
               </div>

           </div>
           <div class="row">
           <div class="col-md-3 col-sm-12" align="center">
               <h4 class="text-uppercase side-heading"> </h4>
           </div>
           <div class="col-md-6 col-sm-12">
             <label> {{__('trans.Departure Transfer')}} </label><br>
             <small style="color: #84754e ;">{{__('trans.Please select Departure transfer package required (see Essential Information for details)')}}':</small>
               <div class="form-group {{ $errors->has('transfer_packages') ? ' has-error' : '' }}">
                   <select name="transfer_packages_return" placeholder="{{__('trans.Select Transfer Package')}} "  class="select23 form-control" id="transfer_from">
                       <option value=""> {{__('trans.Select Transfer Package')}} </option>
                       @foreach( $tpackages as $ls)
                        @if($ls->t_mode == 2)
                           <option  value="{{ $ls->id }}"  @if(isset($invitation->transfer_return) && $invitation->transfer_return->tpackage_id == $ls->id) selected @endif >
                               {{ $ls->name }} - {{ $ls->t_type }} - {{ "$ ".$ls->cost }}</option>
                      @endif
                       @endforeach
                   </select>
                   @if ($errors->has('transfer_packages'))
                       <span class="help-block">
                   <strong>{{ $errors->first('transfer_packages') }}</strong>
               </span>
                   @endif
               </div>
               {{--<div class="form-group">--}}
               {{--<label> Pickup Location </label>--}}
               {{--<input type="text" placeholder="Pickup Location" name="t_pickup_loc" class="form-control " value="">--}}
               {{--</div>--}}

           </div>

           <div class="col-md-3 col-sm-12"> </div>

       </div>
       @else
       @endif
        @if($bookingForm->hotels == 1)
            <br></br>
            <div class="row">
                <div class="col-md-3 col-sm-12" align="center">
                  @if($bookingForm->tents == 1)
                  <h4 class="text-uppercase side-heading">{{__('trans.Transit Accommodation')}}</h4>
                  @else
                   <h4 class="text-uppercase side-heading">{{__('trans.Accommodation Details')}}</h4>
                  @endif
                </div>
                <div class="col-md-6 col-sm-12">

                    <div class="form-group {{ $errors->has('hotel') ? ' has-error' : '' }}">
                        <label> {{__('trans.Hotels')}} </label>
                        <select name="hotel" placeholder="Select Hotel" id="hotel-selector" class="form-control validate[]">
                            <option value=""> {{__('trans.Select One')}} </option>

                            @foreach( $hotel->sortBy('hotel.order_custom') as $ls)
                                <option value="{{ $ls->hotel_id }}" @if (isset($invitation->hotel) && $invitation->hotel->hotel_id == $ls->hotel_id) selected @endif  > {{ $ls->hotel->name }} </option>
                            @endforeach
                              <option value="0" @if (isset($invitation->hotel) && $invitation->hotel->hotel_id == 0 ) selected @endif > Other </option>
                            {{--<option data-content="<span class='label label-success'>Relish</span>">Relish</option>--}}
                        </select>
                        @if ($errors->has('hotel'))
                            <span class="help-block">
                        <strong>{{ $errors->first('hotel') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('h_name') ? ' has-error' : '' }}" id="hotel-details" style="display: none;">
                        <label> {{__('trans.Hotel Name')}} </label>
                        <input type="text" placeholder="{{__('trans.Hotel Name')}}" name="h_name" id="" class="form-control"
                               value="@if (isset($invitation->hotel) && $invitation->hotel->otherhotel) {{ $invitation->hotel->otherhotel->name }} @else  {{ Request::old('h_name') }}@endif">
                        @if ($errors->has('h_name'))
                            <span class="help-block">
                        <strong>{{ $errors->first('h_name') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('h_arrival') ? ' has-error' : '' }}">
                        <label> {{__('trans.Arrival Date')}} </label>
                        <input type="text" placeholder="{{__('trans.Arrival Date')}}" name="h_arrival" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                               value="@if (isset($invitation->hotel)) {{ date('d-m-Y',strtotime($invitation->hotel->arrival_date))}} @endif">
                        @if ($errors->has('h_arrival'))
                            <span class="help-block">
                        <strong>{{ $errors->first('h_arrival') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('h_arrival') ? ' has-error' : '' }}">
                        <label> {{__('trans.Departure Date')}} </label>
                        <input type="text" placeholder="{{__('trans.Departure Date')}}" name="h_departure" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy" data-date-start-date="+0d"
                               value="@if (isset($invitation->hotel)) {{ date('d-m-Y',strtotime($invitation->hotel->departure_date)) }} @endif">
                        @if ($errors->has('h_departure'))
                            <span class="help-block">
                        <strong>{{ $errors->first('h_departure') }}</strong>
                    </span>
                        @endif
                    </div>

                    <br>
                    <small style="color: #84754e ;">
                        {{__('trans.Note: Delegates are required to book and pay directly with their selected hotel.Please check the Essential Information (Accommodation section) for details of recommended hotels')}}
                    </small>

                </div>
                <div class="col-md-3 col-sm-12">
                  <?php
                   $a = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Accommodation')->first();
                  ?>
                  @if(!empty($a))
                  @if($bookingForm->tents == 1)
                  <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$a->id}}" class ="btn blue-button" target="_blank">
                    {{__('trans.Click here for more information on transit accommodation details')}}
                  </a>
                  @else
                  <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$a->id}}" class ="btn blue-button" target="_blank">
                    {{__('trans.Click here for more information on accommodation details')}}

                  </a>
                  @endif
                  @else
                  @endif

                 </div>
            </div>
            @else
            @endif
            @if($bookingForm->tents == 1)
            <br><br>
             <div class="row">
                 <div class="col-md-3 col-sm-12" align="center">
                     <h4 class="text-uppercase side-heading">{{__('trans.Accommodation Details')}}</h4>
                 </div>

                 <div class="col-md-6 col-sm-12">
                   <div class="form-group {{ $errors->has('bed_type') ? ' has-error' : '' }}">
                       <label>{{__('trans.Bed Type')}} </label><br>
                       <select name="bed_type" placeholder="{{__('trans.Select Bed Type')}}"  class="select25 form-control">
                           <option value="">Select</option>
                           <option value="Single">Single</option>
                           <option value="Twin">Twin</option>
                           <option value="Double">Double</option>
                       </select>
                       @if ($errors->has('bed_type'))
                           <span class="help-block">
                       <strong>{{ $errors->first('bed_type') }}</strong>
                   </span>
                       @endif
                       <!-- <small style="color: #84754e ;"> * {{__('trans.Please note that double beds are only available on nights of 7th & 10th December')}} </small> -->
                   </div>
                   @foreach($tents as $tent)
                   <div class="form-group {{ $errors->has('tent_accomodation') ? ' has-error' : '' }}">
                   <div class="radio">
                      <label><input type="radio" name="tent_accomodation" value="{{$tent->price}}" id="tent_accomodation"@if (($invitation->tent_accomodation) && $invitation->tent_accomodation->tent_id == $tent->id ) checked @else{!!Request::old('tent_accomodation',null)!!}@endif>{!!$tent->description!!} </label>

                    </div>
                    @if ($errors->has('tent_accomodation'))
                        <span class="help-block">
                    <strong>{{ $errors->first('tent_accomodation') }}</strong>
                </span>
                    @endif
                </div>
                   @endforeach
                   <div class="form-group {{ $errors->has('tent_sharing_name') ? ' has-error' : '' }}" id="tent_sharing" style="display: block;">
                       <label> {{__('trans.Tent Sharing Name')}} </label>
                       <input type="hidden" name="tent_upgrade_price" id="" class="form-control"
                              value="{{ ($invitation->profile->tent_upgrade_price? $invitation->profile->tent_upgrade_price :  Request::old('tent_upgrade_price')) }}">
                       <input type="text" placeholder="{{__('trans.Name of Person sharing tent with')}}" name="tent_sharing_name" id="tent_share" class="form-control"
                              value=" {{ Request::old('tent_sharing_name') }}">
                       @if ($errors->has('tent_sharing_name'))
                           <span class="help-block">
                       <strong>{{ $errors->first('tent_sharing_name') }}</strong>
                   </span>
                       @endif
                   </div>
                 </div>
                 <div class="col-md-3 col-sm-12">
                   <?php
                    $a = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Accommodation')->first();
                   ?>
                   @if(!empty($a))
                   <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$a->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on accommodation details')}}</a>
                    @endif
                  </div>
             </div>
             @else
             @endif
             @if($bookingForm->activities == 1)
             <br>
                       <br>
                       <div class="row">

                           <div class="col-md-3 col-sm-12" align="center">
                               <h4  class="text-uppercase side-heading"> {{__('trans.Activities')}} </h4>
                           </div>
                           <div class="col-md-6 col-sm-12">
                               <div class="form-group {{ $errors->has('activities') ? ' has-error' : '' }}">
                                   <label> {{__('trans.Activities')}} </label>
                                   <select name="activities[]" id="form-activities" placeholder="Select Activities" multiple class="form-activities select2 form-control">
                                       <option value=""> {{__('trans.Select Activity')}} </option>
                                       @foreach( $activities as $ls)
                                           <option value="{{ $ls->id }}" @if (($invitation->activity->count() > 0) && $invitation->activity->contains('activity_id',$ls->id))  @endif
                                           > {{ $ls->name }} - {{ "$ ".$ls->cost }}</option>
                                       @endforeach
                                   </select>
                                   @if ($errors->has('activities'))
                                       <span class="help-block">
                                   <strong>{{ $errors->first('activities') }}</strong>
                               </span>
                                   @endif
                               </div>
                           </div>
                           <div class="col-md-3 col-sm-12"> </div>
                           <?php
                            $act = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Activities')->first();
                           ?>
                           @if(!empty($act))
                           <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$act->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on activity details')}}</a>
                            @endif
                       </div>
             @else
             @endif

             @if($bookingForm->custom_fields == 1)
             <br>
                       <br>
                       <div class="row">

                           <div class="col-md-3 col-sm-12" align="center">
                               <h4  class="text-uppercase side-heading"> {{__('trans.Attendance category additional information')}} </h4>
                           </div>
                           <div class="col-md-6 col-sm-12">
                             @if ( ($event->fields->where('delegate_type_id',$invitation->delegate_id)) && (($event->fields->where('delegate_type_id',$invitation->delegate_id))->count() > 0 ) )
                                 <small style="color: #84754e ;"> {{__('trans.Basing on the description, please fill in data as required')}}. </small>
                                 <br><br>

                                 @foreach($event->fields->where('delegate_type_id',$invitation->delegate_id) as $list )
                                     @if ( $list->delegate_type_id == $invitation->delegate_id)
                                         @if($list->field_type == 1)
                                          <label> {!! $list->field_title !!} </label> <small> {!! $list->field_description !!} </small>
                                            <input type="text" name="{{'custom-'.$list->id}}" class="form-control" value="@if ($invitation->customdata)@if ($invitation->customdata->where('field_id',$list->id)->first())@php $data = $invitation->customdata->where('field_id',$list->id)->where('created_by','=', Auth::user()->id)->first();@endphp{!! $data->value!!}@endif
                                             @else
                                            {{Request::old('custom-'.$list->id) }}
                                            @endif">
                                         @elseif($list->field_type == 2)
                                          <br>
                                          <label class="termsconditions"><input type="checkbox" id="custom_check" name="custom_b" value=""
                                             @if($invitation->customdata)
                                             @if($invitation->customdata->where('field_id',$list->id)->first())
                                              @php $data = $invitation->customdata->where('field_id',$list->id)->where('created_by','=', Auth::user()->id)->first();@endphp
                                              @if($data->value == 1)
                                              checked
                                              @endif
                                             @endif
                                             @else
                                             {{Request::old('custom-'.$list->id) }}
                                             @endif>
                                              <p>{!! $list->field_title !!} <small> {!! $list->field_description !!} </small> </p></label>
                                             <input type="hidden" id="custom_box" name="{{'custom-'.$list->id}}" value="
                                             @if ($invitation->customdata)
                                           @if ($invitation->customdata->where('field_id',$list->id)->first())
                                           @php $data = $invitation->customdata->where('field_id',$list->id)->where('created_by','=', Auth::user()->id)->first();@endphp
                                           {{ $data->value}}
                                           @endif
                                           @else
                                           {{Request::old('custom-'.$list->id) }}
                                           @endif
                                                   ">
                                         @endif
                                     @else
                                     @endif
                                 @endforeach

                             @else

                                 <hr>
                                 <p> {{__('trans.Not Applicable')}} </p>
                                 <hr>

                             @endif
                           </div>
                           <div class="col-md-3 col-sm-12"> </div>
                       </div>
             @else
             @endif


 @if($bookingForm->media == 1)
            <br>
            <br>
            <div class="row">
                <div class="col-md-3 col-sm-12" align="center">
                    <h4 class="text-uppercase side-heading"> {{__('trans.Media')}}</h4>
                </div>
                <div class="col-md-6 col-sm-12">
                  <label class="termsconditions"><input type="checkbox" id="media_check" name="media" value="" @if (($invitation->profile->media) && $invitation->profile->media == 1 ) checked @else{!!Request::old('media',null)!!}@endif>
                      <p>{{__('trans.Photographs and video footage will be taken during this event. If you do not wish to have images of yourself utilised for publicity purposes, please tick here')}}.
                      </p></label>
                      <input type="hidden" name="media" value="@if($invitation->profile->media){{$invitation->profile->media}}@endif" id="media">
                       <br><br>
                      <label class="termsconditions"><input type="checkbox" id="interview_check" name="interview" value="" @if (($invitation->profile->interview) && $invitation->profile->interview == 1 ) checked @else{!!Request::old('interview',null)!!}@endif>
                          <p>{{__('trans.You may be requested to provide an interview to the media during or regarding this event. If you do not wish to do so, please tick here')}}.
                          </p></label>
                          <input type="hidden" name="interview" value="@if($invitation->profile->interview){{$invitation->profile->interview}}@endif" id="interview">
                    </div>

                <div class="col-md-3 col-sm-12"> </div>
                <?php
                 $m = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Media')->first();
                ?>
                @if(!empty($m))
                <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$m->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on media details')}}</a>
                 @endif
            </div>

            @else
            @endif



            <br>
            <br>

            <div class="row">
                <div class="col-md-3 col-sm-12" align="center">
                    <h4 class="text-uppercase side-heading"> {{__('trans.Other Details')}}</h4>
                </div>
                <div class="col-md-6 col-sm-12 " id="">

                    <div role="tabpanel" id="otherdetails"  style="padding: 10px;">
                        <ul class="nav nav-tabs" style="border-bottom: 1px solid #ddd;">
                            <li class="active"><a href="#tab-1" data-toggle="tab"> {{__('trans.Food Requirements')}} </a></li>
                            <li><a href="#tab-2" data-toggle="tab"> {{__('trans.Technical Requirements')}}</a></li>
                            <li><a href="#tab-3" data-toggle="tab" style="display:none"> {{__('trans.Other Contacts')}}</a></li>
                            <li><a href="#tab-4" data-toggle="tab" style="display:none"> {{__('trans.Other Information')}} </a></li>
                        </ul>
                        <div class="tab-content" style=" padding: 6px ;border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;">
                            <div class="tab-pane fade in active" id="tab-1">
                                <div class="{{ $errors->has('food_requirements') ? ' has-error' : '' }}">
                                    <small style="color: #84754e ;"> {{__('trans.Please let us know of any special dietary requests')}}. </small> <hr>
                                    <textarea placeholder="Food Requirements" name="food_requirements" class="form-control " >{{ ($invitation->food_requirements ? $invitation->food_requirements: Request::old('food_requirements')) }} </textarea>
                                    @if ($errors->has('food_requirements'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('food_requirements') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-2">
                                <div class=" {{ $errors->has('tech_requirements') ? ' has-error' : '' }}">
                                    <small style="color: #84754e ;"> {{__('trans.Please enter details of technical items that you may require')}}. </small> <hr>
                                    <textarea placeholder="Technical Requirements" name="tech_requirements" class="form-control ">{{ ($invitation->tech_requirements ? $invitation->tech_requirements : Request::old('tech_requirements')) }} </textarea>
                                    @if ($errors->has('tech_requirements'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('tech_requirements') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-12"> </div>
            </div>

 @if($bookingForm->terms_conditions == 1)
            <br>
            <br>

            <div class="row">
                <div class="col-md-3 col-sm-12" align="center">
                    <h4 class="text-uppercase side-heading">{{__('trans.Terms and Conditions')}}</h4>
                </div>
                <div class="col-md-6 col-sm-12">
                    <!-- <a href="{{ route('del.view.event.info',[$event->slug,$code]) }}" target="_blank" class="btn blue-button btn-small"> Click here to read Terms and Conditions</a>-->
                     <!-- <a href="{{route('get.terms')}}" class="btn  blue-button btn-small"  > Download Terms and Conditions</a><br> -->
                  @foreach($tconditions as $tcondition)
                    <div class="form-group {{ $errors->has('terms_conditions') ? ' has-error' : '' }}">
                      <span class="terms">*</span> <label class="termsconditions"><input type="checkbox" id="terms_conditions" name="terms_conditions[]" value="">{!!$tcondition->description!!}</label>
                        @if ($errors->has('terms_conditions'))
                            <span class="help-block">
                        <strong>{{ $errors->first('terms_conditions') }}</strong>
                    </span>
                        @endif
                    </div>
                    @endforeach

                   <span id="terms" class="terms"> </span>
                </div>
                <div class="col-md-3 col-sm-12">
                  <?php
                   $ter = DB::table('event_news')->where('event_id','=',$event->id)->where('category','=','Booking & Cancellation Terms & Conditions')->first();
                  ?>
                  @if(!empty($ter))
                  <a href ="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_{{$ter->id}}" class ="btn blue-button" target="_blank">{{__('trans.Click here for more information on terms & conditions')}}</a>
                   @endif

                </div>
            </div>
            @else
            @endif
             <br><br>
            <div class="row">
                <div class="col-md-2 col-sm-12">

                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="form-group" style="font-weight:bold;">

                        @foreach($edisclaimer as $disclaimer)
                        {!!$disclaimer->description!!}

                        @endforeach

                        <br>
                        <input class="btn blue-button" type="submit" name="Save " value="{{__('trans.Save')}}">
                        {{--<input class="btn btn-succes" type="submit" name="Save " value="Save">--}}
                        <a href="{{route('del.view.invitation',[$code,$event->slug])}}" class="btn yellow-button"> {{__('trans.Cancel and view profile')}} </a>
                        <a href="{{route('del.view.invitation',[$code,$event->slug])}}#tab-5"  class="btn yellow-button"> {{__('trans.View Payments Required')}}  </a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12"></div>
            </div>

        </form>

        <br>
        <br>

    </div>
  </div>
    <div class="container-fluid">
       <div class="row service-box margin-bottom-10" style="background-color:#ffd52d;">

         <div class=" col-xs-12 col-sm-12 col-md-12" >
           @if($event->slug === 'africa-s-wildlife-economy-summit')
           <h4 style="color:#045061; text-align:center; padding:16px; font-family:'Lato-Bold';font-size:24px; text-transform: uppercase">{{__('trans.A new initiative to forge a new deal for tourism, rural communities and wildlife by 2030')}} </h4>
           @else
           <h4 style="color:#045061; text-align:center; padding:16px; font-family:'Lato-Bold';font-size:24px;text-transform: uppercase">{{__("trans.We show the value of conserving Africa's great wild landscapes because when you value something, you fight to protect it")}}</h4>
                 <!-- <p> Thank you for your interest in the {{$event->name}}. To register, please Click on the confirmation link that was sent to your email </p> -->
           @endif

         </div>

       </div>
       </div>

    {{--//hotel modals--}}

    @foreach( $activities as $lst)
        <div class="modal fade" id="act-{{$lst->id}}" data-target="#act-{{$lst->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header orange">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <hr>
                        <div class="row">
                            {{--<div class="col-md-1"></div>--}}
                            <div class="col-md-5">
                                <div class="" style="width: auto; height:300px;margin-top:20px;">
                                    <img src="{{ ($lst->images->first()) ? url($lst->images->first()->image_path) : asset('uploads/default-poster.jpg') }}" class="img-responsive" style="width:100%;">
                                </div>
                            </div>
                            <div class="col-md-7" align="left">

                                <h3 style="color:#84754e;font-weight: bolder;font-family: 'Libre Baskerville', serif !important;">
                                    {{__('trans.Activity')}} :    {{ $lst->name}}
                                </h3>

                                <br>
                                <div align="left">
                                    {!! $lst->description !!}
                                </div>

                                <div align="left" style="color:#84754e;font-weight: bolder;">
                                    <p> {{__('trans.Price')}} : $ {{ $lst->cost }} </p>
                                    <p> {{__('trans.Date')}}: {{ $lst->valid_from }} </p>
                                    <p> {{__('trans.Location')}} : {{ $lst->location }} </p>
                                </div>
                            </div>
                            {{--<div class="col-md-1"></div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


    @foreach( $hotel as $ls)

        <div class="modal fade" id="hotel-{{$ls->hotel_id}}" data-target="#hotel-{{$ls->hotel_id}}">
            <div class="modal-dialog" style="width:1200px;">
                <div class="modal-content">
                    <div class="modal-header orange">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>

                        <hr>

                        <div class="row">
                            <div class="col-md-1"> </div>
                            <div class="col-md-4">
                                <div id="myCarousel-21-b-{{$ls->id}}" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                    @if($ls->hotel)
                                        <!-- Wrapper for slides -->
                                            <?php $ii = 0; ?>
                                            @foreach(  $ls->hotel->images as $lsii)
                                                <div class="item @if ($ii == 0) active @endif">
                                                    <img src="{{ url($lsii->image_path) }}" alt="Los Angeles" class="img-responsive" style="width:100%;">
                                                </div>
                                                <?php $ii++; ?>
                                            @endforeach
                                        @else
                                            <div class="item active">
                                                <img src="{{ asset('uploads/default-poster.jpg') }}" alt="Los Angeles" class="img-responsive" style="width:100%;" height="300px">
                                            </div>
                                        @endif
                                        <a class="left carousel-control" href="#myCarousel-21-b-{{$ls->id}}" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel-21-b-{{$ls->id}}" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" align="left">

                                <h3 class="headings"> {{ $ls->hotel->name }} </h3>
                                {!! $ls->hotel->description !!}
                                <br>
                                <h5 class="headings-small"> {{__('trans.Primary Information')}}  </h5>
                                <strong>{{__('trans.Name')}}:</strong>  {{ $ls->hotel->contact_person }} <br>
                                <strong>{{__('trans.Phone')}}:</strong> {{ $ls->hotel->contact_phone }} <br>
                                <strong>{{__('trans.Email')}}:</strong> {{ $ls->hotel->contact_email }} <br>
                                <strong>{{__('trans.Reference Code')}}:</strong> {!! $ls->hotel->coupon !!} <br>
                                <br>
                                <a href="http://{!!$ls->hotel->website!!}"  target="_blank" class="btn blue-button "> {{__('trans.Visit Website')}}</a>

                            </div>
                            <div class="col-md-1"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

    @foreach( $tpackages as $ls)

        <div class="modal fade" id="return-{{$ls->id}}" data-target="#return-{{$ls->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header orange">
                      <h3 class="headings">{{ $ls->name }}</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>

                        <hr>

                        <div class="row">
                            <div class="col-md-1"> </div>

                            <div class="col-md-12" align="left">
                              <h5 class="headings-small"> {{__('trans.Primary Information')}}  </h5>
                              <strong>{{__('trans.Pickup Point')}}:</strong>  {{ $ls->location }} <br>
                              <strong>{{__('trans.Transport Mode')}}:</strong>  {{ $ls->t_type }} <br>
                              <strong>{{__('trans.Price')}}:</strong>@if($ls->cost > 0)  ${{ $ls->cost }} per Person @else ${{ $ls->cost }} @endif<br>
                              <br>
                              <br>
                              <p style="color: #045061;"> * {{__('trans.For more information, please visit the Essential Information page')}}. <a href="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_19" target="_blank">Click Here</a>   </p>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

    @foreach( $tpackages as $ls)

        <div class="modal fade" id="transfer-{{$ls->id}}" data-target="#transfer-{{$ls->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header orange">
                        <h3 class="headings">{{ $ls->name }}</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>

                        <hr>

                        <div class="row">
                            <div class="col-md-1"> </div>

                            <div class="col-md-12" align="left">
                              <h5 class="headings-small"> {{__('trans.Primary Information')}}  </h5>
                              <strong>{{__('trans.Pickup Point')}}:</strong>  {{ $ls->location }} <br>
                              <strong>{{__('trans.Transport Mode')}}:</strong>  {{ $ls->t_type }} <br>
                              <strong>{{__('trans.Price')}}:</strong>@if($ls->cost > 0)  ${{ $ls->cost }} per Person @else ${{ $ls->cost }} @endif<br>
                              <br>
                              <br>
                              <p style="color: #045061;"> * {{__('trans.For more information, please visit the Essential Information page')}}. <a href="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}#news_19" target="_blank">Click Here</a>   </p>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection

@section('footer')



    @javascript('availableTags',  $titles )
    @foreach( $tpackages as $ls)
        @if($ls->t_type == 'Air' && $ls->cost > 0)
        <script>
                document.getElementById('flight_upgrade_check').addEventListener('change', function () {
                     var checked =document.getElementById("flight_upgrade_check").checked;
                     if(checked == true){
                       $('#flight_upgrade').val('250');
                      if(this.value == "{{$ls->id}}"){
                        $("#transfer-{{$ls->id}}").modal('show');
                      }
                     }else{
                       $('#flight_upgrade').val('');
                     }
                    // // var style =  this.value == 1700 ? 'none' : 'block';
                    // $('#flight_upgrade').val( this.value);;
                });
        </script>
      @endif
    @endforeach
    <script>

        //     var availableTags = [
        //         "Mr.","Ms.","Mrs.","Dr.","Prof.","His Excellency","Her Excellency","Hon.","His Excellency Hon.","Her Excellency Hon."
        //     ];

        // $('.select2-multiple-2').selectpicker({
        //     iconBase: 'fa',
        //     tickIcon: 'fa-check'
        // });
        $('.select2-multiple-2').selectpicker('render');

            $('input[name="flight-type"]').click(function () {
                $(this).closest('tabpanel').tab('show');
            });

        // $('.select-hotels').selectpicker({
        //     iconBase: 'fa',
        //     tickIcon: 'fa-check'
        // });



        // $('#profile-form').validate({
        //     errorElement : "span",
        //     errorPlacement: function(error, element) {
        //         error.appendTo(".upload-error");
        //     },
        //     rules: {
        //         my_profile_pic: {
        //             accept: "image/*"
        //         },
        //         national_id_image: {
        //             accept: "image/*|pdf|doc|docx",
        //             extension: "image/*|pdf|doc|docx"
        //         }
        //     }
        // });

        $(function () {
            $('#listnav input:radio').click(function (e) {
                e.stopPropagation();
                $('#flight-info li').removeClass('active')
                $(this).parent().parent().addClass('active');
                var tabpane = $(this).parent().attr('aria-controls');
                $('#flight-info .tab-content').children().removeClass('active');
                $('#flight-info #' + tabpane).addClass('active');
            });
            $('flight-info a').click(function (e) {
                $(this).find("#listnav input[type=radio]").trigger('click');
            });
        });

        $("#my_file").change(function(){
            readURL(this);
            //this.css('width:150px;');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.demo').each(function() {
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                defaultValue: $(this).attr('data-defaultValue') || '',
                inline: $(this).attr('data-inline') === 'true',
                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                opacity: $(this).attr('data-opacity'),
                position: $(this).attr('data-position') || 'bottom left',
                change: function(hex, opacity) {
                    if (!hex) return;
                    if (opacity) hex += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(hex);
                    }
                },
                theme: 'bootstrap'
            });
        });

        $('.clockface_1').clockface();

        $('.date-picker').datepicker({
            //rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });

        $(function () {
            $('.this-travel').click(function (e) {
                e.stopPropagation();
                $('li').removeClass('active')
                $(this).parent().parent().addClass('active');
            });
            $('a').click(function (e) {
                $(this).children().trigger('click');
            });
        });

        $("#upload_icon").click(function() {
            $("input[id='my_file']").click();
        });



        $(".tag-this").tagit({
            availableTags : availableTags,singleField: true, allowSpaces: true,
            showAutocompleteOnFocus : true,placeholderText: 'Official Title',tagLimit:1,
            autocomplete: {delay: 0, minLength: 0, autoFocus: true},
        });

        $(".tag-this-open").tagit({
            singleField: true,
            allowSpaces: true,
            showAutocompleteOnFocus : true,
            placeholderText: 'Vehicle Registration Number',
            tagLimit:5,
            //autocomplete: {delay: 0, minLength: 0, autoFocus: true},
        });

        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "{{__('trans.Select one or more activities')}}",
            width: null
        });
        $(".select23, .select2-multiple3").select2({
            placeholder: "{{__('trans.Select one Transfer package (Optional)')}}",
            width: null
        });

        $(".select24, .select2-multiple4").select2({
            placeholder: "{{__('trans.Select one Hotel package (Optional)')}}'",
            width: null
        });
        $(".select25, .select2-multiple5").select2({
            placeholder: "{{__('trans.Select Bed Type')}}",
            width: null
        });

        $(function () {
            // $('#start,#end').datetimepicker({sideBySide: true});
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });

        $(function () {
            $(".search_airport").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ route('search.airport') }}",
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
            });
        });

        $(function () {
            $(".search_airline").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ route('search.airline') }}",
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
            });
        });

        $(function () {
            $(".search_aircraft").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ route('search.aircraft') }}",
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
            });
        });

        $(function () {
            $(".search_number").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ route('search.flight.number') }}",
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
            });
        });

        $('.mt-repeater').each(function(){
            $(this).repeater({
                show: function () {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        //rtl: App.isRTL(),
                        orientation: "left",
                        autoclose: true
                    });
                },
                hide: function (deleteElement) {
                    if(confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                ready: function (setIndexes) {
                }
            });
        });



        document.getElementById('media_check').addEventListener('change', function () {

             var checked =document.getElementById("media_check").checked;
             if(checked == true){
               $('#media').val('1');
             }else{
               $('#media').val('');
             }
            // // var style =  this.value == 1700 ? 'none' : 'block';
            // $('#flight_upgrade').val( this.value);;
        });

        document.getElementById('custom_check').addEventListener('change', function () {

             var checked =document.getElementById("custom_check").checked;
             if(checked == true){
               $('#custom_box').val('1');
             }else{
               $('#custom_box').val('');
             }
            // // var style =  this.value == 1700 ? 'none' : 'block';
            // $('#flight_upgrade').val( this.value);;
        });

        document.getElementById('interview_check').addEventListener('change', function () {

             var checked =document.getElementById("interview_check").checked;
             if(checked == true){
               $('#interview').val('1');
             }else{
               $('#interview').val('');
             }
            // // var style =  this.value == 1700 ? 'none' : 'block';
            // $('#flight_upgrade').val( this.value);;
        });


        document.getElementById('hotel-selector').addEventListener('change', function () {
            console.log(this.value);
            var style =  this.value == 0 ? 'block' : 'none';
            document.getElementById('hotel-details').style.display = style;
        });

          $('input[name=tent_accomodation]').click(function() {
             var tent = $('input[name=tent_accomodation]:checked').val();
             console.log(tent);
              if(tent == 0){
                 var style =  this.value == 0 ? 'block' : 'none';
                 document.getElementById('tent_sharing').style.display = style;
              }else if (tent == 800){
                var style =  this.value == 800 ? 'block' : 'none';
                document.getElementById('tent_sharing').style.display = style;
              }
              else if (tent == 700){
                var style =  this.value == 700 ? 'none' : 'block';
                document.getElementById('tent_sharing').style.display = style;
                $('#tent_share').val('');
              }else{
                var style =  this.value == 1700 ? 'none' : 'block';
                document.getElementById('tent_sharing').style.display = style;
                $('#tent_share').val('');
              }

          });


    function validate(form) {
      var terms = document.getElementById('terms');
      if(form.terms_conditions[0].checked == false || form.terms_conditions[1].checked == false || form.terms_conditions[2].checked == false){
        terms.innerHTML ='Please read and agree to the Terms and Conditions before submitting the form. ';
        return false;
      }
      return true;

  	}


    </script>

    @foreach( $hotel as $ls)
        <script>
            $("#hotel-selector").change(function () {
                if ($(this).val() == "{{$ls->hotel_id}}") {
                    $("#hotel-{{$ls->hotel_id}}").modal('show');
                }
            });
        </script>
    @endforeach


    @foreach( $tpackages as $ls)
    @if($ls->t_type == 'Air' && $ls->name != 'Upgrade flight')
        <script>
            $("#transfer_from").change(function () {
                if ($(this).val() == "{{$ls->id}}") {
                    $("#return-{{$ls->id}}").modal('show');
                }
            });
        </script>
      @endif
    @endforeach

    @foreach( $tpackages as $ls)
        <script>
            $("#transfer_to").change(function () {
                if ($(this).val() == "{{$ls->id}}") {
                    $("#transfer-{{$ls->id}}").modal('show');
                }
            });
        </script>
    @endforeach


    @foreach( $activities as $ls)
        <script>
            $(".form-activities").change(function () {
                var selected = []; //array to store value
                $(this).find("option:selected").each(function(key,value){
                  var Value = $(".form-activities option:selected").val();
                  // console.log(Value);
                  if(Value == 2){
                    $('.bootstrap-select').removeClass('open');
                    $('.form-activities').selectpicker('refresh');
                    $("#act-2").modal('show');
                  }else{
                    $("#act-2").modal('hide');
                  }
                });
          });
            //$('.form-activities').on('changed.bs.select', function (e,clickedIndex, newValue, oldValue) {
                // do something...
                //var Value = $(".form-activities option:selected").val()
                //console.log(e)
                //console.log(Value)
                 // console.log(clickedIndex)
                // console.log(newValue)
                // console.log(oldValue)
                // console.log({{$ls->id}})
                //if (clickedIndex == "{{$ls->id}}" && newValue == true) {
                  //  $('.form-activities').selectpicker('toggle');
                    //$('.form-activities').selectpicker('refresh');
                    //$("#act-{{$ls->id}}").modal('show');
              //  }
            //});
        </script>
    @endforeach

    <script>
        function wasDeselected (sel, val) {
            if (!val) {
                return true;
            }
            return sel && sel.some(function(d) { return val.indexOf(d) == -1; })
        }
    </script>
    <!-- <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script> -->
@endsection
