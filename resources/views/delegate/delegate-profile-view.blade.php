<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/07/2017
 * Time: 23:39
 */
?>

@extends('layouts.front-v-iii')

@section('content')

    <style>
        h4 { color: #9F906E !important; }

        body {
            color: black;
        }

        .badge-circle {
            border-radius: 50%!important;
            display: inline-block;
            /*margin-right: 20px;*/
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            background: transparent;
            color:#045061;
            font-family:'Bison-Bold';
            font-size: 25px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            background: transparent;
            color: #045061;

            /*padding: 5px 15px 4px;*/
            /*padding:80px 30px 5px 0px !important;*/
            padding: 0px;
            align-content: center;
            justify-content: center;
            text-align:center;
            font-family:'Bison-Bold';
            font-size: 25px;
        }

        .nav-tabs > li > a, .nav-tabs > li > a:hover, .nav-tabs > li > a:focus {
            background: transparent;
            color: dimgrey;
            border-radius: 0;
            /*padding: 5px 15px 4px;*/
            /*padding:80px 30px 5px 0px !important;*/
            padding: 0px;
            border: none !important;
            align-content: center;
            justify-content: center;
            text-align:center;
            font-family:'Bison-Bold';
            font-size: 25px;
        }

        .tab-content {
            background:transparent;
            padding: 0px;
            color: #84754e;
        }

        .nav-tabs {
            border-bottom:none;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
        {
            border-top: none;
        }


        .nav-tabs {
            padding-bottom: 1px;
            font-size: 13px;
            margin-bottom: 0;
        }

        .delegate-profile h1.title, .delegate-profile h2.title, .delegate-profile h3.title {
            font-family: 'Libre Baskerville', serif !important;
        }

        .nav-tabs>li>a {
            float:none;
            display:inline-block;
            align-content: center;
            justify-content: center;
            text-align: center;
        }

        .details-1  {
            background-image:url({{ asset('assets/img/hotel-2-512-2.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .details-2  {
            background-image:url({{ asset('assets/img/airplane-57-512-2.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .details-3  {
            background-image:url({{ asset('assets/img/car-3-512-2.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .details-4  {
            background-image:url({{ asset('assets/img/tear-of-calendar-512-2.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .details-5  {
            background-image:url({{ asset('assets/img/visa-512-2.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .nav-tabs > li {
            margin: 0px 0px 10px 10px;
            padding: 80px 10px 10px 10px;
        }

        .nav-tabs > li.active.details-1  {
            background-image:url({{ asset('assets/img/hotel-2-512.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .nav-tabs > li.active.details-2  {
            background-image:url({{ asset('assets/img/airplane-57-512.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .nav-tabs > li.active.details-3  {
            background-image:url({{ asset('assets/img/car-3-512.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .nav-tabs > li.active.details-4  {
            background-image:url({{ asset('assets/img/tear-of-calendar-512.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .nav-tabs > li.active.details-5  {
            background-image:url({{ asset('assets/img/visa-512.png') }});
            background-repeat:no-repeat;
            background-position:50% 50%;
        }

        .progress {
            overflow : visible;
        }
        /*,.progress::after*/
        .progress > .progress-bar::after  {
            content: "";
            width: 15px;
            height: 15px;
            line-height: 18px;
            border-radius: 50%;
            background:  #045061;
            display: block;
            position: absolute;
            top: 50%;
            left: calc(100% + 0px);
            bottom: 0;
            transform: translateY(-50%);
            z-index: 100;

        }
  @media (min-width: 768px){
.modal-dialog {
    width: 600px;
    margin: 30px auto;
    margin-top: 20%;
}
.nav-tabs > li {
    margin: 0px 40px 10px 10px;
    padding: 80px 10px 10px 10px;
}
}
    </style>
    <!-- position: relative;
 background: {{ $invitation->delegateType->color_hexcode }};
   width: 80px;
   height: 80px;
   clear: both;

   vertical-align: middle;
   text-align: center;
   display: inline-block;
   vertical-align: middle;
   line-height: normal; -->

    <div class="delagate-profile">
  <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="row" align="">
            <div class="col-md-1 col-sm-12" align="right" >
            </div>
            <div class="col-md-8 col-sm-12" align="center" style="    vertical-align: middle;   text-align: center;   position: relative;">
                <table align="center">
                    <tr>
                        <td width="15%" align="center" style="">
                            <span class="badge-circle " style="

                                    ">
                                 <img src="{{ url($invitation->delegateType->image_path) }}" width="80">
                            </span>
                        </td>
                          <!-- Group Name: -->
                        <td width="85%" align="left" style="padding-left: 24px;">
                            <span class="title-team">
                               {{ (ucfirst($invitation->delegateType->name))}}
                            </span>
                            <br>
                            <!-- {!!ucwords($invitation->profile->position)!!}@if ($invitation->profile->position && $invitation->profile->organisation), @endif  {{ ucwords($invitation->profile->organisation) }} -->
                             @if($invitation->delegation->type == 2)
                             @else
                            <span class="title-leader">Group Leader: {{ucfirst($invitation->delegation->contact_name)}} </span>
                            @endif
                        </td>
                    </tr>
                </table>

            </div>
            <div class="col-md-3 col-sm-12" align="left">

                    <div style="border-left: 1px solid #045061; padding-left:10px; height:35px">
                        <!-- Edit icon by Icons8 -->
                        <!-- Edit icon by Icons8 -->
                        <!-- <img style="float:left; position: relative;" class="icon icons8-Edit" width="26" height="26" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACzUlEQVR4Xu2a0XWbMBSG73UGaEdINugI2aDuBLX8nPigDdIJcHCfjTtBnQniDeoR3AmaDBBuDyEcc2wEVwiBhOE1QuH79EtcZCFc+IUXzg+jgDEBF25gnAIXHoBxEfRyCqwfF1+RaAaA1wDwGYAOhLjBZPIk5PJFJ9VeCYjD4AvgWwiAtwrIA1Aihfy55UrwRkAGnzxnI15zEQkhV5u6ZunfvRCgBZ9TU/KNkwTnBTSCzyS8AE1u6tYEpwUYwGcKCH4IGT1UTQVnBRjDZ9QHEUQ33gloCf6dWwRR5SA7l4A24b0T0DY8Ef2dy1VaLCkvZxKggieiV0T8xHmnn7UheBQyCpwXoBx5IgFwtSd42+lKeBcHV9fOvwar4PNqLm2jLYFZDfY6BTjweXy1JDCin/fbmwAd+KIEwORP1Zwmgl9zGc24a0YvAprAHyXczwAxLgVkVH6n93UuIA7vbgEnv8++6phz1kRembROBcShYvR6gu/0c9hF+M4EuArfiQCX4a0LcB3eqgAf4K0J8AXeigCf4FsX4Bt8qwLM4c2KJG7tb6UU9hW+lQT4DG8swHd4IwFDgG8sYCjwjQQMCV5bwNDgtQQMEV5PwHLxr/k2Vj9FDqc4Ym2JxeHd9GMf79gnexvLXXh2AtbhYoMI33N67taz6bThjKBpG14CTuPPOH7iAzwrAafxT39zm8tV5UElX+BZAnTj7xM8S0CsEX/f4GsFcOP/cXJzCoDnv8kx3xami1nT+ysXwar4F6CnysOLjsPXJ+Cs+KFlBotq6OO7kn1as+notXGfMgGlxQ/jP2YnMyDgHlVldGm1iVLAafyrniKFBsAtQrLlHE+1SqTZuToBZbV/oXOfoYuOSgWo4j8U6FoBxfinZ+0QcAtZvHeaCXO+eWkC1uH9HgF3AJONkMu98xQGD8j6GDLo3/lbRwHOD5HlBxwTYFmw893/B6sog1+i1th7AAAAAElFTkSuQmCC"> -->

                         <p style="color:#045061;"><span><i class="fa fa-pencil fa-2x" aria-hidden="true" ></i></span>
                        {{ $invitation->calculateCompletion($event->id) }} % {{__('trans.of the information completed')}}  </p>
                        @if($invitation->calculateCompletion($event->id) == 100)
                         <a href="{{ route('del.manage.invitation',[$invitation->code,$event->slug]) }}" class="btn yellow-button">{{__('trans.Click here to edit your profile')}}</a>
                        @else
                        <a href="{{ route('del.manage.invitation',[$invitation->code,$event->slug]) }}" class="btn yellow-button">{{__('trans.Click here to complete your profile')}}</a>
                        @endif




                    </div>

            </div>

            <br>

            <div class="col-md-12" align="center" style="margin-top: 12px; color:#84754e ">
                {{__('trans.Please see below your attendance summary for')}} {{ ucwords(strtolower($event->name))  }}
                {{ $event->venue }}
            </div>

            <div class="col-md-12" align="center" style="margin-top: 20px;margin-bottoms: 20px; "> <a href="#content" style="color: white;">
                    <i class="fa fa-angle-double-down" style="font-size:48px;color:#9F906E"></i>
                {{--<i class="fa fa-angle-double-down"> </i>--}}
                <!-- Expand Arrow icon by Icons8 -->
                    {{--<img class="icon icons8-Expand-Arrow" width="42" height="42" src=" {{ asset('assets/img/icons8-Double Down_100.png') }}">--}}
                </a>
            </div>
        </div>

        <div class="row" align="center" style="margin-top: 20px; ">

            <div class="col-md-2 col-sm-12" align=""> </div>
            <div class="col-md-8 col-sm-12" align="center">
                <table class="" width="100%" align="center">
                    <tr>
                        <td width="20%" valign="top" align="right">
                          <!-- @if($invitation->profile->national_id_image != NULL)
                            <img src="{{ asset($invitation->profile->national_id_image)}}" style=" border-radius: 50% !important;" class="img-responsive" width="100">
                          @else
                             <img src="{{ asset('assets/frontend/corporate/img/user.jpg')}}" style=" border-radius: 50% !important;" class="img-responsive" width="100">
                          @endif -->
                        </td>
                        <td width="80%" valign="top" style=" font-size: 18px; color: #84754e;">
                          {{__('trans.Name')}}: {{ ucwords($invitation->profile->fname) .' '. ucwords($invitation->profile->surname) }} <br>
                            {{__('trans.Official Title')}}: {{ ucwords($invitation->profile->title) }}<br>
                            {{__('trans.Organisation')}}: {{ ucwords($invitation->profile->organisation) }}<br>
                            {{__('trans.Position')}}: {{ ucwords($invitation->profile->position) }}<br>
                            {{__('trans.Residential')}}: {{ucwords($invitation->profile->residential)}}<br>
                            {{__('trans.Country')}}: {{ ucwords($invitation->profile->country) }}<br>
                            {{__('trans.Nationality')}}: {{ ucwords($invitation->profile->nationality) }}<br>
                            @if($invitation->profile->national_id_type === "National ID")
                            {{__('trans.National ID')}}:  {{ $invitation->profile->national_id or null}}<br>
                            @elseif($invitation->profile->national_id_type === "Passport")
                            {{__('trans.Passport')}}:  {{ $invitation->profile->national_id or null}}<br>
                            @else
                            {{__('trans.Identification Number')}}: {{ $invitation->profile->national_id or null}}<br>
                            @endif
                            {{__('trans.Email')}}: {{ $invitation->profile->email or null }} <br>
                            {{__('trans.Telephone')}}: {{ ($invitation->profile->cell_phone_code ? $invitation->profile->cell_phone_code : '')  }} {{ $invitation->profile->cell_phone or null }} <br>
                            {{__('trans.Office Tel')}}: {{ ($invitation->profile->cell_phone_code ? $invitation->profile->cell_phone_code : ' ')  }} {{ $invitation->profile->cell_office or null }} <br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-2 col-sm-12" align=""> </div>

        </div>

        <div class="row">

            <br>
            <br>

            <div class="progress" style="height: 5px; border-radius: 50px !important;">
                <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="5"
                     style="width: 20%;
                     {{--background-image: url({{ url($invitation->delegateType->image_path) }});--}}
                             background-repeat: repeat ;
                             background-color: #045061 ;
                             position: relative;
                             display: block;">
                </div>
            </div>

            <ul class="nav nav-tabs">
              <div class="col-md-1"></div>
              <div class="col-md-8"></div>
              <div class="col-md-3">
              @if($invitation->calculateCompletion($event->id) == 100)
               <a href="{{ route('del.manage.invitation',[$invitation->code,$event->slug]) }}" class="btn yellow-button ">{{__('trans.Click here to edit your profile')}}</a>
              @else
              <a href="{{ route('del.manage.invitation',[$invitation->code,$event->slug]) }}" class="btn yellow-button ">{{__('trans.Click here to complete your profile')}}</a>
              @endif
            </div>
                <li class="active details-1"  >
                    <a href="#tab-1" data-toggle="tab" data-step="1">
                        {{__('trans.Your Accommodation Details')}}
                    </a>
                </li>
                <li class="details-2">
                    <a href="#tab-2" data-toggle="tab" data-step="2" >
                        {{__('trans.Your Travel Details')}}
                    </a>
                </li>
                <li class="details-3">
                    <a href="#tab-3" data-toggle="tab" data-step="3">
                        {{__('trans.Transfer Package')}}
                    </a>
                </li>
                <li class="details-4">
                    <a href="#tab-4" data-toggle="tab" data-step="4"  >
                        {{__('trans.Your Activities')}}
                    </a>
                </li>
                <li class="details-5">
                    <a href="#tab-5" data-toggle="tab" data-step="5"  >
                        {{__('trans.Registration Fees & Payment')}}
                    </a>
                </li>
            </ul>
            <div class="tab-content" style="min-height:450px;">
                <div class="tab-pane row fade in active" id="tab-1">
                    @if($hotel)
                        @if ($hotel->hotel_id > 0 )
                            <div class="row" style="margin-right: 0px;  margin-left: 0px; ">
                                <div class="col-md-5">
                                    <div class="" style="width: auto; height:300px !important;margin-top:20px;">
                                        {{--<h2></h2>--}}
                                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @if($hotel->hotel)
                                                    <?php $i = 0; ?>
                                                    @foreach(  $hotel->hotel->images as $ls)
                                                        <div class="item @if ($i == 0) active @endif">
                                                            <img src="{{ url($ls->image_path) }}" alt="Los Angeles" class="img-responsive" style="width:100%;">
                                                        </div>
                                                        <?php $i++; ?>
                                                    @endforeach
                                                @else
                                                    <div class="item active" >
                                                        <img src="{{ asset('uploads/default-poster.jpg') }}" alt="Los Angeles" class="img-responsive" style="width:100%;" height="300px">
                                                    </div>
                                                @endif
                                            </div>
                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-7" style="padding-top: 20px;">

                                    @if($hotel->hotel)
                                        <h3 class="headings"> {{ $hotel->hotel->name }} </h3>
                                        {!! $hotel->hotel->description !!}
                                        <br>
                                        <h5 class="headings-small">{{__('trans.Primary Information')}}  </h5>
                                        <strong>{{__('trans.Name')}}:</strong>  {{ $hotel->hotel->contact_person }} <br>
                                        <strong>{{__('trans.Phone')}}:</strong> {{ $hotel->hotel->contact_phone }} <br>
                                        <strong>{{__('trans.Email')}}:</strong> {{ $hotel->hotel->contact_email }} <br>
                                        <strong>{{__('trans.Reference Code')}}:</strong> {{ $hotel->hotel->coupon }} <br>
                                        <br>
                                        <a href="{!!$hotel->hotel->website!!} "  target="_blank" class="btn yellow-button ">{{__('trans.Visit Website')}} </a>

                                        <hr>

                                        <br>
                                        <h5 class="headings-small">{{__('trans.Your Booking')}}  </h5>
                                        <strong>{{__('trans.Arrival')}}:</strong>  {{ date('d M Y',strtotime($hotel->arrival_date)) }} <br>
                                        <strong>{{__('trans.Departure')}}:</strong> {{ date('d M Y',strtotime($hotel->departure_date)) }} <br>
                                    @else

                                    @endif
                                </div>
                            </div>

                        @else

                            <div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">

                                    @if ($hotel->name)

                                        <h3 class="headings"> {{ $hotel->name }} </h3>
                                        <br>
                                        <hr>
                                        <h5 class="headings-small">{{__('trans.Your Booking')}}  </h5>
                                        <strong>{{__('trans.Arrival')}}:</strong>  {{ date('d M Y',strtotime($hotel->arrival_date)) }} <br>
                                        <strong>{{__('trans.Departure')}}:</strong> {{ date('d M Y',strtotime($hotel->departure_date)) }} <br>

                                    @else
                                        {{__('trans.You have NOT set any ACCOMMODATION information')}}


                                    @endif


                                </div>
                                <div class="col-md-4"></div>

                            </div>

                        @endif

                    @else

                        {{--You have NOT set any ACCOMMODATION information.--}}

                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-7 form">
                                <form action="{{ route ('del.manage.hotel.invitation',[$invitation->code,$event->id]) }}" method="post" class="form-inline" enctype="multipart/form-data" >
                                    {{ csrf_field() }}

                                    <div class="form-group {{ $errors->has('hotel') ? ' has-error' : '' }}">
                                        <label class="sr-only" > {{__('trans.Hotels')}} </label>
                                        <select  name="hotel" placeholder="{{__('trans.Select Hotel')}}" id="hotel-selector" class="form-control validate[]">
                                            <option value="0"> {{__('trans.Select Hotel')}} </option>
                                            <option value="0" @if (isset($invitation->hotel) && $invitation->hotel->hotel_id == 0 ) selected @endif > Other </option>
                                            @foreach( $all_hotels->sortBy('hotel.order_custom') as $ls)
                                                <option value="{{ $ls->hotel_id }}" @if (isset($invitation->hotel) && $invitation->hotel->hotel_id == $ls->hotel_id) selected @endif  > {{ $ls->hotel->name }} </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('hotel'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('hotel') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('h_name') ? ' has-error' : '' }}" id="hotel-details" style="display: none;">
                                        <label class="sr-only" > {{__('trans.Hotel Name')}} </label>
                                        <input type="text" placeholder="Hotel name" name="h_name" id="" class="form-control"
                                               value="@if (isset($invitation->hotel) && $invitation->hotel->otherhotel) {{ $invitation->hotel->otherhotel->name }} @else  {{ Request::old('h_name') }}@endif">
                                        @if ($errors->has('h_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('h_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('h_arrival') ? ' has-error' : '' }}">
                                        <label class="sr-only"> {{__('trans.Arrival Date')}} Arrival Date </label>
                                        <input type="text" placeholder="{{__('trans.Arrival Date')}}" name="h_arrival" class="form-control date-picker"
                                               data-date-format="dd-mm-yyyy" data-date-start-date="{{ date('d-m-Y',strtotime(\Carbon\Carbon::parse($invitation->event->start)->subDays(10))) }}"
                                               value="@if (isset($invitation->hotel)) {{ date('d-m-Y',strtotime($invitation->hotel->arrival_date))}} @endif">
                                        @if ($errors->has('h_arrival'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('h_arrival') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('h_arrival') ? ' has-error' : '' }}">
                                        <label class="sr-only"> {{__('trans.Departure Date')}} </label>
                                        <input type="text" placeholder="{{__('trans.Departure Date')}}" name="h_departure" class="form-control date-picker"
                                               data-date-format="dd-mm-yyyy" data-date-start-date="{{ date('d-m-Y',strtotime(\Carbon\Carbon::parse($invitation->event->end))) }}"
                                               value="@if (isset($invitation->hotel)) {{ date('d-m-Y',strtotime($invitation->hotel->departure_date)) }} @endif">
                                        @if ($errors->has('h_departure'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('h_departure') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <button type="submit" class="btn yellow-button">{{__('trans.Submit')}}</button>

                                </form>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            @if ($all_hotels->count() > 0 )
                                <div class="activities" align="center">
                                    <?php $i = 0; ?>
                                    @foreach(  $all_hotels as $ls)
                                        <div class="row">
                                            <div class="col-md-1"> </div>
                                            <div class="col-md-4">
                                                <div id="myCarousel-21-b-{{ $i }}" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                    @if($ls->hotel)
                                                        <!-- Wrapper for slides -->
                                                            <?php $ii = 0; ?>
                                                            @foreach(  $ls->hotel->images as $lsii)
                                                                <div class="item @if ($ii == 0) active @endif">
                                                                    <img src="{{ url($lsii->image_path) }}" alt="Los Angeles" class="img-responsive" style="width:100%;">
                                                                </div>
                                                                <?php $ii++; ?>
                                                            @endforeach
                                                        @else
                                                            <div class="item active">
                                                                <img src="{{ asset('uploads/default-poster.jpg') }}" alt="Los Angeles" class="img-responsive" style="width:100%;" height="300px">
                                                            </div>
                                                        @endif
                                                        <a class="left carousel-control" href="#myCarousel-21-b-{{ $i }}" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">{{__('trans.Previous')}}</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel-21-b-{{ $i }}" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">{{__('trans.Next')}}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-1"></div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endif

                </div>
                <div class="tab-pane row fade in " id="tab-2">

                    @if ($inv_flight_in && $inv_flight_out)

                        <input checked type="radio"> {{ $inv_flight_in->flight_type }}
                        <hr>
                        <br>

                        <div class="row " style="margin-top: 20px; margin-right: 0px;  margin-left: 0px;">
                            <div class="col-md-3">
                                <h3 style="color:#045061;font-family: 'Bison-Bold' !important; font-size:35px"> {{__('trans.Your flight Itenerary')}} </h3>
                                <em> {{__('trans.Your Country')}} : {{ $invitation->delegation->countryname->name }}</em>
                            </div>
                            <div class="col-md-7">

                                <strong> {{__('trans.Inbound Flight')}}</strong><br>
                                {{__('trans.Arrival Date')}}: {{ $inv_flight_in->arrival_date }} <br>
                                {{__('trans.Arrival Time')}}: {{ $inv_flight_in->arrival_time }} <br>
                                {{--@if ( $inv_flight_in->flight->type == 'Scheduled Flight')--}}
                                {{--Depart {{ $inv_flight_in->airport_name }} 2115hrs – Arrive Johannesburg, (O.R. Tambo Int)0030hrs--}}
                                {{--@endif--}}
                                {{--Flight Number: {{ $inv_flight_in->flight_number }} <br>--}}
                                {{__('trans.Plane Registration')}}: {{ $inv_flight_in->plane_reg }} <br>
                                {{__('trans.Flight Number')}} : {{ $inv_flight_in->flight_number }} <br>
                                {{__('trans.Flight Carrier')}}: {{ $inv_flight_in->flight_carrier }} <br>
                                <br>
                                {{__('trans.Transfer Required')}} :- {{ $inv_flight_in->transfer_req }}

                                <br><br>

                                <strong>{{__('trans.Outbound Flight')}} </strong><br>
                                {{__('trans.Departure Date')}}: {{ $inv_flight_out->departure_date }} <br>
                                {{__('trans.Departure Time')}}: {{ $inv_flight_out->departure_time }} <br>
                                {{--@if ( $inv_flight_in->flight->type == 'Scheduled Flight')--}}
                                {{--Depart {{ $inv_flight_in->airport_name }} 2115hrs – Arrive Johannesburg, (O.R. Tambo Int)0030hrs--}}
                                {{--@endif--}}
                                {{--Flight Number: {{ $inv_flight_out->flight_number }} <br>--}}
                                {{__('trans.Plane Registration')}}: {{ $inv_flight_out->plane_reg }} <br>
                                {{__('trans.Flight Number')}}: {{ $inv_flight_out->flight_number }} <br>
                                {{__('trans.Flight Carrier')}}: {{ $inv_flight_out->flight_carrier }} <br>
                                <br>
                                {{__('trans.Transfer Required')}} :- {{ $inv_flight_out->transfer_req }}
                            </div>
                        </div>

                    @else

                      {{__('trans.You have NOT set any flight information')}}


                    @endif

                </div>

                <div class="tab-pane row fade in " id="tab-3">
                    <div class="row " style="margin-top: 20px; margin-right: 0px;  margin-left: 0px;">
                        <!-- <div class="col-md-5" align="center">
                            <i  style="color:#9F906E;font-size: 24px;" class="fa fa-car fa-6" aria-hidden="true"></i> <br> <br>
                            <h3 style="color:#9F906E;font-family: 'Libre Baskerville', serif !important;"> PRIVATE VEHICLE <br> SUMMIT PASS REQUIRED </h3>
                        </div> -->
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <h3 style="color:#045061;font-family: 'Bison-Bold' !important; font-size:35px"> {{__('trans.Arrival Transfer')}} </h3>
                            @if (isset($invitation->transfer))

                                <strong>{{__('trans.Name')}}:</strong> {{ $invitation->transfer->package->name}}<br>
                                <strong>{{__('trans.Pick Up Point')}}:</strong> {{ $invitation->transfer->package->location}}<br>
                                <strong>{{__('trans.Transfer Mode')}}:</strong> {{ $invitation->transfer->package->t_type}}<br>
                                <strong>{{__('trans.Description')}}:</strong> {!!$invitation->transfer->package->description!!}<br>

                            @else

                                {{__('trans.You have NOT requested for a transfer package')}}.

                            @endif
                          </div>

                          <div class="col-md-6">
                            <h3 style="color:#045061;font-family: 'Bison-Bold' !important; font-size:35px"> Depature Transfer</h3>
                            @if (isset($invitation->transfer_return))


                                <strong>{{__('trans.Name')}}:</strong> {{ $invitation->transfer_return->package_return->name}}<br>
                                <strong>{{__('trans.Pick Up Point')}}:</strong> {{ $invitation->transfer->package->location}}<br>
                                <strong>{{__('trans.Transfer Mode')}}:</strong> {{ $invitation->transfer_return->package_return->t_type}}<br>
                                <strong>{{__('trans.Description')}}: </strong>{!!$invitation->transfer_return->package_return->description!!}<br>

                            @else

                                {{__('trans.You have NOT requested for a transfer package')}}.

                            @endif
                          </div>
                          <!-- + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                          + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0 ) -->


                        </div>
                    </div>
                </div>

                <div class="tab-pane row fade in " id="tab-4">
                    <div class="row " style="margin-top:20px;">
                        {{--<div class="col-md-1" align="center">--}}
                        {{--</div>--}}
                        <div class="col-md-12">

                            <div class="activities" align="center" >
                                @foreach($inv_act as $lst)
                                    <div class="" >
                                        <div class="row" style="margin-right: 0px;  margin-left: 0px";>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <div class="" style="width: auto; height:300px;margin-top:20px;">
                                                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                      <!-- Wrapper for slides -->
                                                      <div class="carousel-inner">
                                                          @if($lst->activity)
                                                              <?php $i = 0; ?>
                                                              @foreach( $lst->activity->images as $ls)
                                                                  <div class="item @if ($i == 0) active @endif">
                                                                      <img src="{{ url($ls->image_path) }}" alt="Los Angeles" class="img-responsive" style="width:100%;">
                                                                  </div>
                                                                  <?php $i++; ?>
                                                              @endforeach
                                                          @else
                                                              <div class="item active" >
                                                                  <img src="{{ asset('uploads/default-poster.jpg') }}" alt="Los Angeles" class="img-responsive" style="width:100%;" height="300px">
                                                              </div>
                                                          @endif
                                                      </div>
                                                      <!-- Left and right controls -->
                                                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                          <span class="glyphicon glyphicon-chevron-left"></span>
                                                          <span class="sr-only">Previous</span>
                                                      </a>
                                                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                          <span class="glyphicon glyphicon-chevron-right"></span>
                                                          <span class="sr-only">Next</span>
                                                      </a>
                                                  </div>







                                                    <!-- <img src="{{ ($lst->activity->images->first()) ? url($lst->activity->images->first()->image_path) : asset('uploads/default-poster.jpg') }}" class="img-responsive" style="width:100%;"> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6" align="left">

                                                <h3 style="color:#9F906E;font-weight: bolder;font-family: 'Libre Baskerville', serif !important;">
                                                    {{__('trans.Activity')}} :    {{ $lst->activity->name}}
                                                </h3>

                                                <br>
                                                <div align="left">
                                                    {!! $lst->activity->description !!}
                                                </div>

                                                <div align="left" style="color:#9F906E;font-weight: bolder;">
                                                    <p> {{__('trans.Price')}} : $ {{ $lst->activity->cost }} </p>
                                                    <p> {{__('trans.Date')}}: {{ $lst->activity->valid_from }} </p>
                                                    <p> {{__('trans.Location')}} : {{ $lst->activity->location }} </p>
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                        {{--<div class="col-md-1" align="center">--}}
                        {{----}}
                        {{--</div>--}}
                    </div>

                </div>


                <div class="tab-pane row fade in " id="tab-5">
                    <div class="row " style="margin-top:20px; margin-right: 0px;  margin-left: 0px";>

                        <table align="center" class="my-invoice" width="80%" style="font-size:13px !important; font-family: 'Lato-Regular' !important;color:#84754e;">
                        <thead>
                        <tr>
                            <td colspan="4">
                                <!-- <h4 style="color:#9F906E;padding-left: 10px;font-weight: bolder;font-family: 'Lato-Regular' !important;">
                                    Invoice Total: $
                                    {{ number_format (($invitation->delegatePrice->price) + ($invitation->activity->sum('activity.cost'))
                                    + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                    + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0 )
                                    + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price :0 ) ,2)  }} </h4> -->

                                <a target="_blank" href="{{ route('del.events.inv.print.all',[$invitation->id,$invitation->event->id]) }}" class="pull-right btn yellow-button " style="margin:5px;"> {{__('trans.Print Statement')}} </a>
                                <a target="_blank" href="{{ route('del.sales.accounts.print.receipt',[$invitation->id,$invitation->event->id]) }}" class="pull-right btn yellow-button " style="margin:5px;"> {{__('trans.Print Receipts')}} </a>
                                {{--<a target="_blank" href="{{ route('del.sales.accounts.print.receipt',[$invitation->id,$invitation->event->id]) }}" class="pull-right btn btn-primary " style="background:#9F906E;margin:5px;"> Print Invoice </a>--}}
                            </td>
                        </tr>

                        <tr class="table-header">
                            {{--<th>Quantity</th>--}}
                            <th width="50%">{{__('trans.Description')}}</th>
                            <th width="25">{{__('trans.Price')}}</th>
                            <th width="25">{{__('trans.Amount')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        {{--<tr> <th colspan="5"> Attendance package </th></tr>{{ $invitation->delegateType->name }}--}}
                        <tr>
                            {{--<td> 1 </td>--}}
                            <td> {{__('trans.Attendance Category')}} ({{$invitation->delegateType->name}}) </td>
                            <td> $  {{ number_format ($invitation->delegatePrice->price,2) }} </td>
                            <td width="150"> $ {{ number_format ($invitation->delegatePrice->price,2) }}</td>
                        </tr>

                        {{--<tr> <th colspan="5"> Event Activities </th></tr>--}}
                        @foreach($invitation->activity  as $key => $list)
                            <tr>
                                {{--<td> {{ $key+1 }} </td>--}}
                                <td> {{__('trans.Activity')}} : {{ $list->activity->name }}</td>
                                <td> $  {{ number_format ($list->activity->cost,2) }} </td>
                                <td width="150"> $ {{ number_format ($list->activity->cost,2) }}</td>
                            </tr>
                        @endforeach

                        {{--<tr> <th colspan="5"> Event Transport </th></tr>--}}
                        {{--@foreach($invitation->transfer  as $key => $list)--}}
                        {{--<tr>--}}
                        {{--<td> {{ $key+1 }} </td>--}}
                        {{--<td> {{ $list->package->name }}</td>--}}
                        {{--<td> $  {{ $list->package->cost }} </td>--}}
                        {{--<td width="150"> $ {{ $list->package->cost }}</td>--}}
                        {{--</tr>--}}
                        {{--@endforeach--}}
                        @if ($invitation->transfer)
                            <tr>
                                {{--<td> 1 </td>--}}
                                <td> {{__('trans.Transfer Package')}}  ({{$invitation->transfer->package->name}}, {{$invitation->transfer->package->t_type}}) </td>
                                <td> $  {{number_format ($invitation->transfer->package->cost ,2) }} </td>
                                <td width="150"> $ {{number_format ($invitation->transfer->package->cost ,2) }}</td>
                            </tr>
                        @endif

                        @if ($invitation->transfer_return)
                            <tr>
                                {{--<td> 1 </td>--}}
                                <td> {{__('trans.Transfer Package Return')}} ({{$invitation->transfer_return->package_return->name}}, {{$invitation->transfer_return->package_return->t_type}}) </td>
                                <td> $  {{number_format ($invitation->transfer_return->package_return->cost ,2)}} </td>
                                <td width="150"> $ {{number_format ($invitation->transfer_return->package_return->cost ,2)}}</td>
                            </tr>
                        @endif

                        @if ($invitation->tent_accomodation)
                            <tr>
                                {{--<td> 1 </td>--}}
                                <td> {{__('trans.Tent Accomodation')}} ({{$invitation->tent_accomodation->tent->name}}) </td>
                                <td> $  {{number_format ($invitation->tent_accomodation->tent->price ,2)}} </td>
                                <td width="150"> $ {{number_format ($invitation->tent_accomodation->tent->price ,2)}}</td>
                            </tr>
                        @endif
                        @if ($invitation->complimentary == 1)
                            <tr>
                                {{--<td> 1 </td>--}}
                                <td> {{__('trans.Complimentary')}} </td>
                                <td> {{__('trans.Yes')}} </td>
                                <td width="150"> $ {{number_format (0 ,2)}}</td>
                            </tr>
                        @endif
                        </tbody>

                        <footer>

                            <tr class="totals-row">
                                <td colspan="" class="wide-cell"></td>
                                <td style="font-family:'Lato-Bold'">{{__('trans.Total')}}</td>
                                <td coslpan="">
                                  @if ($invitation->complimentary == 1)
                                      $ {{number_format (0 ,2)}}
                                    @else
                                  $ {{ number_format ( (($invitation->delegatePrice->price) + ($invitation->activity->sum('activity.cost'))
                                            + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                            + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0 )
                                            + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price :0 ) ) ,2 )  }} </td>
                                  @endif
                            </tr>
                            <tr class="totals-row">
                                <td colspan="" class="wide-cell"></td>
                                <td style="font-family:'Lato-Bold'">{{__('trans.Payments')}}</td>
                                <td coslpan="">$  {{ number_format ($invitation->payments->sum('amount'),2) }} </td>
                            </tr>
                            <tr class="totals-row">
                                <td colspan="" class="wide-cell"></td>
                                <td style="font-family:'Lato-Bold'">{{__('trans.Refunds')}}</td>
                                <td coslpan="">$  {{ number_format ($invitation->refunds->sum('amount'),2) }}</td>
                            </tr>
                            <tr class="totals-row">
                                <td colspan="" class="wide-cell"></td>
                                <td style="font-family:'Lato-Bold'">{{__('trans.Outstanding Balance')}}</td>
                                <td coslpan="">
                                  @if ($invitation->complimentary == 1)
                                      $ {{number_format (0 ,2)}}
                                    @else

                                  $ {{ number_format ( ( ($invitation->delegatePrice->price + $invitation->activity->sum('activity.cost') +
                                        ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                         + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0 )
                                        + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price :0 ) ) - $invitation->payments->sum('amount') ) , 2 ) }} </td>
                                @endif
                            </tr>
                            <tr class="totals-row">
                                <td colspan="" class="wide-cell"></td>
                                <td coslpan="" style="font-family:'Lato-Bold'"> {{__('trans.Overpayment')}} </td>
                                <td coslpan="">$ {{
                                        ($invitation->payments->sum('amount') - ($invitation->delegatePrice->price +
                                        $invitation->activity->sum('activity.cost') +
                                        ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )) ) > 0 ?

                                        number_format ( ($invitation->payments->sum('amount') - ($invitation->delegatePrice->price +
                                        $invitation->activity->sum('activity.cost') +
                                        ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )) ), 2)

                                        :

                                        0
                                        }} </td>
                            </tr>
                        </footer>

                        <tr>
                            <td colspan="4" align="right">
                                <div class="pull-right">
                                    <h4 style="color:#9F906E;padding-left: 10px;font-weight: bolder;font-family: 'Lato-Bold' !important;">
                                        {{__('trans.Invoice Total')}}:
                                        @if ($invitation->complimentary == 1)
                                            $ {{number_format (0 ,2)}}
                                          @else


                                        $ {{ number_format ( ($invitation->delegatePrice->price + $invitation->activity->sum('activity.cost') +
                                                ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                                + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0 )
                                                + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price :0 )),2) }} </h4>
                                         @endif
                                </div>
                            </td>
                        </tr>

                        </table>

                        <br>

                        <table align="center" class="my-invoice-2" width="80%" style="font-family: "Lato-Regular" !important;">

                        <tr>
                            <td>
                                <h3 style="font-family: 'Bison-Bold' !important; color:#045061;font-size:25px;padding-left: 10px;font-weight: bolder; border-bottom: 2px solid #045061;margin-bottom: 20px;">
                                    {{__('trans.Payment Instructions')}}
                                </h3>
                                <div class="" >
                                    @foreach($invitation->event->newsPayment as $list )
                                        {!! $list->text !!}
                                    @endforeach
                                </div>
                            </td>
                        </tr>

                        </table>

                    </div>

                </div>
            </div>


        </div>
        <div class="row"> </div>


        <br>
        <br>
</div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="exampleModal">
    <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color:#045061; color:#fff;padding:0 40px 30px 40px">
    <div class="modal-header" style="border-bottom:1px solid #045061;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
    <img src="{{asset('assets/img/thumbs-up.png')}}" width="80px" style="margin-left:40%;margin-bottom:10px;"/>
    <h1 style="text-align:center">Congratulations</h1>
    <p style="color:#fff; font-size:14pt;text-align:center;" >{{__('trans.Your Profile is 100% complete. Please visit the profile summary section to confirm the details filled in are correct')}} </p>
    </div>

    </div>
    </div>
    </div>

@endsection

@section('footer')
@if( Session::has('complete') && $invitation->calculateCompletion($event->id) == 100)
   <script type="text/javascript">
      $(document).ready(function() {
        $('#exampleModal').modal();
      });
   </script>
@endif

    <script>
        $('.date-picker').datepicker({
            //rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });

        $('.demo').each(function() {
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                defaultValue: $(this).attr('data-defaultValue') || '',
                inline: $(this).attr('data-inline') === 'true',
                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                opacity: $(this).attr('data-opacity'),
                position: $(this).attr('data-position') || 'bottom left',
                change: function(hex, opacity) {
                    if (!hex) return;
                    if (opacity) hex += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(hex);
                    }
                },
                theme: 'bootstrap'
            });
        });

        $('.next').click(function(){
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href=#'+nextId+']').tab('show');
            return false;

        })

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            //update progress
            var step = $(e.target).data('step');
            var percent = (parseInt(step) / 5) * 100;
            $('.progress-bar').css({width: percent + '%'});
//         $('.progress-bar').text("Step " + step + " of 5");
            //e.relatedTarget // previous tab
            $('.activities').slick('setPosition',0);
//         /$('.activities').slick('setPosition',0);
            $('.activities').get(0).slick.setPosition()

        })

        $('.first').click(function(){
            $('#myWizard a:first').tab('show')
        })

    </script>

    <script>
        $('.activities').slick({
            prevArrow: '<i class="fa fa-arrow-left fa-6 btn-prev " style="font-size: 36px;color: black;" aria-hidden="true" aria-label="Previous" ></i>',
            nextArrow:'<i class="fa fa-arrow-right fa-6 btn-next " style="font-size: 36px;color: black;" aria-hidden="true" aria-label="Next" ></i>',
            infinite: true,//centerMode: true,
            //speed: 300,
            arrows: true,
            slidesToShow: 1,slidesToScroll: 1,
            //adaptiveHeight: true
        });

        $('ul.tabs').on('toggled', function (event, tab) {
            $('.activities').slick('setPosition',0);
            $('.activities').get(0).slick.setPosition()
        });

        document.getElementById('hotel-selector').addEventListener('change', function () {
            console.log(this.value);
            var style =  this.value == 0 ? 'block' : 'none';
            document.getElementById('hotel-details').style.display = style;
        });

    </script>
    <script>

        var query = location.href.split('#');
           if(query[1] == undefined){

             console.log('old');
           }else{
             var tabs =$('a[data-toggle="tab"]');
             var link = '#'+query[1];
             $.each( tabs, function( key, value ) {
               var tab = $(value);
               $(tab).parent().removeClass('active');
               if ($(tab).attr('href') == link){
                 $(tab).parent().addClass('active');

                var tabpane =$('.tab-pane');
                $.each( tabpane, function( k, v ) {
                  var pane = $(v);
                  $(pane).removeClass('active');
                   // console.log(pane);
                     if ($(pane).attr('id') == query[1]){
                       $(pane).addClass('active');
                     }
                });
             }
            });


           }
    // .attr('href')

        // document.cookies = 'anchor=' + query[1];

        </script>
@endsection
