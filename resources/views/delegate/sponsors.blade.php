<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 21:00
 */
?>

@extends('layouts.front-v-iii')

@section('slider')

    <div class="title-wrapper">
        <div class="container">
            <div class="container-inner" align="center">
                <h1 style="font-size: 30px; font-weight: 500; line-height: 1.4; color: white;">SPONSORS OF {{ $event->name }}</h1>
                <em style="font-size: 20px;   font-weight: normal;   line-height: 1.4; color: white;font-family: 'Libre Baskerville', serif;">
                    We are extremely grateful to all those who have generously donated to Space for Giants.
                    Each one of you has helped us to get one step closer to achieving our mission. Thank You.
                </em>
            </div>
        </div>
    </div>


@endsection

@section('content')

    <div class="row-fluid  margin-bottom-40">

        @foreach( $event->sponsor as $lst)

        <div class="col-md-6 col-sm-12" style="height: 220px">
            <div class="row card">
                <div class="col-md-4 col-sm-4">
                    <img src="{{ asset($lst->logo) }}" alt="Avatar" style="width:180px;">
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="details" style="text-align: justify; padding-bottom: 30px;padding-left: 5px;padding-right: 5px;">
                        {!! $lst->brief  !!}
                        <br>
                        <a href="{!! $lst->website !!}" target="_blank" style="color: #433A32; font-weight: bold;" > {!! $lst->name !!} </a>
                        <br> <br>
                    </div>
                </div>
            </div>
        </div>

        @endforeach


    </div>


@endsection

@section('footer')



@endsection