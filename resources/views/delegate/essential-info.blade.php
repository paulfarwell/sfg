<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 21:00
 */
?>

@extends('layouts.front')

@section('slider')

    <style>
        .faq-tabbable, .faq-tabbable li a {
            border-left: none;
            border-bottom: none;
            color: #84724C;
        }

        .faq-tabbable li a {
            color: #84724C;
        }

        .faq-tabbable li:hover a, .faq-tabbable li.active a {
            background: transparent;
            color: #413831;
            font-weight: bold;
            border-bottom: solid 2px #6F6C67;
        }

        .faq-tabbable li {
            position: relative;
            margin-bottom: 5px;
            background: transparent;
            border: none;
        }

        .faq-tabbable li a {
            background: transparent;
            border-bottom: solid 1px #6F6C67;
        }

        .faq-tabbable li a i {
            margin-left: 20px;
            margin-right: 10px;
        }

        .faq-tabbable li.active a {
            background: transparent;
            color: #413831;
            font-weight: bold;
            border-bottom: solid 2px #6F6C67;
        }

        .faq-tabbable li.active:after {
            content: '';
            display: inline-block;
            border-bottom: 6px solid transparent;
            border-top: 6px solid transparent;
            border-left: 6px solid transparent;
            position: absolute;
            top: 16px;
            right: -5px;
        }

        .tab-pane strong , .tab-pane h3 {
            color: #84724C;
        }

    </style>

    <div class="title-wrapper">
        <div class="container">
            <div class="container-inner" align="center">
                <h1 style="font-size: 30px; font-weight: 500; line-height: 1.4; color: white;"> ESSENTIAL INFORMATION </h1>
                <em style="font-size: 20px;   font-weight: normal;   line-height: 1.4; color: white;font-family: 'Libre Baskerville', serif;"> A general information guide on the {{ $event->name }}.</em>
            </div>
        </div>
    </div>


@endsection

@section('content')

    <div class="content-page">

        {{--<div class="row">--}}
        {{--<div class="col-md-3 col-sm-3">--}}
        {{--<ul class="tabbable faq-tabbable tab-sfg">--}}
        {{--<li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> General Questions</a></li>--}}
        {{--<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Membership</a></li>--}}
        {{--<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Terms Of Service</a></li>--}}
        {{--<li><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> License Terms</a></li>--}}
        {{--<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Payment Rules</a></li>--}}
        {{--<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Other Questions</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--<div class="col-md-9 col-sm-9">--}}
        {{--<div class="tab-content" style="padding:0; background: #fff;">--}}
        {{--<!-- START TAB 1 -->--}}
        {{--<div class="tab-pane active" id="tab_1"> 1 </div>--}}
        {{--<div class="tab-pane " id="tab_2"> 2 </div>--}}
        {{--<div class="tab-pane " id="tab_3"> 3 </div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="row">

                <div class="col-md-4 col-sm-4">

                    <ul class="tabbable faq-tabbable tab-sfg">
                        <?php  $i = 0;  ?>
                        @foreach( $event->newsFull as $ls)
                            <li class="@if ($i == 0) active @endif"><a href="#news_{{$ls->id}}" data-toggle="tab"> <i class="fa fa-plus" aria-hidden="true"></i>  {{ $ls->category }}</a></li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="tab-content" style="padding:0; background: #fff;color:black;">
                        <?php    $i = 0;      ?>
                        @foreach( $event->news as $ls)
                            <div class="tab-pane @if ($i == 0) active @endif" id="news_{{$ls->id}}">
                                {!! $ls->text !!}
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>


        </div>

    </div>


@endsection

@section('footer')


@endsection