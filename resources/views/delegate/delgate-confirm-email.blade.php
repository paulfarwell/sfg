<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/08/2017
 * Time: 12:36
 */
?>

@extends('layouts.delegate-adv-login')

@section('content')
<h1>Participant | Confirm email address </h1>
<p> {{ $message or ' Please enter and confirm your email address in the form below then click proceed to save access your profile.' }} </p>


    <form action="{{ route('delegate.login.confirm.email',[$code,$eventid])  }}" class="form form-horizontal login-form " method="post" id="validatation" >
        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
        <div class="fetc">

            <div class="col-xs-12">
                <input class="form-control form-control-solid placeholder-no-fix form-group validate[custom[email]]"
                       type="email" autocomplete="off" value=""
                       placeholder="Enter your email address" name="email" required id="email"/>
                {{--<small> please not that all emails must be unique </small>--}}
                @if ($errors->has('email'))
                    <span class="help-block" style="color:red;">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                @endif
            </div>

            <div class="col-xs-12">
                <input class="form-control form-control-solid placeholder-no-fix form-group validate[custom[email]]"
                       type="email" autocomplete="off" value=""
                       placeholder="Confirm your email address" name="confirm_email" required id="correspondence_email"/>
                {{--<small> this email will receive a copy of all your emails sent to the emails above </small>--}}
                @if ($errors->has('confirm_email'))
                    <span class="help-block" style="color:red;">
                <strong>{{ $errors->first('confirm_email') }}</strong>
            </span>
                @endif
            </div>

            <div class="col-xs-12" style="padding-right: 30px;">
                <button type="submit" class="btn blue-button pull-right" type="submit">Proceed </button>
            </div>

        </div>
    </form>



@endsection
