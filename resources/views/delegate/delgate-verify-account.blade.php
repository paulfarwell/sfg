<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/08/2017
 * Time: 12:36
 */
?>

@extends('layouts.delegate-adv-login')

@section('content')


    <form action="{{ route('delegate.login',[$code,$eventid])  }}" class="form form-horizontal login-form " method="post" id="validatation" >

        <h1 class="use-perpetua-normal"> {{ strtolower('Delegate Login') }} </h1>
        <p> {{ $message or 'Please enter the one-time password that was sent to your email' }} </p>

        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
        <div class="form-body">
            <div class="form-group">
                <div class="input-group">
                    <input type="password" name="password" class="form-input round-input validate[required] form-control"
                           placeholder="Please enter your password sent in the email here">
                    <span class="input-group-btn">
                        <button id="genpassword" type="submit" class="btn round-button" > Proceed </button>
                            </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="rem-password">
                    {{--<label class="rememberme mt-checkbox mt-checkbox-outline">--}}
                        {{--<input type="checkbox" name="remember" value="1" /> Remember me--}}
                        {{--<span></span>--}}
                    {{--</label>--}}
                </div>
            </div>
            <div class="col-sm-8 text-right">
                <div class="forgot-password">
                    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
                {{--<button class="btn green" type="submit">Sign In</button>--}}
            </div>
        </div>

    </form>




@endsection
