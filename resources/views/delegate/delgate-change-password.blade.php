<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/08/2017
 * Time: 12:36
 */
?>

@extends('layouts.delegate-adv-login2')

@section('content')
<style>
.form-horizontal .form-group {
    margin-left: 0px;
    margin-right: 0px;
}
#password1{
  border-right:none;
}
#password2{
  border-right:none;
}
.input-group-addon>i {
    color: #045061;
}

</style>
<h1>{{__('trans.Participant | Change your password')}} </h1>
<p>{{__('trans.Please enter a new password so as to secure your account')}}</p>

        @if (Auth::check())
        <form action="{{ route('delegate.login.change.code',[$code,$eventid])  }}" class="form form-horizontal login-form " method="post" id="validatation" >
        @else
        <form action="{{ route('delegate.login.reset.code',[$code,$eventid,$reset_token])  }}" class="form form-horizontal login-form " method="post" id="validatation" >
        @endif

        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
        <input type="hidden" id="reset_token" name="reset_token" value="{{ $reset_token  }}">
        <div class="">

            <div class="col-xs-12 form-group">
                <input class="form-control form-control-solid placeholder-no-fix form-group validate[]"
                       type="password" autocomplete="off" placeholder="{{__('trans.Password')}}" name="password" required id="password1" data-toggle="password"/>

                       @if ($errors->has('password'))
                           <span class="help-block" style="color:red;">
                       <strong>{{ $errors->first('password') }}</strong>
                   </span>
                       @endif
            </div>

            <div class="col-xs-12 form-group">
                <input class="form-control form-control-solid placeholder-no-fix form-group validate[] "
                       type="password" autocomplete="off" placeholder="{{__('trans.Confirm Password')}}" name="confirm_password" required id="password2" data-toggle="password"/>
                       @if ($errors->has('confirm_password'))
                           <span class="help-block" style="color:red;">
                       <strong>{{ $errors->first('confirm_password') }}</strong>
                   </span>
                       @endif
            </div>

            <div class="col-xs-12" style="padding-right: 30px;">
                <button type="submit" class="btn blue-button pull-right" type="submit">{{__('trans.Proceed')}}  </button>
            </div>

        </div>
    </form>


@endsection
