<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:13
 */
?>

@extends('print.tpl')

@section('doc-header')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="left" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">DELEGATION</span></strong></td>
                        <td valign="top">
                            <div class="client_info">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td style="padding-left:25px;">
                                            <span class="editable-area" id="client_info"> {{ $delegation->name }}<br />
                                                {{ $delegation->countryname->name}}<br />
                                            C/O {{ $delegation->contact_name}},{{ $delegation->contact_email}} </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td align="right" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td align="right"><strong><span class="editable-text" id="label_date">Date</span></strong></td>
                        <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ date('d m Y') }}</span></td>
                    </tr>
                    <!-- Fieldl-->
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"></span></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('doc-body')
    <table class="table table-bordered table-condensed table-striped items-table">
        <thead>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Delegate Type</th>
            <th>Badge</th>
            <th width="100">Price</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="totals-row">
            <td colspan="5" class="wide-cell"></td>
            <td><strong>Total</strong></td>
            <td coslpan="">$ {{ $delegation->invitations->sum('delegatePrice.price') }} </td>
        </tr>
        </tfoot>
        <tbody>

        @foreach($delegation->invitations  as $list)
        <tr>
            <td> {{ $list->code }}</td>
            <td> {{ $list->profile->fname }} {{ $list->profile->surname }} </td>
            <td> {{ $list->profile->email }} </td>
            <td> {{ $list->profile->phone }} </td>
            <td> {{ $list->delegateType->name }} </td>
            <td> {{ $list->delegateType->color }} </td>
            <td>$ {{ $list->delegatePrice->price }}</td>
        </tr>
        @endforeach

        </tbody>
    </table>
@endsection
