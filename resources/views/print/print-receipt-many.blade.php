<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:13
 */
?>

@extends('print.tpl')

@section('doc-header')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="left" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">{{__('trans.Delegate')}}</span></strong></td>
                        <td valign="top">
                            <div class="client_info">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr>
                                          <td align="right"><strong><span class="editable-text" id="label_date">{{__('trans.Name')}}</span></strong></td>
                                          <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ $invitation->profile->fname }} {{ $invitation->profile->surname }}</span></td>
                                      </tr>
                                      <!-- Fieldl-->
                                      <tr class="field1_row">
                                          <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Phone')}} </span></strong></td>
                                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{isset($invitation->profile->cell_phone) ? $invitation->profile->cell_phone_code.'-'.$invitation->profile->cell_phone  :',' }}</span></td>
                                      </tr>
                                      <tr class="field1_row">
                                          <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Email')}} </span></strong></td>
                                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value">{{ $invitation->profile->email}}</span></td>
                                      </tr>
                                      <tr class="field1_row">
                                          <td align="right"><strong><span class="editable-text" id="label_field1">C/O </span></strong></td>
                                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegation->contact_name}}, {{ $invitation->delegation->contact_email}}</span></td>
                                      </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td align="right" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td align="right"><strong><span class="editable-text" id="label_date">{{__('trans.Print Date')}}</span></strong></td>
                        <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ date('d-m-Y') }}</span></td>
                    </tr>
                    <!-- Fieldl-->
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Invitation Code')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->code }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Delegate Type')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->name }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Color')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->color }}</span></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('doc-body')
    <table class="table table-bordered table-condensed table-striped items-table">
        <thead>
        <tr>
            <th>{{__('trans.Item')}} </th>
            <th>{{__('trans.Description')}}</th>
            <th>{{__('trans.Quantity')}} </th>
            <th>{{__('trans.Price')}} </th>
            <th width="100">{{__('trans.Total')}}</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>{{__('trans.Total')}}</strong></td>
            <td coslpan="">$ {{ $payment->sum('amount') }}</td>
        </tr>
        </tfoot>
        <tbody>
        @foreach($payment  as $key => $list)
            <tr>
                <td> {{ $key + 1 }}  </td>
                <td> {{ $list ->pay_method }} <br> {{ $list->details }} </td>
                <td> 1 </td>
                <td> {{ $list ->amount }} </td>
                <td width="100"> $ {{ $list->amount }}</td>
            </tr>
        @endforeach
        <tr> <th colspan="5">    </th></tr>

        </tbody>
    </table>
@endsection
