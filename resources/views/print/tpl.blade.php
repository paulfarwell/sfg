<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:24
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $document_name or null }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        .totals-row td {
            border-right:none !important;
            border-left:none !important;
        }
        td {
            white-space: nowrap;
        }
        .items-table td ,#notes { white-space:normal;}
        .totals-row td strong,.items-table th {
            white-space:nowrap;
        }
    </style>
    <style type="text/css">
        .is_logo {display:none;}
    </style>
</head>
<body>
<div id="editor" class="edit-mode-wrap" style="margin-top: 20px">
    <style type="text/css">
        .is_logo {display:none;}
    </style>
    <style type="text/css">* { margin:0; padding:0; }
        body { background:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; }
        #extra {text-align: right; font-size: 22px;  font-weight: 700}
        .invoice-wrap { width:700px; margin:0 auto; background:#FFF; color:#000 }
        .invoice-inner { margin:0 15px; padding:20px 0 }
        .invoice-address { border-top: 3px double #000000; margin: 25px 0; padding-top: 25px; }
        .bussines-name { font-size:18px; font-weight:100 }
        .invoice-name { font-size:22px; font-weight:700 }
        .listing-table th { background-color: #e5e5e5; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; text-align:left; padding:6px 4px }
        .listing-table td { border-bottom: 1px solid #555555; text-align:left; padding:5px 6px; vertical-align:top }
        .total-table td { border-left: 1px solid #555555; }
        .total-row { background-color: #e5e5e5; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; }
        .row-items { margin:5px 0; display:block }
        .notes-block { margin:50px 0 0 0 }
        /*tables*/
        table td { vertical-align:top}
        .items-table { border:1px solid #1px solid #555555; border-collapse:collapse; width:100%}
        .items-table td, .items-table th { border:1px solid #555555; padding:4px 5px ; text-align:left}
        .items-table th { background:#f5f5f5;}
        .totals-row .wide-cell { border:1px solid #fff; border-right:1px solid #555555; border-top:1px solid #555555}
    </style>
    <div class="invoice-wrap">
        <div class="invoice-inner">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td align="left" class="is_logo" valign="top"> </td>
                    <td align="right" valign="top">
                        <div class="business_info">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td><span class="editable-area" id="business_info">
                                            <p style="font-size: 14pt;"> {{ $invitation->event->name }} </p>

                                            <br />
                                            <p> {{ 'Wildlife Economy' }} </p>
                                            <!-- <p>{{ $business->address }}</p>
                                            <p>{{ $business->city }}, {{ $business->state }} {{ $business->postal }} </p> -->
                                            <p>{{ '+254 736 442 264' }}</p>
                                            <p>{{ 'Invites@wildlifeeconomy.com' }}</p>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td align="right" valign="top">

                        <img class="editable-area" id="logo" src="{{asset('assets/frontend/corporate/img/logos/logo-2.png')}}" width="50%"/>
                        <p class="editable-text" id="extra"><span style="font-size: 12pt;"> {{ $document_name or null }}</span></p>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="invoice-address">
                @yield('doc-header')
            </div>

            <div id="items-list">
                @yield('doc-body')
            </div>

            <div class="notes-block">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div class="editable-area" id="notes" style=""> {!! $document_notes or null !!}  </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br />
            <br />
            <br />
            <br />
            &nbsp;</div>
    </div>
</div>
<style>
    body {
        background: #EBEBEB;
    }
    .invoice-wrap {box-shadow: 0 0 4px rgba(0, 0, 0, 0.1); margin-bottom: 20px; }
    #mobile-preview-close a {
        position:fixed; left:20px; bottom:30px;
        background-color: #27c24c;
        font-weight: 600;
        outline: 0 !important;
        line-height: 1.5;
        border-radius: 3px;
        font-size: 14px;
        padding: 7px 10px;
        border:1px solid #27c24c;
        text-decoration:none;
    }
    #mobile-preview-close img {
        width:20px;
        height:auto;
    }
    #mobile-preview-close a:nth-child(2) {
        left:190px;
        background:#f5f5f5;
        border:1px solid #9f9f9f;
        color:#555 !important;
    }
    #mobile-preview-close a:nth-child(2) img {
        height: auto;
        position: relative;
        top: 2px;
    }
    .invoice-wrap {padding: 20px;}


    @media print {
        #mobile-preview-close a {
            display:none
        }
        .invoice-wrap {0}
        body {
            background: none;
        }
        .invoice-wrap {box-shadow: none; margin-bottom: 0px;}

    }
</style>

<?php
    $key = 0;
?>

</body>
</html>
