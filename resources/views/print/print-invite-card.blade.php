<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:13
 */
?>

@extends('print.tpl-card')

@section('delegate-image')
    <img src="{{ url(asset($invitation->profile->national_id_image) ? $invitation->profile->national_id_image : 'uploads/default-poster.jpg') }}" width="150">
@endsection

@section('doc-header')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="left" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">DELEGATE</span></strong></td>
                      </tr>
                      <tr>
                        <td valign="top">
                            <div class="client_info">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td style="padding-left:5px;">
                                            <span class="editable-area" id="client_info">
                                                NAME : {{ $invitation->profile->fname }} {{ $invitation->profile->surname }}<br />
                                                DELEGATION : {{ $invitation->delegation->name}}<br />
                                                Date of Passport Issue: {{$invitation->profile->date_issue}}</br>
                                                Date of Passport Expiration:{{$invitation->profile->date_expire}}
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td align="right" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    {{--<tr>--}}
                        {{--<td align="right"><strong><span class="editable-text" id="label_date">Print Date</span></strong></td>--}}
                        {{--<td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ date('d m Y') }}</span></td>--}}
                    {{--</tr>--}}
                    <!-- Fieldl-->
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">Invitation Code </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->code }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> Delegate Type </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->name }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> Color </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->color }}</span></td>

                    </tr>
                    <!-- <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> &nbsp; </span></strong></td>
                        <td align="left" style="padding-left:10px;"><img src="{{ asset($invitation->delegateType->image_path) }}" width="60px" style="float: right; position: relative; clear: both;"></td>
                    </tr> -->
                    </tbody>
                </table>
            </td>
        </tr>

        </tbody>
    </table>
    @endsection

@section('doc-body')

@endsection
