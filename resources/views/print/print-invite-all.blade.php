<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:13
 */
?>

@extends('print.tpl')



@section('doc-header')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>  <td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">{{__('trans.Delegate')}}</span></strong></td></tr>
        <tr>
            <td align="left" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                          <td align="right"><strong><span class="editable-text" id="label_date">{{__('trans.Name')}}</span></strong></td>
                          <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ $invitation->profile->fname }} {{ $invitation->profile->surname }}</span></td>
                      </tr>
                      <!-- Fieldl-->
                      <tr class="field1_row">
                          <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Phone')}} </span></strong></td>
                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{isset($invitation->profile->cell_phone) ? $invitation->profile->cell_phone_code.'-'.$invitation->profile->cell_phone  :',' }}</span></td>
                      </tr>
                      <tr class="field1_row">
                          <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Email')}} </span></strong></td>
                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value">{{ $invitation->profile->email}}</span></td>
                      </tr>
                      <tr class="field1_row">
                          <td align="right"><strong><span class="editable-text" id="label_field1">C/O </span></strong></td>
                          <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegation->contact_name}}, {{ $invitation->delegation->contact_email}}</span></td>
                      </tr>
                    </tbody>
                </table>
            </td>
            <td align="right" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td align="right"><strong><span class="editable-text" id="label_date">{{__('trans.Date')}}</span></strong></td>
                        <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ date('d-m-Y') }}</span></td>
                    </tr>
                    <!-- Fieldl-->
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Invitation Code')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->code }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Delegate Type')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->name }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">{{__('trans.Color')}} </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $invitation->delegateType->color }}</span></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('doc-body')
    <table class="table table-bordered table-condensed table-striped items-table">
        <thead>
        <tr>
          <th>{{__('trans.Item')}} </th>
          <th>{{__('trans.Description')}}</th>
          <th>{{__('trans.Quantity')}} </th>
          <th>{{__('trans.Price')}} </th>
          <th width="100">{{__('trans.Total')}}</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>{{__('trans.Total')}}</strong></td>
            <td coslpan="">
              @if ($invitation->complimentary == 1)
                  $ {{number_format (0 ,2)}}
                @else

              $ {{  number_format( (($invitation->delegatePrice->price) + ($invitation->activity->sum('activity.cost'))
                                            + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                            + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0)
                                            + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price : 0 )) )  }}</td>
             @endif
        </tr>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>{{__('trans.Payments')}}</strong></td>
            <td coslpan="">$  {{ number_format($invitation->payments->sum('amount'),2) }} </td>
        </tr>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>{{__('trans.Refunds')}}</strong></td>
            <td coslpan="">$  {{ number_format($invitation->refunds->sum('amount'),2) }}</td>
        </tr>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>{{__('trans.Outstanding Balance')}}</strong></td>
            <td coslpan="">
              @if ($invitation->complimentary == 1)
                  $ {{number_format (0 ,2)}}
                @else

              $ {{ number_format(((($invitation->delegatePrice->price) + ($invitation->activity->sum('activity.cost'))
                                            + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                            + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0)
                                            + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price : 0 ) ) - $invitation->payments->sum('amount')))  }} </td>

              @endif
        </tr>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>  {{__('trans.Overpayment')}} </strong></td>
            <td coslpan="">
              @if ($invitation->complimentary == 1)
                  $ {{number_format (0 ,2)}}
                @else

              $ {{ number_format(($invitation->payments->sum('amount') - (($invitation->delegatePrice->price) + ($invitation->activity->sum('activity.cost'))
                                            + ( isset($invitation->transfer) ? $invitation->transfer->package->cost : 0 )
                                            + (isset($invitation->transfer_return) ? $invitation->transfer_return->package_return->cost : 0)
                                            + (isset($invitation->tent_accomodation) ? $invitation->tent_accomodation->tent->price : 0 ) )))  }} </td>
            @endif
        </tr>
        </tfoot>
        <tbody>

        {{--//ticket--}}
        <tr> <th colspan="5"> {{__('trans.Attendance Package')}} </th></tr>
        <tr>
            <td> 1 </td>
            <td> {{ $invitation->delegateType->name }}</td>
            <td> 1 </td>
            <td> $ {{ number_format($invitation->delegatePrice->price) }} </td>
            <td width="100"> $ {{ number_format($invitation->delegatePrice->price) }}</td>
        </tr>
        {{--//activities--}}
        <tr> <th colspan="5"> {{__('trans.Event Activities')}} </th></tr>
        @foreach($invitation->activity  as $key => $list)
            <tr>
                <td> {{ $key+1 }} </td>
                <td> {{ $list->activity->name }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($list->activity->cost) }} </td>
                <td width="100"> $ {{ $list->activity->cost }}</td>
            </tr>
        @endforeach

        {{--//transport--}}
        @if ($invitation->transfer)
        <tr> <th colspan="5"> {{__('trans.Event Arrival Transfer')}} </th></tr>
        {{--@foreach($invitation->transfer  as $key => $list)--}}
            <tr>
                <td> 1 </td>
                <td> {{ $invitation->transfer->package->name }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($invitation->transfer->package->cost) }} </td>
                <td width="100"> $ {{ number_format($invitation->transfer->package->cost) }}</td>
            </tr>
        {{--@endforeach--}}
        @endif
        @if ($invitation->transfer_return)
        <tr> <th colspan="5"> {{__('trans.Event Departure Transfer')}} </th></tr>
        {{--@foreach($invitation->transfer  as $key => $list)--}}
            <tr>
                <td> 1 </td>
                <td> {{ $invitation->transfer_return->package_return->name }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($invitation->transfer_return->package_return->cost) }} </td>
                <td width="100"> $ {{ number_format($invitation->transfer_return->package_return->cost) }}</td>
            </tr>
        {{--@endforeach--}}
        @endif

        @if ($invitation->tent_accomodation)
        <tr> <th colspan="5"> {{__('trans.Event Tent Accomodation')}} </th></tr>
        {{--@foreach($invitation->tent_accomodation  as $key => $list)--}}
            <tr>
                <td> 1 </td>
                <td> {{ $invitation->tent_accomodation->tent->name }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($invitation->tent_accomodation->tent->price) }} </td>
                <td width="100"> $ {{ number_format($invitation->tent_accomodation->tent->price) }}</td>
            </tr>
        {{--@endforeach--}}
        @endif

        <tr> <th colspan="5">  {{__('trans.Hotel Details')}} </th></tr>

        @if ($invitation->hotel)
        <tr>
            <td> 1 </td>
            <td> {{ $invitation->hotel->hotel->name or null }}</td>
            <td> 1 </td>
            <td> billed by Hotel </td>
            <td width="100"> billed by Hotel </td>
        </tr>
        @endif


        <tr> <th colspan="5">  {{__('trans.Flight Details')}} </th></tr>
        @if($invitation->inflight)
        <tr>
            <td colspan="">  1 </td>
            <td colspan="">
                <strong> {{__('trans.Inbound Flight')}}</strong><br>
                {{__('trans.Arrival Date')}}: {{ $invitation->inflight->arrival_date }} <br>
                {{__('trans.Arrival Time')}}` `: {{ $invitation->inflight->arrival_time }} <br>
                {{--@if ( $inv_flight_in->flight->type == 'Scheduled Flight')--}}
                {{--Depart {{ $inv_flight_in->airport_name }} 2115hrs – Arrive Johannesburg, (O.R. Tambo Int)0030hrs--}}
                {{--@endif--}}
                {{--Flight Number: {{ $inv_flight_in->flight_number }} <br>--}}
                {{__('trans.Plane Registration')}}: {{ $invitation->inflight->plane_reg }} <br>
                {{__('trans.Flight Number')}}: {{ $invitation->inflight->flight_number }} <br>
                {{__('trans.Flight Carrier')}}: {{ $invitation->inflight->flight_carrier }} <br>
                <br>
                {{__('trans.Transfer Required')}} :- {{ $invitation->inflight->transfer_req }}
            </td>
            <td colspan="">  1 </td>
            <td colspan="">  {{__('trans.billed by airline')}} </td>
            <td colspan="">  {{__('trans.billed by airline')}} </td>
        </tr>
        <tr>
            <td colspan="">  2 </td>
            <td colspan="">
                <strong> {{__('trans.Outbound Flight')}}</strong><br>
                {{__('trans.Departure Date')}}: {{ $invitation->outflight->departure_date }} <br>
                {{__('trans.Departure Time')}}: {{ $invitation->outflight->departure_time }} <br>
                {{--@if ( $inv_flight_in->flight->type == 'Scheduled Flight')--}}
                {{--Depart {{ $inv_flight_in->airport_name }} 2115hrs – Arrive Johannesburg, (O.R. Tambo Int)0030hrs--}}
                {{--@endif--}}
                {{--Flight Number: {{ $inv_flight_out->flight_number }} <br>--}}
                {{__('trans.Plane Registration')}}: {{ $invitation->outflight->plane_reg }} <br>
                {{__('trans.Flight Number')}}: {{ $invitation->outflight->flight_number }} <br>
                {{__('trans.Flight Number')}}: {{ $invitation->outflight->flight_carrier }} <br>
                <br>
                {{__('trans.Transfer Required')}} :- {{ $invitation->outflight->transfer_req }}
            </td>
            <td colspan="">  1 </td>
            <td colspan="">  {{__('trans.billed by airline')}} </td>
            <td colspan="">  {{__('trans.billed by airline')}} </td>
        </tr>
        @endif

        <tr> <th colspan="5">  {{__('trans.Payment Details')}} </th></tr>
        @foreach($invitation->payments  as $key => $list)
            <tr>
                <td> {{ $key+1 }} </td>
                <td> {{ $list->pay_method }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($list->amount) }} </td>
                <td width="100"> $ {{ number_format($list->amount) }}</td>
            </tr>
        @endforeach

        <tr> <th colspan="5">  {{__('trans.Refund Details')}} </th></tr>
        @foreach($invitation->refunds  as $key => $list)
            <tr>
                <td> {{ $key+1 }} </td>
                <td> {{ $list->payout_method }}</td>
                <td> 1 </td>
                <td> $ {{ number_format($list->amount) }} </td>
                <td width="100"> $ {{ number_format($list->amount) }}</td>
            </tr>
        @endforeach

        <tr> <th colspan="5">    </th></tr>

        </tbody>
    </table>
@endsection
