<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 27/08/2017
 * Time: 15:13
 */
?>

@extends('print.tpl')

@section('doc-header')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="left" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">DELEGATE</span></strong></td>
                        <td valign="top">
                            <div class="client_info">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td style="padding-left:25px;">
                                            <span class="editable-area" id="client_info">
                                            {{ $payment->invitation->profile->fname }} {{ $payment->invitation->profile->surname }}<br />
                                            {{ $payment->invitation->profile->phone}}, {{ $payment->invitation->profile->email}}<br />
                                            C/O,{{ $payment->invitation->delegation->contact_name}}, {{ $payment->invitation->delegation->contact_email}} </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td align="right" valign="top" width="50%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td align="right"><strong><span class="editable-text" id="label_date">Print Date</span></strong></td>
                        <td align="left" style="padding-left:20px"><span class="editable-text" id="date"> {{ date('d m Y') }}</span></td>
                    </tr>
                    <!-- Fieldl-->
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1">Invitation Code </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $payment->invitation->code }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> Delegate Type </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $payment->invitation->delegateType->name }}</span></td>
                    </tr>
                    <tr class="field1_row">
                        <td align="right"><strong><span class="editable-text" id="label_field1"> Color </span></strong></td>
                        <td align="left" style="padding-left:20px;"><span class="editable-text" id="field1_value"> {{ $payment->invitation->delegateType->color }}</span></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('doc-body')
    <table class="table table-bordered table-condensed table-striped items-table">
        <thead>
        <tr>
            <th> Item </th>
            <th> Description</th>
            <th> Qty </th>
            <th> Price </th>
            <th width="100">Total</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="totals-row">
            <td colspan="3" class="wide-cell"></td>
            <td><strong>Total</strong></td>
            <td coslpan="">$ {{ $payment->amount }}</td>
        </tr>
        </tfoot>
        <tbody>
        {{--@foreach($payment  as $key => $list)--}}
            <tr>
                <td> 1  </td>
                <td> {{ $payment ->pay_method }} <br> {{ $payment->details }} </td>
                <td> 1 </td>
                <td> {{ $payment ->amount }} </td>
                <td width="100"> $ {{ $payment->amount }}</td>
            </tr>
        {{--@endforeach--}}
        <tr> <th colspan="5">    </th></tr>

        </tbody>
    </table>
@endsection
