<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 22/06/2017
 * Time: 20:27
 */
?>

<li class="nav-item start ">
    <a href="{{ route('org.index') }}" class="nav-link ">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>

<li class="nav-item  ">
    <a href="{{ route('org.events') }}" class="nav-link">
        <i class="icon-docs"></i>
        <span class="title"> Events </span>
    </a>
</li>

@if (isset($event))
<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-bar-chart"></i>
        <span class="title"> Manage Event </span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="{{ route('org.view.overview',$event->id) }}" class="nav-link ">
                <span class="title">Event Summary</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.edit.event',$event->id) }}" class="nav-link ">
                <span class="title">Event Edit </span></a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.images',$event->id) }}" class="nav-link ">
                <span class="title"> Event Images</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.edit.event.meta',$event->id) }}" class="nav-link ">
                <span class="title">Event Social Info </span></a>
        </li>
        <!-- <li class="nav-item  ">
            <a href="{{ route('org.manage.event.prog',$event->id) }}" class="nav-link ">
                <span class="title">Event Programme</span> </a>
        </li > -->
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.news',$event->id) }}" class="nav-link ">
                <span class="title">Event News</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.sponsors',$event->id) }}" class="nav-link ">
                <span class="title">Event Sponsors</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.organizers',$event->id) }}" class="nav-link ">
                <span class="title">Event Organizers</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.delegate.types',$event->id) }}" class="nav-link ">
                <span class="title">Delegate Types </span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.tickets',$event->id ) }}" class="nav-link ">
                <span class="title"> Event Tickets </span></a>
        </li>

        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.hotels',$event->id) }}" class="nav-link ">
                <span class="title">Event Hotels</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.tents',$event->id) }}" class="nav-link ">
                <span class="title">Event Tents</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.activities',$event->id) }}" class="nav-link ">
                <span class="title">Event Activities</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.transfer',$event->id) }}" class="nav-link ">
                <span class="title">Event Transfer Packages</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.medias',$event->id) }}" class="nav-link ">
                <span class="title">Event Media </span> </a>
        </li>

        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.terms',$event->id) }}" class="nav-link ">
                <span class="title">Event Terms & Conditions</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.disclaimers',$event->id) }}" class="nav-link ">
                <span class="title">Event Disclaimer </span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.events.custom.fields',$event->id) }}" class="nav-link ">
                <span class="title">Custom Fields</span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.manage.event.bookingForms',$event->id) }}" class="nav-link ">
                <span class="title">Booking Form </span> </a>
        </li>

        <li class="nav-item  ">
            <a href="{{ route('org.events.delegations',$event->id) }}" class="nav-link ">
                <span class="title">Delegations </span> </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.events.export.database',$event->id) }}" class="nav-link ">
                <span class="title">Delegate Database </span> </a>
        </li>
    </ul>
</li>
@endif

<li class="nav-item  ">
    <a href="{{ route('org.activities.info') }}" class="nav-link">
        <i class="icon-social-dribbble"></i>
        <span class="title"> Event Activities </span>
    </a>
</li>

{{--<li class="nav-item  ">--}}
    {{--<a href="javascript:;" class="nav-link">--}}
        {{--<i class="icon-feed"></i>--}}
        {{--<span class="title"> Event Access Areas </span>--}}
    {{--</a>--}}
{{--</li>--}}

<li class="nav-item  ">
    <a href="{{ route('org.hotels.info') }}" class="nav-link ">
        <i class="icon-diamond"></i>
        <span class="title"> Event Hotels </span>
    </a>
</li>

<li class="nav-item  ">
    <a href="{{ route('org.transport.info') }}" class="nav-link ">
        <i class="icon-plane"></i>
        <span class="title"> Delegate Flights </span>
    </a>
</li>

<li class="nav-item  ">
    <a href="{{ route('org.logistics.info') }}" class="nav-link ">
        <i class="icon-calculator"></i>
        <span class="title"> Logistics </span>
    </a>
</li>

<li class="nav-item  ">
    <a href="{{ route('org.invitations.info') }}" class="nav-link ">
        <i class="icon-layers"></i>
        <span class="title"> Invitations </span>
    </a>
</li>
<li class="nav-item  ">
    <a href="{{ route('org.delegate.info') }}" class="nav-link ">
      <i class="icon-emoticon-smile"></i>
        <span class="title">Delegate Types </span> </a>
</li>
<li class="nav-item  ">
    <a href="{{ route('org.delegations.info') }}" class="nav-link ">
        <i class="icon-emoticon-smile"></i>
        <span class="title"> Delegations </span>
    </a>
</li>

<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-bar-chart"></i>
        <span class="title"> Reports </span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href=" {{ route('org.payments') }}" class="nav-link ">
                <span class="title"> Payments </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href=" {{ route('org.refunds') }}" class="nav-link ">
                <span class="title"> Refunds </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href=" {{ route('org.logistics.info') }}" class="nav-link ">
                <span class="title"> Transport </span>
            </a>
        </li>
    </ul>
</li>

<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-wallet"></i>
        <span class="title"> Accounts | Monies </span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href=" {{ route('org.accounts') }}" class="nav-link ">
                <span class="title"> Delegate Accounts </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href=" {{ route('org.payments') }}" class="nav-link ">
                <span class="title"> Payments </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href=" {{ route('org.refunds') }}" class="nav-link ">
                <span class="title"> Refunds </span>
            </a>
        </li>
    </ul>
</li>

<li class="nav-item  ">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">System</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="{{ route('org.email.template') }}" class="nav-link ">
                <span class="title"> Email Templates </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.titles.info') }}" class="nav-link ">
                <span class="title"> Delegate Titles </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.delegation.types') }}" class="nav-link ">
                <span class="title"> Delegation Types </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.flight.info') }}" class="nav-link ">
                <span class="title"> Flight Information </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.country.info') }}" class="nav-link ">
                <span class="title"> Country Information </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.newcategory.info') }}" class="nav-link ">
                <span class="title"> News Categories </span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.users.mgr') }}" class="nav-link ">
                <span class="title"> Users Management</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('org.users.logs') }}" class="nav-link ">
                <span class="title"> Users Logs </span>
            </a>
        </li>
    </ul>
</li>
