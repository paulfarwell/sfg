
<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 21/06/2017
 * Time: 08:51
 */
?>

<link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>

@if (session('message'))
    <script>
        $(document).ready(function () {
            toastr.info('{{Session::get('message')}}');
        });
    </script>
@endif

@if (session('error'))
    <script>
        $(document).ready(function () {
            toastr.error('{{Session::get('error')}}');
        });
    </script>
@endif

@if (session('success'))
    <script>
        $(document).ready(function () {
            toastr.success('{{Session::get('success')}}');
        });
    </script>
@endif

@if (session('warning'))
    <script>
        $(document).ready(function () {
            toastr.warning('{{Session::get('warning')}}');
        });
    </script>
@endif

@if (session('info'))
    <script>
        $(document).ready(function () {
            toastr.info('{{Session::get('info')}}');
        });
    </script>
@endif
