
<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 21/06/2017
 * Time: 08:51
 */
?>

<div class="modal fade" id="myImgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Photo </h4>
            </div>
            <div class="modal-body" align="center">
                <img src="" id="img" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>

<script>
    $('#myImgModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var img = button.data('img')
        console.log(img)
        $('#img').attr('src', img);
    })
</script>


<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center">Confirm </h5>
            </div>
            <div class="modal-body">

                <form class="" id="confirmModalForm" method="post" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div id="message"> Are you sure you want to delete this item, Action is not Reversible ? </div>
                    <br>
                    <button type="submit" name="Delete" value="Delete" class="btn btn-error red">
                        <i class="fa fa-trash-o"></i> Yes, Go Ahead
                    </button>
                    <button type="button" name="Cancel" value="Cancel" class="btn btn-error blue" data-dismiss="modal" aria-label="Close" >
                        <i class="fa fa-angle-double-right"></i> Cancel
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>


<script>
    $('#confirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        console.log(button.data('url'));
        $('#confirmModalForm').attr('action', button.data('url'));
        $('#message').text(button.data('msg'));
    })
</script>


<div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-labelledby="confirmModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Send Email </h5>
            </div>
            <div class="modal-body">

                <form class="" action="{{route('org.email.profile')}}" id="sendEmailForm" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="event" id="send-event" class="form-control ">
                    <div class="form-group">
                        <label> Email to </label>
                        <input type="email" name="email" id="send-email" class="form-control validate[required,custom[email]]">
                    </div>
                    <div class="form-group">
                        <label> Subject </label>
                        <input type="text" name="subject" id="subject" class="form-control validate[required]">
                    </div>
                    <div class="form-group">
                        <label> Message </label>
                        <textarea id="message" name="message" class="form-control tinymcemodal validate[required]"></textarea>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label> Attachment </label>--}}
                        {{--<input type="file" name="upload">--}}
                    {{--</div>--}}
                    <br>
                    <button type="submit" name="Send" value="Send" class="btn btn-error blue">
                        <i class="fa fa-tick-o"></i> Send
                    </button>
                    <button type="button" name="Cancel" value="Cancel" class="btn btn-error red" data-dismiss="modal" aria-label="Close" >
                        <i class="fa fa-angle-double-right"></i> Cancel
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>


<script>
    $('#sendEmail').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#send-email').val(button.data('email'));
        $('#send-event').val(button.data('event'));
        tinymce.remove('textarea.tinymcemodal');
        var editor_config_modal = {
            path_absolute : "",
            selector: "textarea.tinymcemodal",
            //plugins: ,
            height: 200,
            theme: 'modern',
            plugins: 'paste print preview variable code fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic mybutton strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            paste_as_text: true,
            paste_text_sticky_default: true,
            powerpaste_allow_local_images: true,
            powerpaste_word_import: 'prompt',
            powerpaste_html_import: 'prompt',
            relative_urls: false,
            setup : function(ed) {
                window.tester = ed;
                ed.addButton('mybutton', {
                    type: 'listbox',
                    title : 'My button',
                    values: [
                    {text: 'name',value: 'name'},
                    {text: 'eventname',value:'eventname'},
                    {text: 'eventdate',value:'eventdate'},
                    {text: 'eventlocation',value:'eventlocation'},
                    {text: 'eventlink', value: 'eventlink'},
                    {text: 'rawlink',value:'rawlink'},
                    {text: 'Attendance Category', value:'attendance_category'},
                    {text: 'Invitation Information', value:'invitation_information'}
                    ],
                    text : 'Select variable',
                    onselect : function() {
                        ed.plugins.variable.addVariable(this.value());
                    }
                });

                ed.on('variableClick', function(e) {
                   console.log('click', e);
                });
            },

            // variable plugin related
            // plugins: "variable,code ",
            variable_mapper: {
                username: 'Username',
                name: 'Name',
                eventname: 'Events Name',
                eventdate: 'Events Date',
                eventlocation: 'Events Location',
                eventlink: 'Events Link',
                rawlink: 'Raw Link',
                attendance_category: 'Attendance_category',
                invitation_information: 'Invitation Information',
                phone: 'Phone',
                community_name: 'Community name',
                email: 'Email address',
                sender: 'Sender name',
                account_id: 'Account ID'
            },
            variable_prefix: '{',
            variable_suffix: '}',
            // variable_class: 'bbbx-my-variable',
            variable_valid: ['username','name','eventname','eventdate','eventlocation','eventlink','attendance_category','invitation_information','rawlink', 'sender', 'phone', 'community_name', 'email'],
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config_modal.path_absolute + route_prefix + '?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
            init_instance_callback : function(editor) {
                editor.setContent('');
                editor.setContent(button.data('desc'));
            }
        };
        tinymce.init(editor_config_modal);
    })
</script>

<div class="modal fade" id="sendSms" tabindex="-1" role="dialog" aria-labelledby="confirmModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Send Sms </h5>
            </div>
            <div class="modal-body">

                <form class="" action="{{route('org.sms.profile')}}" id="sendSmsForm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label> Sms to </label>
                        <input type="text" name="phone" id="send-no" class="form-control validate[required]">
                    </div>
                    <div class="form-group">
                        <label> Message </label>
                        <textarea id="message" maxlength="160" onkeyup="calculaRestante(this)" name="message" class="form-control validate[required]"></textarea>
                        <span id="counter">160 Characters</span>
                    </div>
                    <br>
                    <button type="submit" name="Send" value="Send" class="btn btn-error blue">
                        <i class="fa fa-tick-o"></i> Send
                    </button>
                    <button type="button" name="Cancel" value="Cancel" class="btn btn-error red" data-dismiss="modal" aria-label="Close" >
                        <i class="fa fa-angle-double-right"></i> Cancel
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>


<script>
    $('#sendSms').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#send-no').val(button.data('phone'));
    })

    var calculaRestante = function (element) {
        "use strict";
        var maxlength = element.getAttribute("maxlength");
        var caracteres = element.value.length;
        var restante = maxlength - caracteres;
        document.getElementById("counter").innerHTML = restante + " Characters";
    };

</script>
