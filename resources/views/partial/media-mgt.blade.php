<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/08/2017
 * Time: 15:52
 */
?>


{{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

<div class="modal fade" id="editmedias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Edit Media and Conditions for an Event </h5>
            </div>
            <div class="modal-body">

                <form action="" method="post" id="editmediasForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <div class="form-group">
                            <label > Description : </label>
                            <div >
                                <textarea name="description" id="description"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div >
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update </button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>
    </div>
</div>

<script>
    $('#editmedias').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#editmediasForm').attr('action', button.data('url'));
        // $('#name').val(button.data('name'));
        $('#description').val(button.data('desc'));

      
        $('#description').eq(0).summernote('destroy');
        $('#description').summernote({
            dialogsInBody: true,
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, bufferText);
                    }, 10);
                }
            }
        });
    })
</script>
