<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/08/2017
 * Time: 15:52
 */
?>


{{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

<div class="modal fade" id="editTerms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Edit Terms and Conditions for an Event </h5>
            </div>
            <div class="modal-body">

                <form action="" method="post" id="editTermsForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <div class="form-group">
                            <label > Description : </label>
                            <div >
                                <textarea name="description" id="description"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Status: </label>
                            <div >
                              <select id="status" name="status" class="form-control ">
                               <option value="active">Active</option>
                               <option value="deactivate">Deactivate</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div >
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update </button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>
    </div>
</div>

<script>
    $('#editTerms').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#editTermsForm').attr('action', button.data('url'));
        // $('#name').val(button.data('name'));
        $('#description').val(button.data('desc'));
        $('#status').val(button.data('status'));
        // $('#price').val(button.data('price'));

        // for ( var j=0; j < sel.length; j++ ) {
        //     sel[j].selected = false;
        // }
        //
        // for (index = 0; index < del.length; ++index) {
        //     for (var i = 0; i < sel.options.length; i++) {
        //         console.log(sel.options[i].value); console.log(del[index]);
        //         if ( del[index] == sel.options[i].value ) {
        //             sel.options[i].selected = true;
        //         }
        //     }
        // }
        //$(".select2-multiple-2").select2('destroy');
        //$(".select2-multiple-2").select2({
        //    placeholder: "select one or more entries",
        //    width: null
        //});

        // $('.select2-multiple-2').selectpicker({
        //     iconBase: 'fa',
        //     tickIcon: 'fa-check'
        // });
        //
        // $('.select2-multiple-2').selectpicker('render');
        $('#description').eq(0).summernote('destroy');
        $('#description').summernote({
            dialogsInBody: true,
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, bufferText);
                    }, 10);
                }
            }
        });
    })
</script>
