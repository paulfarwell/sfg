<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/08/2017
 * Time: 12:40
 */
?>

<div class="modal fade" id="editActivities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Edit Activity Information for an Event </h5>
            </div>
            <div class="modal-body">

                <form action="" method="post" id="editActivitiesForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <div class="form-group">
                            <label > Delegate Type : </label>
                            <div >
                                <select class="form-control validate[required] select2-multiple-2" id="delegate_id" name="delegate_id[]" id="multiple" multiple>
                                    <option value="">Select...</option>
                                    @foreach( $delegate_types as $list )
                                        <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label> Name : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="name" id="name" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Location : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="location" id="location" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Cost : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="cost" id="cost" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Description: </label>
                            <div >
                                <textarea name="description" class="form-control validate[required]" id="description"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label> Start Date</label>
                            <div class='input-group date' id='start3'>
                                <input type='text' name="valid_from" class="form-control validate[required]" />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label> End Date</label>
                            <div class='input-group date' id='end3'>
                                <input type='text' name="valid_to" class="form-control validate[required]" />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div >
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update </button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>
    </div>
</div>

{{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

<script>

    $.fn.select2.defaults.set("theme", "bootstrap");
    //var placeholder = "Select a State";
    $(".select2, .select2-multiple").select2({
        placeholder: "select one or more entries",
        width: null
    });

    $('#editActivities').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#editActivitiesForm').attr('action', button.data('url'));
        var del = button.data('delegate').toString().split(","); console.log(del);
        var sel =  document.getElementById('delegate_id');
        $('#name').val(button.data('name'));
        $('#location').val(button.data('location'));
        $('#cost').val(button.data('cost'));
        $('#description').val(button.data('desc'));

        for ( var j=0; j < sel.length; j++ ) {
            sel[j].selected = false;
        }

        for (index = 0; index < del.length; ++index) {
            for (var i = 0; i < sel.options.length; i++) {
                console.log(sel.options[i].value); console.log(del[index]);
                if ( del[index] == sel.options[i].value ) {
                    sel.options[i].selected = true;
                }
            }
        }

        $('.select2-multiple-2').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
        $('.select2-multiple-2').selectpicker('render');

        $(function () {
            $('#start3').datetimepicker( {
                defaultDate: button.data('start_date'),
                useCurrent: false,sideBySide: true
            });
            $('#end3').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true,
                defaultDate: button.data('end_date')
            });
            $("#start3").on("dp.change", function (e) {
                $('#end3').data("DateTimePicker").minDate(e.date);
            });
            $("#end3").on("dp.change", function (e) {
                $('#start3').data("DateTimePicker").maxDate(e.date);
            });
        });
        $('#description').eq(0).summernote('destroy');
        $('#description').summernote({
            dialogsInBody: true,
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, bufferText);
                    }, 10);
                }
            }
        });
    })
</script>
