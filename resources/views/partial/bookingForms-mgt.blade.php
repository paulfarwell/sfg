<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/08/2017
 * Time: 15:52
 */
?>


{{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

<div class="modal fade" id="editbookingForms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Edit Booking Form Information for an Event </h5>
            </div>
            <div class="modal-body">

                <form action="" method="post" id="editbookingForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">


                        <div class="form-group">
                            <label>  Upload Section Text : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="upload_section" id="upload_section" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Hotels : </label>
                            <div class="col-md-8" >
                                <label class="edit_hotels"><input type="checkbox" id="edit_hotels" name="edit_hotels" value="">
                                 <input type="hidden" name="edit_hotel" value="" id="edit_hotel">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Tents : </label>
                            <div class="col-md-8" >
                              <label class="edit_tents">  <input type="checkbox" id="edit_tents" name="edit_tents" value="">
                                <input type="hidden" name="edit_tent" value="" id="edit_tent">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Flights : </label>
                            <div class="col-md-8" >
                              <label class="edit_flights">  <input type="checkbox" id="edit_flights" name="edit_flights" value="">
                                <input type="hidden" name="edit_flight" value="" id="edit_flight">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Transfers : </label>
                            <div class="col-md-8" >
                              <label class="edit_transfers">  <input type="checkbox" id="edit_transfers" name="edit_transfers" value="">
                                <input type="hidden" name="edit_transfer" value="" id="edit_transfer">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Activities : </label>
                            <div class="col-md-8" >
                              <label class="edit_activities">  <input type="checkbox" id="edit_activities" name="edit_activities" value="">
                                <input type="hidden" name="edit_activity" value="" id="edit_activity">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Custom Fields : </label>
                            <div class="col-md-8" >
                              <label class="edit_custom_fields">  <input type="checkbox" id="edit_custom_fields" name="edit_custom_fields" value="">
                                <input type="hidden" name="edit_custom_field" value="" id="edit_custom_field">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Media : </label>
                            <div class="col-md-8" >
                              <label class="edit_medias">  <input type="checkbox" id="edit_medias" name="edit_medias" value="">
                                <input type="hidden" name="edit_media" value="" id="edit_media">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Terms Conditions : </label>
                            <div class="col-md-8" >
                              <label class="edit_terms_conditions">  <input type="checkbox" id="edit_terms_conditions" name="edit_terms_conditions" value="">
                                <input type="hidden" name="edit_term_condition" value="" id="edit_term_condition">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Health Insurance : </label>
                            <div class="col-md-8" >
                              <label class="edit_health_insurances">  <input type="checkbox" id="edit_health_insurances" name="edit_health_insurances" value="">
                                <input type="hidden" name="edit_health_insurance" value="" id="edit_health_insurance">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Emergency Contacts : </label>
                            <div class="col-md-8" >
                              <label class="edit_emergency_contacts">  <input type="checkbox" id="edit_emergency_contacts" name="edit_emergency_contacts" value="">
                                <input type="hidden" name="edit_emergency_contact" value="" id="edit_emergency_contact">
                            </div>
                        </div>

                        <div class="form-group">
                            <div >
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update </button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>
    </div>
</div>


<script>
    $('#editbookingForms').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#editbookingForm').attr('action', button.data('url'));
        $('#upload_section').val(button.data('upload_section'));
        if(button.data('hotels') == 1){
        $('input:checkbox[name="edit_hotels"]').prop("checked", true);
        $('input[name="edit_hotel"]').val(button.data('hotels'));
        }
        if(button.data('tents') == 1){
        $('input:checkbox[name="edit_tents"]').prop("checked", true);
          $('input[name="edit_tent"]').val(button.data('tents'));
        }
        if(button.data('flights') == 1){
        $('input:checkbox[name="edit_flights"]').prop("checked", true);
          $('input[name="edit_flight"]').val(button.data('flights'));
        }
        if(button.data('transfers') == 1){
        $('input:checkbox[name="edit_transfers"]').prop("checked", true);
          $('input[name="edit_transfer"]').val(button.data('transfers'));
        }
        if(button.data('activities') == 1){
        $('input:checkbox[name="edit_activities"]').prop("checked", true);
          $('input[name="edit_activity"]').val(button.data('activities'));
        }
        if(button.data('custom_fields') == 1){
        $('input:checkbox[name="edit_custom_fields"]').prop("checked", true);
          $('input[name="edit_custom_field"]').val(button.data('custom_fields'));
        }
        if(button.data('medias') == 1){
        $('input:checkbox[name="edit_medias"]').prop("checked", true);
          $('input[name="edit_media"]').val(button.data('medias'));
        }
        if(button.data('terms_conditions') == 1){
        $('input:checkbox[name="edit_terms_conditions"]').prop("checked", true);
          $('input[name="edit_term_condition"]').val(button.data('terms_conditions'));
        }
        if(button.data('health_insurances') == 1){
        $('input:checkbox[name="edit_health_insurances"]').prop("checked", true);
          $('input[name="edit_health_insurance"]').val(button.data('health_insurances'));
        }
        if(button.data('emergency_contacts') == 1){
        $('input:checkbox[name="edit_emergency_contacts"]').prop("checked", true);
          $('input[name="edit_emergency_contact"]').val(button.data('emergency_contacts'));
        }



    });
</script>
<script>
document.getElementById('edit_hotels').addEventListener('change', function () {

       var hotels =document.getElementById("edit_hotels").checked;
       if(hotels == true){
         $('#edit_hotel').val('1');
       }else{
         $('#edit_hotel').val('');
       }
  });
  document.getElementById('edit_tents').addEventListener('change', function () {

           var tents =document.getElementById("edit_tents").checked;
           if(tents == true){
             $('#edit_tent').val('1');
           }else{
             $('#edit_tent').val('');
           }
      });
document.getElementById('edit_flights').addEventListener('change', function () {

       var flights =document.getElementById("edit_flights").checked;
       if(flights == true){
         $('#edit_flight').val('1');
       }else{
         $('#edit_flight').val('');
       }
});
document.getElementById('edit_transfers').addEventListener('change', function () {

       var transfers =document.getElementById("edit_transfers").checked;
       if(transfers == true){
         $('#edit_transfer').val('1');
       }else{
         $('#edit_transfer').val('');
       }
});
document.getElementById('edit_activities').addEventListener('change', function () {

       var activities =document.getElementById("edit_activities").checked;
       if(activities == true){
         $('#edit_activity').val('1');
       }else{
         $('#edit_activity').val('');
       }
});
document.getElementById('edit_custom_fields').addEventListener('change', function () {

       var custom_fields =document.getElementById("edit_custom_fields").checked;
       if(custom_fields == true){
         $('#edit_custom_field').val('1');
       }else{
         $('#edit_custom_field').val('');
       }
});
document.getElementById('edit_medias').addEventListener('change', function () {

       var medias =document.getElementById("edit_medias").checked;
       if(medias == true){
         $('#edit_media').val('1');
       }else{
         $('#edit_media').val('');
       }
});
document.getElementById('edit_terms_conditions').addEventListener('change', function () {

       var terms_conditions =document.getElementById("edit_terms_conditions").checked;
       if(terms_conditions == true){
         $('#edit_term_condition').val('1');
       }else{
         $('#edit_term_condition').val('');
       }
});
document.getElementById('edit_health_insurances').addEventListener('change', function () {

       var health_insurances =document.getElementById("edit_health_insurances").checked;
       if(health_insurances == true){
         $('#edit_health_insurance').val('1');
       }else{
         $('#edit_health_insurance').val('');
       }
});
document.getElementById('edit_emergency_contacts').addEventListener('change', function () {

       var emergency_contacts =document.getElementById("edit_emergency_contacts").checked;
       if(emergency_contacts == true){
         $('#edit_emergency_contact').val('1');
       }else{
         $('#edit_emergency_contact').val('');
       }
});
</script>
