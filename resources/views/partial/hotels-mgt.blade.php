<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/08/2017
 * Time: 15:52
 */
?>


{{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

<div class="modal fade" id="editHotels" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 id="edit-header-txt" class="text-center"> Edit Hotel Information for an Event </h5>
            </div>
            <div class="modal-body">

                <form action="" method="post" id="editHotelsForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <div class="form-group">
                            <label > Delegate Type : </label>
                            <div >
                                <select class="form-control validate[required] select2-multiple-2" id="delegate_id" name="delegate_id[]" id="multiple" multiple>
                                    <option value="">Select...</option>
                                    @foreach( $delegate_types as $list )
                                        <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label> Name : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="name" id="name" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Contact person : </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="contact_person" id="contact_person" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Contact email : </label>
                            <div>
                                <input type="text" value="" class="form-control validate[required,custom[email]]" name="contact_email" id="contact_email" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Contact phone : </label>
                            <div>
                                <input type="text" value="" class="form-control validate[required]" name="contact_phone" id="contact_phone" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > website :      </label>
                            <div >
                                <input type="text" value="" class="form-control validate[required]" name="website" id="website" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Reference Code Details : </label>
                            <div >
                                <textarea name="coupon" id="coupon" class="form-control"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label > Custom Order : </label>
                            <div >
                                <input type="text" name="order_custom" id="order_custom" class="form-control validate[required,custom[number]]">
                            </div>
                        </div>


                        <div class="form-group">
                            <label > Description : </label>
                            <div >
                                <textarea name="description" id="description"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div >
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update </button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>
    </div>
</div>

<script>
    $('#editHotels').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#editHotelsForm').attr('action', button.data('url'));
        var del = button.data('delegate').toString().split(","); console.log(del);
        var sel =  document.getElementById('delegate_id');
        $('#name').val(button.data('name'));
        $('#contact_person').val(button.data('person'));
        $('#contact_phone').val(button.data('phone'));
        $('#contact_email').val(button.data('email'));
        $('#website').val(button.data('website'));
        $('#description').val(button.data('desc'));
        $('#coupon').val(button.data('coupon'));
        $('#order_custom').val(button.data('order_custom'));

        for ( var j=0; j < sel.length; j++ ) {
            sel[j].selected = false;
        }

        for (index = 0; index < del.length; ++index) {
            for (var i = 0; i < sel.options.length; i++) {
                console.log(sel.options[i].value); console.log(del[index]);
                if ( del[index] == sel.options[i].value ) {
                    sel.options[i].selected = true;
                }
            }
        }
        //$(".select2-multiple-2").select2('destroy');
        //$(".select2-multiple-2").select2({
        //    placeholder: "select one or more entries",
        //    width: null
        //});

        $('.select2-multiple-2').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });

        $('.select2-multiple-2').selectpicker('render');
        $('#description').eq(0).summernote('destroy');
        $('#description').summernote({
            dialogsInBody: true,
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, bufferText);
                    }, 10);
                }
            }
        });
    })
</script>
