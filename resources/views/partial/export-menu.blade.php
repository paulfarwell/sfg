<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 22/08/2017
 * Time: 13:13
 */
?>

<div class="btn-group  ">
    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
        <i class="fa fa-share"></i>
        <span class="hidden-xs"> Export Tools </span>
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-right export-tools" id="export-tools">
        <li>
            <a href="javascript:;" data-action="0" class="tool-action">
                <i class="icon-printer"></i> Print</a>
        </li>
        <li>
            <a href="javascript:;" data-action="1" class="tool-action">
                <i class="icon-check"></i> Copy</a>
        </li>
        <li>
            <a href="javascript:;" data-action="2" class="tool-action">
                <i class="icon-doc"></i> PDF</a>
        </li>
        <li>
            <a href="javascript:;" data-action="3" class="tool-action">
                <i class="icon-paper-clip"></i> Excel</a>
        </li>
        <li>
            <a href="javascript:;" data-action="4" class="tool-action">
                <i class="icon-cloud-upload"></i> CSV</a>
        </li>
        <li class="divider"> </li>
        <li>
            <a href="javascript:;" data-action="5" class="tool-action">
                <i class="icon-refresh"></i> Reload</a>
        </li>
        </li>
    </ul>
</div>
