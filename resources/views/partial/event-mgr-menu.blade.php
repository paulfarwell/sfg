<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 09/08/2017
 * Time: 09:47
 */
?>

<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-exchange"></i> Delegations </a>
<a href="{{ route('org.events') }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> All Events </a>

<div class="btn-group">
    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Manage Event
        <i class="fa fa-angle-down"></i>
    </button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>
            <a href="{{ route('org.view.overview',$event->id) }}"> Event Summary </a>
        </li>
        <li>
            <a href="{{ route('org.edit.event',$event->id) }}"> Event Edit </a>
        </li>
        <li>
            <a href="{{ route('org.edit.event.meta',$event->id) }}"> Event Social Info </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.prog',$event->id) }}"> Event Programme </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.tickets',$event->id ) }}"> Event Tickets </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.images',$event->id) }}"> Event Images </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.hotels',$event->id) }}"> Event Hotels </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.activities',$event->id) }}"> Event Activities </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.transfer',$event->id) }}"> Event Transfer Packages </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.sponsors',$event->id) }}"> Event Sponsors </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.organizers',$event->id) }}"> Event Organizers </a>
        </li>
        <li>
            <a href="{{ route('org.manage.event.news',$event->id) }}"> Event News </a>
        </li>
        <li>
            <a href="{{route('org.events.export.database', $event->id)}}"> Delegate Database </a>
        </li>
    </ul>
</div>
