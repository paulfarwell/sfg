<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 17/07/2017
 * Time: 13:33
 */
?>

<!-- THIS EMAIL WAS BUILT AND TESTED WITH LITMUS http://litmus.com -->
<!-- IT WAS RELEASED UNDER THE MIT LICENSE https://opensource.org/licenses/MIT -->
<!-- QUESTIONS? TWEET US @LITMUSAPP -->
<!DOCTYPE html>
<html>
<head>
    <title>User Account </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* FONTS */
        @media screen {
            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato-Bold';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { -ms-interpolation-mode: bicubic; }

        /* RESET STYLES */
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
        table { border-collapse: collapse !important; }
        body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px){
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] { margin: 0 !important; }
    </style>
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="white" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" >
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="#" target="_blank">
                            <img alt="Logo" src="{{asset('assets/frontend/corporate/img/logos/logo-2.jpg')}}" style="display: block;font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" >

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;border-top: 2px #f4f4f4 solid;" >
                <!-- COPY -->

                <tr>

                    <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;" >
                        <p> <strong> Dear {{isset($delegations->primaryprofile) && $delegations->primaryprofile->title !='' ?  $delegations->primaryprofile->title : '' }} {{ ucwords($delegations->contact_name) }},</strong> </p>

                        <p>Thank you for your interest in the {{ ucwords(strtolower($event->name)) }} planned for {{ date("d",strtotime($event->start)) }} - {{ date("d",strtotime($event->end)) }} {{ date("F",strtotime($event->end)) }}, in {{$event->venue}}.  The Registration website is now open and an account has been created for you to register.  We recommend that you complete your registration as soon as possible as spaces for the Summit are limited, as is accommodation.
                        </p>

                        <p>
                            Please find attached your Information & Registration Guidelines providing comprehensive information on the registration procedure, accommodation, travel, etc.
                        </p>

                        <p> <strong> Registration Website:</strong> Click <a href="{{ route('org.view.event',$event->slug)}}"> here </a> to access the registration website or copy and paste the following link in your browser: {{ route('org.view.event',$event->slug)}}
                        </p>

                        <p>
                            <strong>Attendance Categories:</strong> Your delegation has been allocated the Attendance Categories listed below.  Details of applicable personnel, access allowed and relevant attendance category price may be found in the Delegation Information & Registration Guidelines document each badge category listed.  Please note that each attendance category allows access for one person only and is not transferable.
                        </p>

                        <p>
                            Attendance category available for your delegation include:
                        <ul>
                            {{--<li>Two (2) delegates badge: Green (note: one of these must be the Head of State) </li>--}}
                            {{--<li>Three (3) delegate badges: Green & White  </li>--}}
                            {{--<li>Two (2) delegates badges: Blue </li>--}}
                            {{--<li>One (1) delegate badges: Orange </li>--}}
                            @foreach( $count as $key => $list)
                                <li> ({{ $list }}) {{ $key }} </li>
                            @endforeach
                        </ul>
                        </p>

                        <p>
                            If you wish to change the Attendance Category / Badge colour of any of your delegation members or if you wish to add additional Attendance Categories / badges for support team members in the White badge category (see Delegation Information & Registration Guidelines for details), please contact the organisers to issue appropriate registration codes.
                        </p>

                        <p>
                            <strong>Login details:</strong> Below are login details for each individual according to badge colour.  Each individual will need to be registered against the specific registration code provided.  Please ensure you utilise the correct registration code for the relevant personnel as registrations will be rejected if not correctly allocated.
                        </p>

                        <p>
                        To log you will be prompted to input the credentials:
                        Please note that you will be requested to reset each password at first access for confidentiality.
                        </p>

                        <p>
                        @foreach( $delegations->invitations as $list)
                            <ul>
                                <li><strong>Unique registration code:</strong> {{ $list->code }}
                                <li><strong>Name:</strong> {{ $list->profile->fname }} {{ $list->profile->surname }}
                                <li><strong>Username:</strong> {{ $list->profile->email }}
                                <li><strong>Password:</strong> {{ $list->code }}
                                <li><strong>Badge:</strong> {{ $list->delegateType->color }}
                                <li><strong>Category:</strong> {{ strtoupper($list->delegateType->name)  }}
                            </ul>
                        @endforeach
                        </p>

                        <p> </p>

                        <p>
                            <strong>Accommodation:</strong> Please note that Attendance Categories exclude accommodation.Please note that you accommodation bookings and payments are to be made directly with your hotel.  Information on accommodation available is provided in the Delegation Information & Registration Guidelines.
                        </p>

                        <p>
                            Any additional rooms for support staff must be made at alternative hotels as indicated in the Guidelines.
                        </p>

                        <p>
                            @if(isset($msg))
                                {!! $msg !!}
                            @endif
                        </p>

                        <p>
                            For further information, kindly visit the Registration website or contact the Giants Club Summit Registration Coordinator on summit@thegiantsclub.org.
                        </p>

                    </td>
                </tr>

                <tr>
                    <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;" >
                        <p style="margin: 0;">Yours sincerely,<br>The Giants Club Team </p>
                    </td>
                </tr>

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- SUPPORT CALLOUT -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 30px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" >
                <!-- HEADLINE -->
                <tr>
                    <td bgcolor="#FFECD1" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;" >
                        <h2 style="font-size: 20px; font-weight: 400; color: #111111; margin: 0;">Need any help?</h2>
                        <p style="margin: 0;"><a href="mailto:summit@thegiantsclub.org?Subject=Help%20on%20user%20Account%20access" target="_blank" style="color: #947C4E;">We&rsquo;re here, ready to talk</a></p>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


</table>

<!-- LITMUS ATTRIBUTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" >
                <tr>
                    <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; color: #666666; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;" >
                        <p style="font-size: 10px; line-height: 20px;"> </p>
                        <p style="font-size: 10px; line-height: 20px;"> WWW.SPACEFORGIANTS.ORG | GIANTSCLUB@SPACEFORGIANTS.ORG | +254 (0) 20 800 2975</p>
                        <!-- <p style="font-size: 12px; line-height: 20px;"> Space for Giants is an international conservation charity, registered in the UK (charity no: 1139771), USA (EIN: 47-1805681) and Kenya, governed by a voluntary Board of Trustees</p> -->
                        <p style="font-size: 10px; line-height: 20px;"> Copyright © <?php echo date('Y'); ?> Space for Giants. All rights reserved.</p>

                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>
<!-- END LITMUS ATTRIBUTION -->

</body>
</html>
