$name= $delegate_profile->fname.' '.$delegate_profile->surname;
$eventdate = date("d",strtotime($event->start)). '-'. date("d",strtotime($event->end)).' '.date("F",strtotime($event->end));
$invitation_information = '<ul><li>Name:'.$delegate_profile->fname.' '.$delegate_profile->surname.'</li>
                            <li>Unique registration code: '. $invitation->code.'</li>
                            <li>Username: '. $delegate_profile->email.'</li>
                            <li>Password: '. $invitation->code.'</li>
                            <li>Badge: '. $invitation->delegateType->color.'</li>
                            <li>Category: '.$invitation->delegateType->name.'</li><ul>';
 $attendance_category = '<ul><li> '.$invitation->delegateType->name .' </li></ul>';
 $link = "<a href='{{ route('org.view.event',$event->slug)}}' target='_blank'style='font-size: 20px; font-family: 'Lato-Bold'; color: #045061; text-decoration: none;  text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #ffd52d; display: inline-block;'> Registration Website </a>";
$content = EmailTemplate::find($request->get('emailtemplate'));
$replace = array('{name}','{eventname}','{eventdate}','{eventlocation}','{eventlink}','{attendance_category}','{invitation_information}');
$new = array($name, $event->name,$eventdate,$event->venue, $link, $attendance_category,$invitation_information );

$content = str_replace($replace, $new, $content->content);

// $data = ['delegations' => $delegation ,'event'=>$delegation->event,'invitation'=>$invitation->profile,'count'=>$count,'invitations'=>$invitation];
$data = [
 'logo' => $event->logo,
 'content'=> $content,
];
