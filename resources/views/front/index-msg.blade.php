<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 24/07/2017
 * Time: 23:41
 */
?>

@extends('layouts.front-v-ii')

@section('slider')
    <!-- Indicators -->
    {{--<ol class="carousel-indicators carousel-indicators-frontend">--}}
    {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
    {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
    {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
    {{--</ol>--}}
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php
        $first = collect($event->poster)->first();
        $i = 0;
        ?>
        @foreach( $event->poster as $ls)
            <div class="item @if ($i == 0) active @endif " style="background: linear-gradient( rgba(0, 0, 0, 0.70), rgba(0, 0, 0, 0.45) ), url({{ asset($ls->image_path) }});
                    background-size: cover;
                    background-position: center center;">
                <div class="container">
                    <div class="carousel-position-six" align="center">
                        <h2 class="animate-delay carousel-title-v6 text-uppercase" data-animation="animated fadeInDown">
                            {{ $event->name }}
                        </h2>
                        <p>"{!! $event->theme !!}" </p>
                    </div>
                </div>
            </div>
            <?php $i++; ?>
        @endforeach
    </div>
    <!-- Controls -->
    <a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
    </a>
    <a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </a>

    {{--<div class="home-down-arrow" align="center" style="position: relative;--}}
    {{--margin-top: -150px;"> <a href="#content" style="  font-size: 130px;  color: white;">--}}
            {{--<!-- Expand Arrow icon by Icons8 -->--}}
            {{--<img class="icon icons8-Expand-Arrow" width="64" height="64" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACgElEQVR4Xu2Yz67NUBjF10I8juQ+gaEJEiQkwgATV4grMWLMTBCCCQZEQoIEE0MvYHLfRrhLtrTJSe22u+3+d9Ovw3N2T7/161prn5ZY+cGV64cBMAesnIBFYOUGsBK0CFgEVk7AIrByA9guYBGwCKycgEVg5QawXcAiMBYBSUdJ/hhbV+P3IbMPOkDSPQB3AGyTfFGjyL6ZJF0F8BzAfZJ3+9b1ApDkhDsA7bFvIGyIb2e/TfKBD4IXgKQtAD89J1wj6ahWe0jaBvDMM+ARkrvdz4cccBPAI88PXSb5ukYCki4BeOWZbYfk42AHtAsl+SAIwJXaIDTiXwL/be294p3O0W1wP0CYKz4IgFtUM4Ql4oMBjEC4QPJdiU6QdB7A26m235x1NAKbi3ucsAfgYm4Ijfg3AA504A9mPngXGPiD4SvGrBBiiZ8UgVqcEFP8bAADneCccJbkxxSdIOkMgA9LbT+7A7qiejrhD4BzsSE04t8DOLgk84s7oASEVOIXRSCgE6I4IaX4aAAGOsFBOEXy65xOkHQCwOfYto/WAYFx+A3g9FQIjfhPAA7FzHz0DkgBIZf4qBEI6IQgJ+QUnwzAQCc4CMdJfvd1gqRjAL6ltn2yDgiMwy8AJ7sQGvFfABxOmfnkHTAHQinxSSMQ0An/nNCsy37n2/kmPQ7P2cvbcyTtAHgY+Bu3SPreRwaeHr4sG4CBYuxOO+l5Plyqf2VWAAEQsorP1gGBxZhdfDEAjROuA3jSwLlB8ulSO885P3sEOruDe8GxR9I98BQ5igIoorhzUQNQw10oOYM5oCT9Gq5tDqjhLpScwRxQkn4N1zYH1HAXSs5gDihJv4Zr/wXj1xtQnsYwtQAAAABJRU5ErkJggg==">--}}
        {{--</a>--}}
    {{--</div>--}}

@endsection