<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 21:00
 */
?>

@extends('layouts.front-v')

@section('slider')

    <!-- <div class="title-wrapper">
        <div class="container">
            <div class="container-inner" align="center">
                <h1 style="font-size: 30px; font-weight: 500; line-height: 1.4; color: white;" class="impact-normal">SPONSORS OF {{ $event->name }}</h1>
                <em class="perpetua-std" style="font-size: 20px;   font-weight: normal;   line-height: 1.4; color: white;">
                    We are extremely grateful to all those who have generously donated to Space for Giants.
                    Each one of you has helped us to get one step closer to achieving our mission. Thank You.
                </em>
            </div>
        </div>
    </div> -->

    {{--<ol class="carousel-indicators carousel-indicators-frontend">--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
    {{--</ol>--}}
    <!-- Wrapper for slides -->
    <div id="carousel-example-generic" class="carousel slide carousel-slider" data-interval="10000" >
    <div class="carousel-inner" role="listbox">
        <?php
            $first = collect($event->poster)->first();
            $i = 0;
        ?>

        <form action="{{ route('del.validate.code',[$event->id])  }}" class="form form-horizontal login-form col-md-4 col-xs-12 col-sm-6 col-lg-4 pull-right invite-validate" method="post" id="validatation" >
            <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
            <div class="form-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="code" class="form-input round-input validate[required] form-control invite-field-validate"
                               placeholder="{{__('trans.Please enter your invitation code here') }}" id="ufocus">
                        <span class="input-group-btn">
                            <!-- <button id="genpassword" type="submit" class="btn round-button" > Validate </button> -->
                             <button type="submit" class="btn round-button" >{{__('trans.Validate') }}  </button>
                                </span>
                    </div>
                </div>
            </div>
        </form>
        @foreach( $event->poster as $ls)
                <div class="item @if ($i == 0) active @endif " style="background: linear-gradient( rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.45) ), url({{ asset($ls->image_path) }});
                    background-size: cover;
                    background-position: center center;">
                    <div class="container">
                        <div class="carousel-position-six" align="center">
                            <h2 class="animate-delay carousel-title-v6 text-uppercase impact-normal-B" data-animation="animated fadeInDown" style="font-family:Lato-Light">
                              @php
                                $title = $event->name ;
                                $words = explode(' ',$title);
                                  @endphp

                                  <!-- {{implode(' ',array_slice($words,0,4))}} -->
                                  <span style="font-family:Lato-Black">{{__('trans.Partners of') }} {{ $title}}</span>

                               <!-- {{ $event->name }} -->
                            </h2>
                            <p class="impact-normal">{{__('trans.We are extremely grateful to all partners') }}</p>

                        </div>
                    </div>
                </div>
               <?php $i++; ?>
        @endforeach

    </div>
    <!-- Controls -->
    <a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
    </a>
    <a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </a>
    <div class="home-down-arrow" align="center" style="position: relative;
      margin-top: -150px;"> <a href="#content" style="  font-size: 130px;  color: white;">
              <!-- Expand Arrow icon by Icons8 -->
              <img class="icon icons8-Expand-Arrow" width="64" height="64" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACgElEQVR4Xu2Yz67NUBjF10I8juQ+gaEJEiQkwgATV4grMWLMTBCCCQZEQoIEE0MvYHLfRrhLtrTJSe22u+3+d9Ovw3N2T7/161prn5ZY+cGV64cBMAesnIBFYOUGsBK0CFgEVk7AIrByA9guYBGwCKycgEVg5QawXcAiMBYBSUdJ/hhbV+P3IbMPOkDSPQB3AGyTfFGjyL6ZJF0F8BzAfZJ3+9b1ApDkhDsA7bFvIGyIb2e/TfKBD4IXgKQtAD89J1wj6ahWe0jaBvDMM+ARkrvdz4cccBPAI88PXSb5ukYCki4BeOWZbYfk42AHtAsl+SAIwJXaIDTiXwL/be294p3O0W1wP0CYKz4IgFtUM4Ql4oMBjEC4QPJdiU6QdB7A26m235x1NAKbi3ucsAfgYm4Ijfg3AA504A9mPngXGPiD4SvGrBBiiZ8UgVqcEFP8bAADneCccJbkxxSdIOkMgA9LbT+7A7qiejrhD4BzsSE04t8DOLgk84s7oASEVOIXRSCgE6I4IaX4aAAGOsFBOEXy65xOkHQCwOfYto/WAYFx+A3g9FQIjfhPAA7FzHz0DkgBIZf4qBEI6IQgJ+QUnwzAQCc4CMdJfvd1gqRjAL6ltn2yDgiMwy8AJ7sQGvFfABxOmfnkHTAHQinxSSMQ0An/nNCsy37n2/kmPQ7P2cvbcyTtAHgY+Bu3SPreRwaeHr4sG4CBYuxOO+l5Plyqf2VWAAEQsorP1gGBxZhdfDEAjROuA3jSwLlB8ulSO885P3sEOruDe8GxR9I98BQ5igIoorhzUQNQw10oOYM5oCT9Gq5tDqjhLpScwRxQkn4N1zYH1HAXSs5gDihJv4Zr/wXj1xtQnsYwtQAAAABJRU5ErkJggg==">
          </a>
      </div>

    </div>
@endsection

@section('content')

    <div class="row-fluid  margin-bottom-40">

        @if ($event->sponsor->count() > 0 )
          <div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
              <div class="row card">
        @foreach( $event->sponsor as $lst)
              @if(empty($list->brief))
                <div class="col-md-2 col-sm-4 col-md-offset-1">
              <img src="{{ asset($lst->logo) }}" class="img-responsive" alt="Avatar" >
              <br>
              <a href="{!! $lst->website !!}" target="_blank" style="text-decoration: none ;font-size:14px;font-weight: bold;" > {{__('trans.Visit Website')}} </a>
            </div>
              @else
                <div class="col-md-4 col-sm-4">
                    <img src="{{ asset($lst->logo) }}" class="img-responsive" alt="Avatar" >
                    <br>
                    <a href="{!! $lst->website !!}" target="_blank" style="text-decoration: none ;font-size:14px;font-weight: bold;" >  {{__('trans.Visit Website')}} </a>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="details" style="text-align: left; padding-bottom: 30px;padding-left: 5px;padding-right: 5px;">
                        <p>{!! $lst->brief  !!}</p>

                    </div>
                </div>
                @endif



        @endforeach
        </div>
         </div>
            @else

            <style>
                #content- {
                    display: block;
                    position: relative;
                    top: 50%;
                    transform: translateY(-50%);
                }
            </style>

            <div style="height:310px;text-align: center;" class="col-md-12">

                <div id="content">
                    <h4 align="center"> Coming Soon...</h4>
                    <br>
                    <img class="img-responsive" style="margin: auto;" src="{{ asset('assets/img/no-sponsor.jpg') }}">
                </div>

            </div>

        @endif


    </div>


@endsection

@section('footer')



@endsection
