<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 21:00
 */
?>

@extends('layouts.front-v')

@section('slider')

    <style>
        .faq-tabbable, .faq-tabbable li a {
            border-left: none;
            border-bottom: none;
            color: #045061;
            font-family:'Lato-Bold';
        }

        .faq-tabbable li a {
            color: #045061;
        }

        .faq-tabbable li:hover a, .faq-tabbable li.active a {
            background: transparent;
            color: #045061;
            font-weight: bold;
            border-bottom: solid 2px #045061;
        }

        .faq-tabbable li {
            position: relative;
            margin-bottom: 5px;
            background: transparent;
            border: none;
        }

        .faq-tabbable li a {
            background: transparent;
            border-bottom: solid 1px #045061;
        }

        .faq-tabbable li a i {
           margin-left: 20px;
            margin-right: 10px;
        }


          .faq-tabbable li.active a {
    background: transparent;
    color: #84724C;
    font-weight: bold;
    border-bottom: solid 2px #84724C;

        }

        .faq-tabbable li.active:after {
            content: '';
            display: inline-block;
            border-bottom: 6px solid transparent;
            border-top: 6px solid transparent;
            border-left: 6px solid transparent;
            position: absolute;
            top: 16px;
            right: -5px;
        }

        .tab-pane strong , .tab-pane h3 {
            color: #84724C;
            font-family:'Lato-Bold';
        }



    </style>

    <!-- <div class="title-wrapper">
        <div class="container">
            <div class="container-inner" align="center">
                <h1 style="font-size: 30px; font-weight: 500; line-height: 1.4; color: white;" class="impact-normal"> ESSENTIAL INFORMATION </h1>
                <em class="perpetua-std"  style="font-size: 20px;   font-weight: normal;   line-height: 1.4; color: white;"> A general information guide on the {{ ucwords(strtolower($event->name))  }}.</em>
            </div>
        </div>
    </div> -->
    <!-- Indicators -->
    {{--<ol class="carousel-indicators carousel-indicators-frontend">--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
    {{--</ol>--}}
    <!-- Wrapper for slides -->
    <div id="carousel-example-generic" class="carousel slide carousel-slider" data-interval="10000" >
    <div class="carousel-inner" role="listbox">
        <?php
            $first = collect($event->poster)->first();
            $i = 0;
        ?>

        <form action="{{ route('del.validate.code',[$event->id])  }}" class="form form-horizontal login-form col-md-4 col-xs-12 col-sm-6 col-lg-4 pull-right invite-validate" method="post" id="validatation" >
            <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
            <div class="form-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="code" class="form-input round-input validate[required] form-control invite-field-validate"
                               placeholder="{{__('trans.Please enter your invitation code here') }}" id="ufocus">
                        <span class="input-group-btn">
                            <!-- <button id="genpassword" type="submit" class="btn round-button" > Validate </button> -->
                             <button type="submit" class="btn round-button" >{{__('trans.Validate') }}  </button>
                                </span>
                    </div>
                </div>
            </div>
        </form>
        @foreach( $event->poster as $ls)
                <div class="item @if ($i == 0) active @endif " style="background: linear-gradient( rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.45) ), url({{ asset($ls->image_path) }});
                    background-size: cover;
                    background-position: center center;">
                    <div class="container">
                        <div class="carousel-position-six" align="center">
                            <h2 class="animate-delay carousel-title-v6 text-uppercase impact-normal-B" data-animation="animated fadeInDown" style="font-family:Lato-Light">
                              @php
                                $title = 'ESSENTIAL INFORMATION';
                                $words = explode(' ',$title);
                                  @endphp

                                  {{__('trans.Essential') }}
                                  <span style="font-family:Lato-Black">{{__('trans.Information') }}</span>

                               <!-- {{ $event->name }} -->
                            </h2>
                            <p class="impact-normal">{{__('trans.A general information guide on the') }} {{ $event->name  }}</p>

                        </div>
                    </div>
                </div>
               <?php $i++; ?>
        @endforeach

    </div>
    <!-- Controls -->
    <a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
    </a>
    <a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </a>

    <div class="home-down-arrow" align="center" style="position: relative;
      margin-top: -150px;"> <a href="#content" style="  font-size: 130px;  color: white;">
              <!-- Expand Arrow icon by Icons8 -->
              <img class="icon icons8-Expand-Arrow" width="64" height="64" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACgElEQVR4Xu2Yz67NUBjF10I8juQ+gaEJEiQkwgATV4grMWLMTBCCCQZEQoIEE0MvYHLfRrhLtrTJSe22u+3+d9Ovw3N2T7/161prn5ZY+cGV64cBMAesnIBFYOUGsBK0CFgEVk7AIrByA9guYBGwCKycgEVg5QawXcAiMBYBSUdJ/hhbV+P3IbMPOkDSPQB3AGyTfFGjyL6ZJF0F8BzAfZJ3+9b1ApDkhDsA7bFvIGyIb2e/TfKBD4IXgKQtAD89J1wj6ahWe0jaBvDMM+ARkrvdz4cccBPAI88PXSb5ukYCki4BeOWZbYfk42AHtAsl+SAIwJXaIDTiXwL/be294p3O0W1wP0CYKz4IgFtUM4Ql4oMBjEC4QPJdiU6QdB7A26m235x1NAKbi3ucsAfgYm4Ijfg3AA504A9mPngXGPiD4SvGrBBiiZ8UgVqcEFP8bAADneCccJbkxxSdIOkMgA9LbT+7A7qiejrhD4BzsSE04t8DOLgk84s7oASEVOIXRSCgE6I4IaX4aAAGOsFBOEXy65xOkHQCwOfYto/WAYFx+A3g9FQIjfhPAA7FzHz0DkgBIZf4qBEI6IQgJ+QUnwzAQCc4CMdJfvd1gqRjAL6ltn2yDgiMwy8AJ7sQGvFfABxOmfnkHTAHQinxSSMQ0An/nNCsy37n2/kmPQ7P2cvbcyTtAHgY+Bu3SPreRwaeHr4sG4CBYuxOO+l5Plyqf2VWAAEQsorP1gGBxZhdfDEAjROuA3jSwLlB8ulSO885P3sEOruDe8GxR9I98BQ5igIoorhzUQNQw10oOYM5oCT9Gq5tDqjhLpScwRxQkn4N1zYH1HAXSs5gDihJv4Zr/wXj1xtQnsYwtQAAAABJRU5ErkJggg==">
          </a>
      </div>
    </div>


@endsection

@section('content')

    <div class="content-page">

        {{--<div class="row">--}}
            {{--<div class="col-md-3 col-sm-3">--}}
                {{--<ul class="tabbable faq-tabbable tab-sfg">--}}
                    {{--<li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> General Questions</a></li>--}}
                    {{--<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Membership</a></li>--}}
                    {{--<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Terms Of Service</a></li>--}}
                    {{--<li><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> License Terms</a></li>--}}
                    {{--<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Payment Rules</a></li>--}}
                    {{--<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-plus" aria-hidden="true"></i> Other Questions</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-md-9 col-sm-9">--}}
                {{--<div class="tab-content" style="padding:0; background: #fff;">--}}
                    {{--<!-- START TAB 1 -->--}}
                    {{--<div class="tab-pane active" id="tab_1"> 1 </div>--}}
                    {{--<div class="tab-pane " id="tab_2"> 2 </div>--}}
                    {{--<div class="tab-pane " id="tab_3"> 3 </div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">


                <div class="col-md-4 col-sm-4">

                    <ul class="tabbable faq-tabbable tab-sfg">
                        <?php  $i = 0;  ?>
                        @foreach( $event->newsFull->sortBy('order_custom') as $ls)
                            <li class="@if ($i == 0) active @endif"><a href="#news_{{$ls->id}}" data-toggle="tab"> <i class="fa fa-plus" aria-hidden="true"></i>  {{ $ls->category }}</a></li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="tab-content" style="font-size:18px; background: #fff;color: #999999;padding-left: 40px;font-family: "Helvetica neue",Helvetica,Arial,sans-serif !important;
                    direction: ltr;">
                        <?php    $i = 0;      ?>
                        @foreach( $event->newsFull->sortByDesc('order_custom') as $ls)
                            <div class="tab-pane @if ($i == 0) active @endif" id="news_{{$ls->id}}">
                                {!! Linkify::process($ls->text) !!}

                                @if ($ls->category == 'General Information')
                                    <h3> Map to Event Venue </h3>
                                    <div class="map_canvas" id="map_canvas" style="width: 100%; height: 400px;"></div>
                                    <input name="venue" type="hidden" value="{{ $event->venue or null}}" id="geocomplete" class="form-control validate[required]"  placeholder="">
                                    <input name="lat" type="hidden" value="{{  ((explode(',', $event->coordinates))[0]) or null}}" class="form-control validate[required]">
                                    <input name="lng" type="hidden" value="{{ ((explode(',', $event->coordinates))[1]) or null}}" class="form-control validate[required]">

                                @endif


                                @if ($ls->category == 'Programme')

                                    @foreach( $event->programmes as $ls)
                                        <div style="font-size: 18px;   font-weight: normal;   line-height: 1.4;">
                                            <h3 style="color: black !important;"> {{ $ls->title }} </h3>
                                            <p style="color: black !important;"> {!! $ls->description !!} </p>
                                        </div>
                                        <br>
                                    @endforeach

                                @endif

                                @if ($ls->category == 'Pre and Post Summit Trips')
                                <div class="row service-box margin-bottom-40">
                                   <p><strong><span style="color: #00aeef;">Pre Trips</span></strong></p>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Pre-Trip 1.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%"> AU UN Pre-Trip 1</a>
                                    </div>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Pre-Trip 2.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">AU UN Pre-Trip 2</a>
                                    </div>

                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Pre-Trip 3.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">AU UN Pre-Trip 3</a>
                                    </div>

                                </div>
                                <div class="row service-box margin-bottom-40">
                                    <p><strong><span style="color: #00aeef;">Post Trips</span></strong></p>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Post-Trip 1.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%"> AU UN Post-Trip 1</a>
                                    </div>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Post-Trip 2.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">AU UN Post-Trip 2</a>
                                    </div>

                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Post-Trip 3.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">AU UN Post-Trip 3</a>
                                    </div>

                                </div>
                                <div class="row service-box margin-bottom-40">

                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Post-Trip 4.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%"> AU UN Post-Trip 4</a>
                                    </div>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/AU UN Post-Trip 5.pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">AU UN Post-Trip 5</a>
                                    </div>

                                </div>

                                <div class="row service-box margin-bottom-40">
                                   <p><strong><span style="color: #00aeef;">2 Night Package</span></strong></p>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/2 Nights Hwange Package.pdf') }}" class="btn blue-button btn-large programme-button" style="width:61%"> 2 Nights Hwange Package</a>
                                    </div>
                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/2 Nights Botswana Package.pdf') }}" class="btn blue-button btn-large programme-button" style="width:61%">2 Nights Botswana Package</a>
                                    </div>


                                </div>
                                <div class="row service-box margin-bottom-40">
                                   <p><strong><span style="color: #00aeef;">Terms & Conditions</span></strong></p>

                                    <div class=" col-xs-12 col-sm-4 col-md-4" >
                                        <a href="{{ url('/uploads/pdf/CTS Terms & Conditions .pdf') }}" class="btn blue-button btn-large programme-button" style="width:60%">CTS Terms & Conditions </a>
                                    </div>

                                </div>
                                @endif

                            </div>



                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>


                <!-- <div class="col-md-4 col-sm-4">

                    <ul class="tabbable faq-tabbable tab-sfg">
                        <?php  $i = 0;  ?>
                        @foreach( $event->news->sortByDesc('order_custom') as $ls)
                            <li class="@if ($i == 0) active @endif"><a href="#news_{{$ls->id}}" data-toggle="tab"> <i class="fa fa-plus" aria-hidden="true"></i>  {{ $ls->category }}</a></li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="tab-content" style="padding:0; background: #fff;color:black;">
                        <?php    $i = 0;      ?>
                        @foreach( $event->news->sortByDesc('order_custom') as $ls)
                            <div class="tab-pane @if ($i == 0) active @endif" id="news_{{$ls->id}}">
                                {!! Linkify::process($ls->text) !!}
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div> -->



        </div>

    </div>


@endsection

@section('footer')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBCneBlloJz09T_BzXSZD3EHPDDtKWLhIc"></script>
    <script src="{{ asset("assets/geocomplete/jquery.geocomplete.js") }}"> </script>
<script>

    var query = location.href.split('#');
       if(query[1] == undefined){

         console.log('old');
       }else{
         var tabs =$('a[data-toggle="tab"]');
         var link = '#'+query[1];
         $.each( tabs, function( key, value ) {
           var tab = $(value);
           $(tab).parent().removeClass('active');
           if ($(tab).attr('href') == link){
             $(tab).parent().addClass('active');

            var tabpane =$('.tab-pane');
            $.each( tabpane, function( k, v ) {
              var pane = $(v);
              $(pane).removeClass('active');
               // console.log(pane);
                 if ($(pane).attr('id') == query[1]){
                   $(pane).addClass('active');
                 }
            });
         }
        });


       }
// .attr('href')

    // document.cookies = 'anchor=' + query[1];

    </script>
    <script>
//        $('.activities').slick({
//            prevArrow: '<i class="fa fa-arrow-left fa-6 btn-prev " style="font-size: 36px;color: black;" aria-hidden="true" aria-label="Previous" ></i>',
//            nextArrow:'<i class="fa fa-arrow-right fa-6 btn-next " style="font-size: 36px;color: black;" aria-hidden="true" aria-label="Next" ></i>',
//            infinite: true,//centerMode: true,
//            //speed: 300,
//            arrows: true,
//            slidesToShow: 1,slidesToScroll: 1,
//            //adaptiveHeight: true
//        });
//        $('.activities').get(0).slick.setPosition()

// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
// var target = $(e.target).attr("href") // activated tab
// alert(target);
// });

        //
//                                     $(function(){
        {{--$("#geocomplete").geocomplete({--}}
            {{--map: ".map_canvas",--}}
            {{--details: "form",--}}
            {{--markerOptions: {--}}
                {{--draggable: true--}}
            {{--},--}}
            {{--location: "{{ $event->venue }}",--}}
            {{--types: ["geocode", "establishment"],--}}
        {{--});--}}

        {{--$("#geocomplete").bind("geocode:dragged", function(event, latLng){--}}
            {{--console.log(latLng);--}}
            {{--$("input[name=lat]").val(latLng.lat());--}}
            {{--$("input[name=lng]").val(latLng.lng());--}}
            {{--$("#geocomplete").geocomplete("find", latLng.lat() + "," + latLng.lng());--}}
            {{--$("#reset").show();--}}
        {{--});--}}


        {{--$("#reset").click(function(){--}}
            {{--$("#geocomplete").geocomplete("resetMarker");--}}
            {{--$("#reset").hide();--}}
            {{--return false;--}}
        {{--});--}}

        {{--$("#find").click(function(){--}}
            {{--$("#geocomplete").trigger("geocode");--}}
        {{--});--}}
        //                                        });

        {{--$(function () {--}}
            {{--$("#geocomplete").geocomplete({--}}
                {{--map: ".map_canvas",--}}
                {{--details: "form",--}}
                {{--mapOptions: {--}}
                    {{--zoom: 15--}}
                {{--},--}}
                {{--markerOptions: {--}}
                    {{--draggable: true--}}
                {{--},--}}
                {{--location: "{{ $event->venue }}",--}}
                {{--types: ["geocode", "establishment"],--}}
            {{--});--}}

            {{--$("#geocomplete").bind("geocode:dragged", function (event, latLng) {--}}
                {{--console.log(latLng);--}}
                {{--$("input[name=lat]").val(latLng.lat());--}}
                {{--$("input[name=lng]").val(latLng.lng());--}}
                {{--$("#geocomplete").geocomplete("find", latLng.lat() + "," + latLng.lng());--}}
                {{--$("#reset").show();--}}
            {{--});--}}


            {{--$("#reset").click(function () {--}}
                {{--$("#geocomplete").geocomplete("resetMarker");--}}
                {{--$("#reset").hide();--}}
                {{--return false;--}}
            {{--});--}}

            {{--$("#find").click(function () {--}}
                {{--$("#geocomplete").trigger("geocode");--}}
            {{--});--}}
        {{--});--}}


        jQuery(function($) {
            $(document).ready(function() {
                var latlng = new google.maps.LatLng($("input[name=lat]").val(), $("input[name=lng]").val());
                var myOptions = {
                    zoom: 10,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                console.dir(map);
                google.maps.event.trigger(map, 'resize');

                //$('a[href="#profile"]').on('shown', function(e) {
                //    google.maps.event.trigger(map, 'resize');
                //});
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    google.maps.event.trigger(map, 'resize');
                })

                $("#map_canvas").css("width", '100%').css("height", 400);
            });
        });

        $(document).ready(function() {
           //console.log($('.tab-pane').find('img'));
         $('.tab-pane').find('img').addClass('img-responsive')
      });
    </script>

@endsection
