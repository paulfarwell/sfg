<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event extra fields   </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Custom Fields - {{ $event->name or 'set event name' }}
        <small> manage fields you would like to collect data on </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">

                <div class="portlet-body form">
                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Fields </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">

                                <table class="table table-bordered" id="tbl1">
                                    <thead>
                                        {{--<th> Id</th>--}}
                                        <th> Name</th>
                                        <th> Description</th>
                                        <th> Delegates </th>
                                        <th> Status </th>
                                        <th> Tools </th>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane form" id="tab_5_2">

                                {!! Form::open(['route' => ['org.events.custom.fields',$event->id], 'class' => 'form-horizontal form-row-seperated', 'files' => true]) !!}

                                <div class="form-body">

                                <div class="form-group" id="other-delegates">
                                    <label class="col-md-1 control-label"> Add Fields </label>
                                    <div class="col-md-10">
                                        <div class="mt-repeater">
                                            <div data-repeater-list="group-b">
                                                <div data-repeater-item class="row">
                                                    <div class="col-md-3">
                                                        <label class="control-label">Name</label>
                                                        <input type="text" name="name" class="form-control validate[required]" required /> </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">Description</label>
                                                        <input type="text" name="desc" class="form-control validate[required]" required /> </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Field Type</label>
                                                            <select class="form-control validate[required] " required name="field_type" >
                                                                <option value=""> select </option>
                                                                <option value="1"> Text Field </option>
                                                                <option value="2"> Check Box </option>
                                                            </select>
                                                             </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Delegate (multiple select)</label>
                                                        <select class="form-control validate[required] select2-multiple-2" required name="delegate_id" id="multiple" multiple>
                                                            {{--<option value=""> select </option>--}}
                                                            @foreach( $delegate as $list )
                                                                <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger pull-right">
                                                            <i class="fa fa-close"></i> Remove Field
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                <i class="fa fa-plus"></i> Add Delegate </a>
                                            <br>
                                            <br> </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>
                            </div>

                            {!!Form::close()!!}

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="modal fade" id="editCustomField" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Activity Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editCustomFieldForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Delegate Type : </label>
                                <div >
                                    <select class="form-control validate[required] select2-multiple-2" id="delegate_id" name="delegate_id[]" id="multiple" multiple>
                                        <option value="">Select...</option>
                                        @foreach( $delegate as $list )
                                            <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Name : </label>
                                <div >
                                    <input type="text" value="" class="form-control validate[required]" name="name" id="name" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Description: </label>
                                <div >
                                    <textarea name="description" class="form-control validate[required]" id="description"> </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label > Field Type: </label>
                                <div >
                                  <select class="form-control validate[required] " required name="field_type" id="field_type" >
                                      <option value=""> select </option>
                                      <option value="1"> Text Field </option>
                                      <option value="2"> Check Box </option>
                                  </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')

    <script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script>

        var FormRepeater = function () {

            return {
                //main function to initiate the module
                init: function () {
                    $('.mt-repeater').each(function(){
                        $(this).repeater({
                            show: function () {
                                $(this).slideDown();
                                $('.date-picker').datepicker({
                                    rtl: App.isRTL(),
                                    orientation: "left",
                                    autoclose: true
                                });
                                $('.select2-multiple-2').selectpicker({
                                    iconBase: 'fa',
                                    tickIcon: 'fa-check'
                                });
                                $('.select2-multiple-2').selectpicker('render');
                            },
                            hide: function (deleteElement) {
                                if(confirm('Are you sure you want to delete this element?')) {
                                    $(this).slideUp(deleteElement);
                                }
                            },

                            ready: function (setIndexes) {

                            }
                            ,isFirstItemUndeletable: true
                        });
                    });
                }

            };

        }();

        jQuery(document).ready(function() {
            FormRepeater.init();
        });


        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'List of Custom Fields',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'List of Custom Fields',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'List of Custom Fields',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'List of Custom Fields',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'List of Custom Fields',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events.fields') !!}',
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
//                {data: 'id', name: 'id', orderable: false, searchable: true},
                {data: 'field_title', name: 'field_title', orderable: false, searchable: true},
                {data: 'field_description', name: 'field_description', orderable: false, searchable: false},
                {data: 'field_type', name: 'field_type', orderable: false, searchable: false},
                {data: 'delegates_names', name: 'delegates_names', orderable: false, searchable: false},
                {data: 'status', name: 'status', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data == 0 ? 'active' : 'inactive';
                }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#editCustomField').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editCustomFieldForm').attr('action', button.data('url'));
            var del = button.data('delegate').toString().split(","); console.log(del);
            var sel =  document.getElementById('delegate_id');
            $('#name').val(button.data('name'));
            $('#description').val(button.data('desc'));

            for ( var j=0; j < sel.length; j++ ) {
                sel[j].selected = false;
            }

            for (index = 0; index < del.length; ++index) {
                for (var i = 0; i < sel.options.length; i++) {
                    console.log(sel.options[i].value); console.log(del[index]);
                    if ( del[index] == sel.options[i].value ) {
                        sel.options[i].selected = true;
                    }
                }
            }

            $('.select2-multiple-2').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
            $('.select2-multiple-2').selectpicker('render');
        })

    </script>
@endsection
