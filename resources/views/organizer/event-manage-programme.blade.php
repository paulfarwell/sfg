<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}
            <a href="{{ route('org.manage.event.tickets',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Tickets </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.edit.event.meta',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Social Info </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Programme -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Programme Details e.g arrival, speeches </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Programmes </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table id="events_programme" width="100%" class="table table-bordered table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th> Type </th>
                                    <th> Title </th>
                                    <th> Data </th>
                                    <th> start </th>
                                    <th> end </th>
                                    <th> Status </th>
                                    <th> Options </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>


                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.prog',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Title:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control validate[required]" name="title" placeholder="">
                                        @if($errors->has('title'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('title') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required]" name="p_type">
                                            <option value="session"> Session </option>
                                            <option value="activity"> Activity </option>
                                        </select>
                                        @if($errors->has('p_type'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('p_type') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('description',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('description'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('description') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                             </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Dates:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <div class="">
                                            <div class='col-md-5'>
                                                <label> Start Date</label>
                                                <div class="form-group">
                                                    <div class='input-group date' id='start'>
                                                        <input type='text' name="start" class="form-control validate[required]" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        @if($errors->has('start'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('start') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-5'>
                                                <label> End Date</label>
                                                <div class="form-group">
                                                    <div class='input-group date' id='end'>
                                                        <input type='text' name="end" class="form-control  validate[required]" />
                                                        <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                        @if($errors->has('end'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('end') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editProgramme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Programme Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editProgForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label> Title: </label>
                                <div>
                                    <input type="text" class="form-control validate[required]" name="title" id="title" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Type:  </label>
                                <div >
                                    <select class="form-control validate[required]" name="p_type" title="p_type">
                                        <option value="session"> Session </option>
                                        <option value="activity"> Activity </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Description: </label>
                                <div >
                                    <textarea name="description" class="form-control validate[required]" id="description"> </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Start Date</label>
                                <div class='input-group date' id='start3'>
                                    <input type='text' name="start" class="form-control validate[required]" />
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label> End Date</label>
                                <div class='input-group date' id='end3'>
                                    <input type='text' name="end" class="form-control validate[required]" />
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">   <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>


@endsection


@section('footer')

    {{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
    {{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

    @javascript('event_id',  $event->start )
    @javascript('list_event_programme', route('org.dt.list.events.prog',array($event->id)))

    <script src="{{ asset("assets/events/tables.js") }}"> </script>

    <script>
        initTable4();
    </script>

    <script>
        $('#editProgramme').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editProgForm').attr('action', button.data('url'));
            $('#title').val(button.data('title'));
            $('#p_type').val(button.data('type'));
            $('#description').val(button.data('desc'));

            $(function () {
                $('#start3').datetimepicker( {
                    defaultDate: button.data('start_date'),
                    useCurrent: false,sideBySide: true
                });
                $('#end3').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    sideBySide: true,
                    defaultDate: button.data('end_date')
                });
                $("#start3").on("dp.change", function (e) {
                    $('#end3').data("DateTimePicker").minDate(e.date);
                });
                $("#end3").on("dp.change", function (e) {
                    $('#start3').data("DateTimePicker").maxDate(e.date);
                });
            });
            $('#description').eq(0).summernote('destroy');
            $('#description').summernote({
                dialogsInBody: true,
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        // Firefox fix
                        setTimeout(function () {
                            document.execCommand('insertText', false, bufferText);
                        }, 10);
                    }
                }
            });
        })
    </script>

    <script>
        $(function () {
            $('#start').datetimepicker( {
                minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(5))) }}",
                {{--defaultDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->start)) }}",
                useCurrent: false,sideBySide: true
            });
            $('#end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true,
                {{--minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->addDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->end)) }}"
            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>

@endsection


