<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.activities',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Activities </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Hotels -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Hotels  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Hotels </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-hotels">
                                <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Delegates</td>
                                    <td>Address</td>
                                    <td>Contacts</td>
                                    <td>Available</td>
                                    <td>Order</td>
                                    <td>Images </td>
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.hotel',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Delegate Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required] select2-multiple" name="delegate_id[]" id="multiple" multiple>
                                            <option value="">Select...</option>
                                            @foreach( $delegate_types as $list )
                                                <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('delegate_id'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('delegate_id') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Name :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                        @if($errors->has('name'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Contact person :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="contact_person" placeholder="">
                                        @if($errors->has('contact_person'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('contact_person') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Contact email :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required,custom[email]]" name="contact_email" placeholder="">
                                        @if($errors->has('contact_email'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('contact_email') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Contact phone :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="contact_phone" placeholder="">
                                        @if($errors->has('contact_phone'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('contact_phone') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> website :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="website" placeholder="">
                                        @if($errors->has('website'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('website') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Coupon Details :
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-10">
                                        {{--<input type="text" value="" class="form-control validate[required]" name="website" placeholder="">--}}
                                        <textarea name="coupon" class="form-control"> </textarea>
                                        @if($errors->has('coupon'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('coupon') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('description',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('notes'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('notes') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>





@endsection


@section('footer')

    @javascript('event_id',  $event->start )
    @javascript('list_hotels', route('org.dt.list.hotels',array($event->id)) )

    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    <script src="{{ asset("assets/events/ui.js") }}"> </script>

    <script>
        initTable3();
    </script>

    @include('partial.hotels-mgt')

@endsection


