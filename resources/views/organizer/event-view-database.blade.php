<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event extra fields   </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')
<style>
.dataTables_scrollBody{
  position:initial !important;
}
</style>
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Delegate Database - {{ $event->name or 'set event name' }}
        <small> view all delegate information </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">

                <div class="portlet-body form">
                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Active </a>
                            </li>
                            <li class="">
                              <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> In Progress </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_3" data-toggle="tab" aria-expanded="false"> Finished </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">

                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th> </th>
                                        <th> Fname</th>
                                        <th> Sname</th>
                                        <th> Code</th>
                                        <th> Phone</th>
                                        <th> Email</th>
                                        {{--<th> Delegation</th>--}}
                                        <th> Badge</th>
                                        <th> Status</th>
                                        <th> Profile</th>
                                        <th> Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>

                            <div class="tab-pane form" id="tab_5_2">

                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl2">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th> </th>
                                        <th> FName</th>
                                        <th> Name</th>
                                        <th> Code</th>
                                        <th> Phone</th>
                                        <th> Email</th>
                                        <!-- <th> Delegation</th> -->
                                        <th> Badge</th>
                                        <th> Status</th>
                                        <th> Profile</th>
                                        <th> Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>

                            <div class="tab-pane form" id="tab_5_3">

                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl3">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th> </th>
                                        <th> Fname</th>
                                        <th> Sname</th>
                                        <th> Code</th>
                                        <th> Phone</th>
                                        <th> Email</th>
                                        {{--<th> Delegation</th>--}}
                                        <th> Badge</th>
                                        <th> Status</th>
                                        <th> Profile</th>
                                        <th> Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="editInvite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Invite </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editInviteForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label> Delegate Category : </label>
                                <div>
                                    <select name="category" class="form-control validate[required]" id="category">
                                        <option value=""> Select Section</option>
                                        @foreach( $delegate_type as $list)
                                            <option value="{{$list->id}}"> {{$list->name}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label> First Name : </label>
                                <div>
                                    <input type="text" name="fname" id="fname" value="" class="form-control validate[required]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Last Name : </label>
                                <div>
                                    <input type="text" name="sname" id="sname" value="" class="form-control validate[required]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



@endsection


@section('footer')

    <script>

        $('#editInvite').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editInviteForm').attr('action', button.data('url'));
            //var del = button.data('delegate').toString().split(","); console.log(del);
            //var sel =  document.getElementById('delegate_id');
            $('#category').val(button.data('delegate-id'));
            $('#fname').val(button.data('fname'));
            $('#sname').val(button.data('lname'));
        });



        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event delegate List reloaded!');
                    }
                },
            ],
            // responsive: true,
            "scrollX": true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events.delegates.all',array($event->id,'full')) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '', searchable: false
                },
                {data: 'profile.fname', name: 'profile.fname', orderable: false, searchable: true},
                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
//                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'profile.cell_phone', name: 'profile.cell_phone', orderable: false, searchable: true},
                {data: 'profile.email', name: 'profile.email', orderable: false, searchable: true},
                // {data: 'delegation.name', name: 'delegation.name', orderable: false,defaultContent : 'not set', searchable: true},
                {data: 'delegate_type.color', name: 'delegateType.color', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data == 0 ? 'active' : 'inactive';
                }},
                {data: 'completion', name: 'completion', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data + '%';
                }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl1 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable1.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

        var oTable3 = $('#tbl2').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event delegate List reloaded!');
                    }
                },
            ],
            // responsive: true,
            "scrollX": true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events.delegates.all',array($event->id,'half')) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '', searchable: false
                },
                {data: 'profile.fname', name: 'profile.fname', orderable: false, searchable: true},
                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                //{data: 'profile.full_name', name: 'profile.full_name', orderable: false, searchable: false},
//                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'profile.cell_phone', name: 'profile.cell_phone', orderable: false, searchable: true},
                {data: 'profile.email', name: 'profile.email', orderable: false, searchable: true},
                // {data: 'delegation.name', name: 'delegation.name', orderable: false,defaultContent : 'not set', searchable: true},
                {data: 'delegate_type.color', name: 'delegate_type.color', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data == 0 ? 'active' : 'inactive';
                }},
                {data: 'completion', name: 'completion', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data + '%';
                }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl2 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable3.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );




        var oTable3 = $('#tbl3').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Event delegate List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event delegate List reloaded!');
                    }
                },
            ],
            // responsive: true,
            "scrollX": true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events.delegates.all',array($event->id,'cancelled')) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '', searchable: false
                },
                {data: 'profile.fname', name: 'profile.fname', orderable: false, searchable: true},
                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                //{data: 'profile.full_name', name: 'profile.full_name', orderable: false, searchable: false},
//                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'profile.cell_phone', name: 'profile.cell_phone', orderable: false, searchable: true},
                {data: 'profile.email', name: 'profile.email', orderable: false, searchable: true},
                // {data: 'delegation.name', name: 'delegation.name', orderable: false,defaultContent : 'not set', searchable: true},
                {data: 'delegate_type.color', name: 'delegate_type.color', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data == 0 ? 'active' : 'inactive';
                }},
                {data: 'completion', name: 'completion', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data + '%';
                }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl3 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable3.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );







        {{--var oTable2 = $('#tbl2').DataTable({--}}
            {{--buttons: [--}}
                {{--{--}}
                    {{--extend: 'print',--}}
                    {{--className: 'btn dark btn-outline',--}}
                    {{--title: 'Event delegate List - Data export',--}}
                    {{--exportOptions: {columns: [0, 1, 2, 3,4,5]}--}}
                {{--},--}}
                {{--{--}}
                    {{--extend: 'copy',--}}
                    {{--className: 'btn red btn-outline',--}}
                    {{--title: 'Event delegate List - Data export',--}}
                    {{--exportOptions: {columns: [0, 1, 2, 3,4,5]}--}}
                {{--},--}}
                {{--{--}}
                    {{--extend: 'pdf',--}}
                    {{--className: 'btn green btn-outline',--}}
                    {{--title: 'Event delegate List - Data export',--}}
                    {{--exportOptions: {columns: [0, 1, 2, 3,4,5]}--}}
                {{--},--}}
                {{--{--}}
                    {{--extend: 'excel',--}}
                    {{--className: 'btn yellow btn-outline ',--}}
                    {{--title: 'Event delegate List - Data export',--}}
                    {{--exportOptions: {columns: [0, 1, 2, 3,4,5]}--}}
                {{--},--}}
                {{--{--}}
                    {{--extend: 'csv',--}}
                    {{--className: 'btn purple btn-outline ',--}}
                    {{--title: 'Event delegate List - Data export',--}}
                    {{--exportOptions: {columns: [0, 1, 2, 3,4,5]}--}}
                {{--},--}}
                {{--{--}}
                    {{--text: 'Reload',--}}
                    {{--className: 'btn default',--}}
                    {{--action: function (e, dt, node, config) {--}}
                        {{--dt.ajax.reload();--}}
                        {{--// alert('Event List reloaded!');--}}
                        {{--toastr.info('Event delegate List reloaded!');--}}
                    {{--}--}}
                {{--},--}}
            {{--],--}}
            {{--responsive: true,--}}
            {{--"deferRender": true,--}}
            {{--"processing": true,--}}
            {{--"serverSide": true,--}}
            {{--"ordering": false, //disable column ordering--}}
            {{--"lengthMenu": [--}}
                {{--[5, 10, 15, 20, 25, -1],--}}
                {{--[5, 10, 15, 20, 25, "All"] // change per page values here--}}
            {{--],--}}
            {{--"pageLength": 25,--}}
            {{--"ajax": {--}}
                {{--url: '{!! route('org.dt.list.events.delegates.all',array($event->id,'half')) !!}',--}}
                {{--method: 'POST'--}}
            {{--},--}}
            {{--"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable--}}

            {{--columns: [--}}
                {{--{--}}
                    {{--"className":      'details-control',--}}
                    {{--"orderable":      false,--}}
                    {{--"data":           null,--}}
                    {{--"defaultContent": '', searchable: false--}}
                {{--},--}}
                {{--{data: 'profile.full_name', name: 'profile.full_name', orderable: false, searchable: false},--}}
        {{--//                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},--}}
                {{--{data: 'code', name: 'code', orderable: false, searchable: true},--}}
                {{--{data: 'profile.cell_phone', name: 'profile.cell_phone', orderable: false, searchable: true},--}}
                {{--{data: 'profile.email', name: 'profile.email', orderable: false, searchable: true},--}}
                {{--{data: 'delegation.name', name: 'delegation.name', orderable: false,defaultContent : 'not set', searchable: true},--}}
                {{--{data: 'delegate_type.color', name: 'delegate_type.color', orderable: false, searchable: true},--}}
                {{--{data: 'status', name: 'status', orderable: false, searchable: false,"render": function ( data, type, row ) {--}}
                    {{--return data == 0 ? 'active' : 'inactive';--}}
                {{--}},--}}
                {{--{data: 'completion', name: 'completion', orderable: false, searchable: false,"render": function ( data, type, row ) {--}}
                    {{--return data + '%';--}}
                {{--}},--}}
                {{--{data: 'tools', name: 'tools', orderable: false, searchable: false},--}}
            {{--]--}}
        {{--});--}}

        {{--$('#tbl2 tbody').on('click', 'td.details-control', function () {--}}
            {{--var tr = $(this).closest('tr');--}}
            {{--var row = oTable2.row(tr);--}}
            {{--if ( row.child.isShown() ) {--}}
                {{--// This row is already open - close it--}}
                {{--row.child.hide();--}}
                {{--tr.removeClass('shown');--}}
            {{--}--}}
            {{--else {--}}
                // Open this row--}}
                {{--row.child( format(row.data()) ).show();--}}
                {{--tr.addClass('shown');--}}
            {{--}--}}
         {{--)--}};
    </script>

@endsection
