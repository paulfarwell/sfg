<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.activities',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Activities </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Hotels -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events BookingForms  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All BookingForms </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-hotels">
                                <thead>
                                <tr>
                                    <td>Event</td>
                                    <td>Upload Section Text</td>
                                    <td>Hotel Accommodation</td>
                                    <td>Tent Accommodation</td>
                                    <td>Travel Details</td>
                                    <td>Transfers</td>
                                    <td>Activities</td>
                                    <td>Custom Fields</td>
                                    <td>Media</td>
                                    <td>Terms and Conditions</td>
                                    <td>Health Insurance</td>
                                    <td>Emergency Contact</td>
                                    <!-- <td>Order</td> -->
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($bookingForms as $bookingForm)
                                <tr>
                                 <td>{{$bookingForm->event->name}}</td>
                                 <td>{!!$bookingForm->upload_section!!}</td>
                                 <td>{{$bookingForm->hotels}}</td>
                                  <td>{{$bookingForm->tents}}</td>
                                  <td>{{$bookingForm->flights}}</td>
                                  <td>{{$bookingForm->transfers}}</td>
                                  <td>{{$bookingForm->activities}}</td>
                                  <td>{{$bookingForm->custom_fields}}</td>
                                  <td>{{$bookingForm->media}}</td>
                                  <td>{{$bookingForm->terms_conditions}}</td>
                                  <td>{{$bookingForm->health_insurance}}</td>
                                  <td>{{$bookingForm->emergency_contacts}}</td>
                                 <td><div class='btn-group '>
                                 <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                                 aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                                 </button>
                                 <ul class='dropdown-menu pull-right' role='menu'>
                                     <li><a href='#'  data-url='{{route('org.edit.event.bookingForm', [$bookingForm->id,$bookingForm->event_id])}}' data-upload_section='{{$bookingForm->upload_section}}'
                                     data-hotels='{{$bookingForm->hotels}}'data-tents='{{$bookingForm->tents}}' data-flights='{{$bookingForm->flights}}' data-transfers='{{$bookingForm->transfers}}'
                                     data-activities='{{$bookingForm->activities}}' data-custom_fields='{{$bookingForm->custom_fields}}' data-medias="{{$bookingForm->media}}" data-terms_conditions='{{$bookingForm->terms_conditions}}' data-health_insurances='{{$bookingForm->health_insurance}}'
                                     data-emergency_contacts='{{$bookingForm->emergency_contacts}}' data-toggle="modal" data-target="#editbookingForms" > Edit Booking Form </a></li>

                                 </ul>
                                 </div></td>
                                 </tr>
                                 @endforeach

                                 </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.bookingForm',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Upload Section Text :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="upload_section" placeholder="">
                                        @if($errors->has('upload_section'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('upload_section') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Hotel Accommodations :

                                    </label>
                                    <div class="col-md-10">
                                        <label class="hotels"><input type="checkbox" id="hotels" name="hotels" value="">
                                        @if($errors->has('hotels'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('hotels') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="hotel" value="" id="hotel">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Tent Accommodations :

                                    </label>
                                    <div class="col-md-10">
                                        <label class="tents"><input type="checkbox" id="tents" name="tents" value="">
                                        @if($errors->has('tents'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('tents') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="tent" value="" id="tent">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Travel Details:

                                    </label>
                                    <div class="col-md-10">
                                    <label class="flights"><input type="checkbox" id="flights" name="flights" value="">
                                        @if($errors->has('flights'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('flights') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="flight" value="" id="flight">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Transfers :

                                    </label>
                                    <div class="col-md-10">
                                      <label class="transfers"><input type="checkbox" id="transfers" name="transfers" value="">
                                        @if($errors->has('transfers'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('transfers') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="transfer" value="" id="transfer">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Activities :

                                    </label>
                                    <div class="col-md-10">
                                    <label class="activities"><input type="checkbox" id="activities" name="activities" value="">
                                        @if($errors->has('activities'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('activities') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="activity" value="" id="activity">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Custom Fields :

                                    </label>
                                    <div class="col-md-10">
                                      <label class="custom_fields"><input type="checkbox" id="custom_fields" name="custom_fields" value="">
                                        @if($errors->has('custom_fields'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('custom_fields') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="custom_field" value="" id="custom_field">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Media :

                                    </label>
                                    <div class="col-md-10">
                                      <label class="medias"><input type="checkbox" id="medias" name="medias" value="">
                                        @if($errors->has('media'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('media') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="media" value="" id="media">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Terms and Contitions :

                                    </label>
                                    <div class="col-md-10">
                                        <label class="terms_conditions"><input type="checkbox" id="terms_conditions" name="terms_conditions" value="">
                                        @if($errors->has('terms_conditions'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('terms_conditions') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="term_condition" value="" id="term_condition">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Health Insurance :

                                    </label>
                                    <div class="col-md-10">
                                      <label class="health_insurances"><input type="checkbox" id="health_insurances" name="health_insurance" value="">
                                        @if($errors->has('health_insurances'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('health_insurances') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="health_insurance" value="" id="health_insurance">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Emergency Contacts :

                                    </label>
                                    <div class="col-md-10">
                                      <label class="emergency_contacts"><input type="checkbox" id="emergency_contacts" name="emergency_contacts" value="">
                                        @if($errors->has('emergency_contacts'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('emergency_contacts') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                          <input type="hidden" name="emergency_contact" value="" id="emergency_contact">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>





@endsection


@section('footer')

    @javascript('event_id',  $event->start )


    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    <script src="{{ asset("assets/events/ui.js") }}"> </script>

    <script>
        initTable3();
    </script>


    @include('partial.bookingForms-mgt')

    <script>
  document.getElementById('hotels').addEventListener('change', function () {

           var hotels =document.getElementById("hotels").checked;
           if(hotels == true){
             $('#hotel').val('1');
           }else{
             $('#hotel').val('');
           }
      });
      document.getElementById('tents').addEventListener('change', function () {

               var tents =document.getElementById("tents").checked;
               if(tents == true){
                 $('#tent').val('1');
               }else{
                 $('#tent').val('');
               }
          });
    document.getElementById('flights').addEventListener('change', function () {

           var flights =document.getElementById("flights").checked;
           if(flights == true){
             $('#flight').val('1');
           }else{
             $('#flight').val('');
           }
    });
    document.getElementById('transfers').addEventListener('change', function () {

           var transfers =document.getElementById("transfers").checked;
           if(transfers == true){
             $('#transfer').val('1');
           }else{
             $('#transfer').val('');
           }
    });
    document.getElementById('activities').addEventListener('change', function () {

           var activities =document.getElementById("activities").checked;
           if(activities == true){
             $('#activity').val('1');
           }else{
             $('#activity').val('');
           }
    });
    document.getElementById('custom_fields').addEventListener('change', function () {

           var custom_fields =document.getElementById("custom_fields").checked;
           if(custom_fields == true){
             $('#custom_field').val('1');
           }else{
             $('#custom_field').val('');
           }
    });
    document.getElementById('medias').addEventListener('change', function () {

           var medias =document.getElementById("medias").checked;
           if(medias == true){
             $('#media').val('1');
           }else{
             $('#media').val('');
           }
    });
    document.getElementById('terms_conditions').addEventListener('change', function () {

           var terms_conditions =document.getElementById("terms_conditions").checked;
           if(terms_conditions == true){
             $('#term_condition').val('1');
           }else{
             $('#term_condition').val('');
           }
    });
    document.getElementById('health_insurances').addEventListener('change', function () {

           var health_insurances =document.getElementById("health_insurances").checked;
           if(health_insurances == true){
             $('#health_insurance').val('1');
           }else{
             $('#health_insurance').val('');
           }
    });
    document.getElementById('emergency_contacts').addEventListener('change', function () {

           var emergency_contacts =document.getElementById("emergency_contacts").checked;
           if(emergency_contacts == true){
             $('#emergency_contact').val('1');
           }else{
             $('#emergency_contact').val('');
           }
    });
    </script>

@endsection
