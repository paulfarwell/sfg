<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.hotels',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Hotels </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.tickets',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Tickets </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Posters -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Posters  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Poster </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Upload New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="events_posters">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Main </th>
                                    <th>Sort Order</th>
                                    <th>status</th>
                                    <th>Tools</th>
                                </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {{--<div class="alert alert-success margin-bottom-10">--}}
                                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>--}}
                                {{--<i class="fa fa-warning fa-lg"></i> Your image must be 1920x445 and above in dimensions and not more than 2 MB. </div>--}}
                            {{--<div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">--}}
                                {{--<a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn btn-success">--}}
                                    {{--<i class="fa fa-plus"></i> Select Files </a>--}}
                                {{--<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-share"></i> Upload Files </a>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div id="tab_images_uploader_filelist" class="col-md-12 col-sm-12"> </div>--}}
                            {{--</div>--}}

                            <form action="{{route('org.new.event.posters', $event->id)}}" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">
                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                                <h3 class="sbold">Drop files here or click to upload</h3>
                                <p> max file size is 8mb and the image must be 1920x415 or higher . </p>
                            </form>

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Poster Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editImagesForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Sort Order </label>
                                <div >
                                    <input type="text" name="sort" id="sort" class="form-control validate[required]">
                                    <small> 0 means no sorting, changing this to a higher figure make this image appear first</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Main Image   </label>
                                <div >
                                    <input type="text" class="form-control validate[required]" id="is_main" name="is_main" placeholder="">
                                    <small> 0 means  not set as the first image, this is used to overide sorting for first image </small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Image Preview  </label>
                                <div>
                                    <img src="" id="image" class="img-responsive">
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">   <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>



@endsection


@section('footer')
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBCneBlloJz09T_BzXSZD3EHPDDtKWLhIc"></script>
    <script src="{{ asset("assets/geocomplete/jquery.geocomplete.js") }}"> </script>
    {{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
    {{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

    <link href="{{ asset('assets/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/dropzone/basic.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>

    @javascript('event_id',  $event->start )
    @javascript('list_event_poster', route('org.dt.list.events.posters',array($event->id)))
    @javascript('event_poster_upload_url', route('org.new.event.img',array($event->id)))
    @javascript('pl_upload_flash', asset('assets/global/plugins/plupload/Moxie.swf') )
    @javascript('pl_upload_silvg', asset('assets/global/plugins/plupload/Moxie.xap'))

    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    <script src="{{ asset("assets/events/ui.js") }}"> </script>

    <script>
        initTable5();
        handleImages();
    </script>

    <script>
        $('#editImages').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editImagesForm').attr('action', button.data('url'));
            $('#sort').val(button.data('sort'));
            $('#is_main').val(button.data('is_main'));
            $('#image').attr('src',button.data('image'));

            $(function () {
                $('#start3').datetimepicker( {
                    defaultDate: button.data('start_date'),
                    useCurrent: false,sideBySide: true
                });
                $('#end3').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    sideBySide: true,
                    defaultDate: button.data('end_date')
                });
                $("#start3").on("dp.change", function (e) {
                    $('#end3').data("DateTimePicker").minDate(e.date);
                });
                $("#end3").on("dp.change", function (e) {
                    $('#start3').data("DateTimePicker").maxDate(e.date);
                });
            });
            $('#description').eq(0).summernote('destroy');
            $('#description').summernote({
                dialogsInBody: true,
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        // Firefox fix
                        setTimeout(function () {
                            document.execCommand('insertText', false, bufferText);
                        }, 10);
                    }
                }
            });
        })
    </script>

    {{--<script>--}}
        {{--$('#myImgModal').on('show.bs.modal', function (event) {--}}
            {{--var button = $(event.relatedTarget)--}}
            {{--var img = button.data('img')--}}
            {{--console.log(img)--}}
            {{--$('#img').attr('src', img);--}}
        {{--})--}}
    {{--</script>--}}

    <script>
        var FormDropzone = function () {
            return {
                //main function to initiate the module
                init: function () {
                    Dropzone.options.myDropzone = {
                        dictDefaultMessage: "",
                        uploadMultiple: false,
                        parallelUploads: 100,
                        maxFilesize: 8,
                        previewsContainer: '#my-dropzone',
                        //previewTemplate: document.querySelector('#preview-template').innerHTML,
                        addRemoveLinks: true,
                        dictRemoveFile: 'Remove',
                        dictFileTooBig: 'Image is bigger than 8MB',
                        init: function () {
                            this.on("queuecomplete", function (file) {
                                location.reload();
                            });
                        }
                    }
                }
            };
        }();
        jQuery(document).ready(function() {
            FormDropzone.init();
        });
    </script>

@endsection
