<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 24/08/2017
 * Time: 09:46
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend-v')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Send Delegate Email </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')



    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> {{ $delegateType->name }}
        <small> send out delegate email </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-title">
                <div class="caption font-dark">
                    {{ $delegateType->name }} | {{ $delegateType->color }}
                </div>
                <div class="tools"> </div>
                </div>
                <div class="portlet-body form">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> Delegate </th>
                                        <th> Email </th>
                                        <th> Type </th>
                                        <th> Badge Color </th>
                                        <th> Code </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $delegateType->invitations  as $list)
                                        <tr>
                                            <td> {{ $list->profile->fname }} {{ $list->profile->surname }}</td>
                                            <td> {{ $list->profile->email }}</td>
                                            <td> {{ $list->delegateType->name }} </td>
                                            <td> {{ $list->delegateType->color }} </td>
                                            <td> {{ $list->code }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 ">

                            {!! Form::open(['route' => ['org.delegate.send',$delegateType->id], 'class' => 'form-horizontal form-row-seperated', 'files' => true]) !!}
                            
                            <div class="form-body">

                              <div class="form-group">
                                  <label class="col-md-2 control-label"> Email Template to use :
                                    <span class="required"> * </span>
                                  </label>
                                  <div class="col-md-10">
                                      <select class="form-control" name="emailtemplate">
                                        <option value="">Select...</option>
                                        @foreach($emailtemplates as $emailtemplate)
                                        <option value="{{$emailtemplate->id}}">{{$emailtemplate->name}}</option>

                                        @endforeach
                                      </select>
                                      @if($errors->has('emailtemplate'))
                                          <span class="alert-danger help-block help-block-error">
                                              @foreach($errors->get('emailtemplate') as $error)
                                                  {!!$error !!}<br/>
                                              @endforeach
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label"> Email Attachment (pdf) :
                                  </label>
                                  <div class="col-md-10">
                                    <input type="file" name="attachment" class="form-control">
                                    @if($errors->has('attachment'))
                                        <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('attachment') as $error)
                                                {!!$error !!}<br/>
                                            @endforeach
                                        </span>
                                    @endif
                                  </div>
                              </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success pull-right">
                                            <i class="fa fa-check"></i> Send Email </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection


@section('footer')

    <script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>

    <!-- <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script> -->

    <!-- <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" /> -->



@endsection
