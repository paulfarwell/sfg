<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Countries Information
        <small> manage the different countries details </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                        <thead>
                        <tr role="row" class="heading">
                            <th>Flag</th>
                            <th>Name</th>
                            <th>Code</th>
                            {{--<th>Currency</th>--}}
                            <th>Demonym</th>
                            <th>Region</th>
                            <th>Capital</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>


    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Countries Types List - Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Countries Types List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Countries Types List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Countries Types List - Data export',
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Countries Types List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Countries List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.countries.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'flag', name: 'flag', orderable: false, searchable: false, render: function (data, type, row) {
                return '<img src="'+ data +' " width="100" > ';  }},
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'demonym', name: 'demonym', orderable: false, searchable: true},
                {data: 'region', name: 'region', orderable: false, searchable: true},
                {data: 'capital', name: 'capital', orderable: false, searchable: true,"defaultContent": "<i>Not set</i>"},
            ],
        });
    </script>

@endsection
