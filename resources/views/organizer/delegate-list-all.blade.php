<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Delegate Types
        <small> manage the different delegate types </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-dark">--}}

                    {{--</div>--}}
                    {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body">

                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Delegates </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Color</th>
                                        <th>color 1 hexcode</th>
                                        <th>color 2 hexcode</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane " id="tab_5_2">

                                {!! Form::open(['route' => ['org.delegate.info'], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Name :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                            @if($errors->has('name'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Description :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="description" placeholder="">
                                            @if($errors->has('description'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('description') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Color Name :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="color" placeholder="">
                                            @if($errors->has('color'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('color') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Primary Color Code :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required] colorpicker-default" name="color_hexcode" placeholder="">
                                            @if($errors->has('color_hexcode'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('color_hexcode') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Secondary Color Code :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required] colorpicker-default" name="color_hexcode_sec" placeholder="">
                                            @if($errors->has('color_hexcode_sec'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('color_hexcode_sec') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-md-2"> Badge Color Image </label>
                                        <div class="col-md-10">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="file" class="validate[required]"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-success">NOTE!</span> Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead. </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                        </label>
                                        <div class="col-md-10">
                                            <button type="submit" name="Save" value="save" class="btn btn-success">
                                                <i class="fa fa-check"></i> Save </button>
                                        </div>
                                    </div>

                                </div>
                                {!!Form::close()!!}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="editDelegate" tabindex="-1" role="dialog" aria-labelledby="editDelegate"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Delegate type </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editDelegateForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label > Name </label>
                                <input type="text" value="" class="form-control validate[required]" name="name" id="name">
                            </div>

                            <div class="form-group">
                                <label > Description </label>
                                <input type="text" value="" class="form-control validate[required]" name="description"  id="description">
                            </div>

                            <div class="form-group">
                                <label > color </label>
                                <input type="text" value="" class="form-control validate[required]" name="color"  id="color">
                            </div>

                            <div class="form-group">
                                <label > color </label>
                                <input type="text" value="" class="form-control validate[required] colorpicker-default" name="color_hexcode"  id="color1">
                            </div>

                            <div class="form-group">
                                <label > color </label>
                                <input type="text" value="" class="form-control validate[required] colorpicker-default" name="color_hexcode_sec"  id="color2">
                            </div>

                            <div class="form-group">
                                <label > ordering </label>
                                <input type="text" value="" class="form-control validate[required]" name="ordering"  id="ordering">
                            </div>

                            <div class="form-group">
                                <label > image </label>
                                <img src="" id="image" class="img-responsive">
                                <input type="file" value="" class="form-control" name="file"  id="file">
                            </div>

                            <div class="form-group">
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update
                                </button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>

    <script>
        $('#editDelegate').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editDelegateForm').attr('action', button.data('url'));
            $('#name').val(button.data('name'));
            $('#description').val(button.data('description'));
            $('#color').val(button.data('color'));
            $('#color1').val(button.data('color1'));
            $('#color2').val(button.data('color2'));
            $('#ordering').val(button.data('ordering'));
            $('#image').attr('src',button.data('image_path'));
        })
    </script>

    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Delegate Types List - Data export',
                    messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Delegate Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Delegate Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Delegate Types List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Delegate Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Delegate types List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: "{!! route('org.dt.list.delegate.list.all') !!}",
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'poster', name: 'poster', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'color', name: 'color', orderable: false, searchable: true,"defaultContent": "<i>Not set</i>"},
                {data: 'color_hexcode', name: 'color_hexcode', orderable: false, searchable: true},
                {data: 'color_hexcode_sec', name: 'color_hexcode_sec', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
            ],
        });
    </script>

@endsection
