<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Events
        <small> set event parameters </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-body">

                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Active </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Inactive </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_3" data-toggle="tab" aria-expanded="false"> Unpublished </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_4" data-toggle="tab" aria-expanded="false"> Deleted </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        {{--<th></th>--}}
                                        <th>Poster</th>
                                        <th>Name</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Venue</th>
                                        {{--<th>Type</th>--}}
                                        <th>Status</th>
                                        {{--<th>Owner</th>--}}
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_2">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl2">
                                    <thead>
                                    <tr role="row" class="heading">
                                        {{--<th></th>--}}
                                        <th>Poster</th>
                                        <th>Name</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Venue</th>
                                        {{--<th>Type</th>--}}
                                        <th>Status</th>
                                        {{--<th>Owner</th>--}}
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_3">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl3">
                                    <thead>
                                    <tr role="row" class="heading">
                                        {{--<th></th>--}}
                                        <th>Poster</th>
                                        <th>Name</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Venue</th>
                                        {{--<th>Type</th>--}}
                                        <th>Status</th>
                                        {{--<th>Owner</th>--}}
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_4">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl4">
                                    <thead>
                                    <tr role="row" class="heading">
                                        {{--<th></th>--}}
                                        <th>Poster</th>
                                        <th>Name</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Venue</th>
                                        {{--<th>Type</th>--}}
                                        <th>Status</th>
                                        {{--<th>Owner</th>--}}
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection


@section('footer')

    <script>

        {{--function format ( d ) {--}}
            {{--var tbl = '<h4>Delegations</h4> <table cellpadding="5" class="table table-bordered" cellspacing="0" border="0" style="padding-left:50px;">'+--}}
                {{--'<tr>'+--}}
                {{--'<th><strong>Name</strong> </th>'+--}}
                {{--'<th><strong>Contact</strong> </th>'+--}}
                {{--'<th><strong>Country</strong> </th>'+--}}
                {{--'<th><strong>Invitations</strong></th>'+--}}
                {{--'<th><strong>Status</strong></th>'+--}}
                {{--'<th><strong>Manage</strong></th>'+--}}
                {{--'</tr>';--}}
            {{--for (i = 0; i < d.delegations.length; i++) {--}}
                {{--tbl  += '<tr>'+--}}
                    {{--'<td>'+d.delegations[i].name+'</td>'+--}}
                    {{--'<td>'+d.delegations[i].contact_name+'</td>'+--}}
                    {{--'<td>'+d.delegations[i].countryname.name+'</td>'+--}}
                    {{--'<td>'+d.delegations[i].invitations.length+'</td>'+--}}
                    {{--'<td>'+ (d.delegations[i].status == 0 ? "Active" : "Inactive") + '</td>'+--}}
                    {{--'<td> <a href="{{ route('org.events.inv.send') }}/' + d.delegations[i].id + '/' + d.id +'#create_delegations">Add Invitations</a> |' +--}}
                    {{--' <a  href="{{ route('org.events.inv.send') }}/' + d.delegations[i].id + '/' + d.id +'" > View </a> | ' +--}}
                    {{--' <a href="{{ route('org.events.inv.print') }}/' + d.delegations[i].id + '/' + d.id +'" target="_blank"> Print </a>  </td>'+--}}
                    {{--'</tr>';--}}
            {{--}--}}
            {{--return tbl += '</table>';--}}
        {{--}--}}

        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Event List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Event List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Event List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'Event List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Event List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events',array('1')) !!}',
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
//                {
//                    "className":      'details-control',
//                    "orderable":      false,
//                    "data":           null,
//                    "defaultContent": ''
//                },
                {data: 'posters', name: 'posters', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'start', name: 'start', orderable: false, searchable: true},
                {data: 'end', name: 'end', orderable: false, searchable: true},
                {data: 'venue', name: 'venue', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl1 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable1.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

        var oTable2 = $('#tbl2').DataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event List - Data export' ,exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event List - Data export',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event List reloaded!');
                            toastr.info('Event List reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: '{!! route('org.dt.list.events',array('2')) !!}',
                    method: 'POST'
                },
                //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
//                    {   className:'details-control',
//                        orderable:      false,
//                        data:           null,
//                        defaultContent: '',
//                        name: 'id',
//                        searchable: false
//                    },
                    {data: 'posters', name: 'posters', orderable: false, searchable: false},
                    {data: 'name', name: 'name', orderable: false, searchable: true},
                    {data: 'start', name: 'start', orderable: false, searchable: true},
                    {data: 'end', name: 'end', orderable: false, searchable: true},
                    {data: 'venue', name: 'venue', orderable: false, searchable: true},
                    {data: 'status', name: 'status', orderable: false, searchable: false},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });

        $('#tbl2 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable2.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

        var oTable3 = $('#tbl3').DataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event List - Data export' ,exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event List - Data export',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events',array('3')) !!}',
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
//                {   className:'details-control',
//                    orderable:      false,
//                    data:           null,
//                    defaultContent: '',
//                    name: 'id',
//                    searchable: false
//                },
                {data: 'posters', name: 'posters', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'start', name: 'start', orderable: false, searchable: true},
                {data: 'end', name: 'end', orderable: false, searchable: true},
                {data: 'venue', name: 'venue', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl3 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable3.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

        var oTable4 = $('#tbl4').DataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event List - Data export' ,exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event List - Data export',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event List - Data export',exportOptions: {columns: [ 0, 1, 2, 3,4,5]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.events',array('4')) !!}',
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
//                {   className:'details-control',
//                    orderable:      false,
//                    data:           null,
//                    defaultContent: '',
//                    name: 'id',
//                    searchable: false
//                },
                {data: 'posters', name: 'posters', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'start', name: 'start', orderable: false, searchable: true},
                {data: 'end', name: 'end', orderable: false, searchable: true},
                {data: 'venue', name: 'venue', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        $('#tbl4 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable4.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    </script>

@endsection
