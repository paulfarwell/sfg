<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>

            <a href="#" data-toggle="modal" data-target="#TopupAccount" class="btn blue btn-sm pull-right"> Top Up Account </a>

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Payment on a Delegation
        <small> manage bulk payments on a delegation </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-dark">--}}

                    {{--</div>--}}
                    {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body">

                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Add Payment </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Payments Made </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_3" data-toggle="tab" aria-expanded="false"> Refunds Made </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_4" data-toggle="tab" aria-expanded="false"> Account Deposits </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                {!! Form::open(['route' => ['org.events.inv.payment',$delegation->id, $delegation->event->id],'onSubmit' => 'return checkitems()', 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}

                                <div class="form-group">
                                    <div class="col-md-12">

                                        <div class='col-md-4'>
                                            <div class="form-group">
                                                <label> Payment Date </label>
                                                <input type='text' data-date-format="dd MM yyyy" data-date-end-date = "0d" name="payment_date" class="form-control validate[required] date-picker" />
                                                @if($errors->has('start'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('start') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class='col-md-4'>
                                            <div class="form-group">
                                                <label> Payment Method </label>
                                                <select name="payment_method" class="form-control validate[required]">
                                                    <option value=""> Select one </option>
                                                    @foreach($payment_method as $list)
                                                        <option value="{{ $list->name }}"> {{ $list->name }} </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('payment_method'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('payment_method') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class='col-md-4'>
                                            <div class="form-group">
                                                <label> Reference Number </label>
                                                <input type='text' name="reference_number" value="" class="form-control  validate[required]" />
                                                @if($errors->has('end'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('end') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class='col-md-12'>
                                            <div class="form-group">
                                                <label> Description </label>
                                                <textarea name="description" class="form-control validate[required]"> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <table id="items" class="table table-bordered table-condensed">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Invitation</th>
                                                <th>Name</th>
                                                <th>Badge</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($delegation->invitations as $key => $list)
                                                <input type='hidden' class="form-control codeid" value="{{ $list->id }}" readonly name="invitation_id[]">
                                                <tr class='item-row'>
                                                    <td class='item-remove'>  <div class='delete-wpr'><a class='delete' href='javascript:;' title='Remove row'> X <i class="icon-trash"></i></a></div> </td>
                                                    <td> <input class="form-control code" value="{{ $list->code }}" readonly name="code[]"> </td>
                                                    <td>{{ $list->profile->fname }} {{ $list->profile->surname }}</td>
                                                    <td>{{ $list->delegateType->name }}  </td>
                                                    <td><input class="form-control cost" readonly name="amount[]" value="{{  (($list->delegatePrice->price) + ($list->activity->sum('activity.cost'))
                                            + ( isset($list->transfer) ? $list->transfer->package->cost : 0 ) ) - $list->payments->sum('amount') < 0 ? 0 : (($list->delegatePrice->price) + ($list->activity->sum('activity.cost'))
                                            + ( isset($list->transfer) ? $list->transfer->package->cost : 0 ) ) - $list->payments->sum('amount') }}"></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="4" align="right">Total</td>
                                                <td colspan=""><input type='text' id="subtotal" readonly name="total" value="0" class="form-control validate[required,custom[number]]" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">Amount Paid</td>
                                                <td colspan=""><input type='text' id="payment_amount" name="payment_amount" value="0" class="form-control  validate[required,custom[number]]" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">Prepaid Amount</td>
                                                <td colspan=""><input type='text' id="account" readonly name="prepaid_amount" value="{{ $delegation->overpayments->sum('amount') }}" class="form-control  validate[required,custom[number]]" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">Total Paid</td>
                                                <td colspan=""><input type='text' id="totalpaid" readonly name="total_paid" value="0" class="form-control  validate[required,custom[number]]" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">Balance</td>
                                                <td colspan=""><input type='text' id="balance" readonly name="balance" value="0" class="form-control  validate[required,custom[number]]" /></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 pull-right">
                                            <p > <strong>Instructions</strong> </p>
                                            <ol>
                                                <li> Populate "amount paid" field </li>
                                            </ol>

                                            Case 1:
                                            <ol>
                                            <li> If "amount paid" is equal to the pending balance of the delegation, click "save" to add payment</li>
                                            </ol>

                                            Case 2
                                            <ol>
                                            <li> If "amount paid" is equal to one or more delegates total bill, but not equal to the whole delegation's bill,
                                                click on the "X" button against the delegates invitation codes, to only remain with the delegates that you wish
                                                to pay for in your current payment
                                            </li>
                                            </ol>

                                            Case 3
                                            <ol>
                                                <li> If "amount paid" is not equal to any of the delegates total bill, click "top up account" to enter the amount into a holding account.</li>
                                                <li> Once the total amount in the holding account is enough to offset one or more delegates or delegation's bill, click "save payment" to settle the delegation's payment. </li>
                                            </ol>
                                            {{--<li> in event where the account had a pending balance, th  existing payment which is unused will be added automatically</li>--}}
                                            {{--<li> remove delegates from the list if amount is smaller than the total</li>--}}
                                            {{--<li> if amount being paid is smaller than the delegates profile total, please the topup account feature</li>--}}
                                            {{--<li> Any overpayment will be added to a holding account and  be refunded </li>--}}
                                        </ol>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 pull-right">
                                        <input name="Save" type="submit" value="Save Payment" class="btn green pull-right" style="margin:5px;">
                                        <a href="#" data-toggle="modal" data-target="#TopupAccount" class="btn blue pull-right" style="margin:5px;"> Top Up Account </a>
                                    </div>
                                </div>

                                {!!Form::close()!!}
                            </div>
                            <div class="tab-pane " id="tab_5_2">
                                <table class="table table-bordered table-condensed" id="delegation-payment">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Pay date</th>
                                            <th>Reference</th>
                                            <th>Method</th>
                                            <th>Description</th>
                                            <th>Amount($)</th>
                                            <th>Status</th>
                                            <th>Tools</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane " id="tab_5_3">
                                <table class="table table-bordered table-condensed" id="delegation-refund">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Pay date</th>
                                        <th>Reference</th>
                                        <th>Method</th>
                                        <th>Description</th>
                                        <th>Payment Reference</th>
                                        <th>Amount($)</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane " id="tab_5_4">
                                <table class="table table-bordered table-condensed" id="delegation-deposit">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Pay date</th>
                                        <th>Description</th>
                                        <th>Payment Reference</th>
                                        <th>Amount($)</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    <div class="modal fade" id="TopupAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Add a Payment Deposit to this delegation </h5>
                </div>
                <div class="modal-body">

                    <form action="{{ route('org.add.overpayment',array($delegation->id,$delegation->event->id)) }}" method="post" id="TopupAccountForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label> Date : </label>
                                <div >
                                    <input type="text" value="" class="form-control validate[required] date-picker" name="pay_date" id="pay_date" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Reference number : </label>
                                <div >
                                    <input type="text" value="" class="form-control validate[required]]" name="reciept_number" id="reciept_number" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Amount : </label>
                                <div >
                                    <input type="text" value="" class="form-control validate[required,custom[number]]" name="amount" id="amount" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Description: </label>
                                <div >
                                    <textarea name="description" class="form-control validate[required]" id="description"> </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Add Payment </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')

    <script src="{{ asset('assets/payments/delegation-payment.js') }}"> </script>

    <script>
        var oTable1 = $('#delegation-payment').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'delegation Payments - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'delegation Payments - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'delegation Payments - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'delegation Payments - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'delegation Payments - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('Event List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.delegation.payment',array($delegation->id)) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'id', name: 'id', orderable: false, searchable: true},
                {data: 'payment_date', name: 'payment_date', orderable: false, searchable: true},
                {data: 'reciept_number', name: 'reciept_number', orderable: false, searchable: true},
                {data: 'pay_method', name: 'pay_method', orderable: false, searchable: true},
                {data: 'details', name: 'details', orderable: false, searchable: false},
                {data: 'amount', name: 'amount', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
    </script>

    <script>
        var oTable1 = $('#delegation-refund').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'delegation refund - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'delegation refund - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'delegation refund - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'delegation refund - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'delegation refund - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('refund list reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.delegation.refund',array($delegation->id)) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'id', name: 'id', orderable: false, searchable: true},
                {data: 'payment_date', name: 'payment_date', orderable: false, searchable: true},
                {data: 'reciept_number', name: 'reciept_number', orderable: false, searchable: true},
                {data: 'payout_method', name: 'payout_method', orderable: false, searchable: true},
                {data: 'details', name: 'details', orderable: false, searchable: false},
                {data: 'amount', name: 'amount', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
    </script>

    <script>
        var oTable1 = $('#delegation-deposit').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'delegation deposit - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'delegation deposit - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'delegation deposit - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'delegation deposit - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'delegation deposit - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('deposit list reloaded!');
                    }
                },
            ],
            //responsive: true,
            "scrollX": true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.delegation.overpayment',array($delegation->id)) !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            columns: [
                {data: 'id', name: 'id', orderable: false, searchable: true},
                {data: 'payment_date', name: 'payment_date', orderable: false, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'reciept_number', name: 'reciept_number', orderable: false, searchable: true},
                {data: 'amount', name: 'amount', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
    </script>

@endsection
