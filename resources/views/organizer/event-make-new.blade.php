<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Create new Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            <a href="{{ route('org.events') }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> All Events </a>

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event
        <small> set new event parameters </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span> Creating a new event </span>
                    </div>
                </div>
                <div class="portlet-body">

                        {!! Form::open(['route' => ['org.new.event'], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}

                        <div class="form-body">

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Name:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{ Input::old('event-name') or null }}" class="form-control validate[required]" name="event-name" placeholder="">
                                @if($errors->has('name'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('name') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Theme:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{Input::old('theme') or null}}" class="form-control validate[required]" name="theme" placeholder="">
                                @if($errors->has('theme'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('theme') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Venue:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{ Input::old('venue') or null}}" id="geocomplete" class="form-control validate[required]" name="venue" placeholder="">
                                <input name="formatted_address" type="hidden" value="">
                                @if($errors->has('venue'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('venue') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Logo:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="file" name="logo" class="form-control">
                                @if($errors->has('logo'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('logo') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"> Map to Venue:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <div class="map_canvas" style="width: 100%;"></div>
                                <div class='col-md-5'>
                                <label>Latitude</label>
                                <input name="lat" type="text" value="{{ Input::old('lat') or null}}" class="form-control validate[required]">
                                </div>
                                <div class='col-md-5'>
                                <label>Longitude</label>
                                <input name="lng" type="text" value="{{ Input::old('lng') or null}}" class="form-control validate[required]">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Dates:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <div class="">
                                    <div class='col-md-5'>
                                        <div class="form-group">
                                            <label> Start Date </label>
                                            <div class='input-group date' id='start'>
                                                <input type='text' value=" {{ Input::old('start') or null }}" name="start" class="form-control validate[required]" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                @if($errors->has('start'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('start') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-md-5'>
                                        <div class="form-group">
                                            <label> End Date </label>
                                            <div class='input-group date' id='end'>
                                                <input type='text' name="end" value=" {{ Input::old('end') or null }}" class="form-control  validate[required]" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                @if($errors->has('end'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('end') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Description:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                {!! Form::textarea('description', Input::old('description') or null , ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                @if($errors->has('description'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('description') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"> Event Programme (PDF):
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="file" name="event_programme" class="form-control">
                                @if($errors->has('event_programme'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('event_programme') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                            </label>
                            <div class="col-md-10">
                                <button type="submit" name="Save" value="save" class="btn btn-success">
                                    <i class="fa fa-check"></i> Save </button>
                            </div>
                        </div>

                    </div>

                    {!!Form::close()!!}


                </div>
            </div>

        </div>

    </div>

@endsection


@section('footer')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBCneBlloJz09T_BzXSZD3EHPDDtKWLhIc"></script>
    <script src="{{ asset("assets/geocomplete/jquery.geocomplete.js") }}"> </script>

    <script>
        $(function () {
            $('#start').datetimepicker( { minDate:new Date(), sideBySide: true} );
            $('#end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true
            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });

        $(function(){
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                types: ["geocode", "establishment"],
            });

            $("#find").click(function(){
                $("#geocomplete").trigger("geocode");
            });
        });
    </script>

@endsection
