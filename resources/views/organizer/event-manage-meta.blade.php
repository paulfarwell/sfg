<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events') }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> All Events </a>--}}
            <a href="{{ route('org.manage.event.prog',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Programme </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Social Info -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Social Info - e.g. videos </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Social Info </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table id="events_meta" width="100%" class="table table-bordered table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th> Type </th>
                                    <th> Title </th>
                                    <th> Data </th>
                                    <th> Status </th>
                                    <th> Options </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">
                            {!! Form::open(['route' => ['org.new.meta',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Meta Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required]" name="field_name">
                                            <option value="">Select...</option>
                                            @foreach( $meta_types as $list )
                                                <option value="{{ $list->data }}"> {{ $list->data }} </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('field_name'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('field_name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Title :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="content_title" placeholder="">
                                        @if($errors->has('content_title'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('content_title') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('content_description',null, ['class'=>'form-control ckeditor  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('content_description'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('content_description') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>

                            {!!Form::close()!!}
                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editMeta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Meta Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editMetaForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label> Meta Type   </label>
                                    <select class="form-control validate[required]" name="field_name" id="field_name">
                                        <option value="">Select...</option>
                                        @foreach( $meta_types as $list )
                                            <option value="{{ $list->data }}"> {{ $list->data }} </option>
                                        @endforeach
                                    </select>
                            </div>

                            <div class="form-group">
                                    <label > Title  </label>
                                    <input type="text" value="" class="form-control validate[required]" name="content_title" id="content_title">
                            </div>

                            <div class="form-group">
                                <label > Description  </label>
                                <textarea name="content_description" class="form-control validate[required]" id="content_description"> </textarea>
                           </div>

                            <div class="form-group">
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update
                                    </button>
                            </div>

                            </div>
                    </form>

                    </div>


                </div>
            </div>
        </div>

@endsection


@section('footer')
    @javascript('event_id',  $event->start )
    @javascript('list_events', route('org.dt.list.events.meta',array($event->id)) )

    <script src="{{ asset("assets/events/tables.js") }}"> </script>

    <script>
        initTable1();
    </script>

    <script>
        $('#editMeta').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editMetaForm').attr('action', button.data('url'));
            $('#content_title').val(button.data('title'));
            $('#field_name').val(button.data('type'));
            $('#content_description').val(button.data('desc'));
        })
    </script>



@endsection


