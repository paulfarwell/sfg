<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$stats['sales']}}">{{$stats['sales']}}</span>
                            <small class="font-green-sharp">$</small>
                        </h3>
                        <small>TOTAL SALES </small>
                    </div>
                    <div class="icon">
                        <i class="icon-pie-chart"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-red-haze">
                            <span data-counter="counterup" data-value="{{$stats['invitations']}}">{{$stats['invitations']}}</span>
                        </h3>
                        <small> INVITATIONS </small>
                    </div>
                    <div class="icon">
                        <i class="icon-like"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup" data-value="{{$stats['events']}}">{{$stats['events']}}</span>
                        </h3>
                        <small>EVENTS</small>
                    </div>
                    <div class="icon">
                        <i class="icon-basket"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-purple-soft">
                            <span data-counter="counterup" data-value="{{$stats['users']}}">{{$stats['users']}}</span>
                        </h3>
                        <small>USERS</small>
                    </div>
                    <div class="icon">
                        <i class="icon-user"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">

        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"> Delegate Activities </span>
                </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 338px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">

                        <table class="table table bordered table-condensed" id="tbl1">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Surname</th>
                                    {{--<th>Phone</th>--}}
                                    {{--<th>Email</th>--}}
                                    <th>Invitations</th>
                                    <th>Profile</th>
                                    <th>Tools</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-bubbles font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase"> FEEDS </span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#portlet_comments_1" data-toggle="tab"> System Notifications </a>
                        </li>
                        <li>
                            <a href="#portlet_comments_2" data-toggle="tab"> User Activities </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="portlet_comments_1">
                            <div class="scroller" style="height: 338px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                @foreach($notices as $list)

                                        <div class="mt-comment">
                                            <div class="mt-comment-img">
                                                {{--<img src="../assets/pages/media/users/avatar1.jpg" /> --}}
                                            </div>
                                            <div class="mt-comment-body">
                                                <div class="mt-comment-info">
                                                    <span class="mt-comment-author"> &nbsp; </span>
                                                    <span class="mt-comment-date"> {{$list['time_diff']  }} </span>
                                                </div>
                                                <div class="mt-comment-text">  {{$list['message']  }} </div>
                                                <div class="mt-comment-details">
                                                    <span class="mt-comment-status mt-comment-status-pending">Pending</span>
                                                    <ul class="mt-comment-actions">
                                                        <li>
                                                            <a href="{{ $list['url'] }}"> Fix It </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                @endforeach
                                </div>

                            </div>
                            <!-- END: Comments -->
                        </div>
                        <div class="tab-pane" id="portlet_comments_2">
                            <div class="scroller" style="height: 338px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">

                                <table class="table table bordered table-condensed" id="tbl2">
                                    <thead>
                                    <tr>
                                        <th>Table</th>
                                        <th>Action</th>
                                        <th>Record</th>
                                        <th>User</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

@section('footer')

    <script>
        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('delegate profiles List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 5,
            "ajax": {
                url: '{!! route('org.dt.list.events.profiles.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'fname', name: 'fname', orderable: false, searchable: true},
                {data: 'surname', name: 'surname', orderable: false, searchable: true},
                {data: 'invitations', name: 'invitations', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data.length;
                }},
                {data: 'completion', name: 'completion', orderable: false, searchable: false,"render": function ( data, type, row ) {
                    return data + '%';
                }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });

        var oTable2 = $('#tbl2').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'delegate profiles List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Event List reloaded!');
                        toastr.info('delegate profiles List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 5,
            "ajax": {
                url: '{!! route('org.dt.list.user.log.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'TableName', name: 'TableName', orderable: false, searchable: false},
                {data: 'description', name: 'description', orderable: false, searchable: true},
                {data: 'subject_id', name: 'subject_id', orderable: false, searchable: true},
                {data: 'user.name', name: 'user.name', orderable: false, searchable: true},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
            ]
        });
    </script>

@endsection
