<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event Invitations   </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            {{--<a href="{{ route('org.manage.event.sponsors',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Sponsors </a>--}}
            <a href="{{ route('org.events.delegations', $event->id) }}" class="pull-right btn green btn-sm "   > Back to {{ $delegation->name }} Delegation </a>

            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            {{--<a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>--}}

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Invitations on {{$delegation->name}} | {{ $event->name}}
        <small> send invitations for a delegation </small>

        {{--<a href="{{ route('org.edit.event', $event->id) }}" class="pull-right btn green btn-sm btn-outline"   > Back to {{$event->name}} Event </a>--}}
        {{--<a href="{{ route('org.events.delegations', $event->id) }}" class="pull-right btn green btn-sm btn-outline"   > Back to {{ $delegation->countryname->name }} Delegation </a>--}}

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Invitations |  {{ $event->name or 'set event name' }} | {{ $delegation->name }} </span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#list_delegations" data-toggle="tab" aria-expanded="true"> Invitation List </a>
                        </li>
                        <li class="">
                            <a href="#create_delegations" data-toggle="tab" aria-expanded="true"> Create </a>
                        </li>
                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="">

                        <div class="tab-content">
                            <div class="tab-pane active" id="list_delegations" >

                                <table id="invitation" width="100%" class="table table-bordered table table-striped table-bordered table-hover">
                                    <thead>
                                      <th> Photo</th>
                                      <th> Country</th>
                                      <th> Code</th>
                                      <th> First Name</th>
                                      <th> Surname </th>
                                      <th> Delegation </th>
                                      <th> Badge </th>
                                      <th> Status </th>
                                      <th> Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>
                            <div class="tab-pane" id="create_delegations" >

                                <form action="{{route('org.events.inv.send', [$id,$eventid])}}" class="form-horizontal form-row-seperated" method="post" >
                                    <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">

                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Badge / Team :
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-10">
                                                <select class="form-control validate[required] select2-multiple"  id="multiple" name="delegate_id">
                                                    <option value="">Select...</option>
                                                    @foreach( $delegate_types as $list )
                                                        <option value="{{ $list->id }}"> {{ $list->name .' - '. $list->color }} </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('delegate_id'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('delegate_id') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Title :
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-10">
                                                <select class="form-control" name="title">
                                                  <option value="">Select...</option>
                                                  @foreach($titles as $title)
                                                  <option value="{{$title->name}}">{{$title->name}}</option>

                                                  @endforeach
                                                </select>
                                                @if($errors->has('title'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('title') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Names:
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-10">
                                                <div class="">
                                                    <div class='col-md-5'>
                                                        <div class="form-group">
                                                            <div class='input-group date' id=''>
                                                                <input type='text' placeholder="first name" name="fname" class="form-control validate[required]" />
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-user"></span>
                                                                </span>
                                                                @if($errors->has('fname'))
                                                                    <span class="alert-danger help-block help-block-error">
                                                                        @foreach($errors->get('fname') as $error)
                                                                            {!!$error !!}<br/>
                                                                        @endforeach
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-5'>
                                                        <div class="form-group">
                                                            <div class='input-group date' id=''>
                                                                <input type='text' placeholder="surname" name="surname" class="form-control  validate[required]" />
                                                                <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-user"></span>
                                                                </span>
                                                                @if($errors->has('surname'))
                                                                    <span class="alert-danger help-block help-block-error">
                                                                        @foreach($errors->get('surname') as $error)
                                                                            {!!$error !!}<br/>
                                                                        @endforeach
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Email :
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control validate[custom[email]]" name="email" placeholder="">
                                                @if($errors->has('email'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('email') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">CC Email :
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control validate[custom[email]]" name="cc_email" placeholder="">
                                                @if($errors->has('cc_email'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('cc_email') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Email Template to use :
                                              <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-10">
                                                <select class="form-control" name="emailtemplate">
                                                  <option value="">Select...</option>
                                                  @foreach($emailtemplates as $emailtemplate)
                                                  <option value="{{$emailtemplate->id}}">{{$emailtemplate->name}}</option>

                                                  @endforeach
                                                </select>
                                                @if($errors->has('emailtemplate'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('emailtemplate') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                            </label>
                                            <div class="col-md-10">
                                                <!-- <button type="submit" name="save" value="save" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Save </button> -->
                                                <button type="submit" name="save" value="save&send" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Save & Send Mail </button>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                        </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    </div>

    <div class="modal fade" id="myImgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Photo </h4>
                </div>
                <div class="modal-body" align="center">
                    <img src="" id="img" width="250">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>

@endsection


@section('footer')

    <script>
        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });
    </script>

    <script>
        $('#myImgModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var img = button.data('img')
            console.log(img)
            $('#img').attr('src', img);
        })
    </script>

    <script>
        var initTable10 = function () {
            var table10 = $('#invitation');
            var oTable10 = table10.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Delegates invitation lists - Data export' ,exportOptions: {columns: [1, 2,3,4,5,6,7]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Delegates invitation lists - Data export',exportOptions: {columns: [ 1, 2,3,4,5,6,7]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Delegates invitation lists - Data export',exportOptions: {columns: [ 1, 2,3,4,5,6,7]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Delegates invitation lists - Data export',exportOptions: {columns: [ 1, 2,3,4,5,6,7]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Delegates invitation lists - Data export',exportOptions: {columns: [ 1, 2,3,4,5,6,7]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event invitation were reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: " {{ route('org.events.delegations.invitations.list',$delegation->id) }}", //events_programme
                    method: 'POST'
                },
                //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
                    {data: 'passport_photo', name: 'passport_photo', orderable: false, searchable: false},
                    {data: 'delegation.countryname.name', name: 'delegation.countryname.name', orderable: false, searchable: true},
                    {data: 'code', name: 'code', orderable: false, searchable: false},
                    {data: 'profile.fname', name: 'profile.fname', orderable: false, searchable: false},
                    {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: false},
                    {data: 'delegation.name', name: 'delegation.name', orderable: false, searchable: false},
                    {data: 'delegate_type.color', name: 'delegate_type.color', orderable: false, searchable: false},
                    {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                        return data === 0 ?  'enabled' :  'disabled';          }},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });
        }

        initTable10();
    </script>

@endsection
