<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Payment on a Delegation
        <small> manage bulk payments on a delegation </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-dark">--}}

                    {{--</div>--}}
                    {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body">

                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Delegation </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane " id="tab_5_2">

                                {!! Form::open(['route' => 'org.delegation.types', 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Name :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                            @if($errors->has('name'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Description :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="description" placeholder="">
                                            @if($errors->has('description'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('description') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                        </label>
                                        <div class="col-md-10">
                                            <button type="submit" name="Save" value="save" class="btn btn-success">
                                                <i class="fa fa-check"></i> Save </button>
                                        </div>
                                    </div>

                                </div>
                                {!!Form::close()!!}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="editDelegation" tabindex="-1" role="dialog" aria-labelledby="editDelegate"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Delegation type </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editDelegationForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label > Name </label>
                                <input type="text" value="" class="form-control validate[required]" name="name" id="name">
                            </div>

                            <div class="form-group">
                                <label > Description </label>
                                <input type="text" value="" class="form-control validate[required]" name="description"  id="description">
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update
                                </button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>

    <script>
        $('#editDelegation').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editDelegationForm').attr('action', button.data('url'));
            $('#name').val(button.data('name'));
            $('#description').val(button.data('description'));           
        })
    </script>

    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Delegation Types List - Data export',
                    messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Delegation Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Delegation Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Delegation Types List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Delegation Types List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Delegations types List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.delegation.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
            ],
        });
    </script>

@endsection
