<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.transfer',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Transfer Packages </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Activities -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events activities  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Activities </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-act">
                                <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Description</td>
                                    <td>Location</td>
                                    <td>Start</td>
                                    <td>End</td>
                                    <td>Fee($)</td>
                                    <td>Images</td>
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.act',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Delegate Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required] select2-multiple" name="delegate_id[]" id="multiple" multiple>
                                            <option value="">Select...</option>
                                            @foreach( $delegate_types as $list )
                                                <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('delegate_id'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('delegate_id') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Name :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                        @if($errors->has('name'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Price ($):
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required,custom[number]]" name="cost" placeholder="">
                                        @if($errors->has('cost'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('cost') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Location :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="location" placeholder="">
                                        @if($errors->has('location'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('location') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Activity Dates:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <div class="">
                                            <div class='col-md-5'>
                                                <div class="form-group">
                                                    <label> Valid from</label>
                                                    <div class='input-group date' id='start'>
                                                        <input type='text' name="valid_from" class="form-control validate[required]" />
                                                        <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                        @if($errors->has('valid_from'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('valid_from') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-5'>
                                                <div class="form-group">
                                                    <label> Valid to</label>
                                                    <div class='input-group date' id='end'>
                                                        <input type='text' name="valid_to" class="form-control  validate[required]" />
                                                        <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                            </span>
                                                        @if($errors->has('valid_to'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('valid_to') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('description',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('notes'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('notes') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


@endsection


@section('footer')

    @include('partial.activity-mgt')

    @javascript('event_id',  $event->start )
    @javascript('list_event_act', route('org.dt.list.act',array($event->id)))

    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    {{--<script src="{{ asset("assets/events/ui.js") }}"> </script>--}}

    <script>
        initTable6();
    </script>

    <script>
        $(function () {
            $('#start').datetimepicker( {
                minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(5))) }}",
                {{--defaultDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->start)) }}",
                useCurrent: false,sideBySide: true
            });
            $('#end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true,
                {{--minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->addDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->end)) }}"
            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>

@endsection


