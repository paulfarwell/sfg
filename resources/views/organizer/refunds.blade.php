<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Refunds
        <small> manage refunds to payments for invitations </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="tbl11">
                        <thead>
                        <tr role="row" class="heading">
                            <th> </th>
                            <th>First Name</th>
                            <th>Surname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Delegation</th>
                            <th>Invitation</th>
                            <th>Badge</th>
                            <th>Event</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>

    <div class="modal fade" id="addPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Add Payment </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="addPaymentForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Payment Method </label>
                                <input type="text" class="form-control validate[required]" name="pay_method" id="pay_method" >
                            </div>

                            <div class="form-group">
                                <label > Document Number </label>
                                <input type="text" class="form-control validate[required]" name="reciept_number" id="reciept_number" >
                            </div>

                            <div class="form-group">
                                <label > Amount </label>
                                <div >
                                    <input type="text" class="form-control validate[required,custom[number],min[0]]" name="amount" id="amount" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <div>
                                    <textarea name="details" class="form-control validate[required]"  > </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

    <div class="modal fade" id="addRefund" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Add Refund </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="addRefundForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Payout_method </label>
                                <input type="text" class="form-control validate[required]" name="pay_method" id="pay_method" >
                            </div>

                            <div class="form-group">
                                <label > Document Number </label>
                                <input type="text" class="form-control validate[required]" name="reciept_number" id="reciept_number" >
                            </div>

                            <div class="form-group">
                                <label > Amount </label>
                                <div >
                                    <input type="text" class="form-control validate[required,custom[number],min[0]]" name="amount" id="amount" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <div>
                                    <textarea name="details" class="form-control validate[required]"  > </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>

    <script>
        $('#addPayment').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#addPaymentForm').attr('action', button.data('url'));
        })
        $('#addRefund').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#addRefundForm').attr('action', button.data('url'));
        })
    </script>

    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        function format ( d ) {
            var tbl = '<h4>Payments</h4> <table cellpadding="5" class="table table-bordered" cellspacing="0" border="0" style="padding-left:50px;">'+
                '<tr>'+
                '<th><strong>Method</strong> </th>'+
                '<th><strong>Details</strong> </th>'+
                '<th><strong>Document Number</strong></th>'+
                '<th><strong>Amount</strong></th>'+
                '<th><strong>Tools</strong></th>'+
                '</tr>';
            for (i = 0; i < d.payments.length; i++) {
                tbl  += '<tr>'+
                    '<td>'+d.payments[i].pay_method+'</td>'+
                    '<td>'+d.payments[i].details+'</td>'+
                    '<td>'+d.payments[i].reciept_number +'</td>'+
                    '<td> $ '+d.payments[i].amount +'</td>'+
                    '<td> <a href=\'#\' data-url="{{ route('org.sales.accounts.new.refund') }}/' + d.payments[i].id + '/' +  d.id + '/'+ d.event_id +'" data-toggle="modal" data-target="#addRefund" >Add Refund</a> |' +
                    ' <a href="{{ route('org.sales.accounts.print.receipt') }}/'  + d.payments[i].id + '/' + d.id + '/'  + d.event_id +'" target="_blank"> Print Receipt</a> |  '+
                    ' <a href="{{ route('org.sales.accounts.send.receipt') }}/'  + d.payments[i].id +'/' + d.id + '/' + d.event_id +'" > Send Receipt</a>  </td>'+
                    '</tr>';
            }
            return tbl += '</table>';
        }

        var oTable1 = $('#tbl11').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Delegates sales account lists - Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Delegates sales account lists - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Delegates sales account lists - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Delegates sales account lists - Data export',
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Delegates sales account lists - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Delegates transfer packages List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.sales.accounts.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '', searchable: false
                },
                {data: 'profile.fname', name: 'profile.fname', orderable: false, searchable: true},
                {data: 'profile.surname', name: 'profile.surname', orderable: false, searchable: true},
                {data: 'profile.email', name: 'profile.email', orderable: false, searchable: true},
                {data: 'profile.cell_phone', name: 'profile.cell_phone', orderable: false, searchable: true},
                {data: 'delegation.countryname.name', name: 'delegation.countryname.name',defaultContent:'no set',
                    orderable: false, searchable: true},
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'delegate_type.color', name: 'delegate_type.color', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'event.name', name: 'event.name', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'tools', name: 'tools', orderable: false,defaultContent:'no set', searchable: false},
            ],
        });

        $('#tbl11 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable1.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
    </script>

@endsection
