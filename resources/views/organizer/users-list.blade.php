<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> User Logs
        <small> view system activities by users </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-dark">--}}

                    {{--</div>--}}
                    {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body">

                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Users </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Created</th>
                                        <th>Last Login</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane " id="tab_5_2">

                                {!! Form::open(['route' => 'org.users.mgr', 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> User Role:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <select class="form-control validate[required] select2-multiple" name="role">
                                                <option value="">Select...</option>
                                                @foreach( $roles as $list )
                                                    <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('role'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('role') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Name :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                            @if($errors->has('name'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Email :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="email" value="" class="form-control validate[required,custom[email]]" name="email" placeholder="">
                                            @if($errors->has('email'))
                                                <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('email') as $error)
                                                        {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                        </label>
                                        <div class="col-md-10">
                                            <button type="submit" name="Save" value="save" class="btn btn-success">
                                                <i class="fa fa-check"></i> Save </button>
                                        </div>
                                    </div>

                                </div>
                                {!!Form::close()!!}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>



    <div class="modal fade" id="changeRole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Change Role </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="changeRoleForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > User Role </label>
                                <select class="form-control validate[required]" name="role">
                                    <option value="">Select...</option>
                                    @foreach( $roles as $list )
                                        <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update
                                </button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Change Password </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="changePasswordForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label > New Password </label>
                                <input type="text" value="" class="form-control validate[required]" name="password1" id="password1">
                            </div>

                            <div class="form-group">
                                <label > Confirm password  </label>
                                <input type="text" value="" class="form-control validate[required,equals[password1]]" name="password2"  id="password2">
                            </div>

                            <div class="form-group">
                                <button type="submit" name="Update" value="Update" class="btn btn-success">
                                    <i class="fa fa-check"></i> Update
                                </button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('footer')
    <script>
        $('#changePassword').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            console.log(button,button.data('url'));
            $('#changePasswordForm').attr('action', button.data('url'));
            //$('#content_title').val(button.data('title'));
            //$('#field_name').val(button.data('type'));
            //$('#content_description').val(button.data('desc'));
        })
    </script>
    <script>
        $('#changeRole').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#changeRoleForm').attr('action', button.data('url'));
            //$('#content_title').val(button.data('title'));
            //$('#field_name').val(button.data('type'));
            //$('#content_description').val(button.data('desc'));
        })
    </script>
    <script>
        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Users Logs List - Data export',
                    messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Users Logs List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Users Logs List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Users Logs List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Users Logs List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Users List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.user.list') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'email', name: 'email', orderable: false, searchable: true},
                {data: 'role.role.name', name: 'role.role.name', orderable: false, searchable: true,"defaultContent": "<i>Not set</i>"},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
                {data: 'updated_at', name: 'updated_at', orderable: false, searchable: true},
                {data: 'is_disabled', name: 'is_disabled', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
            ],
        });
    </script>

@endsection
