<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event Delegations   </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Event Delegations ({{ $event->name or 'set event name' }})
        <small> set new delegations for the different countries/individuals </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                {{--<div class="caption font-dark">--}}

                {{--</div>--}}
                {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body form">
                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Delegation </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table id="delegation" width="100%" class="table table-bordered table table-striped table-bordered table-hover">
                                    <thead>
                                    <th> Name</th>
                                    <th> Invitations</th>
                                    <th> Team Leader</th>
                                    <th> Email</th>

                                    <th> Type</th>
                                    <th> Status</th>
                                    <th> Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane form" id="tab_5_2">

                                {!! Form::open(['route' => ['org.events.delegations',$event->id], 'class' => 'form-horizontal form-row-seperated', 'files' => true]) !!}

                                <div class="form-body">

                                    {{--<div class="form-group  {{ $errors->has('delegation_name') ? ' has-error' : '' }}">--}}
                                        {{--<label class="col-md-2 control-label"> Delegation Name </label>--}}
                                        {{--<div class="col-md-10">--}}
												{{--<!-- <input type="text" class="form-control validate[required]" name="delegation_name" > -->                                               --}}
												{{--<select class="form-control validate[required]" name="delegation_name" >--}}
													{{--<option value="">  select one </option>--}}
													{{--<option value="Government">  Government </option>--}}
													{{--<option value="Private">  Private </option>--}}
												{{--</select>--}}
                                            {{--@if($errors->has('delegation_name'))--}}
                                                {{--<span class="alert-danger help-block help-block-error">--}}
                                                        {{--@foreach($errors->get('delegation_name') as $error)--}}
                                                        {{--{!!$error !!}<br/>--}}
                                                    {{--@endforeach--}}
                                                {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <label class="col-md-1 control-label"> Basic Info </label>
                                        <div class="col-md-11">

                                            <div class="row">
                                                <div class="col-md-6  {{ $errors->has('country') ? ' has-error' : '' }}">
                                                    <label class="control-label">Country</label>
                                                    <select class="form-control  select2-multiple"  id="multiple" name="country">
                                                        <option value="">Select...</option>
                                                        @foreach( $country as $list )
                                                            <option value="{{ $list->code }}"> {{ $list->name }} </option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('country'))
                                                        <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('country') as $error)
                                                                {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6  {{ $errors->has('delegation_type') ? ' has-error' : '' }}">
                                                    <label class="control-label"> Type </label>
                                                    <select class="form-control validate[required]"  id="multiple-2" name="delegation_type">
                                                        <option value="">Select...</option>
                                                        @foreach( $delegation_type as $list )
                                                            <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('delegation_type'))
                                                        <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('type') as $error)
                                                                {!!$error !!}<br/>
                                                        @endforeach
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                        </div>

                                    <div class="form-group">

                                        <small style="margin-left: 14px;"> If delegation type is individual or delegation has one delegate then fill in the primary delegate only </small>

                                        <label class="col-md-1 control-label"> Primary Delegate </label>
                                        <div class="col-md-11">
                                            <div class="row">



                                            <div class="col-md-2  {{ $errors->has('primary_fname') ? ' has-error' : '' }}">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="primary_fname" class="form-control validate[required]" />
                                                @if($errors->has('primary_fname'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('primary_fname') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2  {{ $errors->has('primary_sname') ? ' has-error' : '' }}">
                                                <label class="control-label ">Surname</label>
                                                <input type="text" name="primary_sname" class="form-control validate[required]" />
                                                @if($errors->has('primary_sname'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('primary_sname') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2  {{ $errors->has('primary_email') ? ' has-error' : '' }}">
                                                <label class="control-label">Email</label>
                                                <input type="text" name="primary_email" class="form-control validate[required,custom[email]]" />
                                                @if($errors->has('primary_email'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('primary_email') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="col-md-2  {{ $errors->has('cc_primary_email') ? ' has-error' : '' }}">
                                                <label class="control-label">CC Email</label>
                                                <input type="text" name="cc_primary_email" class="form-control validate[required,custom[email]]" />
                                                @if($errors->has('primary_email'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('cc_primary_email') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="col-md-2  {{ $errors->has('primary_language') ? ' has-error' : '' }}">
                                                <label class="control-label">Preferred Language</label>
                                                <input type="text" name="primary_language" class="form-control" />
                                                @if($errors->has('primary_language'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('primary_language') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2  {{ $errors->has('primary_badge') ? ' has-error' : '' }}">
                                                <label class="control-label">Badge/Team Name</label>
                                                <select name="primary_badge" class="form-control validate[required]">
                                                    <option value=""> select </option>
                                                    @foreach( $delegate as $list )
                                                        <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('primary_badge'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('primary_badge') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2  {{ $errors->has('complimentary') ? ' has-error' : '' }}">
                                                <label class="control-label">Complimentary</label>
                                                {{--<input type="text" name="complimentary" class="form-control validate[]" />--}}
                                                <select name="complimentary" class="form-control validate[]">
                                                  <option value=""> select </option>
                                                  <option value="1"> Yes</option>
                                                  <option value="0"> No</option>
                                                </select>
                                                @if($errors->has('complimentary'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('complimentary') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="form-group" id="other-delegates" style="display: none;">
                                        <label class="col-md-1 control-label"> Other Delegates </label>
                                        <div class="col-md-11">
                                            <div class="mt-repeater">
                                                <div data-repeater-list="group-b">
                                                    <div data-repeater-item class="row">
                                                        <div class="col-md-2">
                                                            <label class="control-label">First Name</label>
                                                            <input type="text" name="del_fname" class="form-control" /> </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Surname</label>
                                                            <input type="text" name="del_sname" class="form-control" /> </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Email</label>
                                                            <input type="text" name="del_email" class="form-control validate[custom[email]]" /> </div>

                                                        <div class="col-md-2">
                                                            <label class="control-label">CC Email</label>
                                                            <input type="text" name="cc_del_email" class="form-control validate[custom[email]]" /> </div>
                                                        <div class="col-md-2">
                                                                <label class="control-label">Preferred Language</label>
                                                                <input type="text" name="languange" class="form-control" /> </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Badge / Team Name</label>
                                                            <select name="del_badge" class="form-control">
                                                                <option value=""> select </option>
                                                                @foreach( $delegate as $list )
                                                                    <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Complimentary</label>
                                                            <select name="complimentary" class="form-control">
                                                                <option value=""> select </option>
                                                                <option value="1"> Yes</option>
                                                                <option value="0"> No</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger btn-sm pull-right form-control" style="font-size: 14px; margin-top: 27px;">
                                                                <i class="fa fa-close"></i> Remove
                                                            </a>
                                                        </div>

                                                        {{--<div class="col-md-1">--}}
                                                            {{--<label class="control-label">&nbsp;</label>--}}
                                                            {{----}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                <hr>
                                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                    <i class="fa fa-plus"></i> Add Delegate </a>
                                                <br>
                                                <br> </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-1 control-label">
                                        </label>
                                        <div class="col-md-11">
                                            <button type="submit" name="Save" value="save" class="btn btn-success">
                                                <i class="fa fa-check"></i> Save </button>
                                        </div>
                                    </div>
                                </div>

                                {!!Form::close()!!}

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="editDelegation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Delegation </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editDelegationForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Delegation Name </label>
                                <input type="text" class="form-control validate[required]" name="delegation_name" id="delegation_name" >
                            </div>

                            <div class="form-group">
                                <label > Country </label>
                                <div >
                                    <select class="form-control" name="country" id="country">
                                        <option value="">Select...</option>
                                        @foreach( $country as $list )
                                            <option value="{{ $list->code }}"> {{ $list->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Type </label>
                                <div >
                                    <select class="form-control validate[required]" name="delegation_type" id="delegation_type">
                                        <option value="">Select...</option>
                                        @foreach( $delegation_type as $list )
                                            <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Contact Name</label>
                                <div>
                                    <input type="text" name="primary_name" class="form-control validate[required]" id="primary_name"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Contact Email </label>
                                <div>
                                    <input type="text" name="primary_email" class="form-control validate[required,custom[email]]" id="primary_email" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">CC Contact Email </label>
                                <div>
                                    <input type="text" name="cc_primary_email" class="form-control validate[custom[email]]" id="cc_primary_email" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Badge/Team Name</label>
                                <select name="primary_badge" class="form-control validate[required]" id="primary_badge">
                                    <option value=""> select </option>
                                    @foreach( $delegate as $list )
                                        <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Complimentary</label>
                                <select name="complimentary" class="form-control validate[]" id="complimentary">
                                  <option value=""> select </option>
                                  <option value="1"> Yes</option>
                                  <option value="0"> No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')

    <script>
        $('#editDelegation').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editDelegationForm').attr('action', button.data('url'));

            $('#delegation_name').val(button.data('name'));
            $('#country').val(button.data('country'));
            $('#delegation_type').val(button.data('type'));
            $('#primary_name').val(button.data('contact_name'));
            $('#primary_email').val(button.data('contact_email'));
            $('#cc_primary_email').val(button.data('cc_contact_email'));
            $('#primary_language').val(button.data('language'));
            $('#primary_badge').val(button.data('primary_badge'));
            $('#complimentary').val(button.data('complimentary'));
        })
    </script>

    <script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>

    <script>
        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });
    </script>

    <script>
        var initTable10 = function () {
            var table10 = $('#delegation');
            var oTable10 = table10.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event Delegation - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            toastr.info('Event Delegation - reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: " {{ route('org.events.delegations.list',$event->id) }}", //events_programme
                    method: 'POST'
                },
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
                    {data: 'name', name: 'name', orderable: false, searchable: true},
                    {data: 'count_invitations', name: 'count_invitations', orderable: false, searchable: false},
                    {data: 'contact_name', name: 'contact_name', orderable: false, searchable: true},
                    {data: 'contact_email', name: 'contact_email', orderable: false, searchable: true},
                    {data: 'delegationtype.name', name: 'delegationtype.name', defaultContent:'not set',orderable: false, searchable: false},
                    {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                        return data === 0 ?  'enabled' :  'disabled';          }},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });
        }

        initTable10();

        var FormRepeater = function () {

            return {
                //main function to initiate the module
                init: function () {
                    $('.mt-repeater').each(function(){
                        $(this).repeater({
                            show: function () {
                                $(this).slideDown();
                                $('.date-picker').datepicker({
                                    rtl: App.isRTL(),
                                    orientation: "left",
                                    autoclose: true
                                });
                            },

                            hide: function (deleteElement) {
                                if(confirm('Are you sure you want to delete this element?')) {
                                    $(this).slideUp(deleteElement);
                                }
                            },

                            ready: function (setIndexes) {

                            }
                            ,isFirstItemUndeletable: true
                        });
                    });
                }

            };

        }();

        jQuery(document).ready(function() {
            FormRepeater.init();
        });

    </script>

    <script>

        document.getElementById('multiple-2').addEventListener('change', function () {
            var style = this.value == 2 ? 'none' : 'block';
            document.getElementById('other-delegates').style.display = style;
        });

//        function showDiv(elem){
//            if(elem.value == 2)
//                document.getElementById('other-delegates').style.display = "hidden";
//            if(elem.value != 2)
//                document.getElementById('other-delegates').style.display = "block";
//        }
    </script>

    <script>
//        $(function() {
//            $('#multiple-2').change(function(){
//                //$('.colors').hide();
//                //$('#' + $(this).val()).show();
//                if($(this).val() != 2)
//                    ('#other-delegates').show();
//            });
//        });
    </script>

@endsection
