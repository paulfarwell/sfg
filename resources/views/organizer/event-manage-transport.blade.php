<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.sponsors',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Sponsors </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.activities',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Activities </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Transfer Packages -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events transfer packages  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Packages </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-transport">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Pick Up Point</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Fee($)</th>
                                    <th>Images</th>
                                    <th>Tools</th>
                                </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.trans',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Delegate Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required] select2-multiple" name="delegate_id[]" id="multiple" multiple>
                                            <option value="">Select...</option>
                                            @foreach( $delegate_types as $list )
                                                <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('delegate_id'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('delegate_id') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Name :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                        @if($errors->has('name'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Type:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required]" name="t_mode">
                                            {{--<option value="bus"> Bus </option>--}}
                                            <option value="1"> Arrival </option>
                                            <option value="2"> Depature </option>
                                        </select>
                                        @if($errors->has('t_type'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('t_type') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                             </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Mode:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <select class="form-control validate[required]" name="t_type">
                                            {{--<option value="bus"> Bus </option>--}}
                                            <option value="Road"> Road </option>
                                            <option value="Air"> Air </option>
                                            <option value="Water"> Water </option>
                                        </select>
                                        @if($errors->has('t_type'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('t_type') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                             </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Pickup point :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="location" placeholder="">
                                        @if($errors->has('location'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('location') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Price ($):
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required,custom[number]]" name="cost" placeholder="">
                                        @if($errors->has('cost'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('cost') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Transport Dates:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <div class="">
                                            <div class='col-md-5'>
                                                <div class="form-group">
                                                    <label> Valid From</label>
                                                    <div class='input-group date' id='start'>
                                                        <input type='text' name="valid_from" class="form-control validate[required]" />
                                                        <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                        @if($errors->has('valid_from'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('valid_from') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-5'>
                                                <div class="form-group">
                                                    <label> Valid to</label>
                                                    <div class='input-group date' id='end'>
                                                        <input type='text' name="valid_to" class="form-control  validate[required]" />
                                                        <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                        @if($errors->has('valid_to'))
                                                            <span class="alert-danger help-block help-block-error">
                                                                @foreach($errors->get('valid_to') as $error)
                                                                    {!!$error !!}<br/>
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('description',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('notes'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('notes') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editTransportPackages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Transport Packages Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editTransportPackagesForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                    <div class="form-body">

                    <div class="form-group">
                        <label > Delegate Type : </label>
                        <div >
                            <select class="form-control validate[required] select2-multiple-2" id="delegate_id" name="delegate_id[]" id="multiple" multiple>
                                <option value="">Select...</option>
                                @foreach( $delegate_types as $list )
                                    <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Name : </label>
                        <div >
                            <input type="text" value="" class="form-control validate[required]" name="name" id="name" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label> Type : </label>
                        <div >
                            <select class="form-control validate[required]" name="t_type" id="t_mode">
                                <option value="1"> Arrival </option>
                                <option value="2"> Depature </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Mode : </label>
                        <div >
                            <select class="form-control validate[required]" name="t_type" id="t_type">
                                <option value="Road"> Road </option>
                                <option value="Air"> Air </option>
                                <option value="Water"> Water </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Pickup point: </label>
                        <div >
                            <input type="text" value="" class="form-control validate[required]" name="location" id="location" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Cost : </label>
                        <div >
                            <input type="text" value="" class="form-control validate[required]" name="cost" id="cost" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Description: </label>
                        <div >
                            <textarea name="description" class="form-control validate[required]" id="description"> </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Start Date</label>
                        <div class='input-group date' id='start3'>
                            <input type='text' name="valid_from" class="form-control validate[required]" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> End Date</label>
                        <div class='input-group date' id='end3'>
                            <input type='text' name="valid_to" class="form-control validate[required]" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div >
                            <button type="submit" name="Update" value="Update" class="btn btn-success">
                                <i class="fa fa-check"></i> Update </button>
                        </div>
                    </div>

                    </div>
                    </form>

                </div>


            </div>
        </div>
    </div>


@endsection


@section('footer')

    <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    @javascript('event_id',  $event->start )
    @javascript('list_event_trans', route('org.dt.list.trans',array($event->id)))

    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    {{--<script src="{{ asset("assets/events/ui.js") }}"> </script>--}}

    <script>
        initTable7();
    </script>

    <script>

        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });

        $('#editTransportPackages').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editTransportPackagesForm').attr('action', button.data('url'));
            var del = button.data('delegate').toString().split(","); console.log(del);
            var sel =  document.getElementById('delegate_id');
            $('#name').val(button.data('name'));
            $('#location').val(button.data('location'));
            $('#cost').val(button.data('cost'));
            $('#t_type').val(button.data('type'));
            $('#t_mode').val(button.data('mode'));
            $('#description').val(button.data('desc'));

            for ( var j=0; j < sel.length; j++ ) {
                sel[j].selected = false;
            }

            for (index = 0; index < del.length; ++index) {
                for (var i = 0; i < sel.options.length; i++) {
                    console.log(sel.options[i].value); console.log(del[index]);
                    if ( del[index] == sel.options[i].value ) {
                        sel.options[i].selected = true;
                    }
                }
            }

            $('.select2-multiple-2').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
            $('.select2-multiple-2').selectpicker('render');

            $(function () {
                $('#start3').datetimepicker( {
                    defaultDate: button.data('start_date'),
                    useCurrent: false,sideBySide: true
                });
                $('#end3').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    sideBySide: true,
                    defaultDate: button.data('end_date')
                });
                $("#start3").on("dp.change", function (e) {
                    $('#end3').data("DateTimePicker").minDate(e.date);
                });
                $("#end3").on("dp.change", function (e) {
                    $('#start3').data("DateTimePicker").maxDate(e.date);
                });
            });
            $('#description').eq(0).summernote('destroy');
            $('#description').summernote({
                dialogsInBody: true,
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        // Firefox fix
                        setTimeout(function () {
                            document.execCommand('insertText', false, bufferText);
                        }, 10);
                    }
                }
            });
        })
    </script>

    <script>
        $(function () {
            $('#start').datetimepicker( {
                minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(5))) }}",
                {{--defaultDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->start)) }}",
                useCurrent: false,sideBySide: true
            });
            $('#end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true,
                {{--minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->addDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->end)) }}"
            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>

@endsection
