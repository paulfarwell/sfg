<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend-v')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Email Template </span>
            </li>
        </ul>

    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Email Template -  {{ $email->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span> Editing this Email Template </span>
                    </div>
                </div>
                <div class="portlet-body">

                  {!! Form::open(['route' => ['org.email.template.edit', $email->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                  <div class="form-body">

                      <div class="form-group">
                          <label class="col-md-2 control-label"> Name :
                              <span class="required"> * </span>
                          </label>
                          <div class="col-md-10">
                              <input type="text" value="{{ $email->name or null }}" class="form-control validate[required]" name="name" placeholder="">
                              @if($errors->has('name'))
                                  <span class="alert-danger help-block help-block-error">
                                              @foreach($errors->get('name') as $error)
                                          {!!$error !!}<br/>
                                      @endforeach
                                          </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-2 control-label"> Email Body:
                              <span class="required"> * </span>
                          </label>
                          <div class="col-md-10">
                              <!-- {!! Form::textarea('content',$email->content, ['class'=>'form-control tinyvariable  validate[required]', 'placeholder'=>'Email Body']) !!}-->
                              <textarea name="content" class='form-control tinyvariable' >
                                      {!! $email->content !!}
                              </textarea>
                              @if($errors->has('content'))
                                  <span class="alert-danger help-block help-block-error">
                                      @foreach($errors->get('content') as $error)
                                          {!!$error !!}<br/>
                                      @endforeach
                                          </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-md-2 control-label">Status :</label>
                          <div class="col-md-10">
                            <select id="status" name="status" class="form-control ">
                             <option value="0">Active</option>
                             <option value="1">Deactivate</option>
                            </select>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-md-2 control-label">
                          </label>
                          <div class="col-md-10">
                              <button type="submit" name="Save" value="save" class="btn btn-success">
                                  <i class="fa fa-check"></i> Save </button>
                          </div>
                      </div>

                  </div>
                  {!!Form::close()!!}

                </div>
            </div>

        </div>

    </div>

@endsection


@section('footer')



@endsection
