<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Create new Event Tickets </span>
            </li>
        </ul>
        <div class="page-toolbar">
            {{--<div class="btn-group pull-right">--}}
            {{--<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Quick Actions--}}
            {{--<i class="fa fa-angle-down"></i>--}}
            {{--</button>--}}
            {{--<ul class="dropdown-menu pull-right" role="menu">--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<i class="icon-bell"></i> Action</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<i class="icon-shield"></i> Another action</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<i class="icon-user"></i> Something else here</a>--}}
            {{--</li>--}}
            {{--<li class="divider"> </li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<i class="icon-bag"></i> Separated link</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Tickets
        <small> set new event tickets parameters </small>
    </h1>
    <!-- END PAGE TITLE-->

    {!! Form::open(['route' => ['org.new.ticket.bulk',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}

    <div class="form-body">

    @foreach($delegate_types as $ls)

        <hr>
         <h4> Ticket for delegate type - {{ $ls->name }} </h4>
        <hr>

            <div class="form-group">
                <label class="col-md-2 control-label"> Delegate Type:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-10">
                    <select class="form-control validate[required]" name="delegate_id[]">
                        <option value="{{ $ls->id }}"> {{ $ls->name }}</option>
                    </select>
                    @if($errors->has('delegate_id'))
                        <span class="alert-danger help-block help-block-error">
                            @foreach($errors->get('delegate_id') as $error)
                                {!!$error !!}<br/>
                            @endforeach
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label"> Price ($):
                    <span class="required"> * </span>
                </label>
                <div class="col-md-10">
                    <input type="text" value="" class="form-control validate[required]" name="price[]" placeholder="">
                    @if($errors->has('price'))
                        <span class="alert-danger help-block help-block-error">
                            @foreach($errors->get('price') as $error)
                                {!!$error !!}<br/>
                            @endforeach
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label"> Validity Dates:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-10">
                    <div class="">
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='start11'>
                                    <input type='text' value="{{ $event->start }}" name="valid_from[]" class="form-control validate[required]" />
                                    <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                    @if($errors->has('valid_from'))
                                        <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('valid_from') as $error)
                                                {!!$error !!}<br/>
                                            @endforeach
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='end11'>
                                    <input type='text' value="{{ $event->start }}" name="valid_to[]" class="form-control  validate[required]" />
                                    <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        @if($errors->has('valid_to'))
                                            <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('valid_to') as $error)
                                                {!!$error !!}<br/>
                                            @endforeach
                                            </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label"> Instructions:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-10">
                    {!! Form::textarea('notes[]',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                    @if($errors->has('notes'))
                        <span class="alert-danger help-block help-block-error">
                            @foreach($errors->get('notes') as $error)
                                {!!$error !!}<br/>
                            @endforeach
                        </span>
                    @endif
                </div>
            </div>

            <hr>

    @endforeach

        <div class="form-group">
            <label class="col-md-2 control-label">
            </label>
            <div class="col-md-10">
                <button type="submit" name="Save" value="save" class="btn btn-success">
                    <i class="fa fa-check"></i> Save </button>
            </div>
        </div>

    </div>
    {!!Form::close()!!}

@endsection


@section('footer')

@endsection