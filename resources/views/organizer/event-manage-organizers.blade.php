<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.news',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event News </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.sponsors',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Sponsors </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event Organizers -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Organizers  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Organizers </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-org">
                                <thead>
                                <tr>
                                    <td>Logo</td>
                                    <td>Name</td>
                                    <td>Website</td>
                                    <td>Brief</td>
                                    <td>Status</td>
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>


                        </div>

                        <div class="tab-pane " id="tab_5_2">


                            {!! Form::open(['route' => ['org.new.organizer',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Name :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                        @if($errors->has('name'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Logo :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="file" value="" class="form-control validate[required]" name="file" placeholder="">
                                        @if($errors->has('file'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('file') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Website :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="http://" class="form-control validate[required]" name="website" placeholder="">
                                        @if($errors->has('website'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('website') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Brief:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('brief',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('brief'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('brief') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}


                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editSponsors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Sponsor Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editSponsorsForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                    <div class="form-body">

                    <div class="form-group">
                        <label> Name : </label>
                        <div >
                            <input type="text" value="" class="form-control validate[required]" name="name" id="name" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Logo : </label>
                        <div >
                            <input type="file" value="" class="form-control validate[]" name="file" placeholder="">
                            <small> upload a new image to change the logo below </small>
                            <img src="" id="cur_img" class="img-responsive">
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Website : </label>
                        <div >
                            <input type="text" value="" class="form-control validate[required]" name="website" id="website" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label > Description: </label>
                        <div >
                            <textarea name="brief" class="form-control validate[required]" id="description"> </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div >
                            <button type="submit" name="Update" value="Update" class="btn btn-success">
                                <i class="fa fa-check"></i> Update </button>
                        </div>
                    </div>

                    </div>
                    </form>

                </div>


            </div>
        </div>
    </div>


@endsection


@section('footer')

    {{--<link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />--}}
    {{--<script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}

    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    @javascript('event_id',  $event->start )
    @javascript('list_event_organizers', route('org.dt.list.org',array($event->id)))

    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    {{--<script src="{{ asset("assets/events/ui.js") }}"> </script>--}}

    <script>
        initTable9();
    </script>

    <script>

        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });

        $('#editSponsors').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editSponsorsForm').attr('action', button.data('url'));
            $('#cur_img').attr('src', button.data('logo'));
            //var del = button.data('delegate').toString().split(","); console.log(del);
            //var sel =  document.getElementById('delegate_id');
            $('#name').val(button.data('name'));
            $('#website').val(button.data('website'));
            $('#cost').val(button.data('cost'));

            $('#description').val(button.data('desc'));
            $('#description').eq(0).summernote('destroy');
            $('#description').summernote({
                dialogsInBody: true,
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        // Firefox fix
                        setTimeout(function () {
                            document.execCommand('insertText', false, bufferText);
                        }, 10);
                    }
                }
            });
        })
    </script>

    <script>
        $(function () {
            $('#start').datetimepicker( {
                minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(5))) }}",
                {{--defaultDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->subDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->start)) }}",
                useCurrent: false,sideBySide: true
            });
            $('#end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                sideBySide: true,
                {{--minDate: "{{ date("m/d/Y h:i A",strtotime(Carbon\Carbon::parse($event->start)->addDays(10))) }}",--}}
                defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->end)) }}"
            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>

@endsection


