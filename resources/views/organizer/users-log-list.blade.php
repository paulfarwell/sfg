<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> User Logs
        <small> view system activities by users </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                        <thead>
                        <tr role="row" class="heading">
                            {{--<th></th>--}}
                            <th>User</th>
                            <th>Method</th>
                            <th>Ip</th>
                            <th>url</th>
                            <th>Record_id</th>
                            <th>Action</th>
                            <th>Model</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
                </div>
            </div>

    </div>

@endsection


@section('footer')

    <script>
        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Users Logs List - Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Users Logs List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Users Logs List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Users Logs List - Data export',
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Users Logs List - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Users Logs List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.user.logs') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'causer.name', name: 'causer.name', defaultContent:'not set', orderable: false, searchable: true},
                {data: 'method', name: 'method', orderable: false, searchable: true},
                {data: 'ip_address', name: 'ip_address', orderable: false, searchable: true},
                {data: 'url', name: 'true', orderable: false, searchable: true},
                {data: 'subject_id', name: 'subject_id', orderable: false, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: true},
                {data: 'subject_type', name: 'subject_type', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            ]
        });
    </script>

@endsection
