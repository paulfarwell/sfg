<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Event Hotels
        <small> view event hotels created </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filter</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" id="search-form" class="form-inline" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email"> Event Name </label>
                            <select name="event_id" id="event_id" class="form-control validate[required]">
                                <option value=""> </option>
                                @foreach( $events as $list)
                                    <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                </div>
            </div>

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="tbl1">
                        <thead>
                        <tr>
                            <td></td>
                            <td>Name</td>
                            <td>Event</td>
                            <td>Delegates</td>
                            <td>Address</td>
                            <td>Contacts</td>
                            <td>Available</td>
                            {{--<td>Images </td>--}}
                            <td>Tools</td>
                        </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>


@endsection


@section('footer')

    @include('partial.hotels-mgt')

    <script>
        function format ( d ) {
            var tbl = '' +
                '<h4>Images</h4> <table cellpadding="5" class="table table-bordered" cellspacing="0" border="0" style="padding-left:50px;">'+
                '<tr>'+
                '<th><strong> Image </strong> </th>'+
                '</tr>';
            for (i = 0; i < d.images.length; i++) {
                tbl  += '<tr>'+
                    '<td> <a href="#" data-toggle="modal" data-target="#myImgModal" data-img="'+ d.images[i].image_urls +'"> <img src="'+ d.images[i].image_urls +'" width="200" > </a>  </td>'+
                    '</tr>';
            }
            return tbl += '</table>';
        }

        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Hotels List - Data export',
                    messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Hotels List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Hotels List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Hotels List - Data export',
                    exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Hotels List - Data export',messageTop: export_top,
                    exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Hotels List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.hotels',[null]) !!}',
                method: 'POST',
                data: function (d) {
                    d.event_id = $('select[name=event_id]').val();
                }
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '', searchable: false
                },
                {data: 'hotel.name', name: 'hotel.name', orderable: false, searchable: true},
                {data: 'event.name', name: 'event.name', orderable: false, searchable: true},
                {data: 'delegates', name: 'delegates', orderable: false, searchable: false},
                {data: 'hotel.address', name: 'hotel.address', orderable: false, searchable: true},
                {data: 'contacts', name: 'contacts', orderable: false, searchable: false},
                {data: 'available', name: 'available', orderable: false, searchable: false},
                //{data: 'all-images', name: 'all-images', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ],
        });

        $('#search-form').on('submit', function(e) {
            oTable1.draw();
            e.preventDefault();
        });

        $('#tbl1 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable1.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    </script>

@endsection
