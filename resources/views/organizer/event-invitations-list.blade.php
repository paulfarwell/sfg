<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Delegate Invitations
        <small> invitation information for each delegates </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Filter</h3>
              </div>
              <div class="panel-body">
                  <form method="POST" id="search-form" class="form-inline" role="form">
                      {{ csrf_field() }}
                      <div class="form-group">
                          <label for="email"> Event Name </label>
                          <select name="event_id" id="event_id" class="form-control validate[required]">
                              <option value=""> </option>
                              @foreach( $events as $list)
                                  <option value="{{ $list->id }}"> {{ $list->name }} </option>
                              @endforeach
                          </select>
                      </div>
                      <button type="submit" class="btn btn-primary">Search</button>
                  </form>
              </div>
          </div>
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                        <thead>
                        <tr role="row" class="heading">
                            <th>Photo</th>
                            <th>Event</th>
                            <th> Delegation </th>
                            <th> Badge /Team </th>
                            <th> Country</th>
                            <th> Code</th>
                            <th> First Name</th>
                            <th> Surname </th>
                            <th> Organisation</th>
                            <th> Position </th>
                            <th> National ID / Passport Number  </th>
                            <th> Email</th>
                            <th> Cell Phone Code</th>
                            <th> Cell Number</th>
                            <th> Office Phone Code</th>
                            <th> Cell Office</th>
                            <th> Emergency Contact Name</th>
                            <th> Emergency Contact Email</th>
                            <th> Emergency Contact Phone Code </th>
                            <th> Emergency Contact Telephone</th>
                            <th> Insurance Membership Number</th>
                            <th> Insurance Email</th>
                            <th> Insurance Phone Code</th>
                            <th> Insurance Phone Telephone</th>
                              <th> Status</th>
                            <th> Tools</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>


    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl1').DataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Delegates invitation lists -  Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Delegates invitation lists -  Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Delegates invitation lists -  Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Delegates invitation lists -  Data export',
                    exportOptions: {
                      columns: [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 ]
                    }
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Delegates invitation lists -  Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Delegates invitation lists reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "columnDefs": [
                {
                    "targets": [ 8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 ],
                    "visible": false,
                    "searchable": false
                },



            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.events.delegations.invitations.list',[null]) !!}',
                method: 'POST',
                data: function (d) {
                    d.event_id = $('select[name=event_id]').val();
                }
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [

                {data: 'passport_photo', name: 'passport_photo', orderable: false, searchable: false},
                {data: 'event.name', name: 'event.name', orderable: false, searchable: false},
                {data: 'delegation.name', name: 'delegation.name', orderable: false,defaultContent:'no set', searchable: false},
                {data: 'delegate_type.color', name: 'delegate_type.color', orderable: false, searchable: false},
                {data: 'delegation.countryname.name', name: 'delegation.countryname.name',defaultContent:'no set', orderable: false, searchable: false},
                {data: 'code', name: 'code', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'profile.fname', name: 'profile.fname',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.surname', name: 'profile.surname',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.organisation', name: 'profile.organisation',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.position', name: 'profile.position',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.national_id', name: 'profile.national_id',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.email', name: 'profile.email',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.cell_phone_code', name: 'profile.cell_phone_code',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.cell_phone', name: 'profile.cell_phone',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.office_phone_code', name: 'profile.office_phone_code',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.cell_office', name: 'profile.cell_office',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.a_contact_name', name: 'profile.a_contact_name',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.a_contact_email', name: 'profile.a_contact_email',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.a_contact_phone_code', name: 'profile.a_contact_phone_code',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.a_contact_telephone', name: 'profile.a_contact_telephone',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.membership_number', name: 'profile.membership_number',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.membership_email', name: 'profile.membership_email',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.membership_phone_code', name: 'profile.membership_phone_code',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'profile.membership_telephone', name: 'profile.membership_telephone',defaultContent:'no set', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, row) {
                    return data === 0 ?  'enabled' :  'disabled';          }},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ],
        });
        $('#search-form').on('submit', function(e) {
                console.log($('select[name=event_id]').val());
            oTable1.draw();
            e.preventDefault();
        });

        $('#tbl1 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = oTable1.row(tr);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    </script>


@endsection
