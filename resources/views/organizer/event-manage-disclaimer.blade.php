<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.activities',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Activities </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>
            <a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event disclaimers & Conditions -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events Disclaimers  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Disclaimers  </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-hotels">
                                <thead>
                                <tr>
                                    <td>Description</td>
                                    <td>Event</td>
                                    <td>Status</td>
                                    <!-- <td>Order</td> -->
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($disclaimers as $disclaimer)
                                <tr>
                                 <td>{!!$disclaimer->description!!}</td>
                                 <td>{{$disclaimer->event->name}}</td>
                                 <td>{{$disclaimer->status}}</td>
                                 <td><div class='btn-group '>
                                 <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                                 aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                                 </button>
                                 <ul class='dropdown-menu pull-right' role='menu'>
                                     <li><a href='#'  data-url='{{route('org.edit.event.disclaimer', [$disclaimer->id,$disclaimer->event_id])}}' data-desc='{{$disclaimer->description}}'
                                     data-status='{{$disclaimer->status}}' data-toggle="modal" data-target="#editdisclaimers" > Edit disclaimers </a></li>
                                     <!-- <li><a href='{{route('org.new.event.bulk.img', [$disclaimer->event_id,$disclaimer->id,'event_tent'])}}'  > Delete </a></li> -->
                                 </ul>
                                 </div></td>
                                 </tr>
                                 @endforeach

                                 </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.disclaimer',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('description',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('description'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('description') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>





@endsection


@section('footer')

    @javascript('event_id',  $event->start )


    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    <script src="{{ asset("assets/events/ui.js") }}"> </script>

    <script>
        initTable3();
    </script>

    @include('partial.disclaimer-mgt')

@endsection
