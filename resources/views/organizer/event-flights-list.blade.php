<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Delegate Flight Information
        <small> Flight information from the delegates </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Filter</h3>
              </div>
              <div class="panel-body">
                  <form method="POST" id="search-form" class="form-inline" role="form">
                      {{ csrf_field() }}
                      <div class="form-group">
                          <label for="email"> Event Name </label>
                          <select name="event_id" id="event_id" class="form-control validate[required]">
                              <option value=""> </option>
                              @foreach( $events as $list)
                                  <option value="{{ $list->id }}"> {{ $list->name }} </option>
                              @endforeach
                          </select>
                      </div>
                      <button type="submit" class="btn btn-primary">Search</button>
                  </form>
              </div>
          </div>

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption ">

                    </div>
                    <ul class="nav nav-tabs" style="float: left;">
                        <li class="active">
                            <a href="#list_delegations" data-toggle="tab" aria-expanded="true"> Incoming Flight List </a>
                        </li>
                        <li class="">
                            <a href="#create_delegations" data-toggle="tab" aria-expanded="true"> Outgoing Flight List </a>
                        </li>
                    </ul>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">
                        <div class="tab-pane active" id="list_delegations" >
                            <table width="100%" class="table table-striped table-bordered table-hover" id="tbl11">
                                <thead>
                                <tr role="row" class="heading">
                                    <th>Firstname</th>
                                    <th>Surname</th>
                                    <th>Badge</th>
                                    <th>Type</th>
                                    <th>Number</th>
                                    <th>Carrier</th>
                                    <th>Airport</th>
                                    <th>Terminal</th>
                                    <th>Arrival Dates - Time</th>
                                    <th>Plane Reg</th>
                                    <th>Transfer Req</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="create_delegations" >
                            <table width="100%" class="table table-striped table-bordered table-hover" id="tbl12">
                                <thead>
                                <tr role="row" class="heading">
                                    <th>Firstname</th>
                                    <th>Surname</th>
                                    <th>Badge</th>
                                    <th>Type</th>
                                    <th>Number</th>
                                    <th>Carrier</th>
                                    <th>Airport</th>
                                    <th>Terminal</th>
                                    <th>Departure Date - Time</th>
                                    <th>Plane Reg</th>
                                    <th>Transfer Req</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>


    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl11').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Events Incoming Flight List - Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Events Incoming Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Events  Incoming Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Events  Incoming Flight - Data export',
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Events Incoming Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Countries List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.list','in') !!}',
                method: 'POST',
                data: function (d) {
                    d.event_id = $('select[name=event_id]').val();
                }

            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'invitation.profile.fname', name: 'invitation.profile.fname',defaultContent:'', orderable: false, searchable: true},
                {data: 'invitation.profile.surname', name: 'invitation.profile.surname',defaultContent:'', orderable: false, searchable: true},
                {data: 'invitation.delegate_type.color', name: 'invitation.delegate_type.color', orderable: false,defaultContent:'no set', searchable: false},
                {data: 'flight_type', name: 'flight_type', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'flight_number', name: 'flight_number', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'flight_carrier', name: 'flight_carrier', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'airport_name', name: 'airport_name', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'terminal', name: 'terminal', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'arrival_date', name: 'arrival_date', orderable: false,defaultContent:'no set', searchable: false, render: function (data, type, row) {
                    return  data + ' - ' + row.arrival_time ;  }},
                {data: 'plane_reg', name: 'plane_reg', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'transfer_req', name: 'transfer_req', orderable: false,defaultContent:'no set', searchable: true},
            ],
        });
    </script>
    <script>
    var oTable1 = $('#tbl12').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Events Outgoing Flight List - Data export',
                    messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Events Outgoing Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Events Outgoing Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Events Outgoing Flight - Data export',
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Events Outgoing Flight - Data export',messageTop: export_top,
//                    exportOptions: {columns: [0, 1, 2, 3,4,5]}
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        // alert('Users Logs List reloaded!');
                        toastr.info('Countries List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.list','out') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12 pull-left'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'invitation.profile.fname', name: 'invitation.profile.fname',defaultContent:'', orderable: false, searchable: true},
                {data: 'invitation.profile.surname', name: 'invitation.profile.surname',defaultContent:'', orderable: false, searchable: true},
                {data: 'invitation.delegate_type.color', name: 'invitation.delegate_type.color', orderable: false,defaultContent:'no set', searchable: false},
                {data: 'flight_type', name: 'flight_type', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'flight_number', name: 'flight_number', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'flight_carrier', name: 'flight_carrier', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'airport_name', name: 'airport_name', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'terminal', name: 'terminal', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'departure_date', name: 'departure_date', orderable: false,defaultContent:'no set', searchable: false, render: function (data, type, row) {
                    return  data + ' - ' + row.departure_time ;  }},
                {data: 'plane_reg', name: 'plane_reg', orderable: false,defaultContent:'no set', searchable: true},
                {data: 'transfer_req', name: 'transfer_req', orderable: false,defaultContent:'no set', searchable: true},
            ],
        });

        $('#search-form').on('submit', function(e) {
            oTable1.draw();
            e.preventDefault();
        });
    </script>

@endsection
