<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event Delegations   </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Event Delegations Uploads ({{ $event->name or 'set event name' }})
        <small> documents to be sent with every delegation type </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                {{--<div class="caption font-dark">--}}

                {{--</div>--}}
                {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body form">
                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Uploads </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table id="delegation_upload" width="100%" class="table table-bordered table table-striped table-bordered table-hover">
                                    <thead>
                                    <th> Name</th>
                                    <th> Description</th>
                                    <th> Delegation type</th>
                                    <th> File </th>
                                    <th> Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane form" id="tab_5_2">

                                {!! Form::open(['route' => ['org.events.delegations.uploads',$event->id], 'class' => 'form-horizontal form-row-seperated', 'files' => true]) !!}

                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Name </label>
                                        <div class="col-md-10">
                                                <input type="text" class="form-control validate[required]" name="name" >
                                            @if($errors->has('name'))
                                                <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('name') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Delegation Type </label>
                                        <div class="col-md-10">
                                            <select class="form-control validate[required] select2-multiple"  id="multiple-2" name="delegation_id">
                                                <option value="">Select...</option>
                                                @foreach( $delegation_type as $list )
                                                    <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('delegation_id'))
                                                <span class="alert-danger help-block help-block-error">
                                                    @foreach($errors->get('delegation_id') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Description </label>
                                        <div class="col-md-10">
                                                {{--<input type="text" class="form-control validate[required]" name="delegation_name" >--}}
                                                <textarea name="description" id="description" class="description tinymce validate[required]" ></textarea>
                                            @if($errors->has('description'))
                                                <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('description') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Upload </label>
                                        <div class="col-md-10">
                                                <input type="file" class="form-control validate[required]" name="file" >
                                            @if($errors->has('file'))
                                                <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('file') as $error)
                                                        {!!$error !!}<br/>
                                                    @endforeach
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                        </label>
                                        <div class="col-md-10">
                                            <button type="submit" name="Save" value="save" class="btn btn-success">
                                                <i class="fa fa-check"></i> Save </button>
                                        </div>
                                    </div>
                                </div>

                                {!!Form::close()!!}

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="editUploadDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Delegation Upload </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editUploadDetailsForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label > Name : </label>
                                <input type="text" class="form-control validate[required]" name="name" id="name">
                            </div>

                            <div class="form-group">
                                <label > Category : </label>
                                <div >
                                    <select class="form-control validate[required]"  id="delegation_id" name="delegation_id">
                                        <option value="">Select...</option>
                                        @foreach( $delegation_type as $list )
                                            <option value="{{ $list->id }}"> {{ $list->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Description: </label>
                                <div >
                                    <textarea name="description" id="description1" class="description1" ></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Upload </label>
                                <div > <small> leave this blank to keep the previously attached file (<span id="oldfile"></span>) </small>
                                    <input type="file" class="form-control" name="file" id="file">
                                </div>
                            </div>

                            <div class="form-group">
                                <div >
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')

    <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

    <script>
        $('#description').summernote({
            dialogsInBody: true
        });

        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });
    </script>

    <script>
        $('#editUploadDetails').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editUploadDetailsForm').attr('action', button.data('url'));

            $('#name').val(button.data('name'));
            $('#delegation_id').val(button.data('delegation_id'));
            $('#oldfile').text(button.data('oldfile'));

            $('#description1').val(button.data('desc'));
            $('#description1').eq(0).summernote('destroy');
            $('#description1').summernote({
                dialogsInBody: true
            });
        })
    </script>

    <script>
        var initTable10 = function () {
            var table10 = $('#delegation_upload');
            var oTable10 = table10.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event Delegation - Data export' ,exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            toastr.info('Event Delegation upload - reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: " {{ route('org.events.upload.delegations.list',$event->id) }}", //events_programme
                    method: 'POST'
                },
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
                    {data: 'name', name: 'name', orderable: false, searchable: true},
                    {data: 'description', name: 'description', orderable: false, searchable: false},
                    {data: 'delegationtype.name', name: 'delegationtype.name', orderable: false, searchable: true},
                    {data: 'file', name: 'file', orderable: false, searchable: true},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });
        }
        initTable10();
    </script>

@endsection