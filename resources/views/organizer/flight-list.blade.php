<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('header')
    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Events </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Flight Information (IATA)
        <small> manage the different Flight details for delegates invitation form </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-dark">--}}

                    {{--</div>--}}
                    {{--<div class="tools"> </div>--}}
                {{--</div>--}}
                <div class="portlet-body">
                    <div class="tabbable-line">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Airports </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Airlines </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_3" data-toggle="tab" aria-expanded="false"> Aircrafts </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_4" data-toggle="tab" aria-expanded="false"> Routes </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl1">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_2">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl2">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_3">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl3">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_5_4">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="tbl4">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th>Flight Number</th>
                                        <th>Departure</th>
                                        <th>Arrival</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>

                    </div>

                </div>
            </div>
        </div>


    </div>
    </div>


@endsection


@section('footer')
    <script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js') }} " type="text/javascript"></script>

    <script>
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

        var oTable1 = $('#tbl1').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Airports List - Data export',
                    messageTop: export_top,
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Airports List  - Data export',messageTop: export_top,
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Airports List - Data export',messageTop: export_top,
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Airports List  - Data export',
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Airports List - Data export',messageTop: export_top,
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        toastr.info('Airports Logs List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.airports') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
                {data: 'updated_at', name: 'updated_at', orderable: false, searchable: true},
            ],
        });

        var oTable2 = $('#tbl2').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Airlines List - Data export',
                    messageTop: export_top,
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Airlines List  - Data export',messageTop: export_top,
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Airlines List - Data export',messageTop: export_top,
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Airlines List  - Data export',
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Airlines List - Data export',messageTop: export_top,
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        toastr.info('Airlines Logs List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.Airlines') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
                {data: 'updated_at', name: 'updated_at', orderable: false, searchable: true},
            ],
        });

        var oTable3 = $('#tbl3').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Aircrafts List - Data export',
                    messageTop: export_top,
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Aircrafts List  - Data export',messageTop: export_top,
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Aircrafts List - Data export',messageTop: export_top,
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Aircrafts List  - Data export',
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Aircrafts List - Data export',messageTop: export_top,
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        toastr.info('Aircrafts Logs List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.Aircrafts') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'code', name: 'code', orderable: false, searchable: true},
                {data: 'name', name: 'name', orderable: false, searchable: true},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
                {data: 'updated_at', name: 'updated_at', orderable: false, searchable: true},
            ],
        });

        var oTable4 = $('#tbl4').dataTable({
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    title: 'Routes List - Data export',
                    messageTop: export_top,
                },
                {
                    extend: 'copy',
                    className: 'btn red btn-outline',
                    title: 'Routes List  - Data export',messageTop: export_top,
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    title: 'Routes List - Data export',messageTop: export_top,
                },
                {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',messageTop: export_top,
                    title: 'Routes List  - Data export',
                },
                {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    title: 'Routes List - Data export',messageTop: export_top,
                },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                        toastr.info('Routes Logs List reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20, 25, -1],
                [5, 10, 15, 20, 25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: '{!! route('org.dt.list.flight.Routes') !!}',
                method: 'POST'
            },
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'flight_number', name: 'flight_number', orderable: false, searchable: true, },
                {data: 'dept.name', name: 'dept.name', orderable: false, searchable: true, "render": function ( data, type, full, meta ) {
                    //console.log(data);
                    return data === undefined ?  full.departure :  data;
                }},
                {data: 'arrv.name', name: 'arrv.name',orderable: false, searchable: true,"render": function ( data, type, full, meta ) {
                    return data === undefined ?  full.arrival :  data;
                }},
                {data: 'created_at', name: 'created_at', orderable: false, searchable: true},
                {data: 'updated_at', name: 'updated_at', orderable: false, searchable: true},
            ],
        });


    </script>

@endsection
