<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

<style>
    .carousel-control.left, .carousel-control.right {
        background: none !important;
        color: saddlebrown;
        background-image: none !important;
    }

    .carousel-control {
        width: 0% !important;
    }

    .carousel-inner {
        padding-right: 15px;
        padding-left: 15px;
    }
</style>

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}"> home </a>
                @else
                    <a href="{{ route('org.index')  }}"> home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Event Management </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events') }}"   class="pull-right btn green btn-sm"   > All Events </a>--}}
            <a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"> Events
                Delegations </a>
            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> {{ $event->name }} Overview
        <small> overview of settings of the selected event</small>
        {{--<a href="{{ route('org.new.event') }}" class="confirm-action  pull-right btn green btn-sm btn-outline"--}}
        {{--msg="Are you sure you want to create a new event, we can only have one active event at a time !!"--}}
        {{--data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'--}}
        {{--> Create New Event </a>--}}

    </h1>
    <!-- END PAGE TITLE-->

    <style>
        #tab_5_1 .row .portlet {
            height: 450px;
        }

        .row .portlet h5 {
            border-bottom: solid 1px black;
            padding-bottom: 5px;
        }
    </style>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light " style="height: auto !important;">
                <div class="portlet-body" style="height: auto !important;">

                    <div class="tabbable-line" style="height: auto !important;">

                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" > Manage Event </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" > Financials </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_3" data-toggle="tab" > Search Engine </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_4" data-toggle="tab" > Reports </a>
                            </li>
                        </ul>

                        <div class="tab-content" style="height: auto !important;">
                            <div class="tab-pane active" id="tab_5_1">

                                <div class="row">

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Basics Event Information </span>
                                                </div>
                                                <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm btn-outline ">
                                                    Edit Event </a>
                                            </div>
                                            <div class="portlet-body">
                                                <h5> Event Map </h5>
                                                <div class="map_canvas" style="width:auto; height:200px">
                                                    <input name="venue" type="hidden" value="{{ $event->venue or null}}" id="geocomplete"
                                                           class="form-control validate[required]" placeholder="">
                                                    <input name="lat" type="hidden" value="{{  ((explode(',', $event->coordinates))[0]) or null}}"
                                                           class="form-control validate[required]">
                                                    <input name="lng" type="hidden" value="{{ ((explode(',', $event->coordinates))[1]) or null}}"
                                                           class="form-control validate[required]">
                                                </div>
                                                <hr>
                                                <h5> Social Info e.g. videos | {{ $event->meta()->count() }} added | <a
                                                            href="{{ route('org.edit.event.meta',$event->id) }}"
                                                            class=" btn green btn-sm btn-outline "> Manage </a>
                                                </h5>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Event Images </span>
                                                </div>
                                                @if ($event->poster && $event->poster->count() > 0 )
                                                    <a href="{{ route('org.manage.event.images',$event->id) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Images </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->poster && $event->poster->count() > 0 )
                                                    <h5> Current Images </h5>
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->poster as $ls)
                                                                <div class="item @if ($i == 0) active @endif">
                                                                    <img src="{{ url($ls->image_path) }}" alt="Los Angeles" class="img-responsive"
                                                                         style="width:100%;">
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>

                                                @else
                                                    <h5> you have NOT uploaded any |
                                                        <a href="{{ route('org.manage.event.images',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Images </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Event Programme </span>
                                                </div>
                                                @if ($event->programmes && $event->programmes->count() > 0 )
                                                    <a href="{{ route('org.manage.event.prog',$event->id) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Programme </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">
                                                @if ($event->programmes && $event->programmes->count() > 0 )
                                                    <h5> Current Programme </h5>
                                                    <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->programmes as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->title !!} </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->description, $limit = 500, $end = '...')  !!}--}}
                                                                        <div class="" style="height: 300px; overflow-y: auto">
                                                                            {!! $ls->description !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.prog',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Programme </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Attendance Packages </span>
                                                </div>
                                                @if ($event->tickets && $event->tickets->count() > 0 )
                                                    <a href="{{ route('org.manage.event.tickets',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Tickets </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">
                                                @if ($event->tickets && $event->tickets->count() > 0 )
                                                    <h5> Available Tickets </h5>
                                                    <div id="myCarousel3" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->tickets as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->name !!} </strong>
                                                                        <br>
                                                                        Delegate type - {!! $ls->delegates->name !!} <br>
                                                                        Badge color - {{ $ls->delegates->color }} <br>
                                                                        Price - $ {{ number_format($ls->price) }}
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel3" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel3" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    @if ($missing_tickets->count() > 0)
                                                        <a class="pull-right btn green btn-sm btn-outline "
                                                           href="{{ route('org.manage.event.tickets',$event->id ) }}">
                                                            Manage Tickets </a>

                                                        <h5> Ticket for Delegate Type |
                                                        </h5>

                                                        ({{$missing_tickets->count()}}) Delegate types without tickets
                                                        <br>
                                                        <div class="" style="height: 90px; overflow-y: auto">
                                                            <ol>
                                                                @foreach($missing_tickets as $ls)
                                                                    <li> {{$ls->name}} - {{$ls->color}}</li>
                                                                @endforeach
                                                            </ol>
                                                        </div>
                                                    @else
                                                    @endif
                                                @else
                                                    <a href="{{ route('org.manage.event.tickets',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Tickets </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Event Hotels </span>
                                                </div>
                                                @if ($event->hotels && $event->hotels->count() > 0 )
                                                    <a href="{{ route('org.manage.event.hotels',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Hotels </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->hotels && $event->hotels->count() > 0 )
                                                    <h5> Available Hotels </h5>
                                                    <div id="myCarousel4" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                             @foreach($event->hotels as $ls)
                                                               @if($ls->hotel === NULL)

                                                               @else
                                                              <div class="item @if ($i == 0) active @endif ">
                                                                  <div class="truncate header-text text-center">
                                                                      <strong>{!! $ls->hotel->name !!} </strong>
                                                                      <br>
                                                                      {{--{!! str_limit($ls->hotel->description, $limit = 300, $end = '...') !!}--}}
                                                                      <div class="" style="height: 150px; overflow-y: auto">
                                                                          {!! $ls->hotel->description !!}
                                                                      </div>
                                                                      <div class="" style="height: 100px; overflow-y: auto">
                                                                          @if ($ls->delegate )
                                                                              <strong> For Delegates </strong>
                                                                              <ol>
                                                                                  @foreach($ls->delegate as $ls1)
                                                                                      <li> {{ $ls1->type->name }} </li>
                                                                                  @endforeach
                                                                              </ol>
                                                                          @endif
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              @endif
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel4" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel4" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.prog',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Programme </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Event Activities </span>
                                                </div>
                                                @if ($event->activities && $event->activities->count() > 0 )
                                                    <a href="{{ route('org.manage.event.activities',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Activities </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->activities && $event->activities->count() > 0 )
                                                    <h5> Available Activities </h5>
                                                    <div id="myCarousel5" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->activities as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->name !!} </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->description, $limit = 300, $end = '...') !!}--}}
                                                                        <div class="" style="height: 150px; overflow-y: auto">
                                                                            {!! $ls->description !!}
                                                                        </div>>
                                                                        <div class="" style="height: 100px; overflow-y: auto">
                                                                            {{--{{ ($ls->delegates_names) }}--}}
                                                                            @if ($ls->delegates_names )
                                                                                <strong> For Delegates </strong>
                                                                                @if(strpos($ls->delegates_names, ',') !== false)
                                                                                    <ol>
                                                                                        @foreach(explode(',',$ls->delegates_names) as $ls1)
                                                                                            <li> {{ str_replace('"]','',str_replace('["','',$ls1)) }} </li>
                                                                                        @endforeach
                                                                                    </ol>
                                                                                @else
                                                                                    <ol>
                                                                                        {{--@foreach($ls->delegates_names as $ls1)--}}
                                                                                        <li> {{ str_replace('"]','',str_replace('["','',$ls->delegates_names)) }} </li>
                                                                                        {{--@endforeach--}}
                                                                                    </ol>
                                                                                @endif

                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel5" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel5" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.activities',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Activities </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Transfer Packages </span>
                                                </div>
                                                @if ($event->tpackages && $event->tpackages->count() > 0 )
                                                    <a href="{{ route('org.manage.event.transfer',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage Packages </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->tpackages && $event->tpackages->count() > 0 )
                                                    <h5> Available Packages </h5>
                                                    <div id="myCarousel6" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->tpackages as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->name !!} - {!! $ls->type !!} </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->description, $limit = 300, $end = '...') !!} <br>--}}
                                                                        <div class="" style="height: 100px; overflow-y: auto">
                                                                            {!! $ls->description !!} <br>
                                                                        </div>
                                                                        Price - {{ number_format($ls->cost) }} <br>
                                                                        <br>
                                                                        <div class="" style="height: 100px; overflow-y: auto">
                                                                            {{--{{ ($ls->delegates_names) }}--}}
                                                                            @if ($ls->delegates_names )
                                                                                <strong> For Delegates </strong>
                                                                                @if(strpos($ls->delegates_names, ',') !== false)
                                                                                    <ol>
                                                                                        @foreach(explode(',',$ls->delegates_names) as $ls1)
                                                                                            <li> {{ str_replace('"]','',str_replace('["','',$ls1)) }} </li>
                                                                                        @endforeach
                                                                                    </ol>
                                                                                @else
                                                                                    <ol>
                                                                                        {{--@foreach($ls->delegates_names as $ls1)--}}
                                                                                        <li> {{ str_replace('"]','',str_replace('["','',$ls->delegates_names)) }} </li>
                                                                                        {{--@endforeach--}}
                                                                                    </ol>
                                                                                @endif

                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel6" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel6" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.activities',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Activities </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Organizers | Sponsors </span>
                                                </div>
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle"
                                                            data-toggle="dropdown"> Manage
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li>
                                                            <a href="{{ route('org.manage.event.sponsors',$event->id) }}"> Event Sponsors </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('org.manage.event.organizers',$event->id) }}"> Event Organizers </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->organizer && $event->organizer->count() > 0 )
                                                    {{--<a href="{{ route('org.manage.event.organizers',$event->id) }}"--}}
                                                    {{--class="pull-right btn green btn-sm btn-outline "> Manage Organizers </a>--}}
                                                    <h5> Organizers </h5>
                                                    <div id="myCarousel7" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->organizer as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->name !!}  </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->brief, $limit = 200, $end = '...') !!} <br>--}}
                                                                        <div class="" style="height: 100px; overflow-y: auto">
                                                                            {!! ($ls->brief) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel7" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel7" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>

                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.organizers',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Organizers </a>
                                                    </h5>
                                                @endif

                                                <hr>

                                                @if ($event->sponsor && $event->sponsor->count() > 0 )
                                                    {{--<a href="{{ route('org.manage.event.sponsors',$event->id) }}"--}}
                                                    {{--class="pull-right btn green btn-sm btn-outline "> Manage Sponsors </a> <br>--}}
                                                    <h5> Sponsors </h5>
                                                    <div id="myCarousel8" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->sponsor as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->name !!}  </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->brief, $limit = 200, $end = '...') !!} <br>--}}
                                                                        <div class="" style="height: 100px; overflow-y: auto">
                                                                            {!! $ls->brief !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel8" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel8" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>

                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.sponsors',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage Sponsors </a>
                                                    </h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4  col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Event News </span>
                                                </div>
                                                @if ($event->news && $event->news->count() > 0 )
                                                    <a href="{{ route('org.manage.event.news',$event->id ) }}"
                                                       class="pull-right btn green btn-sm btn-outline "> Manage News </a>
                                                @endif
                                            </div>
                                            <div class="portlet-body">

                                                @if ($event->news && $event->news->count() > 0 )
                                                    <h5> Available News items </h5>
                                                    <div id="myCarousel9" class="carousel slide" data-ride="carousel">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <?php $i = 0; ?>
                                                            @foreach($event->news as $ls)
                                                                <div class="item @if ($i == 0) active @endif ">
                                                                    <div class="truncate header-text text-center">
                                                                        <strong>{!! $ls->category !!} </strong>
                                                                        <br>
                                                                        {{--{!! str_limit($ls->text, $limit = 400, $end = '...') !!}--}}
                                                                        <div class="" style="height: 300px; overflow-y: auto">
                                                                            {!! ($ls->text) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $i++; ?>
                                                            @endforeach
                                                        </div>
                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel9" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel9" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <h5> you have NOT added any |
                                                        <a href="{{ route('org.manage.event.news',$event->id) }}"
                                                           class="pull-right btn green btn-sm btn-outline "> Manage News </a>
                                                    </h5>
                                                @endif


                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="tab-pane " id="tab_5_2">

                                <h5> Payments by Month </h5>
                                <div id="site_activities_loading">
                                    <img src="{{asset('assets/global/img/loading.gif')}}" alt="loading" /> </div>
                                <div id="site_activities_content" class="display-none">
                                    <div id="site_activities" style="height: 228px;max-width: none;width:100%;margin-left: 10px;margin-right: 10px;"> </div>
                                </div>
                                <div style="margin: 20px 0 10px 30px">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-success"> Payments: </span>
                                            <h3>$ {{$stats['sales'] or 0}} </h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-success"> Total Orders: </span>
                                            <h3>$ {{$stats['total_orders'] or 0}} </h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-danger"> Owed: </span>
                                            <h3>$ {{$stats['owed'] or 0}} </h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-info"> Refunds: </span>
                                            <h3>$ {{$stats['refunds'] or 0}} </h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-warning"> Delegates: </span>
                                            <h3> {{$stats['delegates'] or 0}} </h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 text-stat">
                                            <span class="label label-sm label-warning"> Delegations: </span>
                                            <h3> {{$stats['delegations'] or 0}} </h3>
                                        </div>
                                    </div>
                                </div>

                                {{-- ticket movement--}}
                                <div class="row">
                                    <div class="col-md-6" style="height: auto; overflow: auto;">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <span> Ticket Movement </span>
                                                </div>
                                                <div class="btn-group pull-right">

                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                    <table class="table table-bordered table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th>Category</th>
                                                                <th>Quantity</th>
                                                                <th>Price</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($sales_tickets as $lst)
                                                                <tr>
                                                                    <td>{{$lst->name}}</td>
                                                                    <td>{{$lst->qty}}</td>
                                                                    <td>${{ number_format($lst->price) }}</td>
                                                                    <td>${{ number_format($lst->price*$lst->qty)}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3"> Total</td>
                                                                <td> ${{number_format($total_ticket_sales)}} </td>
                                                            </tr>
                                                            <tr> <td colspan="4"></td> </tr>
                                                        </tfoot>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="height: auto; overflow: auto;">
                                            <div class="portlet light bordered">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <span> Other Services Sales | Counts </span>
                                                    </div>
                                                    <div class="btn-group pull-right">

                                                    </div>
                                                </div>
                                                <div class="portlet-body">

                                                    <table class="table table-bordered table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th> Service</th>
                                                                <th> Sales </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th colspan="2">  </th>
                                                            </tr>
                                                            <tr>
                                                                <th>Total Activities Sales Value</th>
                                                                <th>${{ number_format($stats['sales_act'])  }}</th>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table class="table table-bordered table-condensed">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Quantity</th>
                                                                            <th>Price</th>
                                                                            <th>Total</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($sales_act as $lst)
                                                                            <tr>
                                                                                <td>{{$lst->name}}</td>
                                                                                <td>{{$lst->qty}}</td>
                                                                                <td>${{ number_format($lst->cost) }}</td>
                                                                                <td>${{ number_format($lst->cost*$lst->qty)}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th> Total Transfer Packages Sales Value </th>
                                                                <th>${{ number_format($stats['sales_tran'])  }}</th>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table class="table table-bordered table-condensed">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Quantity</th>
                                                                            <th>Price</th>
                                                                            <th>Total</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($sales_trans as $lst)
                                                                            <tr>
                                                                                <td>{{$lst->name}}</td>
                                                                                <td>{{$lst->qty}}</td>
                                                                                <td>${{ number_format($lst->cost) }}</td>
                                                                                <td>${{ number_format($lst->cost*$lst->qty)}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                        {{--<tfoot>--}}
                                                                        {{--<tr>--}}
                                                                            {{--<td colspan="3"> Total</td>--}}
                                                                            {{--<td> ${{number_format($total_ticket_sales)}} </td>--}}
                                                                        {{--</tr>--}}
                                                                        {{--</tfoot>--}}
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane form" id="tab_5_3">

                                    <div class='btn-group pull-right' style="clear: both;">
                                        <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                                                aria-expanded='true'> Export Full Database <i class='fa fa-angle-down'></i>
                                        </button>
                                        <ul class='dropdown-menu pull-right' role='menu'>
                                            <li><a href='{{ route ('download.inv.db',[$event->id]) }}?file=pdf'  > PDF </a></li>
                                            <li><a href='{{ route ('download.inv.db',[$event->id]) }}?file=xls'  > Excel </a></li>
                                        </ul>
                                    </div>
                                    <br>
                                   <h5> Global Search  </h5>

                                    <form class="horizontal-form" method="get" action="{{ route ('download.inv.db',[$event->id]) }}">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Delegations</label>
                                                        <select class="form-control select-multiple" name="delegations[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach($event->delegations->where('event_id',$event->id) as $lst)
                                                                <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label">Attendance Category</label>
                                                        <select class="form-control select-multiple" name="tickets[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach($event->tickets as $lst)
                                                                <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label">Title</label>
                                                        <select class="form-control select-multiple" name="title[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach($titles as $lst)
                                                                <option value="{{$lst->name}}"> {{$lst->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Hotel</label>
                                                        <select class="form-control select-multiple" name="Hotels[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            
                                                            @foreach($event->hotels as $lst)
                                                            @if($lst->hotel == NULL)
                                                            @else
                                                                <option value="{{$lst->hotel->id}}"> {{$lst->hotel->name}} </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label">Activities</label>
                                                        <select class="form-control select-multiple" name="activities[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach( $event->activities as $lst)
                                                                <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label">Transfer packages</label>
                                                        <select class="form-control select-multiple" name="tpkgs[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach($event->tpackages as $lst)
                                                                <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Tent Accomodation</label>
                                                        <select class="form-control select-multiple" name="tpkgs[]" multiple>
                                                            <option value="all" selected > All </option>
                                                            @foreach($event->tents as $lst)
                                                                <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label">File Type</label>
                                                        <select class="form-control select-multiple" name="file" >
                                                            <option value="xls" selected > XLS </option>
                                                            <option value="pdf"> PDF </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        {{--<label class="control-label">Tech requirements</label>--}}
                                                        {{--<select class="form-control select-multiple" name="techreq[]" multiple>--}}
                                                            {{--<option value="all" selected > All </option>--}}
                                                            {{--<option value="Yes"> Yes </option>--}}
                                                            {{--<option value="No"> No </option>--}}
                                                        {{--</select>--}}
                                                        <label class="control-label"></label> <br/>
                                                        <input type="submit" name="get" value="Get Delegates Database" class=" btn btn-success">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">

                                                </div>
                                            </div>
                                    </form>
                            </div>

                            <div class="tab-pane" id="tab_5_4">

                                <div class="tabbable-line" >
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_5_5_a" data-toggle="tab" > Full </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_5_5_b" data-toggle="tab" > Hotels </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_5_5_c" data-toggle="tab" > Transfers </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_5_5_d" data-toggle="tab" > Flights </a>
                                        </li>
                                        {{--<li class="">--}}
                                            {{--<a href="#tab_5_5_e" data-toggle="tab" > Payments </a>--}}
                                        {{--</li>--}}
                                        <li class="">
                                            <a href="#tab_5_5_f" data-toggle="tab" > Owing Us </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_5_5_g" data-toggle="tab" > Other data </a>
                                        </li>
                                        {{--<li class="">--}}
                                            {{--<a href="#tab_5_5_h" data-toggle="tab" > Parking Pass </a>--}}
                                        {{--</li>--}}
                                        {{--<li class="">--}}
                                            {{--<a href="#tab_5_5_i" data-toggle="tab" > Food Req </a>--}}
                                        {{--</li>--}}
                                        {{--<li class="">--}}
                                            {{--<a href="#tab_5_5_j" data-toggle="tab" > Tech Req </a>--}}
                                        {{--</li>--}}
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_5_5_a">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="full" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Activities</label>
                                                                <select class="form-control select-multiple" name="activities[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->activities as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                        <div class="form-group ">
                                                            <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_5_5_b">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="hotels" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Hotel</label>
                                                                <select class="form-control select-multiple" name="Hotels[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->hotels as $lst)
                                                                    @if($lst->hotel == NULL)
                                                                    @else
                                                                        <option value="{{$lst->hotel->id}}"> {{$lst->hotel->name}} </option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_5_5_c">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="tpkgs" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Transfer packages</label>
                                                                <select class="form-control select-multiple" name="tpkgs[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tpackages as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_5_5_d">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="flight" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_5_5_f">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="owing" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_5_5_g">
                                            <form class="horizontal-form" method="get" action="{{ route ('download.inv.custom.db',[$event->id]) }}">
                                                {{ csrf_field() }}
                                                <input name="report_type" value="other" type="hidden" readonly>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Delegations</label>
                                                                <select class="form-control select-multiple" name="delegations[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->delegations as $lst)
                                                                        <option value="{{$lst->id}}"> {{$lst->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label class="control-label">Attendance Category</label>
                                                                <select class="form-control select-multiple" name="tickets[]" multiple>
                                                                    <option value="all" selected > All </option>
                                                                    @foreach($event->tickets as $lst)
                                                                        <option value="{{$lst->delegates->id}}"> {{$lst->delegates->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <input type="submit" name="get" value="Get Report" class=" btn btn-success">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection


@section('footer')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBCneBlloJz09T_BzXSZD3EHPDDtKWLhIc"></script>
    <script src="{{ asset("assets/geocomplete/jquery.geocomplete.js") }}"></script>

    <script src="{{ asset("assets/global/plugins/flot/jquery.flot.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/global/plugins/flot/jquery.flot.resize.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/global/plugins/flot/jquery.flot.categories.min.js") }}" type="text/javascript"></script>

    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    @javascript('sales_year',  $r )

    <script>
        $('.select-multiple').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
        $(function () {
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                markerOptions: {
                    draggable: true
                },
                location: "{{ $event->venue }}",
                types: ["geocode", "establishment"],
            });

            $("#geocomplete").bind("geocode:dragged", function (event, latLng) {
                console.log(latLng);
                $("input[name=lat]").val(latLng.lat());
                $("input[name=lng]").val(latLng.lng());
                $("#geocomplete").geocomplete("find", latLng.lat() + "," + latLng.lng());
                $("#reset").show();
            });


            $("#reset").click(function () {
                $("#geocomplete").geocomplete("resetMarker");
                $("#reset").hide();
                return false;
            });

            $("#find").click(function () {
                $("#geocomplete").trigger("geocode");
            });
        });

        if ($('#site_activities').size() != 0) {
            //site activities
            var previousPoint2 = null;
            $('#site_activities_loading').hide();
            $('#site_activities_content').show();

            var data1 = sales_year;

            console.log(sales_year);

            var plot_statistics = $.plot($("#site_activities"),

                [{
                    data: data1,
                    lines: {
                        fill: 0.2,
                        lineWidth: 0,
                    },
                    color: ['#BAD9F5']
                }, {
                    data: data1,
                    points: {
                        show: true,
                        fill: true,
                        radius: 4,
                        fillColor: "#9ACAE6",
                        lineWidth: 2
                    },
                    color: '#9ACAE6',
                    shadowSize: 1
                }, {
                    data: data1,
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 3
                    },
                    color: '#9ACAE6',
                    shadowSize: 0
                }],

                {

                    xaxis: {
                        tickLength: 0,
                        tickDecimals: 0,
                        mode: "categories",
                        min: 0,
                        font: {
                            lineHeight: 18,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    yaxis: {
                        ticks: 5,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        font: {
                            lineHeight: 14,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    }
                });

            $("#site_activities").bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint2 != item.dataIndex) {
                        previousPoint2 = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);
                        showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                    }
                }
            });

            $('#site_activities').bind("mouseleave", function() {
                $("#tooltip").remove();
            });
        }


    </script>

@endsection
