<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events') }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> All Events </a>--}}
            <a href="{{ route('org.edit.event.meta',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-right"></i> Event Social Info </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event -  {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span> Editing this event </span>
                    </div>
                </div>
                <div class="portlet-body">

                    {!! Form::open(['route' => ['org.edit.event',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}

                    <div class="form-body">

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Name:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{ $event->name or null }}" class="form-control validate[required]" name="event-name" placeholder="">
                                @if($errors->has('name'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('name') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Theme:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{ $event->theme or null}}" class="form-control validate[required]" name="theme" placeholder="">
                                <small> &#60;br&#62; or &#60;br/&#62; is used to move text to a new line </small>
                                @if($errors->has('theme'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('theme') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Venue:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <input type="text" value="{{ $event->venue or null}}" id="geocomplete" class="form-control validate[required]" name="venue" placeholder="">
                                <input name="formatted_address" type="hidden" value="">
                                @if($errors->has('venue'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('venue') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Logo:
                                <span class="required">  </span>
                            </label>
                            <div class="col-md-10">
                                <input type="file" name="logo" class="form-control">
                                @if($errors->has('logo'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('logo') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Map to Venue:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <div class="map_canvas" style="width: 100%;"></div>
                                <div class='col-md-5'>
                                    <label>Latitude</label>
                                    <input name="lat" type="text" value="{{  ((explode(',', $event->coordinates))[0]) or null}}" class="form-control validate[required]">
                                </div>
                                <div class='col-md-5'>
                                    <label>Longitude</label>
                                    <input name="lng" type="text" value="{{ ((explode(',', $event->coordinates))[1]) or null}}" class="form-control validate[required]">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Dates:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                <div class="">
                                    <div class='col-md-5'>
                                        <div class="form-group">
                                            <label> Start Date </label>
                                            <div class='input-group date' id='start'>
                                                <input type='text' value="" name="start" class="form-control validate[required]" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                @if($errors->has('start'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('start') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-md-5'>
                                        <div class="form-group">
                                            <label> End Date </label>
                                            <div class='input-group date' id='end'>
                                                <input type='text' name="end" value="" class="form-control  validate[required]" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                @if($errors->has('end'))
                                                    <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('end') as $error)
                                                            {!!$error !!}<br/>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Description:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-10">
                                {{--{!! Form::textarea('description', $event->description , ['class'=>'form-control ckeditor  validate[required]', 'placeholder'=>'Event Description']) !!}--}}
                                <textarea name="description" class='form-control tinymce  validate[required]' >
                                        {!! $event->description !!}
                                </textarea>
                                @if($errors->has('description'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('description') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"> Event Programme (PDF):
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-10">
                                <input type="file" name="event_programme" class="form-control">
                                @if($errors->has('event_programme'))
                                    <span class="alert-danger help-block help-block-error">
                                        @foreach($errors->get('event_programme') as $error)
                                            {!!$error !!}<br/>
                                        @endforeach
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">
                            </label>
                            <div class="col-md-10">
                                <button type="submit" name="Save" value="save" class="btn btn-success">
                                    <i class="fa fa-check"></i> Save </button>
                            </div>
                        </div>

                    </div>

                    {!!Form::close()!!}


                </div>
            </div>

        </div>

    </div>

@endsection


@section('footer')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBCneBlloJz09T_BzXSZD3EHPDDtKWLhIc"></script>
    <script src="{{ asset("assets/geocomplete/jquery.geocomplete.js") }}"> </script>

    <script>
        $('#start').datetimepicker({sideBySide: true,useCurrent: false, defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->start)) }}"});
        $('#end').datetimepicker({sideBySide: true,useCurrent: false, defaultDate: "{{ date("m/d/Y h:i A",strtotime($event->end)) }}"});

        $(function () {
//            $('#start').datetimepicker( { minDate:new Date(), sideBySide: true} );
//            $('#end').datetimepicker({
//                useCurrent: false, //Important! See issue #1075
//                sideBySide: true
//            });
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date);
            });
            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });

        $(function(){
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                markerOptions: {
                    draggable: true
                },
                location: "{{ $event->venue }}",
                types: ["geocode", "establishment"],
            });

            $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                console.log(latLng);
                $("input[name=lat]").val(latLng.lat());
                $("input[name=lng]").val(latLng.lng());
                $("#geocomplete").geocomplete("find", latLng.lat() + "," + latLng.lng());
                $("#reset").show();
            });


            $("#reset").click(function(){
                $("#geocomplete").geocomplete("resetMarker");
                $("#reset").hide();
                return false;
            });

            $("#find").click(function(){
                $("#geocomplete").trigger("geocode");
            });
        });
    </script>

@endsection
