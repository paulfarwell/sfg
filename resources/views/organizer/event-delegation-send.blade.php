<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 24/08/2017
 * Time: 09:46
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend-v')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Manage Event Delegations   </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <a href="{{ route('org.new.event') }}" class="  pull-right btn green btn-sm blue"
               msg="Are you sure you want to create a new event, we can only have one active event at a time !!"
               data-cancel-button-text='No, I changed my Mind' data-confirm-button-text='Yes, Go ahead'
            > Create New Event </a>
        </div>
    </div>
    <!-- END PAGE BAR -->

@endsection

@section('content')



    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> {{ $delegation->name }}  ({{ $event->name or 'set event name' }})
        <small> send out delegation invitations to recipient </small>
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="portlet light ">
                <div class="portlet-title">
                <div class="caption font-dark">
                    {{ $delegation->name }} | @if(!empty($delegation->country)) {{ $delegation->countryname->name }} @endif
                </div>
                <div class="tools"> </div>
                </div>
                <div class="portlet-body form">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> Delegate </th>
                                        <th> Email </th>
                                        <th> Type </th>
                                        <th> Preferred Language</th>
                                        <th> Badge Color </th>
                                        <th> Code </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $delegation->invitations  as $list)
                                        <tr>
                                            <td> {{ $list->profile->fname }} {{ $list->profile->surname }}</td>
                                            <td> {{ $list->profile->email }}</td>
                                            <td> {{ $list->delegateType->name }} </td>
                                            <td> {{ $list->profile->language }}</td>
                                            <td> {{ $list->delegateType->color }} </td>
                                            <td> {{ $list->code }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 ">

                            {!! Form::open(['route' => ['org.delegation.send',$delegation->id], 'class' => 'form-horizontal form-row-seperated', 'files' => true]) !!}
                             {!!Form::hidden('event_id', $event->id)!!}
                            <div class="form-body">

                              <div class="form-group">
                                  <label class="col-md-2 control-label"> Email Template to use :
                                    <span class="required"> * </span>
                                  </label>
                                  <div class="col-md-10">
                                      <select class="form-control" name="emailtemplate">
                                        <option value="">Select...</option>
                                        @foreach($emailtemplates as $emailtemplate)
                                        <option value="{{$emailtemplate->id}}">{{$emailtemplate->name}}</option>

                                        @endforeach
                                      </select>
                                      @if($errors->has('emailtemplate'))
                                          <span class="alert-danger help-block help-block-error">
                                              @foreach($errors->get('emailtemplate') as $error)
                                                  {!!$error !!}<br/>
                                              @endforeach
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label"> Email Attachment (pdf) :
                                  </label>
                                  <div class="col-md-10">
                                    <input type="file" name="attachment[]" class="form-control" multiple>
                                    @if($errors->has('attachment'))
                                        <span class="alert-danger help-block help-block-error">
                                            @foreach($errors->get('attachment') as $error)
                                                {!!$error !!}<br/>
                                            @endforeach
                                        </span>
                                    @endif
                                  </div>
                              </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success pull-right">
                                            <i class="fa fa-check"></i> Send Email </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection


@section('footer')

    <script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>

    <!-- <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script> -->

    <!-- <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" /> -->


    <!-- <script>

    $('.description').summernote({
        dialogsInBody: true
    });

        $.fn.select2.defaults.set("theme", "bootstrap");
        //var placeholder = "Select a State";
        $(".select2, .select2-multiple").select2({
            placeholder: "select one or more entries",
            width: null
        });
    </script> -->

    <script>
        var initTable10 = function () {
            var table10 = $('#delegation');
            var oTable10 = table10.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event Delegation - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event Delegation - Data export',exportOptions: {columns: [ 0, 1, 2,3,4,5,6]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            toastr.info('Event Delegation - reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: " {{ route('org.events.delegations.list',$event->id) }}", //events_programme
                    method: 'POST'
                },
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
                    {data: 'name', name: 'name', orderable: false, searchable: true},
                    {data: 'count_invitations', name: 'count_invitations', orderable: false, searchable: false},
                    {data: 'contact_name', name: 'contact_name', orderable: false, searchable: true},
                    {data: 'contact_email', name: 'contact_email', orderable: false, searchable: true},
                    {data: 'countryname.name', name: 'countryname.name', orderable: false, searchable: false},
                    {data: 'type.name', name: 'type.name', orderable: false, searchable: false},
                    {data: 'status', name: 'status', orderable: false, searchable: false},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });
        }

        initTable10();

        var FormRepeater = function () {

            return {
                //main function to initiate the module
                init: function () {
                    $('.mt-repeater').each(function(){
                        $(this).repeater({
                            show: function () {
                                $(this).slideDown();
                                $('.date-picker').datepicker({
                                    rtl: App.isRTL(),
                                    orientation: "left",
                                    autoclose: true
                                });
                            },

                            hide: function (deleteElement) {
                                if(confirm('Are you sure you want to delete this element?')) {
                                    $(this).slideUp(deleteElement);
                                }
                            },

                            ready: function (setIndexes) {

                            }
                            ,isFirstItemUndeletable: true
                        });
                    });
                }

            };

        }();

        jQuery(document).ready(function() {
            FormRepeater.init();
        });

    </script>

@endsection
