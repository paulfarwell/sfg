<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend-v')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>

    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Email Templates
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this Email Templates  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All Email Templates </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="event-hotels">
                                <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Content</td>
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($emails as $email)
                                <tr>
                                 <td>{{$email->name}}</td>
                                 <td>{!!$email->content!!}</td>
                                 @if($email->status == 0)
                                 <td>Enabled</td>
                                 @else
                                 <td>Disabled</td>
                                 @endif
                                 <td><div class='btn-group '>
                                 <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                                 aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                                 </button>
                                 <ul class='dropdown-menu pull-right' role='menu'>
                                     <li><a href="{{route('org.email.template.edit', [$email->id])}}" > Edit Email Template </a></li>

                                 </ul>
                                 </div></td>
                                 </tr>
                                 @endforeach

                                 </tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.email.template.new'], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Name :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        <input type="text" value="" class="form-control validate[required]" name="name" placeholder="">
                                        @if($errors->has('name'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('name') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Email Body:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('content',null, ['class'=>'form-control tinyvariable  validate[required]', 'placeholder'=>'Email Body']) !!}
                                        @if($errors->has('content'))
                                            <span class="alert-danger help-block help-block-error">
                                                        @foreach($errors->get('content') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Status :

                                    </label>
                                    <div class="col-md-10">
                                      <select id="status" name="status" class="form-control ">
                                       <option value="0">Active</option>
                                       <option value="1">Deactivate</option>
                                      </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>





@endsection


@section('footer')




    <script src="{{ asset("assets/events/tables.js") }}"> </script>
    <script src="{{ asset("assets/events/ui.js") }}"> </script>

    <script>
        initTable3();
    </script>

    @include('partial.email-template-mgt')

@endsection
