<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 03/07/2017
 * Time: 20:03
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}" >  home </a>
                @else
                    <a href="{{ route('org.index')  }}" >  home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Create new Event Tickets </span>
            </li>
        </ul>
        <div class="page-toolbar">
            @if ($imgtype == 'event_hotel')
                <a href="{{ route('org.manage.event.hotels', [$event->id,'#tab_hotels']) }}" class="pull-right btn green btn-sm "   >
                    <i class="fa fa-angle-double-left"></i> Back to {{$event->name}} Event  - Hotels</a>
            @endif
            @if ($imgtype == 'event_act')
                <a href="{{ route('org.manage.event.activities', [$event->id,'#tab_act']) }}" class="pull-right btn green btn-sm "   >
                    <i class="fa fa-angle-double-left"></i> Back to {{$event->name}} Event  - Activities </a>
            @endif
            @if ($imgtype == 'event_trans')
                <a href="{{ route('org.manage.event.transfer', [$event->id,'#tab_trans']) }}" class="pull-right btn green btn-sm "   >
                    <i class="fa fa-angle-double-left"></i> Back to {{$event->name}} Event - Transfer Packages </a>
            @endif

            @if ($imgtype == 'event_tent')
                <a href="{{ route('org.manage.event.tents', [$event->id,'#tab_hotels']) }}" class="pull-right btn green btn-sm "   >
                    <i class="fa fa-angle-double-left"></i> Back to {{$event->name}} Event  - Tents</a>
            @endif

            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa fa-arrows-h"></i> Edit Event </a>

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->

    <div class="modal fade" id="myImgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Photo </h4>
                </div>
                <div class="modal-body" align="center">
                    <img src="" id="img" class="img-responsive">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>



@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Images -
        @if ($imgtype == 'event_hotel') {{ $hotel->hotel->name}} @endif
        @if ($imgtype == 'event_act') {{ $hotel->name}} @endif
        @if ($imgtype == 'event_trans') {{ $hotel->name}} @endif
        @if ($imgtype == 'event_tent') {{ $hotel->name}} @endif

        <small> upload new images </small>

    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light ">

                <div class="portlet-body">

                    <div class="tabbable-line ">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Upload Images </a>
                            </li>
                            <li class="">
                                <a href="#tab_5_2" data-toggle="tab" aria-expanded="true"> Images </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">

                                {{--<form action="{{route('org.new.event.bulk.img', [$event->id,$hotel->hotel_id,$imgtype])}}" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">--}}
                                    {{--<h3 class="sbold">Drop files here or click to upload</h3>--}}
                                    {{--<div class="fallback">--}}
                                        {{--<input name="file" type="file" multiple />--}}
                                    {{--</div>--}}
                                {{--</form>--}}

                                <form action="{{route('org.new.event.bulk.img', [$event->id,$hotelid,$imgtype])}}" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">
                                    <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                                    <h3 class="sbold">Drop files here or click to upload</h3>
                                    <p> max file size is 8mb and the image must be 600x600 . </p>
                                </form>

                            </div>
                            <div class="tab-pane" id="tab_5_2">

                                <table width="100%" class="table table-bordered table table-striped table-bordered table-hover" id="events_hotel_images">
                                    <thead>
                                    <tr>
                                        <td>Image</td>
                                        <td>Main </td>
                                        <td>Sort Order</td>
                                        <td>status</td>
                                        <td>Tools</td>
                                    </tr>
                                    </thead>
                                    <tbody> </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="editImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Poster Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editImagesForm" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label > Sort Order </label>
                                <div >
                                    <input type="text" name="sort" id="sort" class="form-control validate[required]">
                                    <small> 0 means no sorting, changing this to a higher figure make this image appear first</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label > Main Image   </label>
                                <div >
                                    <input type="text" class="form-control validate[required]" id="is_main" name="is_main" placeholder="">
                                    <small> 0 means  not set as the first image, this is used to overide sorting for first image </small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label >Status </label>
                                <div >
                                  <select id="status" class="form-control" name="status">
                                  <option value="active">Active</option>
                                  <option value=" ">Deactivate</option>
                                  </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label > Image Preview  </label>
                                <div>
                                    <img src="" id="image" class="img-responsive">
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">   <i class="fa fa-check"></i> Update </button>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer')

    <link href="{{ asset('assets/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/dropzone/basic.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>

    <script>
        $('#myImgModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var img = button.data('img')
            console.log(img)
            $('#img').attr('src', img);
        })
    </script>

    <script>
        $('#editImages').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editImagesForm').attr('action', button.data('url'));
            $('#sort').val(button.data('sort'));
            $('#is_main').val(button.data('is_main'));
            $('#status').val(button.data('status'));
            $('#image').attr('src',button.data('image'));
        })
    </script>

    <script>
        var FormDropzone = function () {
            return {
                //main function to initiate the module
                init: function () {
                    Dropzone.options.myDropzone = {
                        dictDefaultMessage: "",
                        uploadMultiple: false,
                        parallelUploads: 100,
                        maxFilesize: 8,
                        previewsContainer: '#my-dropzone',
                        //previewTemplate: document.querySelector('#preview-template').innerHTML,
                        addRemoveLinks: true,
                        dictRemoveFile: 'Remove',
                        dictFileTooBig: 'Image is bigger than 8MB',
                        init: function () {
                            // Set up any event handlers
//                            this.on('completemultiple', function () {
//                                location.reload();
//                            });
                            this.on("queuecomplete", function (file) {
                                location.reload();
                            });
                        }
                    }
                }
            };
        }();
        jQuery(document).ready(function() {
            FormDropzone.init();
        });
    </script>

    <script>
        var initTable5 = function () {
            var table5 = $('#events_hotel_images');
            var oTable5 = table5.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event programme reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": false, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: '{{ route('org.dt.list.events.ot.images',[$event->id,$hotelid,$imgtype]) }}', //events_programme
                    method: 'POST'
                },
                //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                columns: [
                    {data: 'image_path', name: 'image_path', orderable: false, searchable: true},
                    {data: 'is_main', name: 'is_main', orderable: false, searchable: false},
                    {data: 'sort', name: 'sort', orderable: false, searchable: true},
                    {data: 'status', name: 'status', orderable: false, searchable: false},
                    {data: 'tools', name: 'tools', orderable: false, searchable: false},
                ]
            });
            // handle datatable custom tools

        }

        initTable5();

    </script>

@endsection
