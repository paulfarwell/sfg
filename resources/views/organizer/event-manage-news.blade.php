<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 23/06/2017
 * Time: 11:52
 */
?>

@extends('layouts.backend')

@section('toolbar')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                {{--<a href="index.html">Home</a>--}}
                @if (Auth::user()->isRole('delegate'))
                    <a href="{{ route('del.index')  }}"> home </a>
                @else
                    <a href="{{ route('org.index')  }}"> home </a>
                @endif
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Edit Event </span>
            </li>
        </ul>
        <div class="page-toolbar">

            {{--<a href="{{ route('org.events.delegations',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Delegations </a>--}}

            <a href="{{ route('org.manage.event.sponsors',$event->id) }}" class="pull-right btn green btn-sm"> <i
                        class="fa fa-angle-double-left"></i> Event Sponsors </a>
            <a href="{{ route('org.edit.event',$event->id) }}" class="pull-right btn green btn-sm"> <i
                        class="fa fa fa-arrows-h"></i> Edit Event </a>
            {{--<a href="{{ route('org.manage.event.images',$event->id) }}" class="pull-right btn green btn-sm"   > <i class="fa fa-angle-double-left"></i> Event Images </a>--}}

            @include('partial.event-mgr-menu')

        </div>
    </div>
    <!-- END PAGE BAR -->


@endsection

@section('content')

    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Event News - {{ $event->name }}
        {{--<small> edit event parameters </small>--}}
    </h1>
    <!-- END PAGE TITLE-->

    <div class="row">

        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <span> Add | Edit this events news  </span>
                    </div>
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> All News </a>
                        </li>
                        <li class="">
                            <a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Create New </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_5_1">

                            <table width="100%"
                                   class="table table-bordered table table-striped table-bordered table-hover"
                                   id="event-news">
                                <thead>
                                <tr>
                                    <td>Category</td>
                                    <td>News</td>
                                    <td>Order</td>
                                    <td>Status</td>
                                    <td>Tools</td>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>

                        <div class="tab-pane " id="tab_5_2">

                            {!! Form::open(['route' => ['org.new.news',$event->id], 'class' => 'form-horizontal form-row-seperated',  'files' => true]) !!}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Category:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {{--<input type="text" class="form-control validate[required]" name="category" placeholder="">--}}
                                        <select name="category" class="form-control validate[required]">
                                            <option value=""> Select Section</option>
                                            {{--<option value="Accreditation Procedure"> Accreditation Procedure </option>--}}
                                            {{--<option value="General Information"> General Information </option>--}}
                                            {{--<option value="Accommodation"> Accommodation </option>--}}
                                            {{--<option value="Travel"> Travel </option>--}}
                                            {{--<option value="Ground Transportation"> Ground Transportation </option>--}}
                                            {{--<option value="Media"> Media </option>--}}
                                            {{--<option value="Wifi"> Wifi </option>--}}
                                            {{--<option value="Security"> Security </option>--}}
                                            {{--<option value="Firearms and Drones"> Firearms and Drones </option>--}}
                                            {{--<option value="Medical and Health Services "> Medical and Health Services </option>--}}
                                            {{--<option value="Pre and Post Summit Safari Packages"> Pre and Post Summit Safari Packages </option>--}}
                                            {{--<option value="Payment of Registration Fees"> Payment of Registration Fees </option>--}}
                                            @foreach( $newscat as $list)
                                                <option value="{{$list->name}}"> {{$list->name}} </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('category'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('category') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-10">
                                        {!! Form::textarea('text',null, ['class'=>'form-control tinymce  validate[required]', 'placeholder'=>'Event Description']) !!}
                                        @if($errors->has('text'))
                                            <span class="alert-danger help-block help-block-error">
                                                @foreach($errors->get('text') as $error)
                                                    {!!$error !!}<br/>
                                                @endforeach
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                    </label>
                                    <div class="col-md-10">
                                        <button type="submit" name="Save" value="save" class="btn btn-success">
                                            <i class="fa fa-check"></i> Save
                                        </button>
                                    </div>
                                </div>

                            </div>
                            {!!Form::close()!!}

                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="editNewsDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 id="edit-header-txt" class="text-center"> Edit Activity Information for an Event </h5>
                </div>
                <div class="modal-body">

                    <form action="" method="post" id="editNewsForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <div class="form-group">
                                <label> Category : </label>
                                <div>
                                    <select name="category" class="form-control validate[required]" id="category">
                                        <option value=""> Select Section</option>
                                        @foreach( $newscat as $list)
                                            <option value="{{$list->name}}"> {{$list->name}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Order : </label>
                                <div>
                                    <input type="text" name="order_custom" id="order_custom" value="" class="form-control validate[required,custom[number]]">
                                </div>
                            </div>

                            <div class="form-group">
                                <label> Description: </label>
                                <div>

                                    <textarea name="text" class="form-control tinymcemodal validate[required]"
                                              id="description"> </textarea>


                                </div>
                            </div>


                            <div class="form-group">
                                <div>
                                    <button type="submit" name="Update" value="Update" class="btn btn-success">
                                        <i class="fa fa-check"></i> Update
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('footer')

    <!-- <link href=" {{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script> -->

    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>

    @javascript('event_id',  $event->start )
    @javascript('list_event_news', route('org.dt.list.events.news',array($event->id)))

    <script src="{{ asset('assets/events/tables.js') }}"></script>
    {{--<script src="{{ asset("assets/events/ui.js") }}"> </script>--}}

    <script>
        initTable10();
    </script>



    <script>

        // Define function to open filemanager window
        var lfm = function (options, cb) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = cb;
        };

        // Define LFM summernote button
        var LFMButton = function (context) {
            var ui = $.summernote.ui;
            var button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'Insert image with filemanager',
                click: function () {

                    lfm({type: 'image', prefix: '/file-manager'}, function (url, path) {
                        context.invoke('insertImage', url);
                    });

                }
            });
            return button.render();
        };

        $('#editNewsDetails').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $('#editNewsForm').attr('action', button.data('url'));
            //var del = button.data('delegate').toString().split(","); console.log(del);
            //var sel =  document.getElementById('delegate_id');
            $('#category').val(button.data('type'));
            $('#order_custom').val(button.data('order_custom'));
            //$('#description').summernote(button.data('text'));
            //$('#description').val(button.data('desc'));

//            $('#description').eq(0).summernote('destroy');
//            $('#description').summernote({
//                toolbar: [
//                    ['popovers', ['lfm']],
//                ],
//                buttons: {
//                    lfm: LFMButton
//                },
//                dialogsInBody: true,
//                callbacks: {
//                    onPaste: function (e) {
//                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
//                        e.preventDefault();
//                        // Firefox fix
//                        setTimeout(function () {
//                            document.execCommand('insertText', false, bufferText);
//                        }, 10);
//                    }
//                }
//            });

            //tinymce.remove("textarea.tinymcemodal");
            //tinymce.execCommand('mceRemoveControl', false, 'textarea.tinymcemodal');
            //tinymce.EditorManager.execCommand('mceRemoveEditor', true, 'textarea.tinymcemodal');
            //$("textarea.tinymcemodal").each(function() { $(this).tinymce.remove();});
            //tinymce.execCommand('mceRemoveControl', true, '.tinymcemodal');
            //tinymce.get("textarea.tinymcemodal").remove();
            //tinymce.EditorManager.execCommand('mceAddEditor', true, ".tinymcemodal");
            tinymce.remove('textarea.tinymcemodal');

            var editor_config_modal = {
                path_absolute: "",
                selector: "textarea.tinymcemodal",
                entities : '160,nbsp,162,cent,8364,euro,163,pound',
                entity_encoding : "raw",
                //plugins: ,
                height: 500,
                theme: 'modern',
                plugins: 'code paste print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                paste_as_text: true,
                paste_text_sticky_default: true,
                powerpaste_allow_local_images: true,
                powerpaste_word_import: 'prompt',
                powerpaste_html_import: 'prompt',
                relative_urls: false,
                file_browser_callback: function (field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config_modal.path_absolute + route_prefix + '?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }
                    tinyMCE.activeEditor.windowManager.open({
                        file: cmsURL,
                        title: 'Filemanager',
                        width: x * 0.8,
                        height: y * 0.8,
                        resizable: "yes",
                        close_previous: "no"
                    });
                },
                init_instance_callback: function (editor) {
                    editor.setContent('');
                    editor.setContent(button.data('desc'));
                }
            };
            tinymce.init(editor_config_modal);
        })
    </script>


@endsection
