<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 06/11/2017
 * Time: 09:06
 */
?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
    <style>
        table {
            display: table;
            border-collapse: separate;
            border-spacing: 2px;
            border-color: grey;
        }
        table td { vertical-align:top}
        table { border:1px solid #1px solid #555555; border-collapse:collapse; width:100%}
        table td, table th { border:1px solid #555555; padding:4px 5px ; text-align:left}
        table th { background:#f5f5f5;}
        .rotated th {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
            /*width: 100px;*/
            /*height: 100px;*/
            /*font-weight: normal;*/
            /*background: white !important;*/
            /*font-size: 10px;*/
            /*white-space: normal;*/
            padding: 1px 30px;
        }
    </style>
</head>
<table class="" width="100">
    <thead>
        <tr>
            <th colspan="9"> Personal Information</th>
            <th></th>
            <th colspan="6"> Invitation Status</th>
            <th></th>
            <th colspan="2"> Registration Details</th>
            <th></th>
            <th colspan="4"> Identification Details</th>
            <th></th>
            <th colspan="8"> Flight Arrival Details</th>
            <th></th>
            <th colspan="8"> Flight Departure Details</th>

        </tr>
    <tr class="rotated">
        <th> First Name</th>
        <th> Surname</th>
        <th> Title</th>
        <th> Country Code</th>
        <th> Phone</th>
        <th> Email</th>
        <th> Organisation</th>
        <th> Position</th>
        <th> Delegation</th>

        <th></th>

        <th> Badge Color</th>
        <th> RSVP</th>
        <th> Link Sent</th>
        <!-- <th> Car Pass </th>
        <th> Car Details</th> -->
        <th> Food Req</th>
        <th> Tech Req</th>
        <th> Other</th>

        <th></th>

        <th> Registration Code </th>
        <th> Attendance Category</th>
        <!-- <th> Total </th> -->

        <th></th>

        <th> Nationality</th>
        <th> ID/Passport No</th>
        <th> ID Copy Received (Y=1)</th>
        <th> Photo received (Y=1)</th>

        <th></th>

        <th>Airport of Origin</th>
        <th>Terminal</th>
        <th>Arrival Date</th>
        <th>Arrival Time</th>
        <th>Flight Carrier</th>
        <th>Flight Number</th>
        <th>Transfer Required</th>
        <th>Flight Type</th>

        <th></th>

        <th>Airport of Departure</th>
        <th>Terminal</th>
        <th>Arrival Date</th>
        <th>Arrival Time</th>
        <th>Flight Carrier</th>
        <th>Flight Number</th>
        <th>Transfer Required</th>
        <th>Flight Type</th>


        <th></th>

       <th>Total</th>
       <th>Paid</th>
       <th>Refund</th>
       <th>Pending</th>
    </tr>
    </thead>

    <tbody>

    @foreach($delegates as $lst )

        <tr class="">
            <th> {{ isset($lst->profile->fname)? $lst->profile->fname:'' }}</th>
            <th> {{ isset($lst->profile->surname)? $lst->profile->surname:'' }}</th>
            <th> {{ isset($lst->profile->title)? $lst->profile->title:'' }}</th>
            <th> {{ isset($lst->profile->cell_phone_code)?$lst->profile->cell_phone_code:'' }}</th>
            <th> {{ isset($lst->profile->cell_phone)?$lst->profile->cell_phone:'' }}</th>
            <th> {{ isset($lst->profile->email)? $lst->profile->email:'' }}</th>
            <th> {{ isset($lst->profile->organisation)? $lst->profile->organisation:'' }}</th>
            <th> {{ isset($lst->profile->position)? $lst->profile->position:'' }}</th>
            <th> {{ isset($lst->delegation)? $lst->delegation->name:'' }}</th>

            <th></th>

            <th> {{ isset($lst->delegateType) ? $lst->delegateType->color : '' }}</th>
            <th> {{ isset($lst->status) && $lst->status == 1 ? 'Cancelled':'Active'}}</th>
            <th> {{ isset($lst->code) ? 'Yes':'No'}}</th>
            <!-- <th> {{ $lst->vehicle_pass }} </th>
            <th> {{ $lst->vehicle_details }} </th> -->
            <th> {{ $lst->food_requirements }}</th>
            <th> {{ $lst->tech_requirements }}</th>
            <th>
                @if (isset($lst->customdata))
                    @foreach($lst->customdata as $l)
                        {{ $l->field->field_title }} -  {{$l->value}} <br>
                    @endforeach
                @endif
            </th>

            <th></th>

            <th> {{ isset($lst->code) ? $lst->code:''}} </th>
            <th> {{$lst->delegatePrice->name or '' }} </th>
            <!-- <th> {{$lst->delegatePrice->price or 0 }} </th> -->

            <th></th>

            <th> {{$lst->profile->nationality or null}}</th>
            <th> {{$lst->profile->national_id or null}}</th>
            <th> {{isset($lst->profile->national_id_image) ? 'Yes':'No'}}</th>
            <th> {{isset($lst->profile->passport_image) ? 'Yes':'No'}}</th>


            <th></th>

            <th>{{ isset($lst->inflight) ? $lst->inflight->airport_name : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->terminal : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->arrival_date : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->arrival_time : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_carrier : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_number : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->transfer_req : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_type : ''}}</th>

            <th></th>

            <th>{{ isset($lst->outflight) ? $lst->outflight->airport_name : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->terminal : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->departure_date : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->departure_time : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_carrier : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_number : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->transfer_req : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_type : ''}}</th>

            <th></th>

            <th> {{ $lst->totalBill() }}</th>
            <th> {{ isset($lst->payments) ? $lst->payments->sum('amount') : 0 }} </th>
            <th> {{ isset($lst->refunds) ? $lst->refunds->sum('amount') : 0 }}</th>
            <th> {{ ($lst->totalBill() - (isset($lst->payments) ? $lst->payments->sum('amount') : 0)) }}</th>

        </tr>

    @endforeach

    </tbody>

    <tfoot>

    </tfoot>

</table>


</html>
