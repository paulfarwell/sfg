<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 06/11/2017
 * Time: 09:06
 */
?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
    <style>
        table {
            display: table;
            border-collapse: separate;
            border-spacing: 2px;
            border-color: grey;
        }
        table td { vertical-align:top}
        table { border:1px solid #1px solid #555555; border-collapse:collapse; width:100%}
        table td, table th { border:1px solid #555555; padding:4px 5px ; text-align:left}
        table th { background:#f5f5f5;}
        .rotated th {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
            /*width: 100px;*/
            /*height: 100px;*/
            /*font-weight: normal;*/
            /*background: white !important;*/
            /*font-size: 10px;*/
            /*white-space: normal;*/
            padding: 1px 30px;
        }
    </style>
</head>
<table class="" width="100">
    <thead>
        <tr>
            <th colspan="9"> Personal Information</th>
            {{--<th></th>--}}
            <th></th>
            <th colspan="7"> Invitation Status</th>
            <th></th>
            <!-- <th colspan="{{$event->tickets->count() + 2}}"> Registration Details</th> -->
            <th></th>
            <th colspan="4"> Identification Details</th>
            <th></th>
            <!-- <th colspan="4"> Extra Contact Details</th> -->
            <th></th>
            <th colspan="8"> Flight Arrival Details</th>
            <th></th>
            <th colspan="8"> Flight Departure Details</th>
            <th></th>
            <th colspan="4"> Transfer Package Details</th>
            <th></th>
            <th colspan="3"> Hotels Details</th>
            <th></th>
            <th colspan="2"> Tent Accomodation Details</th>
            <th></th>
            <th colspan="4"> Activities</th>
            <th></th>
            <th colspan="4"> Billing Details</th>
        </tr>
    <tr class="rotated">
        <th> First Name</th>
        <th> Surname</th>
        <th> Title</th>
        <th> Country Code</th>
        <th> Phone</th>
        <th> Email</th>
        <th> Organisation</th>
        <th> Position</th>
        <th> Delegation</th>

        {{--<th>Status</th>--}}
        <th></th>

        <th> Badge Color</th>
        <th> Complimentary</th>
        <th> RSVP</th>
        <th> Link Sent</th>
        <!-- <th> Car Pass </th>
        <th> Car Details</th> -->
        <th> Food Req</th>
        <th> Tech Req</th>
        <th> Other</th>

        <th></th>

        <!-- <th> Registration Code </th>
        @foreach($event->tickets as $lst)
            <th>{{ $lst->delegates->name}}</th>
        @endforeach
        <th> Total </th> -->

        <th></th>

        <th> Nationality</th>
        <th> ID/Passport No</th>
        <th> Date Passport Issue</th>
        <th>  Date Passport Expire</th>
        <th> ID Copy Received (Y=1)</th>
        <th> Photo received (Y=1)</th>

        <th></th>

        <!-- <th>Admin Name</th>
        <th>Admin Tel</th>
        <th>Admin Office</th>
        <th>Admin Email</th> -->

        <th></th>

        <th>Airport of Origin</th>
        <th>Terminal</th>
        <th>Arrival Date</th>
        <th>Arrival Time</th>
        <th>Flight Carrier</th>
        <th>Flight Number</th>
        <th>Transfer Required</th>
        <th>Flight Type</th>

        <th></th>

        <th>Airport of Departure</th>
        <th>Terminal</th>
        <th>Arrival Date</th>
        <th>Arrival Time</th>
        <th>Flight Carrier</th>
        <th>Flight Number</th>
        <th>Transfer Required</th>
        <th>Flight Type</th>

        <th></th>

        <th>Name</th>
        <th>Location</th>
        <th>Date</th>
        <th>Total</th>

        <th></th>

        <th>Name</th>
        <th>Arrival</th>
        <th>Departure</th>

        <th></th>

        <th>Name</th>
        <th>Price</th>

         <th></th>
        {{--<th>Name</th>--}}
        {{--<th>Location</th>--}}
        {{--<th>Date</th>--}}
        {{--<th>Total</th>--}}
        @foreach($event->activities as $lst)
            <th>{{ $lst->name}}</th>
        @endforeach

        <th></th>

        <th>Total</th>
        <th>Paid</th>
        <th>Refund</th>
        <th>Pending</th>

    </tr>
    </thead>

    <tbody>

    @foreach($delegates as $lst )

        <tr class="">
            <th> {{ isset($lst->profile->fname)? $lst->profile->fname:'' }}</th>
            <th> {{ isset($lst->profile->surname)? $lst->profile->surname:'' }}</th>
            <th> {{ isset($lst->profile->title)? $lst->profile->title:'' }}</th>
            <th> {{ isset($lst->profile->cell_phone_code)?$lst->profile->cell_phone_code:'' }}</th>
            <th> {{ isset($lst->profile->cell_phone)?$lst->profile->cell_phone:'' }}</th>
            <th> {{ isset($lst->profile->email)? $lst->profile->email:'' }}</th>
            <th> {{ isset($lst->profile->organisation)? $lst->profile->organisation:'' }}</th>
            <th> {{ isset($lst->profile->position)? $lst->profile->position:'' }}</th>
            <th> {{ isset($lst->delegation)? $lst->delegation->name:'' }}</th>

            <th></th>

            <th> {{ isset($lst->delegateType) ? $lst->delegateType->color : '' }}</th>
            <th> {{ isset($lst->complimentary) && $lst->complimentary == 1 ?'Yes' : 'NO' }}</th>
            <th> {{ isset($lst->status) && $lst->status == 1 ? 'Activated':'Not Active'}}</th>
            <th> {{ isset($lst->code) ? 'Yes':'No'}}</th>
            <!-- <th> {{ $lst->vehicle_pass }} </th>
            <th> {{ $lst->vehicle_details }} </th> -->
            <th> {{ $lst->food_requirements }}</th>
            <th> {{ $lst->tech_requirements }}</th>
            <th>
                @if (isset($lst->customdata))
                    @foreach($lst->customdata as $l)
                        {{ $l->field->field_title }} -  {{$l->value}} <br>
                    @endforeach
                @endif
            </th>

            <th></th>

            <!-- <th> {{ isset($lst->code) ? $lst->code:''}} </th>
            @foreach($event->tickets as $list)
                @if ($lst->delegate_id == $list->delegate_id)
                    <th> {{$list->price}}</th>
                @else
                    <th> 0 </th>
                @endif
            @endforeach
            <th> {{$lst->delegatePrice->price or 0 }} </th> -->

            <th></th>

            <th> {{$lst->profile->nationality or null}}</th>
            <th> {{$lst->profile->national_id or null}}</th>
            <th> {{$lst->profile->date_issue or null}}</th>
            <th> {{$lst->profile->date_expire or null}}</th>
            <th> {{isset($lst->profile->national_id_image) ? 'Yes':'No'}}</th>
            <th> {{isset($lst->profile->passport_image) ? 'Yes':'No'}}</th>

            <th></th>

            <!-- <th>{{$lst->profile->a_contact_name  or null}}</th>
            <th>{{$lst->profile->a_contact_phone  or null }}</th>
            <th>{{$lst->profile->a_contact_office_phone  or null }}</th>
            <th>{{$lst->profile->a_contact_email  or null }}</th> -->

            <th></th>

            <th>{{ isset($lst->inflight) ? $lst->inflight->airport_name : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->terminal : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->arrival_date : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->arrival_time : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_carrier : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_number : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->transfer_req : ''}}</th>
            <th>{{ isset($lst->inflight) ? $lst->inflight->flight_type : ''}}</th>

            <th></th>

            <th>{{ isset($lst->outflight) ? $lst->outflight->airport_name : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->terminal : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->departure_date : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->departure_time : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_carrier : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_number : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->transfer_req : ''}}</th>
            <th>{{ isset($lst->outflight) ? $lst->outflight->flight_type : ''}}</th>

            <th></th>

            <th>{{ isset($lst->transfer) ? $lst->transfer->package->name : '' }}</th>
            <th>{{ isset($lst->transfer) ? $lst->transfer->package->location : '' }}</th>
            <th>{{ isset($lst->transfer) ? date('d-m-Y h:i',strtotime($lst->transfer->package->valid_from)) : '' }}</th>
            <th>{{ isset($lst->transfer) ? $lst->transfer->package->cost : '' }}</th>

            <th></th>

            <th>{{ isset($lst->hotel) ? (isset($lst->hotel->hotel) ? $lst->hotel->hotel->name : $lst->hotel->name ) : '' }}</th>
            <th>{{ isset($lst->hotel) ? date('d-m-Y h:i',strtotime($lst->hotel->arrival_date)) : '' }}</th>
            <th>{{ isset($lst->hotel) ? date('d-m-Y h:i',strtotime($lst->hotel->departure_date))  : '' }}</th>

            <th></th>

            <th>{{ isset($lst->tent_accomodation) ? $lst->tent_accomodation->tent->name : '' }}</th>
            <th>{{ isset($lst->tent_accomodation) ? $lst->tent_accomodation->tent->price : '' }}</th>

            <th></th>



            @foreach($event->activities as $list)
                <th>
                    @if (isset($lst->activity))
                            @if ($lst->activity->contains('activity_id', $list->id))
                                {{ $list->cost }}
                                @else
                                0
                            @endif
                        @else
                        0
                    @endif
                </th>
            @endforeach

            <th></th>

            <th> {{ $lst->totalBill() }}</th>
            <th> {{ isset($lst->payments) ? $lst->payments->sum('amount') : 0 }} </th>
            <th> {{ isset($lst->refunds) ? $lst->refunds->sum('amount') : 0 }}</th>
            <th> {{ ($lst->totalBill() - (isset($lst->payments) ? $lst->payments->sum('amount') : 0)) }}</th>

        </tr>

    @endforeach

    </tbody>

    <tfoot>

    </tfoot>

</table>


</html>
