@extends('layouts.auth')

@section('content')

    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('forgot.password') }}" method="post" style="margin-bottom:60px;">
        {{ csrf_field() }}
        <h1>{{__('trans.Forgot Password')}} ?</h1>
        <p> {{__('trans.Enter your e-mail address below to reset your password')}}. </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{{__('trans.Email')}}" name="email" /> </div>
        </div>
        <div class="form-actions">
          <button type="button" id="back-btn" class="btn yellow-button btn-outline"> {{__('trans.Back')}} </button>
            <button type="submit" class="btn blue-button pull-right"> {{__('trans.Submit')}} </button>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <!-- <form class="forget-form" action="{{ route('forgot.password') }}" method="post">
        {{ csrf_field() }}
        <h3>Forget Password ?</h3>
        <p> Enter your e-mail address below to reset your password. </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn grey-salsa btn-outline"> Back </button>
            <button type="submit" class="btn green pull-right"> Submit </button>
        </div>
    </form> -->
    <!-- END FORGOT PASSWORD FORM -->


@endsection
