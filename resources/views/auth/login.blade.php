{{--@extends('layouts.app')--}}
@extends('layouts.auth')

@section('content')


    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('login') }}" method="post" style="width:550px">
        {{ csrf_field() }}
        <h1 class="form-title">{{ __('trans.Login to your account') }}</h1>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"> Email </label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" autofocus type="text" autocomplete="off" placeholder="{{ __('trans.Email') }}" name="email" />
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{{ __('trans.Password') }}" name="password" />
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-actions">
            <label class="rememberme mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('trans.Remember Me') }}
                <span></span>
            </label>
            <button type="submit" class="btn yellow-button pull-right"> {{ __('trans.Login') }}</button>
        </div>
        {{--<div class="login-options">--}}
            {{--<h4>Or login with</h4>--}}
            {{--<ul class="social-icons">--}}
                {{--<li>--}}
                    {{--<a class="facebook" data-original-title="facebook" href="javascript:;"> </a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a class="twitter" data-original-title="Twitter" href="javascript:;"> </a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a class="linkedin" data-original-title="Linkedin" href="javascript:;"> </a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <div class="forget-password">
            <h4>{{ __('trans.Forgot your password') }} ?</h4>

            <p> {{__('trans.No worries, click') }} <a href="javascript:;" id="forget-password">{{__('trans.here') }}</a> {{__('trans.to reset your password') }}   </p>
        </div>
        {{--<div class="create-account">--}}
            {{--<p> Don't have an account yet ?&nbsp;--}}
                {{--<a href="javascript:;" id="register-btn"> Create an account </a>--}}
            {{--</p>--}}
        {{--</div>--}}
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="{{ route('forgot.password') }}" method="post">
        {{ csrf_field() }}
        <h1>{{ __('trans.Forgot your password') }} ?</h1>
        <!-- <p> Enter your e-mail address below to reset your password. </p> -->
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{{ __('trans.Email') }}" name="email" /> </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn blue-button btn-outline">{{ __('trans.Back') }}</button>
            <button type="submit" class="btn yellow-button pull-right">{{ __('trans.Submit') }} </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->


@endsection
