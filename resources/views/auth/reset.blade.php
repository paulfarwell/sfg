{{--@extends('layouts.app')--}}

@extends('layouts.auth')

@section('content')

    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('reset.password',[$reset_token]) }}" method="post" style="margin-bottom:60px;">
        {{ csrf_field() }}
        <h1 class="form-title"> Change password </h1>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"> New password </label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input required id="password1" class="form-control placeholder-no-fix" autofocus type="password" placeholder="New password" name="password1" />
                @if ($errors->has('password1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password1') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group {{ $errors->has('password2') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input required id="password2" class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm password" name="password2" />
                @if ($errors->has('password2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password2') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue-button pull-right"> Update </button>
        </div>

        <div class="forget-password">

            <p> Go back to  <a href="/" id="register-btn"> Login </a></p>
        </div>


    </form>
    <!-- END LOGIN FORM -->

@endsection
