<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 20:40
 */
?>

<!DOCTYPE html>
<!--
Template: Metronic Frontend Freebie - Responsive HTML Template Based On Twitter Bootstrap 3.3.4
Version: 1.0.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase Premium Metronic Admin Theme: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <title> Space for Giants | Events Management Portal </title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Giants club, space for giants - fight for the elephants" name="description">
    <meta content="elephants,giants,space" name="keywords">
    <meta content="Giants club" name="author">

    <meta property="og:site_name" content="Giants club summit">
    <meta property="og:title" content="Giants club summit">
    <meta property="og:description" content="Giants club summit">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts START -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">


    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Global styles END -->

    <!-- Page level plugin styles START -->
    <link href="{{ asset('assets/frontend/pages/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="{{ asset('assets/frontend/pages/css/components.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/pages/css/slider.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/themes/red.css')}}" rel="stylesheet" id="style-color">
    <link href="{{ asset('assets/frontend/corporate/css/custom.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/slick/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/slick/slick-theme.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/validation/validationEngine.jquery.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('assets/fonts/all_fonts.css') }}" rel="stylesheet" type="text/css" /> -->

    <!-- Theme styles END -->

    <style>
        /*.modal-dialog {*/
            /*width: 100%;*/
            /*height: 100%;*/
            /*padding: 0;*/
        /*}*/

        /*.modal-content {*/
            /*height: 100%;*/
            /*border-radius: 0;*/
        /*}*/
        .full-overlay {
            position: fixed;
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 2900;
            cursor: pointer;
            margin: 0 auto;text-align: center;
        }
        .full-overlay .content{
            margin:0 auto;
            /*position: relative;*/
            /*top: 50%;*/
            /*transform: translateY(-50%);*/
            position: relative;
            float: left;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .navbar{
          margin-bottom: 0px !important;
        }
         .navbar-dark{
           background-color: #045061 !important;
         }

         .navbar-dark a{
           color:#fff !important;
           font-family:'Lato-Bold' !important;
           font-size: 13pt;
         }
        .navbar-brand{
            height:auto !important;
            /* width:50%; */
          }
          .navbar-dark ul{
           margin-top:50px !important;
          }
   .header-navigation .navbar-nav>li>a:hover {
           text-decoration: none;
           background-color: unset !important;
           color:#ffd52d !important;
        }
    .header-navigation.navbar-nav>li>a:active{
      text-decoration: none;
      background-color: unset !important;
      color:#ffd52d !important;
    }
    .header-container{
          margin-top:-30px;
        }
  .header-navigation ul > li.active > a, .header-navigation ul > li > a:hover{
          color:#ffd52d !important;
        }
.count-menu li > a{
  color:#ffffff !important;
  background-color: #045061 !important;
        }
 .count-menu li > a:hover{
    color:#ffffff;
    background-color: #045061 !important;
  }
    </style>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate helvetica">

<!-- @if($event->is_deleted == 1 )
    <div class="full-overlay">
        <div class="content">
            <h2 style="color: white;">This Event has been deleted and therefore one cannot register</h2>
        </div>
    </div>
    @elseif($event->is_published == 1)
    <div class="full-overlay">
        <div class="content">
            <h2 style="color: white;">This Event has been Unpublished and therefore one cannot register</h2>
        </div>
    </div>
    @elseif($event->end < Carbon\Carbon::now())
    <div  class="full-overlay">
        <div class="content">
            <h2 style="color: white;">This Event has past and therefore one cannot register</h2>
        </div>
    </div>
@endif -->

<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container ">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->

            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->

            <div class="col-md-6 col-sm-6 col-xs-12 additional-nav pull-right">
                <ul class="list-unstyled list-inline pull-right">
                  <li><a href="https://spaceforgiants.org/" target="_blank"><i class="fa fa-home"></i> {{__('trans.Back Home') }}

                  </a></li>
                    @if (Auth::check() && Auth::user()->isRole('delegate') && isset($inviteCode) )
                    <!-- <li><a href="#">History</a></li> -->

                    <li class="dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{Auth::user()->name}}
                      </a>
                      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a href="{{ route('logout',[$event->slug]) }}">{{__('trans.Logout') }}</a></li>
                        <li><a class="dropdown-item" href="{{ route('forgot') }}">{{__('trans.Change Password') }}</a></li>
                        <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                      </ul>
                    </li>
                        @elseif (Auth::check())

                        <li><a href="{{ route('logout',[$event->slug]) }}">{{__('trans.Logout') }}</a></li>
                    @else
                      <li><a href="{{ route('login') }}">{{__('trans.Login') }}</a></li>
                    @endif

                      <li><a href="{{ url('locale/en') }}"  id="en"><img src="https://restcountries.eu/data/gbr.svg" width="30px"/> EN</a></li>
                      <li ><a href="{{ url('locale/fr') }}"  id="fr"><img src="https://restcountries.eu/data/fra.svg" width="30px"/> FR</a></li>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <a class="site-logo" href="#">
      @if(!empty($event->logo))
      <img src="{{ asset('/uploads/event-logos/'.$event->logo)}}" alt="{{ $event->name }}" height="102px" >
      @elseif ($event->slug === 'africa-s-wildlife-economy-summit-test')
        @if ($event->sponsor->count() > 0 )
          @foreach( $event->sponsor as $lst)
             @if($lst->website === 'http://au.int' || $lst->website === 'http://www.unenvironment.org')
             @if($lst->website === 'http://au.int')
            <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-left">
            @else
            <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-right">
            @endif

            @else
            @endif
          @endforeach
          @endif
      @else
      <!-- <img src="{{ asset('assets/frontend/corporate/img/logos/logo-2.png')}}" alt="{{ $event->name }}" height="72px"></a> -->
      @endif
      </a>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit" id="top-menu">
            <ul id="top-menu-links " class="text-uppercase">
                @if (Auth::check() && Auth::user()->isRole('delegate') && isset($inviteCode) )
                    <li><a href="{{ route('del.view.event',[$event->slug,$inviteCode]) }}" > {{ $event->name }} </a></li>
                    <!-- <li ><a href="{{ route('del.view.event.about',[$event->slug,$inviteCode]) }}" > About Giants Club Summit </a></li> -->
                    <li ><a href="{{ route('del.view.event.info',[$event->slug,$inviteCode]) }}" >{{__('trans.Essential Information') }}  </a></li>
                    <li ><a href="{{ route('del.view.event.spo',[$event->slug,$inviteCode]) }}" >{{__('trans.Partners') }}  </a></li>
                    @if (str_contains(Request::url(),'/edit/'))
                        <li ><a href="{{ route('del.manage.invitation',[$inviteCode,$event->slug]) }}" >{{__('trans.Editing Invitation') }}</a></li>
                    @elseif (str_contains(Request::url(),'/prev/edit/'))
                        <li ><a href="{{ route('del.manage.prev.invitation',[$inviteCode,$event->slug]) }}" > Re-editing Invitation </a></li>
                    @else
                        <li ><a href="{{ route('del.view.invitation',[$inviteCode,$event->slug]) }}" >{{__('trans.My Profile') }}</a></li>
                    @endif

                    @else
                    <li ><a href="{{ route('org.view.event',$event->slug) }}" > {{$event->name }} </a></li>
                    <!-- <li ><a href="{{ route('org.view.event.about',$event->slug) }}" > About Giants Club Summit </a></li> -->
                    <li ><a href="{{ route('org.view.event.info',$event->slug) }}" >{{__('trans.Essential Information') }}</a></li>
                    <li ><a href="{{ route('org.view.event.spo',$event->slug) }}" >{{__('trans.Partners') }}</a></li>

                    {{--@if (Auth::check() && Auth::user()->isRole('delegate') )--}}
                        {{--<a href="{{ route('del.view.event.info',[$event->slug,$code]) }}" class="btn" style="background: #9F906E;border-radius: 15px 15px 15px 15px !important; color: white !important;"> Click here for more essential Information </a>--}}
                    <!-- @if (Auth::check())
                        <li> <a href="{{ route('org.view.event.info',$event->slug) }}" > Essential Information </a></li>
                    @endif -->
                @endif
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>

<!-- Header END -->

<!-- BEGIN SLIDER -->

{{--<div class="container">--}}

{{--</div>--}}

<div class="page-slider margin-bottom-40">


        @yield('slider')


</div>
<!-- END SLIDER -->

<div class="main">

    @if (Auth::check())
    <div class="pull-right validate_form" style="margin-right: 100px; margin-top: -590px;clear: both;">

    </div>
        @else
    <!-- <div class="pull-right validate_form" style="margin-right: 100px; margin-top: -590px;clear: both;">
        <form class="form form-horizontal pull-right" style="width:450px;" method="post" id="validatation" action="">
            <div class="form-body">
                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <div class="input-group">
                        <input id="invitation_code" type="text" class="validate[required]" name="invitation_code" placeholder="To proceed, please enter your invitation code here">
                        <span class="input-group-btn">
                                <button id="genpassword" class="btn" type="submit" > Validate </button>
                        </span>
                    </div>
                </div>
            </div>
        </form>
    </div> -->
    @endif

    <div class="container" id="content">
        @yield('content')
    </div>
</div>

<div class="page-slider ">
    <div style="color:white;">
        @yield('overview-content')
    </div>
</div>

<div class="page-slider program">
    <div style="">
        @yield('program-content')
    </div>
</div>

<div class="main">
    <div class="container" id="content">
        {{--@yield('content')--}}
        @yield('meta-content')
        <div class="row service-box margin-bottom-40">
            <div class="slider multiple-items transparent_div" align="center" style="text-align: center;">
                @foreach($event->organizer as $lst)
                    <div class="slide" style="position: relative;text-align: center;"><a href="{{$lst->website}}" target="_blank"> <img src="{{ asset($lst->logo) }}"  > </a> </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


<!--  validate modal  -->

<!-- <div class="modal fade" id="myModal" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"> Invitation Code Validation </h4>
            </div>
            <div class="modal-body">
                <iframe id="frame" name="frame" frameborder="0" width="100%" height="400px"></iframe>
            </div>
            <div class="modal-footer">
                <small class="pull-left"> Do not Close window until code verification is complete</small>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div> -->



<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-12 col-sm-12 padding-top-10" align="center">
                <p> www.wildlifeeconomy.com | Invites@wildlifeeconomy.com | +254 736 442 264</p>
                <p> Space for Giants is an international conservation charity, registered in the UK (charity no: 1139771), USA (EIN: 47-1805681) and Kenya, governed by a voluntary Board of Trustees</p>
                <p> Copyright © <?php echo date('Y'); ?> Space for Giants. All rights reserved.</p>
            </div>
            <!-- END COPYRIGHT -->

        </div>
    </div>
</div>


@javascript('page_up_img',  asset('assets/frontend/corporate/img/up.png') )

<!-- END FOOTER -->

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/plugins/respond.min.js')}}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>

<script src="{{ asset('assets/validation/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine.js') }}"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{ asset('assets/frontend/plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src="{{ asset('assets/frontend/slick/slick.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{ asset('assets/frontend/corporate/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/pages/scripts/bs-carousel.js')}}" type="text/javascript"></script>


@if( Config::get('app.locale') == 'en')
  <script>
    jQuery(document).ready(function() {

     $('#en').addClass('m-active');
    $('#fr').removeClass('m-active');
    });

  </script>
 @elseif ( Config::get('app.locale') == 'fr' )
 <script>
 jQuery(document).ready(function() {

  $('#fr').addClass('m-active');
 $('#en').removeClass('m-active');

 });
 </script>
 @endif


<script type="text/javascript">

    jQuery("form").validationEngine('attach', { prettySelect: true,validateNonVisibleFields: true,
        usePrefix: 's2id_',useSuffix: "select2-offscreen",promptPosition : "bottomRight", autoPositionUpdate: true });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        //Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
        //Layout.initNavScrolling();
        $('[data-toggle="tooltip"]').tooltip();
        var curpage = '{{ Request::url() }}';
        var links = $("#top-menu-links li");
        links.each(function() {
            var url = $( this ).find( "a" ).attr('href'); //console.log(url);
            if (curpage == url ) 	{
                $(this).addClass('active');
                //$(this).parents('li').addClass('active open');
                //$( this ).parents('li').find( "a" ).append("<span class='selected'> </span>");
                //$( this ).parents('li').find( "a" ).append("<span class='arrow open'></span>");
            }
        });

    });
</script>
<script>
  jQuery(document).ready(function() {
    var link = $('.header-navigation>ul>li>a');
     var query = location.href;
      $.each( link, function( key, value ) {
           console.log($(value));
           if ($(value).attr('href') == query){
               $(value).parent().addClass('active');
            }


      });
    // if ($(link).attr('href') == query){
    //    console.log(link);
    // }



  });


</script>

@include('partial.messages')
<script>
    $('.multiple-items').slick({
        prevArrow: '<i class="fa fa-angle-left fa-6 btn-prev " style="font-size: 36px;" aria-hidden="true" aria-label="Previous" ></i>',
        nextArrow:'<i class="fa fa-angle-right fa-6 btn-next " style="font-size: 36px;" aria-hidden="true" aria-label="Next" ></i>',
        slidesToShow: 6,
        slidesToScroll: 6,
        autoplay: false,
        autoplaySpeed: 1000,
        arrows: true,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
</script>
<!-- <script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script> -->
<script>
    var $buoop = {vs:{i:10,f:-4,o:-4,s:8,c:-4},api:4};
    function $buo_f(){
        var e = document.createElement("script");
        e.src = "//browser-update.org/update.min.js";
        document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}
</script>

<!-- END PAGE LEVEL JAVASCRIPTS -->

@yield('footer')

</body>
<!-- END BODY -->
</html>
