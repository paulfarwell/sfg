<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 20:40
 */
?>

<!DOCTYPE html>
<!--
Template: Metronic Frontend Freebie - Responsive HTML Template Based On Twitter Bootstrap 3.3.4
Version: 1.0.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase Premium Metronic Admin Theme: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <title> Space for Giants | Delegate Login </title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Giants club, space for giants - fight for the elephants" name="description">
    <meta content="elephants,giants,space" name="keywords">
    <meta content="Giants club" name="author">

    <meta property="og:site_name" content="Giants club summit">
    <meta property="og:title" content="Giants club summit">
    <meta property="og:description" content="Giants club summit">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts START -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Global styles END -->

    <!-- Page level plugin styles START -->
    <link href="{{ asset('assets/frontend/pages/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="{{ asset('assets/frontend/pages/css/components.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/pages/css/slider.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/themes/red.css')}}" rel="stylesheet" id="style-color">
    <link href="{{ asset('assets/frontend/corporate/css/custom.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/slick/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/slick/slick-theme.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/validation/validationEngine.jquery.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme styles END -->

    <style>
        /*.modal-dialog {*/
            /*width: 100%;*/
            /*height: 100%;*/
            /*padding: 0;*/
        /*}*/

        /*.modal-content {*/
            /*height: 100%;*/
            /*border-radius: 0;*/
        /*}*/
    </style>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">


<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->

            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-6 col-sm-6 additional-nav pull-right">
                <ul class="list-unstyled list-inline pull-right">
                    {{--<li><a href="page-login.html">Log In</a></li>--}}
                    <li><a href="#" style="color: #433A32; font-size: 16px;">  Back to the main Site &nbsp;
                            <img style="position: relative;vertical-align:bottom;" src="{{ asset('assets/frontend/corporate/img/house.png') }}" align="bottom" width="17" class="pull-right">
                    </a></li>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
<a class="site-logo" href="#">
    @if(!empty($event->logo))
    <img src="{{ asset('/uploads/event-logos/'.$event->logo)}}" alt="{{ $event->name }}" height="102px" >
    @elseif ($event->slug === 'africa-s-wildlife-economy-summit-test')
      @if ($event->sponsor->count() > 0 )
        @foreach( $event->sponsor as $lst)
           @if($lst->website === 'http://au.int' || $lst->website === 'http://www.unenvironment.org')
           @if($lst->website === 'http://au.int')
          <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-left">
          @else
          <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-right">
          @endif

          @else
          @endif
        @endforeach
        @endif
    @else
    <!-- <img src="{{ asset('assets/frontend/corporate/img/logos/logo-2.png')}}" alt="{{ $event->name }}" height="72px"></a> -->
    @endif
  </a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit" id="top-menu">
            <ul id="top-menu-links">
                <li ><a href="{{ route('del.view.event',[$event->slug,$code]) }}" > {{ ucwords(strtolower($event->name)) }} </a></li>
                <li ><a href="{{ route('del.view.event.spo',[$event->slug,$code]) }}" > Partners</a></li>
                <li ><a href="{{ route('del.view.event.info',[$event->slug,$code]) }}" > Essential Information </a></li>
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
<!-- Header END -->

<!-- BEGIN SLIDER -->

{{--<div class="container">--}}

{{--</div>--}}

<div class="page-slider ">

    <div id="carousel-example-generic" class="carousel slide carousel-slider">
        @yield('slider')
    </div>

</div>
<!-- END SLIDER -->

<div class="main">

    <div class="pull-right validate_form" style="margin-right: 100px; margin-top: -550px;clear: both;">
        @if ($err == 0 || $err == null)

            <div class="login-form">
                <div class="alert alert-danger">
                    <strong>Error!</strong> {{ $msg }}  <br></br>
                    <p> please click <a href="{{ route('org.view.event',$event->slug) }}" > Here </a> to try again</p>
                </div>
            </div>

        @else

            {{--<a href="{{ route('org.view.event',$eventid) }}" class="pull-right"> X </a>--}}
            <form action="{{ route('delegate.login.with.code',[$code,$eventid])  }}" class="form form-horizontal pull-right" style="width:450px;" method="post" id="validatation" >
                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                <div class="form-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password" class="form-input validate[required] form-control" placeholder="Please enter the one-time password that was sent to your email">
                            <span class="input-group-btn">
                                {{--<input type="submit" name="proceed" id="proceed" data-dismiss="modal" value="proceed" class="btn">--}}
                                <button id="genpassword" class="btn" type="submit" > proceed </button>
                        </span>
                        </div>
                    </div>
                </div>
            </form>

        @endif
    </div>

    <div class="container" id="content">

        @yield('content')

    </div>
</div>


<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-12 col-sm-12 padding-top-10" align="center">
                <p> www.wildlifeeconomy.com | Invites@wildlifeeconomy.com | +254 736 442 264</p>
                <p> Space for Giants is an international conservation charity, registered in the UK (charity no: 1139771), USA (EIN: 47-1805681) and Kenya, governed by a voluntary Board of Trustees</p>
                <p> Copyright © <?php echo date('Y'); ?> Space for Giants. All rights reserved.</p>
            </div>
            <!-- END COPYRIGHT -->

        </div>
    </div>
</div>


@javascript('page_up_img',  asset('assets/frontend/corporate/img/up.png') )

<!-- END FOOTER -->

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/plugins/respond.min.js')}}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>

<script src="{{ asset('assets/validation/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine.js') }}"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{ asset('assets/frontend/plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src="{{ asset('assets/frontend/slick/slick.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{ asset('assets/frontend/corporate/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/pages/scripts/bs-carousel.js')}}" type="text/javascript"></script>


<script type="text/javascript">

    jQuery("form").validationEngine('attach', { prettySelect: true,validateNonVisibleFields: true,
        usePrefix: 's2id_',useSuffix: "select2-offscreen",promptPosition : "bottomRight", autoPositionUpdate: true });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        //Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
        //Layout.initNavScrolling();

        var curpage = '{{ Request::url() }}';
        var links = $("#top-menu-links li");
        links.each(function() {
            var url = $( this ).find( "a" ).attr('href'); //console.log(url);
            if (curpage == url ) 	{
                $(this).addClass('active');
                //$(this).parents('li').addClass('active open');
                //$( this ).parents('li').find( "a" ).append("<span class='selected'> </span>");
                //$( this ).parents('li').find( "a" ).append("<span class='arrow open'></span>");
            }
        });

    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->

@yield('footer')

</body>
<!-- END BODY -->
</html>
