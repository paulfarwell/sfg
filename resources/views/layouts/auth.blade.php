<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 22/06/2017
 * Time: 20:23
 */
?>
<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Space for Giants | User Login </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="space for giants events management portal" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('assets/pages/css/login-3.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    @yield('header')
 <style>
 .background-over {
    background: rgb(132, 117, 78) none repeat scroll 0% 0%;
}
.navbar{
  margin-bottom: 0px !important;
}
 .navbar-dark{
   background-color: #045061 !important;
 }
 .navbar-dark ul{
  margin-top:20px !important;
 }
 .navbar-dark a{
   color:#fff ;
   font-family:'Lato-Bold' !important;
   font-size: 13pt;
 }
.navbar-brand{
    height:auto !important;
    width:40%;
  }
  /* .navbar-brand img{
    margin-left:80%
  } */
  .mt-checkbox, .mt-radio{
    font-size:18px !important;
    font-family:'Lato-Regular' !important;
    color:#fff !important;
  }
.footer{
  background-color: #433A32;
  color:#84754e;
}
.login-form i{
  color:#84754e;
}
.forget-form{
  min-height:308px !important;
}
.forget-form i{
  color:#84754e;
}
.form-control::-webkit-input-placeholder { color: #84754e; font-family: 'Lato-Regular'}  /* WebKit, Blink, Edge */
.form-control:-moz-placeholder { color: #84754e; font-family: 'Lato-Regular' }  /* Mozilla Firefox 4 to 18 */
.form-control::-moz-placeholder { color: #84754e; font-family: 'Lato-Regular'}  /* Mozilla Firefox 19+ */
.form-control:-ms-input-placeholder { color: #84754e; font-family: 'Lato-Regular' }  /* Internet Explorer 10-11 */
.form-control::-ms-input-placeholder { color: #84754e; font-family: 'Lato-Regular' }  /* Microsoft Edge */
.mt-checkbox.mt-checkbox-outline > span{
  background-color:#fff !important;
}
nav > li > a:focus, .nav > li > a:hover {
    text-decoration: none;
    background-color: #045061 !important;
}
@media (max-width: 768px) {
  .login{

  background-color: rgb(132, 117, 78)!important;
  background-image: none !important;
  background-position: center;
  background-size: 100%;
  opacity: 0.8;
  background-repeat: no-repeat;
  }
  .navbar-brand img{
    margin-left:20%
  }

    }
  .dropdown.open>.dropdown-toggle{
      background-color: #045061 !important;
    }
    .dropdown-menu>li>a:hover {
      background-color: #045061 !important;
    }
</style>

</head>
<!-- END HEAD -->

<body >
  @php
  $event = App\Models\Event::where('status', '=', 'active')->where('is_deleted', '=', 'NULL')->first();

  @endphp
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
     <div class="container-fluid" style="margin-right:40px;">
       <a class="navbar-brand" href="#">

       @if(!empty($event->logo))
       <img src="{{ asset('/uploads/event-logos/'.$event->logo)}}" alt="{{ $event->name }}" height="102px" >
       @elseif ($event->slug === 'africa-s-wildlife-economy-summit-test')
         @if ($event->sponsor->count() > 0 )
           @foreach( $event->sponsor as $lst)
              @if($lst->website === 'http://au.int' || $lst->website === 'http://www.unenvironment.org')
              @if($lst->website === 'http://au.int')
             <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-left">
             @else
             <img src="{{ asset($lst->logo) }}"  alt="{{ $event->name }}"  height="102px" style="margin-right:10px" class="pull-right">
             @endif

             @else
             @endif
           @endforeach
           @endif
       @else
       <!-- <img src="{{ asset('assets/frontend/corporate/img/logos/logo-2.png')}}" alt="{{ $event->name }}" height="72px"></a> -->
       @endif

      </a>

      <ul class="nav navbar-nav pull-right" style="margin-top: 30px !important;">
          <!-- BEGIN NOTIFICATION DROPDOWN -->
          <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
          <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
          <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
          <li><a href="{{ url('locale/en') }}"  id="en"><img src="https://restcountries.eu/data/gbr.svg" width="30px"/> EN</a></li>
          <li ><a href="{{ url('locale/fr') }}"  id="fr"><img src="https://restcountries.eu/data/fra.svg" width="30px"/> FR</a></li>
        </ul>
      <ul class="nav navbar-nav pull-right">
        <li style="margin-top:12px;" ><a href="{{url('/event/'.$event->slug) }}" >{{ __('login.Home') }} </a></li>
    </ul>


     </div>
   </nav>
<div class="background-over">
  <div class="login">



<!-- BEGIN LOGO -->
<div class="logo">

</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    @yield('content')

</div>
<div class="logo2">

</div>
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-12 col-sm-12 padding-top-10" align="center">
                <p> www.wildlifeeconomy.com | Invites@wildlifeeconomy.com | +254 736 442 264</p>
                <p> Space for Giants is an international conservation charity, registered in the UK (charity no: 1139771), USA (EIN: 47-1805681) and Kenya, governed by a voluntary Board of Trustees</p>
                <p> Copyright © <?php echo date('Y'); ?> Space for Giants. All rights reserved.</p>
            </div>
            <!-- END COPYRIGHT -->

        </div>
    </div>
</div>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/pages/scripts/login.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
<script src="{{ asset('assets/validation/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine.js') }}"></script>

@if( Config::get('app.locale') == 'en')
  <script>
    jQuery(document).ready(function() {
     console.log('o');
     $('#en').addClass('m-active');
    $('#fr').removeClass('m-active');
    });

  </script>
 @elseif ( Config::get('app.locale') == 'fr' )
 <script>
 jQuery(document).ready(function() {
     console.log('p');
  $('#fr').addClass('m-active');
 $('#en').removeClass('m-active');

 });
 </script>
 @endif

<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<script>
    jQuery("form").validationEngine('attach', { prettySelect: true,validateNonVisibleFields: true, usePrefix: 's2id_',useSuffix: "select2-offscreen", autoPositionUpdate: true });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('footer')

@include('partial.messages')
</div>
</div>
</body>

</html>
