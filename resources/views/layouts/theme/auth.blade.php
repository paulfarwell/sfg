<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 17/06/2017
 * Time: 18:46
 */
?>
<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.0-alpha.6
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
 -->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="manage events online">
    <meta name="author" content="Giants Club powered by Farwell Consultants">
    <meta name="keyword" content="giants,club,events">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
    <title> Giants Club Events Management </title>
    <!-- Icons -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/simple-line-icons.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/validation/validationEngine.jquery.css') }} " rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

    @yield('header')

</head>

<body class="app flex-row align-items-center">

<div class="container">

    @yield('body')

</div>

<!-- Bootstrap and necessary plugins -->
<script src="{{ asset('assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine.js') }}"></script>


<script>
    jQuery("form").validationEngine('attach', { prettySelect: true,validateNonVisibleFields: true, usePrefix: 's2id_',useSuffix: "select2-offscreen", autoPositionUpdate: true });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('footer')

@include('partial.messages')

</body>

</html>
