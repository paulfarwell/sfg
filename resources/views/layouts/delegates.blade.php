<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/07/2017
 * Time: 20:40
 */
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <title> Space for Giants - Delegates Backend </title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Giants club, space for giants - fight for the elephants" name="description">
    <meta content="elephants,giants,space" name="keywords">
    <meta content="Giants club" name="author">

    <meta property="og:site_name" content="Giants club summit">
    <meta property="og:title" content="Giants club summit">
    <meta property="og:description" content="Giants club summit">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts START -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="{{ asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Global styles END -->

    <!-- Page level plugin styles START -->
    <link href="{{ asset('assets/frontend/pages/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="{{ asset('assets/frontend/pages/css/components.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/pages/css/slider.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/corporate/css/themes/red.css')}}" rel="stylesheet" id="style-color">
    <link href="{{ asset('assets/frontend/corporate/css/custom.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/slick/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/slick/slick-theme.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    {{--<link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ asset('assets/validation/validationEngine.jquery.css') }}" rel="stylesheet" type="text/css" />--}}

    <link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/validation/validationEngine.jquery.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/frontend/corporate/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />


    <!-- Theme styles END -->

    <style>
        /*.modal-dialog {*/
        /*width: 100%;*/
        /*height: 100%;*/
        /*padding: 0;*/
        /*}*/

        /*.modal-content {*/
        /*height: 100%;*/
        /*border-radius: 0;*/
        /*}*/
    </style>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">

<!-- BEGIN TOP BAR -->
<div class="pre-header" style="background: black;">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-6 col-sm-6 additional-shop-info">
                <ul class="list-unstyled list-inline">

                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-6 col-sm-6 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    {{--<li><a href="page-login.html">Log In</a></li>--}}
                    <li>
                    </li>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header" style="background: black;align-content: center; padding-bottom: 20px;" align="center">
    <div class="container" align="center" style="align-content: center;">
            <div class="col-md-4 col-sm-4" align="center"></div>
            <div class="col-md-4 col-sm-4" align="center">
                {{--<a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars" style="clear: both;"></i></a>--}}
                <a href=""><img src="{{ asset('assets/frontend/corporate/img/logos/logo-3.jpg')}}" style="clear: both;align-content: center;" alt="giants club summit"></a>
            </div>
            <div class="col-md-4 col-sm-4" align="center">
                <a href="#" class="pull-right" style="color: #433A32; font-size: 13px;font-weight: bold; text-transform: uppercase;padding: 0px 5px 3px;">  Back to the main Site </a>
                <a href="#" class="pull-right" style="color: #433A32; font-size: 13px;font-weight: bold; text-transform: uppercase;padding: 0px 5px 3px;">  Logout </a>
            </div>
    </div>
</div>

<div class="header" style="background: black; align-content: center;" align="center">
    <div align="center" style="align-content: center;background: #333333;color: #9F906E; text-transform: uppercase; margin-top:-23px;height:40px">
        <div class="" align="center" style="clear: both; align-content: center">
            <ul style="margin: 0px auto;  padding: 0;  list-style: none;  font-weight: bold;    padding-top: 12px;text-align: center;">
                <li style="display: inline-block;"><a target="_blank" style="padding: 34px 25px 13px;" href="{{ route('org.view.event',$event->slug) }}" > {{ ucwords(strtolower($event->name)) }} </a></li>
                <li style="display: inline-block;"><a target="_blank" style="padding: 34px 25px 13px;" href="{{ route('org.view.event.spo',$event->slug) }}" > Sponsors </a></li>
                {{--<li style="display: inline-block;"><a target="_blank" style="padding: 34px 25px 13px;" href="{{ route('org.view.event.info',$event->slug) }}" > Essential Information </a></li>--}}
            </ul>
        </div>
    </div>
</div>

<!-- Header END -->


<div class="main">

    <div class="container" id="content" style="min-height: 500px">

        @yield('content')



    </div>

</div>

<div class="container">
<div class="row service-box margin-bottom-40" style="clear: both;">
    <div class="slider multiple-items transparent_div" align="center" style="text-align: center;">
        @foreach($event->organizer as $lst)
            <div class="slide" style="position: relative;text-align: center;"> <img src="{{ asset($lst->logo) }}" width="100" style="width: 100px; margin: auto;" >  </div>
        @endforeach
    </div>
</div>
</div>

<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-12 col-sm-12 padding-top-10" align="center">
                <p> www.wildlifeeconomy.com | Invites@wildlifeeconomy.com | +254 736 442 264</p>
                <p> Space for Giants is an international conservation charity, registered in the UK (charity no: 1139771), USA (EIN: 47-1805681) and Kenya, governed by a voluntary Board of Trustees</p>
                <p> Copyright © <?php echo date('Y'); ?> Space for Giants. All rights reserved.</p>
            </div>
            <!-- END COPYRIGHT -->

        </div>
    </div>
</div>
<!-- END FOOTER -->

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/frontend/plugins/respond.min.js')}}"></script>
<![endif]-->
<script src="{{ asset('assets/frontend/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>

<script src="{{ asset('assets/validation/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('assets/validation/jquery.validationEngine.js') }}"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{ asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{ asset('assets/frontend/plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src="{{ asset('assets/frontend/slick/slick.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/corporate/scripts/tag-it.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/eonasdan-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/horizontal-timeline/horizontal-timeline.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/frontend/corporate/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/pages/scripts/bs-carousel.js')}}" type="text/javascript"></script>


<script type="text/javascript">

    jQuery("form").validationEngine('attach', { prettySelect: true,validateNonVisibleFields: true,
        usePrefix: 's2id_',useSuffix: "select2-offscreen",promptPosition : "bottomRight", autoPositionUpdate: true });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        $.widget.bridge('UITabs', $.ui.tabs );
        //Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
        //Layout.initNavScrolling();

        var curpage = '{{ Request::url() }}';
        var links = $("#top-menu-links li");
        links.each(function() {
            var url = $( this ).find( "a" ).attr('href'); //console.log(url);
            if (curpage == url ) 	{
                $(this).addClass('active');
                //$(this).parents('li').addClass('active open');
                //$( this ).parents('li').find( "a" ).append("<span class='selected'> </span>");
                //$( this ).parents('li').find( "a" ).append("<span class='arrow open'></span>");
            }
        });

    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
    <script>
        $('.multiple-items').slick({
            prevArrow: '<i class="fa fa-angle-left fa-6 btn-prev " style="font-size: 36px;" aria-hidden="true" aria-label="Previous" ></i>',
            nextArrow:'<i class="fa fa-angle-right fa-6 btn-next " style="font-size: 36px;" aria-hidden="true" aria-label="Next" ></i>',
            slidesToShow: 6,
            slidesToScroll: 6,
            autoplay: false,
            autoplaySpeed: 1000,
            arrows: true,
            pauseOnHover: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    </script>


@yield('footer')

@include('partial.messages')
{{--@include('partial.messages-form')--}}

</body>
<!-- END BODY -->
</html>
