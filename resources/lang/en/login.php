<?php

return [
    'Home'       => 'Home',
    'Login' => 'Login',
    'Login to your account' => 'Login to your account',
    'Email'=> 'Email',
    'Password'=> 'Password',
    'Remember Me'=> 'Remember Me',
    'No worries, click' => 'No worries, click',
    'here' => 'here',
    'to reset your password'=>'to reset your password',
    'Forgot your password'=> 'Forgot your password',
    'Submit'=>'Submit',
    'Back'=>'Back'

];
