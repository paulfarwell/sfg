<?php

return [
  'Home' => 'Accueil',
  'Login' => 'Connexion',
  'Login to your account' => 'Connectez-vous à votre compte',
  'Email'=> 'Email',
  'Password'=> 'Mot de passe',
  'Remember Me'=> 'Confirmer le mot de passe ',
  'No worries, click' => 'Pas de soucis,cliquez',
  'here' => 'ici',
  'to reset your password'=>'pour reinitialiser votre mot de passe',
  'Forgot your password'=> 'Mot de passe oublié',
  'Submit'=>'Soumettre',
  'Back'=>'Retour'

];
