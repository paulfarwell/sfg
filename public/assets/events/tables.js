
    var initTable1 = function () {
        var table = $('#events_meta');
        var oTable1 = table.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event Meta data Types - Data export' ,exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event Meta data Types - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event Meta data Types - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event Meta data Types - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event Meta data Types - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event Meta data Types reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": true, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: list_events,
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
            {data: 'field_name', name: 'field_name', orderable: true, searchable: true},
            {data: 'content_title', name: 'content_title', orderable: false, searchable: true},
            {data: 'content_description', name: 'content_description', orderable: false, searchable: false},
            {data: 'status', name: 'statu', orderable: false, searchable: false},
            {data: 'tools', name: 'tools', orderable: false, searchable: true},
        ]
    });
        // handle datatable custom tools
        // $('#sample_3_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable1.DataTable().button(action).trigger();
        // });
    }

    var initTable2 = function () {
        var table2 = $('#ticket_categories');
        var oTable2 = table2.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event tickets Types reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": true, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: list_tickets,
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
            {data: 'delegates.name', name: 'delegates.name', orderable: true, searchable: true},
            {data: 'validity', name: 'validity', orderable: false, searchable: false},
            {data: 'price', name: 'price', orderable: false, searchable: true},
            // {data: 'ticket_number', name: 'ticket_number', orderable: false, searchable: false},
            {data: 'notes', name: 'notes', orderable: false, searchable: false},
            {data: 'tools', name: 'tools', orderable: false, searchable: false},
        ]
    });
    // handle datatable custom tools
    // $('#sample_4_tools > li > a.tool-action').on('click', function() {
    //     var action = $(this).attr('data-action');
    //     oTable2.DataTable().button(action).trigger();
    // });
    }

    var initTable3 = function () {
        var table3 = $('#event-hotels');
        var oTable3 = table3.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event tickets hotels reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": [[5,'asc']], //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: list_hotels,
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
            {data: 'hotel.name', name: 'hotel.name', orderable: true, searchable: true},
            {data: 'delegates', name: 'delegates', orderable: false, searchable: false},
            {data: 'hotel.address', name: 'hotel.address', orderable: false, searchable: true},
            {data: 'contacts', name: 'contacts', orderable: false, searchable: false},
            {data: 'available', name: 'available', orderable: false, searchable: false},
            {data: 'hotel.order_custom', name: 'order_custom', orderable: false, searchable: false},
            {data: 'all-images', name: 'all-images', orderable: false, searchable: false},
            {data: 'tools', name: 'tools', orderable: false, searchable: false},
        ]
    });
    // // handle datatable custom tools
    // $('#sample_5_tools > li > a.tool-action').on('click', function() {
    //     var action = $(this).attr('data-action');
    //     oTable3.DataTable().button(action).trigger();
    // });
    }

    var initTable4 = function () {
        var table3 = $('#events_programme');
        var oTable3 = table3.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event programme reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": true, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: list_event_programme, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
            {data: 'p_type', name: 'p_type', orderable: true, searchable: true},
            {data: 'title', name: 'title', orderable: true, searchable: false},
            {data: 'description', name: 'description', orderable: false, searchable: true},
            {data: 'start', name: 'start', orderable: false, searchable: false},
            {data: 'end', name: 'end', orderable: false, searchable: false},
            {data: 'status', name: 'status', orderable: false, searchable: true},
            {data: 'tools', name: 'tools', orderable: false, searchable: false},
        ]
    });
    // handle datatable custom tools
    // $('#sample_5_tools > li > a.tool-action').on('click', function() {
    //     var action = $(this).attr('data-action');
    //     oTable3.DataTable().button(action).trigger();
    // });
    }

    var initTable5 = function () {
        var table5 = $('#events_posters');
        var oTable5 = table5.dataTable({
                buttons: [
                    { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                    { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            // alert('Event Meta data Types reloaded!');
                            toastr.info('Event programme reloaded!');
                        }
                    },
                ],
                responsive: true,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ordering": true, //disable column ordering
                "lengthMenu": [
                    [5, 10, 15, 20,25, -1],
                    [5, 10, 15, 20,25, "All"] // change per page values here
                ],
                "pageLength": 25,
                "ajax": {
                    url: list_event_poster, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
            {data: 'image_path', name: 'image_path', orderable: false, searchable: true},
            {data: 'is_main', name: 'is_main', orderable: false, searchable: false},
            {data: 'sort', name: 'sort', orderable: false, searchable: true},
            {data: 'status', name: 'status', orderable: false, searchable: false},
            {data: 'tools', name: 'tools', orderable: false, searchable: false},
        ]
    });
    // handle datatable custom tools
    // $('#sample_5_tools > li > a.tool-action').on('click', function() {
    //     var action = $(this).attr('data-action');
    //     oTable3.DataTable().button(action).trigger();
    // });
}

    var initTable6 = function () {

        var table6 = $('#event-act');
        var oTable6 = table6.dataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event Meta data Types reloaded!');
                        toastr.info('Event programme reloaded!');
                    }
                },
            ],
            // responsive: true,
            "deferRender": true,
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: list_event_act, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'location', name: 'location', orderable: false, searchable: true},
                {data: 'valid_from', name: 'valid_from', orderable: false, searchable: true},
                {data: 'valid_to', name: 'valid_to', orderable: false, searchable: true},
                {data: 'cost', name: 'cost', orderable: false, searchable: true},
                {data: 'img', name: 'img', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
        // handle datatable custom tools
        // $('#sample_5_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable6.DataTable().button(action).trigger();
        // });
    }

    var initTable7 = function () {
        var table7 = $('#event-transport');
        var oTable7 = table7.dataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event Meta data Types reloaded!');
                        toastr.info('Event programme reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: list_event_trans, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'location', name: 'location', orderable: false, searchable: false},
                {data: 'valid_from', name: 'valid_from', orderable: false, searchable: false},
                {data: 'valid_to', name: 'valid_to', orderable: false, searchable: false},
                {data: 'cost', name: 'cost', orderable: false, searchable: false},
                {data: 'all-images', name: 'all-images', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
        // handle datatable custom tools
        // $('#sample_5_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable7.DataTable().button(action).trigger();
        // });
    }

    var initTable8 = function () {
        var table8 = $('#event-spo');
        var oTable8 = table8.dataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event Meta data Types reloaded!');
                        toastr.info('Event programme reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: list_event_sponsors, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'logo', name: 'logo', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'website', name: 'website', orderable: false, searchable: true},
                {data: 'brief', name: 'brief', orderable: false, searchable: false},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
        // handle datatable custom tools
        // $('#sample_5_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable8.DataTable().button(action).trigger();
        // });
    }

    var initTable9 = function () {
        var table9 = $('#event-org');
        var oTable9 = table9.dataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event Meta data Types reloaded!');
                        toastr.info('Event programme reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: list_event_organizers, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'logo', name: 'logo', orderable: false, searchable: false},
                {data: 'name', name: 'name', orderable: true, searchable: true},
                {data: 'website', name: 'website', orderable: false, searchable: true},
                {data: 'brief', name: 'brief', orderable: false, searchable: false},
                {data: 'status', name: 'status', orderable: false, searchable: true},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
        // handle datatable custom tools
        // $('#sample_5_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable9.DataTable().button(action).trigger();
        // });
    }

    var initTable10 = function () {
        var table10 = $('#event-news');
        var oTable10 = table10.dataTable({
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline',title: 'Event tickets - Data export' ,exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'copy', className: 'btn red btn-outline',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'pdf', className: 'btn green btn-outline' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                { extend: 'excel', className: 'btn yellow btn-outline ',title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]} },
                { extend: 'csv', className: 'btn purple btn-outline ' ,title: 'Event tickets - Data export',exportOptions: {columns: [ 0, 1, 2,3,4]}},
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        // alert('Event Meta data Types reloaded!');
                        toastr.info('Event programme reloaded!');
                    }
                },
            ],
            responsive: true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": true, //disable column ordering
            "lengthMenu": [
                [5, 10, 15, 20,25, -1],
                [5, 10, 15, 20,25, "All"] // change per page values here
            ],
            "pageLength": 25,
            "ajax": {
                url: list_event_news, //events_programme
                method: 'POST'
            },
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            columns: [
                {data: 'category', name: 'category', orderable: true, searchable: false},
                {data: 'text', name: 'text', orderable: false, searchable: true},
                {data: 'order_custom', name: 'order_custom', orderable: false, searchable: true},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'tools', name: 'tools', orderable: false, searchable: false},
            ]
        });
        // handle datatable custom tools
        // $('#sample_5_tools > li > a.tool-action').on('click', function() {
        //     var action = $(this).attr('data-action');
        //     oTable10.DataTable().button(action).trigger();
        // });
    }
