$(function(){
    $("#geocomplete").geocomplete({
        map: ".map_canvas",
        details: "form",
        types: ["geocode", "establishment"],
    });
    $("#find").click(function(){
        $("#geocomplete").trigger("geocode");
    });
});

$.fn.select2.defaults.set("theme", "bootstrap");
//var placeholder = "Select a State";
$(".select2, .select2-multiple").select2({
    placeholder: "select one or more entries",
    width: null
});


$(function () {
    $('#start3').datetimepicker({sideBySide: true});
    $('#end3').datetimepicker({sideBySide: true});
});


$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start").on("dp.change", function (e) {
        $('#end').data("DateTimePicker").minDate(e.date);
    });
    $("#end").on("dp.change", function (e) {
        $('#start').data("DateTimePicker").maxDate(e.date);
    });
});

$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start1").on("dp.change", function (e) {
        $('#end1').data("DateTimePicker").minDate(e.date);
    });
    $("#end1").on("dp.change", function (e) {
        $('#start1').data("DateTimePicker").maxDate(e.date);
    });
});

$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start2").on("dp.change", function (e) {
        $('#end2').data("DateTimePicker").minDate(e.date);
    });
    $("#end2").on("dp.change", function (e) {
        $('#start2').data("DateTimePicker").maxDate(e.date);
    });
});

$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start21").on("dp.change", function (e) {
        $('#end21').data("DateTimePicker").minDate(e.date);
    });
    $("#end21").on("dp.change", function (e) {
        $('#start21').data("DateTimePicker").maxDate(e.date);
    });
});

$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start3").on("dp.change", function (e) {
        $('#end3').data("DateTimePicker").minDate(e.date);
    });
    $("#end3").on("dp.change", function (e) {
        $('#start3').data("DateTimePicker").maxDate(e.date);
    });
});

$(function () {
    // $('#start,#end').datetimepicker({sideBySide: true});
    $("#start31").on("dp.change", function (e) {
        $('#end31').data("DateTimePicker").minDate(e.date);
    });
    $("#end31").on("dp.change", function (e) {
        $('#start31').data("DateTimePicker").maxDate(e.date);
    });
});


var handleImages = function() {
    // see http://www.plupload.com/
    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',

        browse_button : document.getElementById('tab_images_uploader_pickfiles'), // you can pass in id...
        container: document.getElementById('tab_images_uploader_container'), // ... or DOM Element itself

        url : event_poster_upload_url,//"assets/plugins/plupload/examples/upload.php",

        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"},
                //{title : "Zip files", extensions : "zip"}
            ]
        },

        // Flash settings
        flash_swf_url : pl_upload_flash,//'assets/plugins/plupload/js/Moxie.swf',

        // Silverlight settings
        silverlight_xap_url : pl_upload_silvg,//'assets/plugins/plupload/js/Moxie.xap',

        init: {
            PostInit: function() {
                $('#tab_images_uploader_filelist').html("");

                $('#tab_images_uploader_uploadfiles').click(function() {
                    uploader.start();
                    return false;
                });

                $('#tab_images_uploader_filelist').on('click', '.added-files .remove', function(){
                    uploader.removeFile($(this).parent('.added-files').attr("id"));
                    $(this).parent('.added-files').remove();
                });
            },

            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    $('#tab_images_uploader_filelist').append('<div class="alert alert-warning added-files" id="uploaded_file_' + file.id + '">' + file.name + '(' + plupload.formatSize(file.size) + ') <span class="status label label-info"></span>&nbsp;<a href="javascript:;" style="margin-top:-5px" class="remove pull-right btn btn-sm red"><i class="fa fa-times"></i> remove</a></div>');
                });
            },

            UploadProgress: function(up, file) {
                $('#uploaded_file_' + file.id + ' > .status').html(file.percent + '%');
            },

            FileUploaded: function(up, file, response) {
                var response = $.parseJSON(response.response);

                if (response.result && response.result == 'OK') {
                    var id = response.id; // uploaded file's unique name. Here you can collect uploaded file names and submit an jax request to your server side script to process the uploaded files and update the images tabke

                    $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-success").html('<i class="fa fa-check"></i> Done'); // set successfull upload
                } else {
                    $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-danger").html('<i class="fa fa-warning"></i> Failed'); // set failed upload
                    App.alert({type: 'danger', message: 'One of uploads failed. Please retry.', closeInSeconds: 10, icon: 'warning'});
                }
            },

            Error: function(up, err) {
                App.alert({type: 'danger', message: err.message, closeInSeconds: 10, icon: 'warning'});
            },

            UploadComplete: function (up, files) {
                // destroy the uploader and init a new one
                //up.destroy();
                //initUploader();
                location.reload();
            }

        }
    });
    uploader.init();
}



