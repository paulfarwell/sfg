// JavaScript Document

function checkitems() {
    var bal = $("#balance").val();
    var paid = $("#totalpaid").val();
    var due1 = $("#subtotal").val();
    console.log(paid);
    console.log(due1);

    if ( bal < 0) {
        //alert ('Please select items that are worth '+paid+ ' or less, or set discounts if necessary. You cannot pay less than the amount due');
        swal({
                text: 'Please select items that are worth '+paid+ ' or less, or set discounts if necessary. You cannot pay less than the amount due',
                type: 'error',
                timer: 5000,
                background: '#84754E',
                customClass:'white-text',
                showCancelButton: false,
                showConfirmButton: false
            }
        )
        return false;
    }
    else
    {
        if(confirm("ARE YOU SURE YOU WANT TO ADD THIS PAYMENT.?!"))
        { return true;}
        else
        {
            return false;
        }
    }
}

$(document).ready(function() {
    // Function to recalculate invoice cell values
    var refreshCells  = function() {
        // change total
        var total = 0;
        $("#items").find("td:nth-child(5) input.cost").each(function() {
            total = total + parseFloat($(this).val()); console.log(total);
            $(this).val(Number($(this).val()).toFixed(0));
            if (isNaN($(this).val())) {
                $(this).val("0");
            }
            else if (Number($(this).val()) == 0) {
                $(this).val("0");
            }
        });
        //console.log(total);

        $("#subtotal").val(total);//.toFixed(2)
        $("#totalpaid").val( parseFloat($("#account").val()) + parseFloat($("#payment_amount").val()));//.toFixed(2)
        $("#balance").val( parseFloat($("#totalpaid").val()) - parseFloat($("#subtotal").val()));//.toFixed(2)
    }

    // Parse existing
    refreshCells()
    //getaccount()

    //Update after edits
    $("#items input").focusout(function(){
        refreshCells()
    });

    $("#items input").focus(function(){
        refreshCells()
    });

    $("#items input").blur(function(){
        refreshCells()
    });

    $("#items input").change(function(){
        refreshCells()
    });

// Delete last row
    $(".delete").click(function(){
        $(this).parents('.item-row').remove();
        refreshCells();
        if ($(".delete").length < 2) $(".delete").hide();
    });

});