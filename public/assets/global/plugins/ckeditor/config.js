/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
	// filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
	// filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
	// filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
};
