<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 16/08/2017
 * Time: 18:14
 */

namespace app\Logic;


class OurFunctions
{

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }
        return $randomString;
    }

    public function cleanFilename($string)
    {
        $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-.]/', '', $string);
    }

}