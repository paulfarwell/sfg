<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 31/07/2017
 * Time: 10:28
 */

namespace app\Logic;


class NthNumber
{
    public static  function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

}