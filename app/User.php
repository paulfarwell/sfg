<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Teamwork\Traits\UserHasTeams;
use Caffeinated\Shinobi\Traits\ShinobiTrait;

class User extends Authenticatable
{
    use Notifiable;
    use UserHasTeams;
    use ShinobiTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','code','first_time_login','is_disabled','current_team_id'
        ,'confirmed_email','correspondence_email'
    ];

    protected static $logAttributes = [
        'name', 'email', 'password','code','first_time_login','is_disabled','current_team_id'
        ,'confirmed_email','correspondence_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->hasOne('App\RoleUsers',  'user_id','id');//, 'id', 'store'
    }

    public function team()
    {
        return $this->hasOne('App\Team',  'owner_id','id');//, 'id', 'store'
    }

    public function scopeActive($query)
    {
        return $query->where('is_disabled','=' , 0);
    }
}
