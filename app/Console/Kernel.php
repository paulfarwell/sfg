<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\GetAircrafts::class,
        \App\Console\Commands\GetAirlines::class,
        \App\Console\Commands\GetAirports::class,
        \App\Console\Commands\GetAirroutes::class,
        \App\Console\Commands\SendThankMsg::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('Get:Aircrafts')->daily()->withoutOverlapping();
        $schedule->command('Get:Airlines')->daily()->withoutOverlapping();
        $schedule->command('Get:Airports')->daily()->withoutOverlapping();
        $schedule->command('Get:Airroutes')->daily()->withoutOverlapping();
        $schedule->command('Send:ThankMsg')->daily()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
