<?php

namespace App\Console\Commands;

use App\Models\FlightRoutes;
use Illuminate\Console\Command;

use Config;
use GuzzleHttp\Client;
use Artisan;
use Carbon\Carbon;
use Log;
use Mail;
use Session;

ini_set("allow_url_fopen", 1);

class GetAirroutes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Get:Airroutes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Air routes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = Config::get('settings.iata')['routes'];//.Config::get('settings.iata')['key'];
        $key = Config::get('settings.iata')['key'];
        // get json
        $client = new Client(['verify' => FALSE]);
        $result = $client->get($url.$key); //file_get_contents($url);
        if ($result) {
            $result = json_decode($result->getBody()->getContents());
            foreach ($result->response as $list) {
                FlightRoutes::updateOrCreate (
                    [
                        'flight_number' =>$list->flight_number ,
                    ],
                    [
                        'flight_number' => $list->flight_number ,
                        'arrival' => $list->arrival,
                        'departure' => $list->departure,
                        'created_by' => 1
                    ]
                );
            }
        }
    }
}
