<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FlightAircrafts;

use Config;
use GuzzleHttp\Client;
use Artisan;
use Carbon\Carbon;
use Log;
use Mail;
use Session;

class GetAircrafts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Get:Aircrafts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Aircrafts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = Config::get('settings.iata')['aircraft'];
        $key = Config::get('settings.iata')['key'];
        // get json
        $client = new Client(['verify' => FALSE]);
        $result = $client->get($url.$key);
        if ($result) {
            $result = json_decode($result->getBody()->getContents());
            foreach ($result->response as $list) {
                FlightAircrafts::updateOrCreate (
                    [
                        'code' =>$list->code,
                    ],
                    [
                        'code' => $list->code,
                        'name' => $list->name,
                        'created_by' => 1
                    ]
                );
            }
        }
    }
}
