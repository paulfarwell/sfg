<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Invitations;
use App\Models\EmailTemplate;

use Hash;
use Auth;
use Validator;
use Mail;
use Log;
use Session;
use Input;
use File;
use Image;
use GuzzleHttp\Client;
use DB;
use Config;
use mPDF;
use View;
use PDF;
use Carbon\Carbon;
use Plupload;


class SendThankMsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Send:ThankMsg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invitation = Invitations::where('email_sent','=',0)
            ->where("created_at",">", Carbon::now()->subMonths(2))
            ->with('profile','event')
            ->get();
        foreach ($invitation as $list) {
            self::SendThankyouMail($list);
        }
    }

    public function SendThankyouMail($invitation) {
        if ($invitation->calculateCompletion($invitation->event->id) == 100) {
            $name = $invitation->profile->fname. ' '. $invitation->profile->surname;
            $event = $invitation->event->name;

            $content = EmailTemplate::where('name','=','Thanks')->first();
            $replace = array('{name}','{eventname}');
            $new = array($name, $event);
            $content = str_replace($replace, $new, $content->content);

             $data = ['logo'=> $invitation->event->logo,'content'=>$content];

            Mail::send('email.thank-you-complete', [ 'data' => $data ],
                function ($message) use ($invitation) {
                    $message->to($invitation->profile->email)
                        ->from('no-reply@spaceforgiants.org', 'Space for Giants Events')
                        ->subject('Thank you');
            });
            $invitation->email_sent = 1;
            $invitation->save();
            return true;
        }
        return false;
    }
}
