<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUsers extends Model
{
    //
    protected $table = 'role_user';
    public $timestamps = true;

    protected $fillable = [
        'role_id','user_id'
    ];

    public function role()
    {
        return $this->hasOne('App\Roles', 'id', 'role_id');//, 'id', 'store'
    }

}
