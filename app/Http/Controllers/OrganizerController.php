<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ReportsController as ReportsMaker;

use App\Models\ActivityLog;
use App\Models\EventCustomFields;
use App\Models\InvitationActivity;
use App\Models\InvitationFlightIn;
use App\Models\InvitationFlightOut;
use App\Models\InvitationTransferPackages;
use App\Models\InvitationTransportReturn;
use App\Models\NewsCategories;
use App\Models\Overpayment;
use App\Models\PaymentMethods;
use App\Models\Payments;
use App\Models\Refunds;
use App\Models\Titles;
use App\Team;
use App\Models\Tent;
use App\Models\TermsConditions;
use App\Models\EmailTemplate;
use App\Models\BookingForm;
use App\Models\EventDisclaimer;
use App\Models\EventMedia;
//use Auth;


use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;


//use function var_dump;
use function redirect;
use Yajra\Datatables\Datatables;
use Hash;
use Auth;
use Validator;
use Mail;
use Log;
use Session;
use Input;
use File;
use Image;
use GuzzleHttp\Client;
use DB;
use Config;
use mpdf;
use View;
use PDF;
use Carbon\Carbon;
use Plupload;
use Lang;

use App\Logic\Image\ImageRepository;
use App\Logic\OurFunctions;

use App\Models\Event;
use App\Models\EventsMeta;
use App\Models\EventsMetaData;
use App\Models\EventsTickets;
use App\Models\DelegatesType;
use App\Models\EventHotelPivot;
use App\Models\EventsMap;
use App\Models\EventProgramme;
use App\Models\SysImages;
use App\Models\EventTransport;
use App\Models\EventActivity;
use App\Models\EventHotel;
use App\Models\EventOrganizer;
use App\Models\EventPosters;
use App\Models\EventSponsor;
use App\Models\Hotel;
use App\Models\Countries;
use App\Models\CountriesAdvanced;
use App\Models\DelegateProfile;
use App\Models\Delegation;
use App\Models\DelegationMeta;
use App\Models\DelegationsType;
use App\Models\EventDelegationUploads;
use App\Models\EventHotelDelegatePivot;
use App\Models\EventNews;
use App\Models\FlightAircrafts;
use App\Models\FlightAirlines;
use App\Models\FlightAirports;
use App\Models\FlightRoutes;
use App\Models\Invitations;
use App\Roles;
use App\RoleUsers;
use App\User;
//use App\Team;
use Spatie\Activitylog\Models\Activity;

use Cache;
use Excel;

class OrganizerController extends Controller
{
    //
    protected $image;
    protected $biz;
    protected $payment_method;
    private $notices;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;
        $this->notices = self::EventNotifications();
        $this->biz = Team::where('id','=',1)->first();
        $this->payment_method = PaymentMethods::get();
        View::share ( 'notices', $this->notices );
        View::share ( 'business', $this->biz );
        View::share ( 'payment_method', $this->payment_method );


    }

    public function EventNotifications () {
        $events = Event::where('status','=', 'active')->with('tpackages','programmes','news','poster','tickets','delegations','delegations.countryname','delegations.invitations','organizer','sponsor','meta','map')
            ->orWhere('end', '>', Carbon::now())->get();
        $delegates = DelegatesType::query();
        //$delegations = DelegatesType::query();

        $notices = [];
        foreach ($events as $key => $lst ) {
            if ($lst->poster->count() <= 0) {
                $notices[] = [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->poster->count(). " Posters atleast one is required",
                    'url' => route('org.new.event.posters',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                ];
            }
            if ($lst->tickets->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->tickets->count(). " Tickets, There are ".$delegates->count(). " Delegates types and each requires a ticket",
                    'url' => route('org.new.event.tickets',$lst->id)              ,
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->tickets->count() < $delegates->count() ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->tickets->count(). " Tickets, There are ".$delegates->count(). " Delegates types and each requires a ticket",
                    'url' => route('org.new.event.tickets',$lst->id)              ,
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->delegations->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->delegations->count(). " Delegation, Each event requires atleast 1 delegation to allow for invitations",
                    'url' => route('org.events.delegations',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->tpackages->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->tpackages->count(). " Transfer packages, Add if required",
                    'url' => route('org.events.delegations',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->programmes->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->programmes->count(). " Activities/Sessions on its programme",
                    'url' => route('org.events.delegations',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->news->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->news->count(). " News/Important communications to the delegates",
                    'url' => route('org.events.delegations',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->delegations->count() > 0) {
                foreach ($lst->delegations as $ls) {
                    if ($ls->invitations->count() <=0 ) {
                        $notices[] =  [
                            'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). ", ".$ls->countryname->name. " Delegation  has ".$ls->invitations->count()." invitations, atleast one is required",
                            'url' => route('org.events.inv.send',[$ls->id,$lst->id]),
                            'time_diff' => Carbon::createFromTimeStamp(strtotime($ls->created_at))->diffForHumans()
                        ];
                    }
                }
            }
            if ($lst->organizer->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->organizer->count(). " organizers, Each event requires atleast 1 organizer ",
                    'url' => route('org.new.event.organizers',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->sponsor->count() <= 0) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name :  "ID ".$lst->id). " has ".$lst->sponsor->count(). " sponsors, Each event requires atleast 1 sponsor ",
                    'url' => route('org.new.event.sponsors',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->name == null || $lst->name == '' ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name : "ID ". $lst->id)." must have a name, please complete all the required event information",
                    'url' => route('org.new.event.posters',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->venue == null || $lst->venue == '' ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name : "ID ". $lst->id)."  must have a venue, please complete all the required event information",
                    'url' => route('org.edit.event',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->start == null || $lst->start == '' ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name : "ID ". $lst->id)."  must have a start date and time, please complete all the required event information",
                    'url' => route('org.edit.event',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->end == null || $lst->end == '' ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name : "ID ". $lst->id)."  must have a end date and time, please complete all the required event information",
                    'url' => route('org.edit.event',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
            if ($lst->description == null || $lst->description == '' ) {
                $notices[] =  [
                    'message' => "Event ".(isset($lst->name) ? $lst->name : "ID ".$lst->id)."  must have a description, please complete all the required event information",
                    'url' => route('org.edit.event',$lst->id),
                    'time_diff' => Carbon::createFromTimeStamp(strtotime($lst->created_at))->diffForHumans()
                    ];
            }
        }
        return $notices;
    }

    public function index() {
        $stats = ReportsMaker::DashboardStats();
        return view('organizer.index',compact('stats'));
    }

    public function SystemPayments() {
        return view('organizer.payments');
    }

    public function SystemRefunds() {
        return view('organizer.refunds');
    }

    public function SystemStats() {
        return view('organizer.stats');
    }

    public function EventActivityindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        return view('organizer.event-activity-list',compact('events','delegate_types'));
    }

    public function EventDelegateFlightindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        return view('organizer.event-flights-list',compact('events','delegate_types'));
    }

    public function EventTransferstindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        return view('organizer.event-tpackage-list',compact('events','delegate_types'));
    }

    public function EventDelegateInvitationsindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        return view('organizer.event-invitations-list',compact('events','delegate_types'));
    }

    public function EventDelegationtindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        $country = Countries::get();
        $delegation_type = DelegationsType::get();
        return view('organizer.event-delegations-invitations-list',compact('events','delegate_types','country','delegation_type'));
    }

    public function EventHotelsindex() {
        $events = Event::get();
        $delegate_types = DelegatesType::get();
        return view('organizer.event-hotels-list',compact('events','delegate_types'));
    }


    public function SalesAccounts() {
        return view('organizer.sales-accounts');
    }

    public function exportEventDatabase (Request $request, $event_id) {
        $event = Event::where('id','=', $event_id)->first();
        $delegate_type = DelegatesType::all();
        if ($event) {
            return view('organizer.event-view-database',compact('event','delegate_type'));
        }
        return redirect()->back()->withError('You selected an invalid event');
    }


    public function SendEmail (Request $request) {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'email' => 'email|required',
                'subject' => 'required|max:255',
                'message' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors('Please enter all the required data')->withInput()->withErrors($validator);
            } else {

            $event = Event::where('id','=',$request->get('event'))->first();
            $delegate=  DelegateProfile::where('email','=',$request->get('email'))->first();
              $name = $delegate->fname. ' '. $delegate->surname;
              $content = $request->get('message');
              $replace = array('{name}');
              $new = array($name);

              $content= str_replace($replace, $new, $content);
              $data = [
               'logo' => $event->logo,
               'content'=> $content,
              ];
              Mail::send('email.contact', $data,
                    function ($message) use ($request) {
                        $message->to($request->get('email'))
                            ->from('invites@wildlifeeconomy.com', 'Joyce')
                            ->subject($request->get('subject'));
                });
                return redirect()->back()->withSuccess('Email has been sent successfully');
            }
        }
        return redirect()->back()->withError('invalid request, only posts are allowed');
    }

    public function SendSms (Request $request) {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'phone' => 'required',
                'message' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors('Please enter all the required data')->withInput()->withErrors($validator);
            } else {
                return redirect()->back()->withSuccess('Feature is currently inactive');
            }
        }
        return redirect()->back()->withError('invalid request, only posts are allowed');
    }

    public function UserActivity (Request $request) {
        $list = ActivityLog::whereNotNull('causer_id')->with('user');
        return Datatables::of($list)
            ->addColumn('tools', function ($list)  {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>

                </ul>
                </div>";
            })
            ->rawColumns(['tools'])
            ->make(true);
    }

    public function ProfilesList (Request $request) {
        $list = DelegateProfile::with('invitations');
        return Datatables::of($list)
            ->addColumn('tools', function ($list)  {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.email.profile') . "' data-email='".$list->email."'data-event='".$list->invitations->event_id."' data-toggle=\"modal\" data-target=\"#sendEmail\" > Email </a></li>
                    <li><a href='#' data-url='" . route('org.sms.profile') . "' data-phone='".$list->cell_phone_code.$list->cell_phone."' data-toggle=\"modal\" data-target=\"#sendSms\"  > Sms </a></li>
                    <li><a href='" . (isset($list->national_id_image) && File::exists(public_path().'/'.$list->national_id_image) ?  asset($list->national_id_image): "#") . "' target='_blank'   > Download ID </a></li>
                    <li><a href='" . (isset($list->passport_image) && File::exists(public_path().'/'.$list->passport_image) ? asset($list->passport_image) : "#" ). "' target='_blank'  > Download Photo </a></li>
                    </ul>
                </div>";
            })
            ->addColumn('passport_photo', function ($list) {
                if ( File::exists($list->passport_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' .url($list->passport_image) . '">
                    <img style="width: 50px !important" src="' . url($list->passport_image) . '" /> </a>';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('id_photo', function ($list) {
                if ( File::exists($list->national_id_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->national_id_image) . '">
                    <img style="width: 50px !important" src="' . url($list->national_id_image) . '" /> </a> ';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->rawColumns(['tools','passport_photo','id_photo'])
            ->make(true);
    }

    public function UsersLoginAs (Request $request, $userid) {
        //$user =
        $invite = Invitations::with('profile')
            ->with(['profile.user_account' => function ($query) use ($userid) {
                $query->where('id','=',$userid);
            }])
            ->with('delegateType','delegatePrice','hotel','transfer','payments')
            ->with('activity','activity.activity','delegation','delegation.countryname','inflight','outflight','refunds')
            ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
            ->with('delegation.countryname.details')
//            ->with('profile.user_account' => function($query) {
//                    $query->where('region_id', '=', 6);
//            }))
            //->
            //->where('id','=',$invitation_id)
            ->orderBy('id', 'desc')->first();
        ;
        if ($invite) {
            $user = $invite->profile->user_account;
            Auth::loginUsingId($userid, true);
            //Auth::user()->code = $invite->code;
            //Auth::user()->save();
            return redirect()->route('del.view.invitation',[$invite->code,$invite->event->slug])
                ->withSuccess('Login Successful')->withEvent($invite->event)->withInvitation($invite);

        }
        return redirect()->back()->withError('Invalid invitation');
    }

    public function loginAsDelegate (Request $request,$invitation_id,$event_id) {
    	//->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
	    $invite = Invitations::where('event_id','=',$event_id)
                   ->with('delegateType','delegatePrice','hotel','transfer','profile','profile.user_account','payments')
                   ->with('activity','activity.activity','delegation','delegation.countryname','inflight','outflight','refunds')
                   ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
                   ->with('delegation.countryname.details')
        ->where('id','=',$invitation_id)
        ->orderBy('id', 'desc')->first()
        ;
	    if ($invite) {
	    	$user = $invite->profile->user_account;
		    Auth::loginUsingId($user->id, true);
		    Auth::user()->code = $invite->code;
		    Auth::user()->save();
		    return redirect()->route('del.view.invitation',[$invite->code,$invite->event->slug])
             ->withSuccess('Login Successful')->withEvent($invite->event)->withInvitation($invite);

	    }
	    return redirect()->back()->withError('Invalid invitation');
    }

    public function editInvite (Request $request, $id) {
        $invite = Invitations::where('id','=',$id)
            ->with('delegateType','delegatePrice','hotel','transfer','profile')
            ->with('activity','activity.activity','delegation','delegation.countryname','inflight','outflight','payments','refunds')
            ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
            ->first()
        ;
        if ($invite){
            //$invite->profile->fname = $request->get('fname');
            //$invite->profile->surname = $request->get('sname');
            //$post->comments()->save($comment);
            $invite->profile->update(
                [   'fname' =>  $request->get('fname') ,
                    'surname'   => $request->get('sname')
                ]);
            $invite->profile->user_account->update(
                [
                    'name'=>$request->get('fname') .' '.$request->get('sname')
                ]);
            $invite->delegate_id = $request->get('category');
            $invite->save();

            return redirect()->back()->withSuccess('you have edited the invite');
        }

        return redirect()->back()->withError('invalid invite selected');
    }

    public function listEventExportInvitations (Request $request, $event_id, $type) {

        //Log::alert($request);
        //Log::alert($request->has('search.value'));
        //Log::alert($request->get('search'));

        if ($type == 'full') {
            $list = Invitations::where('event_id','=',$event_id)
                ->with('delegateType','delegatePrice','hotel','transfer','profile')
                ->with('activity','activity.activity','delegation','inflight','outflight','payments','refunds')
                ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
                ->where('status','=',0);
                $datatables =  Datatables::of($list);
//                $datatables->where('completion', '=', "100");
//                $datatables->filter(function ($query) use ($request) {
//                    $query->where('completion', '=', "100");
//                });
        }elseif ($type == 'half') {
            $list = Invitations::where('event_id','=',$event_id)
                ->with('delegateType','delegatePrice','hotel','transfer','profile')
                ->with('activity','activity.activity','delegation','delegation.countryname','inflight','outflight','payments','refunds')
                ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
                ->where('status','>',0)
                ->where('email_sent','=',0);
                $datatables =  Datatables::of($list);
                //$datatables->where('profile.completion', '=', "100");
//                $datatables->filter(function ($query) use ($request) {
//                    $query->where('completion', '<', "100");
//                });
        }if ($type == 'cancelled') {
            $list = Invitations::where('event_id','=',$event_id)
                ->with('delegateType','delegatePrice','hotel','transfer','profile')
                ->with('activity','activity.activity','delegation','delegation.countryname','inflight','outflight','payments','refunds')
                ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
                ->where('status','>',0)
                ->where('email_sent','=',1);
                $datatables =  Datatables::of($list);
                //$datatables->where('profile.completion', '=', "100");
//                $datatables->filter(function ($query) use ($request) {
//                    $query->where('profile.completion', '<', "100");
//                });
        }

//        if ($request->has('search.value')) {
//            $search = $request->get('search');
//            $list
//                // you'll have to declare your other filters here so that they are used in conjunction
//                ->where(function($q) use ($list,$search,$request) {
//                    $keyword = $search['value'];//$request->get('search.value') ;//$list->request()->input('profile_full_name');
//                    $q
//                        //->where('foo', 'like', "%{$keyword}%")
//                        ->whereHas('profile', function($q) use ($keyword) {
//                            //$q->whereRaw("CONCAT(fname,' ',surname) like ?", ["%{$keyword}%"]);
//                            $q->where('fname', 'like', "%".$keyword."%");
//                            $q->where('surname', 'like', "%".$keyword."%");
//                        });
//                        //->orWhereHas('some.other.foo', function($q) use ($keyword) {
//                        //    $q->where('bar', 'like', "%{$keyword}%");
//                        //});
//                });
//        }

//        if ($request->has('search.value')) {
//            $search = $request->get('search'); $keyword = $search['value'];
////            $list->WhereHas('profile', function ($query) use($keyword) {
////                $query->orWhere('fname', 'like', "%".$keyword."%");
////                $query->orWhere('surname', 'like', "%".$keyword."%");
////            });
//
////            $datatables->filter(function ($query) use($keyword) {
////                $query->orWhere('fname', 'like', "%".$keyword."%");
////                $query->orWhere('surname', 'like', "%".$keyword."%");
////            });
//
//            $datatables->filterColumn('profile.full_name', function($query, $keyword) {
//                //$sql = "CONCAT(profile.fname,' ',profile.surname)  like ?";
//                //$query->whereRaw($sql, ["%{$keyword}%"]);
//                $query->orWhere('fname', 'like', "%{$keyword}%");
//                $query->orWhere('surname', 'like', "%{$keyword}%");
//            });
//
//        }

        return $datatables

            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.invite',[$list->id]) . "' data-fname='".$list->profile->fname."' data-lname='".$list->profile->surname."' data-delegate-id='".$list->delegate_id."' data-toggle=\"modal\" data-target=\"#editInvite\" > Edit Invite </a></li>
                    <li><a href='#' data-url='" . route('org.email.profile') . "' data-email='".$list->profile->email."'data-event='".$list->event_id."' data-toggle=\"modal\" data-target=\"#sendEmail\" > Email </a></li>
                    <li><a href='#' data-url='" . route('org.sms.profile') . "' data-phone='".$list->profile->cell_phone_code.$list->profile->cell_phone."' data-toggle=\"modal\" data-target=\"#sendSms\"  > Sms </a></li>
                    <li><a href='" . (isset($list->profile->national_id_image) && File::exists(public_path().'/'.$list->profile->national_id_image) ?  asset($list->profile->national_id_image): "#") . "' target='_blank'   > Download ID </a></li>
                    <li><a href='" . (isset($list->profile->passport_image) && File::exists(public_path().'/'.$list->profile->passport_image) ? asset($list->profile->passport_image) : "#" ). "' target='_blank'  > Download Photo </a></li>
                    <li><a href='#' data-url='" . route('org.events.inv.status', [$list->id,$list->event_id]) . "'
                        class='confirm-this' data-msg='Are you sure you want to change status of this item, Action is Reversible ?'
                        data-toggle=\"modal\" data-target=\"#confirmModal\"
                     > Cancel | Activate </a></li>

                    <li><a href='" . route('org.events.login.as.delegate', [$list->id,$list->event_id]) . "'  > Login as Delegate </a></li>
                    <li><a href='" . route('org.events.inv.print.card', [$list->id,$list->event_id]) . "' target='_blank'  > Print Card </a></li>
                    <li><a href='" . route('org.events.inv.print.all', [$list->id,$list->event_id]) . "' target='_blank'   > View Details </a></li>
                    </ul>
                </div>";
            })
            ->addColumn('passport_photo', function ($list) {
                if ( File::exists($list->profile->passport_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' .url($list->profile->passport_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->passport_image) . '" /> </a>';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('id_photo', function ($list) {
                if ( File::exists($list->profile->national_id_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->profile->national_id_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->national_id_image) . '" /> </a> ';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->rawColumns(['tools','passport_photo','id_photo'])
            ->make(true);
    }

    public function ListTransferPackages (Request $request,$type) {
        //$list = Invitations::with('profile','delegateType','delegation','transfer','transfer.package');
        // $list = InvitationTransferPackages::
        // with('package','invitation','invitation.delegation','invitation.profile','invitation.delegateType','invitation.delegation.countryname');
        // return Datatables::of($list)
        //     ->make(true);
        // if($type == 'in'){
        //
        //   if($request->isMethod('post')){
        //          dd($request->get('event_id'));
        //   }
        // }
            if ($type == 'in') {
              if($request->get('event_id')){

                dd($request->get('event_id'));
              }
                $list = InvitationTransferPackages::
                with('package','invitation','invitation.delegation','invitation.profile','invitation.delegateType','invitation.delegation.countryname');
              }
            else {
                $list = InvitationTransportReturn::
                with('package_return','invitation','invitation.delegation','invitation.profile','invitation.delegateType','invitation.delegation.countryname');
            }

            return Datatables::of($list)
                ->make(true);
    }

    public function ListFlightPackages (Request $request,$type) {
        if ($type == 'in') {
            $list = InvitationFlightIn::
            with('invitation','invitation.delegation',
                'invitation.profile','invitation.delegateType','invitation.delegation.countryname');        }
        else {
            $list = InvitationFlightOut::
            with('invitation','invitation.delegation',
                'invitation.profile','invitation.delegateType','invitation.delegation.countryname');
        }

        return Datatables::of($list)
            ->make(true);
    }

    public function newEventDelegationsUploads (Request $request, $event_id) {
        $event = Event::where('id','=', $event_id)->first();

        $delegation_type  = DelegationsType::get();
        if ($event){
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'delegation_id'    => 'required',
                    'name' => 'required',
                    'description' => 'required',
                    'file' => 'required|mimes:pdf|max:5000'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
                } else {
                    $uploads = new EventDelegationUploads();
                    $func = new OurFunctions();
                    $directory = public_path().'/uploads/files/';//storage_path() . '/public/organizer_files/files/';

                    //file upload
                    $file = $request->file('file');
                    $oldName = $file->getClientOriginalName();
                    $filename = $func->generateRandomString(5).'-'.$func->cleanFilename($oldName);
                    $file->move($directory, $filename);
                    //Storage::putFileAs('photos', new File('/path/to/photo'), 'photo.jpg');
                    //save record
                    $uploads->event_id = $event_id;
                    $uploads->delegation_id = $request->get('delegation_id');
                    $uploads->name = $request->get('name');
                    $uploads->description = $request->get('description');
                    $uploads->file = 'uploads/files/'.$filename;
                    $uploads->created_by = auth()->user()->id;
                    $uploads->save();

                    return redirect()->back()->withSuccess('Your upload was completed '. $filename);

                }
            }
            return view('organizer.event-uploads-delegations',compact('event','delegation_type'));
        }
        return redirect()->back()->withError('invalid event was selected');
    }

    public function EditEventDelegationUploads(Request $request, $id) {
        $uploads = EventDelegationUploads::where('id','=',$id)->first();
        if ($uploads) {
            if ($request->isMethod('post')) {
                //dd($request);
                $validator = Validator::make($request->except('_token'), [
                    'delegation_id'     => 'required',
                    'name'              => 'required',
                    'description'       => 'required',
                    //'file' => 'sometimes|nullable|mimes:pdf|max:5000'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
                } else {

                    //$uploads->event_id = $event_id;
                    $uploads->name = $request->get('name');
                    $uploads->description = $request->get('description');
                    $uploads->delegation_id = $request->get('delegation_id');
                    $uploads->created_by = auth()->user()->id;
                    $directory = public_path().'/uploads/files/';

                    if (!is_null($request->get('file'))) {
                        //unlink(public_path().'/'.$uploads->file);
                        $func = new OurFunctions();
                        $directory = public_path().'/uploads/files/';//storage_path() . '/public/organizer_files/files/';
                        //file upload
                        $file = $request->file('file');
                        $oldName = $file->getClientOriginalName();
                        $filename = $func->generateRandomString(5).'-'.$func->cleanFilename($oldName);
                        $file->move($directory, $filename);
                        $uploads->file = 'uploads/files/'.$filename;//'organizer_files/files/'.$filename;
                    }

                    $uploads->save();
                    return redirect()->back()->withSuccess('Your upload was edited '. $uploads->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid upload was selected');
    }

    public function DeleteEventDelegationUploads(Request $request, $id) {
        $uploads = EventDelegationUploads::where('id','=',$id)->first();
        if ($uploads) {
	        //unlink(public_path().'/'.$uploads->file);
	        if(file_exists(public_path().'/'.$uploads->file)){
		        unlink(public_path().'/'.$uploads->file);
	        }else{
		        //echo 'file not found';
	        }$uploads->delete();
            return redirect()->back()->withSuccess('you deleted the uploaded file - '. $uploads->name);
        }
        return redirect()->back()->withError('Invalid upload was selected');
    }

    public function DeleteEventInvitation(Request $request, $id,$eid) {
        $inv = Invitations::where('id','=',$id)->first();
        if ($inv) {
	        $inv->delete();
            return redirect()->back()->withSuccess('you deleted the selected invitation - '. $inv->code);
        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function Delegateindex (Request $request, $id) {
      $event = Event::where('id','=',$id)->first();
      if ($request->isMethod('post')) {
          $validator = Validator::make($request->except('_token'), [
              'name'    => 'required',
              'description' => 'required',
              'color' => 'required',
              'color_hexcode' => 'required',
              'color_hexcode_sec' => 'required',
          ]);
          if ($validator->fails()) {
              return redirect()->route('org.delegate.types',['#tab_5_2'])->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
          } else {
              $input = $request->except('_token');
              $delegate = New DelegatesType();
              //upload folder
              if (!File::exists('uploads/color-codes/')) {
                  File::makeDirectory('uploads/color-codes/');
              }
              //upload
              if (!is_null($request->file('file'))) {
                  $extension = $request->file('file')->getClientOriginalExtension();
                  $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                  $upload = $request->file('file')->move('uploads/color-codes/', $fileName);
                  if ($upload) {
                      $input['image_path'] = 'uploads/color-codes/'.$fileName;
                  } else { $input['image_path'] = null; }
              }
              //dd($input);
              $delegate->image_path = isset($input['image_path']) ? $input['image_path'] : null;
              $delegate->name = $input['name'];
              $delegate->description = $input['description'];
              $delegate->color = $input['color'];
              $delegate->color_hexcode = $input['color_hexcode'];
              $delegate->color_hexcode_sec = $input['color_hexcode_sec'];
              $delegate->event_id = $id;
              $delegate->save();
              return redirect()->back()->withSuccess('You created a new delegate type');
          }
      }
      return view('organizer.delegate-list',compact('event'));
    }
    public function EventDelegatetindex (Request $request) {
      if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'name'    => 'required',
                'description' => 'required',
                'color' => 'required',
                'color_hexcode' => 'required',
                'color_hexcode_sec' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->route('org.delegate.info',['#tab_5_2'])->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
            } else {
                $input = $request->except('_token');
                $delegate = New DelegatesType();
                //upload folder
                if (!File::exists('uploads/color-codes/')) {
                    File::makeDirectory('uploads/color-codes/');
                }
                //upload
                if (!is_null($request->file('file'))) {
                    $extension = $request->file('file')->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                    $upload = $request->file('file')->move('uploads/color-codes/', $fileName);
                    if ($upload) {
                        $input['image_path'] = 'uploads/color-codes/'.$fileName;
                    } else { $input['image_path'] = null; }
                }
                //dd($input);
                $delegate->image_path = isset($input['image_path']) ? $input['image_path'] : null;
                $delegate->name = $input['name'];
                $delegate->description = $input['description'];
                $delegate->color = $input['color'];
                $delegate->color_hexcode = $input['color_hexcode'];
                $delegate->color_hexcode_sec = $input['color_hexcode_sec'];
                $delegate->save();
                return redirect()->back()->withSuccess('You created a new delegate type');
            }
        }

        return view('organizer.delegate-list-all');
    }

    public function Delegationindex (Request $request) {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'name'    => 'required',
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->route('org.delegation.types',['#tab_5_2'])->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
            } else {
                $input = $request->except('_token');
                $delegate = New DelegationsType();
                $delegate->internal_name = $input['name'];
                $delegate->name = $input['name'];
                $delegate->description = $input['description'];
                $delegate->save();
                return redirect()->back()->withSuccess('You created a new delegation type');
            }
        }
        return view('organizer.delegation-list');
    }

    public function EditDelegate (Request $request, $id) {
        $delegate =DelegatesType::where('id','=',$id)->first();
        if ($delegate) {
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'name'    => 'required',
                    'description' => 'required',
                    'color' => 'required',
                    'color_hexcode' => 'required',
                    'color_hexcode_sec' => 'required',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
                } else {
                    $input = $request->except('_token');

                    //upload folder
                    if (!File::exists('uploads/color-codes/')) {
                        File::makeDirectory('uploads/color-codes/');
                    }
                    //upload
                    if (!is_null($request->file('file'))) {
                        $extension = $request->file('file')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                        $upload = $request->file('file')->move('uploads/color-codes/', $fileName);
                        if ($upload) {
//                            $input['image_path'] = 'uploads/color-codes/'.$fileName;
                            $delegate->image_path = 'uploads/color-codes/'.$fileName;
                        } else {
//                            $input['image_path'] = null;
                        }
                    }
                    //dd($input);
                    //if (isset($input['image_path'])) {
                    //    $delegate->image_path = $input['image_path'];
                    //}
                    $delegate->name = $input['name'];
                    $delegate->description = $input['description'];
                    $delegate->color = $input['color'];
                    $delegate->color_hexcode = $input['color_hexcode'];
                    $delegate->color_hexcode_sec = $input['color_hexcode_sec'];
                    $delegate->save();
                    return redirect()->back()->withSuccess('You edited delegate type '. $delegate->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid delegate id');
    }

    public function EditDelegation (Request $request, $id) {
        $delegate =DelegationsType::where('id','=',$id)->first();
        if ($delegate) {
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'name'    => 'required',
                    'description' => 'required',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, and provide all the required fields')->withInput()->withErrors($validator);
                } else {
                    $input = $request->except('_token');
                    $delegate->internal_name = $input['name'];
                    $delegate->name = $input['name'];
                    $delegate->description = $input['description'];
                    $delegate->save();
                    return redirect()->back()->withSuccess('You edited a delegation type '.$delegate->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid delegation id');
    }

    public function FlightAirportsindexlist (){
        return Datatables::of(FlightAirports::all())->make(true);
    }

    public function FlightAirlinesindexlist (){
        return Datatables::of(FlightAirlines::all())->make(true);
    }

    public function FlightAircraftsindexlist (){
        return Datatables::of(FlightAircrafts::all())->make(true);
    }

    public function FlightRoutesindexlist (){
        return Datatables::of(FlightRoutes::with('dept','arrv'))->make(true);
    }

    public function Flightindex () {
        return view('organizer.flight-list');
    }

    public function Countryindex () {
        return view('organizer.countries-list');
    }

    public function Titlesindex (Request $request) {
        if ($request->isMethod('post')) {
            $validate = Validator::make($request->except('_token'), [
                    'name' => 'required'
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $title = Titles::updateOrCreate(
                    [
                        'name' => $request->get('name')
                    ],
                    [
                        'name' => $request->get('name'),
                    ]
                );
                return redirect()->back()->withSuccess('You created a new Title - '. $title->name);
            }
        }
        return view('organizer.titles-list');
    }

    public function NewsCategoryindex (Request $request) {
        if ($request->isMethod('post')) {
            $validate = Validator::make($request->except('_token'), [
                    'name' => 'required'
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                 $news = NewsCategories::updateOrCreate(
                    [
                        'name' => $request->get('name')
                    ],
                    [
                        'name' => $request->get('name'),
                    ]
                );
                return redirect()->back()->withSuccess('You created a new News Category - '. $news->name);
            }
        }
        return view('organizer.news-category-list');
    }

    public function EmailTemplatesIndex (Request $request) {
        $emails = EmailTemplate::all();

     return view('organizer.email-templates',compact('emails'));
    }

    public function EmailTemplatesNew (Request $request) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'content' => 'required',
    //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $email = new EmailTemplate();
                    $email->name = $request->input('name');
                    $email->content = $request->input('content');
                    $email->status = $request->input('status');
                    $email->created_by = auth()->user()->id;
                    $email->save();

                    return redirect()->back()->withSuccess('You created a new Email Template.');
                }
            }
    }

    public function EmailTemplatesEdit (Request $request, $id) {
        $email = EmailTemplate::find($id);
        if ($request->isMethod('post') ) {

          $validate = Validator::make($request->except('_token'), [
              'name' => 'required',
              'content' => 'required',
//                    'address' => 'required',
          ]);

          if ($validate->fails()) {
              //return redirect()->back()->withErrors($validate)->withInput();
              return redirect()->back()
                  ->withError('The data you submitted has some errors, see highlighted in red .')
                  ->withErrors($validate)->withInput();
          } else {
              //new hotel
              $email->name = $request->input('name');
              $email->content = $request->input('content');
              $email->status = $request->input('status');
              $email->created_by = auth()->user()->id;
              $email->update();

              return redirect()->route('org.email.template')->withSuccess('Email Tempalte has been Updated.');
          }
         }
          return view('organizer.email-template-edit',compact('email'));

       }

    public function UsersLogindex() {
        return view('organizer.users-log-list');
    }

    public function Usersindex(Request $request) {
        $roles = Roles::where('id','!=',4)->get();
        if ($request->isMethod('post')) {
            $validate = Validator::make($request->except('_token'), [
                    'role'       => 'required',
                    'name'       => 'required',
                    'email'      => 'required|email|unique:users',
                ]
            );
            if ($validate->fails()) {
                return redirect()->route('org.users.mgr','#tab_5_2')
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $randomToken = self::generateRandomString(10);
                $user = New User();
                $user->name = $request->get('name');
                $user->email = $request->get('email');
                $user->password = Hash::make($randomToken);
                $user->remember_token = $randomToken;
                $user->is_disabled = 0;
                $user->current_team_id = 1;
                //$user->save();
                $saved = $user->save();
                if ($saved) {
                    //$saved->syncRoles([$request->get('role')]);
                    $new_role = New RoleUsers();
                    $new_role->role_id = $request->get('role');
                    $new_role->user_id = $user->id;
                    $new_role->save();
                    //emails user with cridentials
                    $data = [
                        'user' => $user,
                        'password' => $randomToken,
                    ];
                    Mail::send('email.other-user-acc', $data, function ($message) use ($request,$user) {
                        $message->from('invites@wildlifeeconomy.com', 'Space for Giants');
                        $message->to($user->email, $request->get('name'));
                        $message->subject('Space for Giants - User Credentials');
                    });
                    return back()->withSuccess('User was created successfully');
                }
                return back()->withError('User was creation failed');
            }
        }
        return view('organizer.users-list',compact('roles'));
    }


    public function UsersLogindexlist() {
        return Datatables::of(Activity::with('causer'))->make(true);
    }

    public function EditdelegateStatus(Request $request , $id) {
        $delegate = DelegatesType::where('id','=', $id)->first();
        if ($delegate) {
            if ($request->isMethod('post')) {
                $delegate->status =  $delegate->status == 0 ? 1 : 0 ;
                $updated = $delegate->save();
                if ($updated){
                    return redirect()->back()->withSuccess('You '.($delegate->status == 0 ? 'Enabled' : 'Disabled' ).' Delegate '.$delegate->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid delegate selected');
    }

    public function EditdelegationStatus(Request $request , $id) {
        $delegate = DelegationsType::where('id','=', $id)->first();
        if ($delegate) {
            if ($request->isMethod('post')) {
                $delegate->status =  $delegate->status == 0 ? 1 : 0 ;
                $updated = $delegate->save();
                if ($updated){
                    return redirect()->back()->withSuccess('You '.($delegate->status == 0 ? 'Enabled' : 'Disabled' ).' Delegation '.$delegate->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid delegation selected');
    }

    public function statusEventDelegations(Request $request ,$id, $event_id) {
        $event_delegation = Delegation::where('id','=', $id)
                            ->where('event_id','=',$event_id)
                            ->first();
        if ($event_delegation) {
            if ($request->isMethod('post')) {
	            $event_delegation->status =  $event_delegation->status == 0 ? 1 : 0 ;
                $updated = $event_delegation->save();
                if ($updated){
                    return redirect()->back()->withSuccess('You '.($event_delegation->status == 0 ? 'Enabled' : 'Disabled' ).' Delegation '.$event_delegation->name);
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid delegation selected');
    }

    public function UsersFixstatus(Request $request , $userid) {
        $user = User::where('id','=', $userid)->first();
        if ($user) {
            if ($request->isMethod('post')) {
                if ( array_has( Config::get('settings.protected_user'), $userid) ) {
                    $user->is_disabled =  $user->is_disabled == 0 ? 1 : 0 ;
                    $updated = $user->save();
                    if ($updated){
                        return redirect()->back()->withSuccess('You '.($user->is_disabled == 0 ? 'Enabled' : 'Disabled' ).' User account');
                    }
                }
                else {
                    return redirect()->back()->withError('You cannot disable the selected user');
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid user selected');
    }

    public function UsersFixrole(Request $request , $userid) {
        $user = User::where('id','=', $userid)->with('role')->first();
        if ($user) {
            if ($request->isMethod('post')) {
                if ( array_has( Config::get('settings.protected_user'), $userid) ) {
                    if ($user->role == null) {
                        $new_role = New RoleUsers();
                        $new_role->role_id = $request->get('role');
                        $new_role->user_id = $user->id;
                        $new_role->save();
                    }  else {
                        $role = RoleUsers::where('id','=',$user->role->id)->first();
                        $role->role_id = $request->get('role');
                        $role->save();
                    }
                    return redirect()->back()->withSuccess('User was assigned ');
                }
                else {
                    return redirect()->back()->withError('You cannot change the role selected user');
                }
                return redirect()->back()->withError('User has a role already assigned');
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid user selected');
    }

    public function UsersFixpassword(Request $request , $userid) {
        $user = User::where('id','=', $userid)->first();
        if ($user) {
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'password1'    => 'required',
                    'password2' => 'required|same:password1',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, the passwords dont match')->withInput()->withErrors($validator);
                } else {
                    $updated = $user->update(['password' => Hash::make($request->get('password1'))]);
                    if ($updated) {
                        return redirect()->back()->withSuccess('User password was changed succefully');
                    }
                    return redirect()->back()->withError('we couldnt complete your request');
                }
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid user selected');
    }

    public function UsersListindexlist() {
        return Datatables::of(User::with('role','role.role'))
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.fix.user.status', $list->id) . "' class='confirm-this' data-msg='Are you sure you want to change user status ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                    <li><a href='#' data-url='" . route('org.fix.user.password', $list->id) . "' class='confirm-this' data-msg='Are you sure you want to change user password ?'  data-toggle=\"modal\" data-target=\"#changePassword\" > Change Password </a></li>
                    <li><a href='#' data-url='" . route('org.fix.user.loginas', $list->id) . "' class='confirm-this' data-msg='Are you sure you want to login as selected users ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Login As - User </a></li>
                    <li><a href='#' data-url='" . route('org.fix.user.role', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to change user role ?'  data-toggle=\"modal\" data-target=\"#changeRole\"> Fix User Role </a></li>

                    </ul>
                </div>";
            })
            ->rawColumns(['tools'])
            ->make(true);
    }

    public function DelegateListindexlist($id) {
        $list= DelegatesType::where('event_id','=',$id);
        $datatables =  Datatables::of($list);
        return $datatables
            ->addColumn('poster', function ($list) {
                if ($list->image_path) {
                    return '<img style="width: 50px !important" src="' . url($list->image_path) . '" />';

                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.fix.delegate.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change delegate status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                    <li><a href='#' data-url='" . route('org.fix.delegate.edit', $list->id) . "'
                    data-name='" . $list->name . "' data-description='" . htmlentities($list->description, ENT_QUOTES) . "'
                    data-color='" . $list->color . "' data-color1='" . $list->color_hexcode . "'
                    data-ordering='" . $list->ordering . "'
                    data-color2='" . $list->color_hexcode_sec . "'  data-image_path='" . url( isset($list->image_path) ? $list->image_path : 'uploads/default.jpg' ) . "'
                    data-toggle=\"modal\" data-target=\"#editDelegate\"> Edit Delegate </a></li>

                    </ul>
                </div>";
            })
            ->rawColumns(['tools','poster'])
            ->make(true);
    }
    public function DelegateListindexall() {
        $list= DelegatesType::all();
        $datatables =  Datatables::of($list);
        return $datatables
            ->addColumn('poster', function ($list) {
                if ($list->image_path) {
                    return '<img style="width: 50px !important" src="' . url($list->image_path) . '" />';

                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.fix.delegate.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change delegate status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                    <li><a href='".route('org.delegate.send',[$list->id])."'  > Send Email </a></li>
                    <li><a href='#' data-url='" . route('org.fix.delegate.edit', $list->id) . "'
                    data-name='" . $list->name . "' data-description='" . htmlentities($list->description, ENT_QUOTES) . "'
                    data-color='" . $list->color . "' data-color1='" . $list->color_hexcode . "'
                    data-ordering='" . $list->ordering . "'
                    data-color2='" . $list->color_hexcode_sec . "'  data-image_path='" . url( isset($list->image_path) ? $list->image_path : 'uploads/default.jpg' ) . "'
                    data-toggle=\"modal\" data-target=\"#editDelegate\"> Edit Delegate </a></li>

                    </ul>
                </div>";
            })
            ->rawColumns(['tools','poster'])
            ->make(true);
    }
    public function CountriesListindexlist() {
        return Datatables::of(CountriesAdvanced::all())->make(true);
    }

    public function NewsCatEdit (Request $request, $id) {
        if ($request->isMethod('post')) {
            $newscat = NewsCategories::where('id','=', $id)->first();
            if ($newscat){
                $validator = Validator::make($request->except('_token'), [
                    'name'    => 'required',
                    'order_custom' => 'required'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('please provide for the name field')->withInput()->withErrors($validator);
                } else {
                    $oldcat = $newscat->name;
                    $newscat->name = $request->get('name');
                    $newscat->order_custom = $request->get('order_custom');
                    $newscat->save();
                    //update all related news items
                    $update_news = EventNews::where('category','=',$oldcat)->update(['category' =>$request->get('name')]);
                }
                return redirect()->back()->withSuccess('You set a title status to '.$newscat->name);
            }
            return redirect()->back()->withError('You selected, an invalid news category');
        }
        return redirect()->back()->withError('Invalid request, only posts are allowed');
    }

    public function TitlesEdit (Request $request, $id) {
        //dd($request);
        if ($request->isMethod('post')) {
            $title = Titles::where('id','=', $id)->first();
            if ($title){
                $validator = Validator::make($request->except('_token'), [
                    'titlename'    => 'required',
                    'order_custom'  => 'required',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify, the passwords dont match')->withInput()->withErrors($validator);
                } else {
                    $oldtitle = $title->name;
                    $title->name = $request->get('titlename');
                    $title->order_custom = $request->get('order_custom');
                    $title->save();
                    //update all profile titles
                    $update_news = DelegateProfile::where('title','=',$oldtitle)->update(['title' => $request->get('name')]);
                }
                return redirect()->back()->withSuccess('You edited title status to '.$title->name);
            }
            return redirect()->back()->withError('You selected, an invalid title');
        }
        return redirect()->back()->withError('Invalid request, only posts are allowed');
    }

    public function NewsCatStatus(Request $request, $id) {
        if ($request->isMethod('post')) {
            $newscat = NewsCategories::where('id','=', $id)->first();
            if ($newscat){
                $newscat->status = ($newscat->status == 'Active') ? 'Inactive' : 'Active';
                $newscat->save();
                return redirect()->back()->withSuccess('You set a title status to '.$newscat->status);
            }
            return redirect()->back()->withError('You selected, an invalid news category');
        }
        return redirect()->back()->withError('Invalid request, only posts are allowed');
    }

    public function TitlesStatus (Request $request, $id) {
        if ($request->isMethod('post')) {
            $title = Titles::where('id','=', $id)->first();
            if ($title){
                $title->status = ($title->status == 'Active') ? 'Inactive' : 'Active';
                $title->save();
                return redirect()->back()->withSuccess('You set a title status to '.$title->status);
            }
            return redirect()->back()->withError('You selected, an invalid title');
        }
        return redirect()->back()->withError('Invalid request, only posts are allowed');
    }

    public function TitlesListindexlist() {
        return Datatables::of(Titles::all())
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.title.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change this titles status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                    <li><a href='#' data-url='" . route('org.titles.edit', $list->id) . "'
                    data-name='" . $list->name . "' data-order_custom='" . ($list->order_custom) . "'
                    data-toggle=\"modal\" data-target=\"#editTitles\"> Edit Title </a></li>
                    </ul>
                </div>";
            })->rawColumns(['tools'])
            ->make(true);
    }

    public function NewsCatListindexlist() {
        return Datatables::of(NewsCategories::all())
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.newscat.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change this news category status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                    <li><a href='#' data-url='" . route('org.newscat.edit', $list->id) . "'
                    data-name='" . $list->name . "' data-order_custom='" . ($list->order_custom) . "'
                    data-toggle=\"modal\" data-target=\"#editNewsCategory\"> Edit Category </a></li>
                    </ul>
                </div>";
            })->rawColumns(['tools'])
            ->make(true);
    }

    public function DelegationListindexlist() {
        return Datatables::of(DelegationsType::all())
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.fix.delegation.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change delegate status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Disable | Enable </a></li>
                     <li><a href='#' data-url='" . route('org.fix.delegation.status', $list->id) . "'
                    class='confirm-this' data-msg='Are you sure you want to change delegate status ?'
                    data-toggle=\"modal\" data-target=\"#confirmModal\"> Add Payment </a></li>
                    <li><a href='#' data-url='" . route('org.fix.delegation.edit', $list->id) . "'
                    data-name='" . $list->name . "' data-description='" . htmlentities($list->description, ENT_QUOTES) . "'
                    data-toggle=\"modal\" data-target=\"#editDelegation\"> Edit Delegation </a></li>

                    </ul>
                </div>";
            })
            ->rawColumns(['tools'])
            ->make(true);
    }

    public function EventList () {
        return view('organizer.event-list');
    }

    public function EventListOverview ( Request $request, $event_id) {
        $event = Event::whereId($event_id)
            ->with('poster','tickets','tickets.delegates','tpackages','programmes','news','activities','tents')
            ->with('sponsor','organizer','meta','delegations','fields','hotels','hotels.hotel','hotels.delegate','hotels.delegate.type')
            ->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $titles = Titles::active()->get();
            $delegate_types = DelegatesType::active()->get();
            $meta_types  = EventsMetaData::active()->get();
            $all_delegate_types = $delegate_types->pluck('id')->toArray();
            $all_tickets = $event->tickets->pluck('delegate_id')->toArray();
            if (count(array_diff($all_delegate_types,$all_tickets)) > 0 ) {
                $missing_tickets = DelegatesType::where('event_id','=',$event_id)->active()->whereIn('id',array_diff($all_delegate_types,$all_tickets))->get();
            }
            else {
                $missing_tickets = collect([]);
            }
            //dd($event->activities);
            $sales_tickets = ReportsMaker::EventSalesStatsTicket($event_id);
            $sales_act = ReportsMaker::EventSalesStatsAct($event_id);
            $sales_trans = ReportsMaker::EventSalesStatsTrans($event_id);
            $sales_year = ReportsMaker::EventSalesStatsMonth($event_id,'json');
            $stats = ReportsMaker::DashboardEventStats($event_id);
            $s = json_decode($sales_year->getContent(),true);
            $sales_year_array= collect($s)->mapWithKeys(function ($item) {
                return [$item['period'] => $item['sales']];
            });
            $sales_year_array->all();
            $r = [];
            foreach ($s as $ls) {
                $r[] = [$ls['period'],$ls['sales']];
            }
            //dd($sales_tickets);
            $total_ticket_sales = $sales_tickets->sum(function ($item) {
                return $item['qty'] * $item['price'];
            });
            //dd($sales_act);
	        //dd($event->tickets->delegates);
            return view('organizer.event-overview',compact('r','sales_act','titles','sales_trans','total_ticket_sales','stats','sales_year_array','event','delegate_types','meta_types','missing_tickets','sales_tickets','sales_year'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function DelegateDbDownload (Request $request, $event_id) {
    	$data = array();
	    $event = Event::whereId($event_id)
	                  ->with('poster','tickets','tickets.delegates','tpackages','programmes','news','activities')
	                  ->with('sponsor','organizer','meta','delegations','fields','hotels','hotels.hotel','hotels.delegate','hotels.delegate.type')
	                  ->first();
	    if ($event) {

		    //	    	$delegates = Invitations::where('event_id','=',$event_id)->active()
//                       ->with('delegateType','delegatePrice','hotel','transfer','transfer.package','profile')
//                       ->with('activity','delegation','inflight','outflight','payments','refunds')
//                       ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
//                       //->where('id','=',9)
//                       ->get();
		    $delegates = Invitations::query();

		    //dd($request);
		    if ($request->has('delegations') && count($request->get('delegations')) >= 1 ) {
			    //delegations[]
			    if( in_array('all',$request->get('delegations')) ){	   }   else {
				    //$delegates1 = $delegates->whereIn('delegation_id', $request->get('delegations'));
				    //$delegates = $delegates1->all();
				    $delegates = $delegates->whereIn('delegation_id', $request->get('delegations'));
			    }
		    }

		    if ($request->has('tickets') && count($request->get('tickets')) >= 1 ) {
			    //tickets[]
			    if( in_array('all',$request->get('tickets')) ){	   }   else {
				    //$delegates2 = $delegates->whereIn('delegation.id', $request->get('tickets'));
				    //$delegates = $delegates2->all();
				    $delegates = $delegates->whereIn('delegate_id', $request->get('tickets'));
			    }
		    }

		    if ($request->has('title') && count($request->get('title')) >= 1 ) {
			    //title[]
			    if( in_array('all',$request->get('title')) ){    }   else {
				    //$delegates3 = $delegates->whereIn('profile.title', $request->get('title'));
				    //$delegates = $delegates3->all();
				    $delegates = $delegates->whereHas('profile', function ($query) use($request) {
					    $query->whereIn('delegate_profile.title', $request->get('title'));
				    });
			    }
		    }

		    if ($request->has('Hotels') && count($request->get('Hotels')) >= 1 ) {
			    //Hotels[]
			    if( in_array('all',$request->get('Hotels')) ){    }    else {
				    //$delegates4 = $delegates->whereIn('hotel.hotel_id', $request->get('Hotels'));
				    //$delegates = $delegates4->all();
				    $delegates = $delegates->whereHas('hotel', function ($query) use($request) {
					    $query->whereIn('invitation_acc.hotel_id', $request->get('Hotels'));
				    });
			    }
		    }

		    if ($request->has('activities') && count($request->get('activities')) >= 1 ) {
			    //activities[]
			    if ( in_array( 'all', $request->get( 'activities' ) ) ) {   } else {
				    //$delegates5 = $delegates->whereIn( 'activity.id', $request->get( 'activities' ) );
				    //$delegates  = $delegates5->all();
				    $delegates = $delegates->whereHas('activity', function ($query) use($request) {
					    $query->whereIn('invitation_act.activity_id', $request->get('activities'));
				    });
			    }
		    }
		    //dd($delegates);

		    if ($request->has('tpkgs') && count($request->get('tpkgs')) >= 1 ) {
			    //tpkgs[]
			    if( in_array('all',$request->get('tpkgs')) ){    }    else {
				    //$delegates6 = $delegates->whereIn('transfer.tpackage_id', $request->get('tpkgs'));
				    //$delegates = $delegates6->all();
				    $delegates = $delegates->whereHas('transfer', function ($query) use($request) {
					    $query->whereIn('invitation_tpackage.tpackage_id', $request->get('tpkgs'));
				    });
			    }
		    }

		    if ($request->has('ppass') && count($request->get('ppass')) >= 1 ) {
			    //ppass[]
			    if( in_array('all',$request->get('ppass')) ){    }    else {
				    $delegates = $delegates->whereIn('vehicle_pass', $request->get('ppass'));
				    //$delegates = $delegates7->all();
			    }
		    }
		    //dd($request);
		    $delegates = $delegates->where('event_id','=',$event_id)//->active()
		                           ->with('delegateType','delegatePrice','hotel','transfer','transfer.package','profile')
		                           ->with('activity','delegation','inflight','outflight','payments','refunds')
		                           ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
		                           ->get();

		    //dd($request);
		    if ($request->has('file') && $request->get('file') == 'pdf') {
			    Excel::create( $event->name.' Database - '.date("d-m-Y"), function($excel) use($event,$delegates) {
				    //$excel->setTitle('Our new awesome title');
				    $excel->sheet('New sheet', function($sheet) use($delegates,$event) {
					    $sheet->loadView('export.delegate-db')->with('delegates',$delegates)->with('event',$event);
				    })->setTitle($event->name.' Database');
			    })->download('pdf');
		    }
		    else {
			    Excel::create( $event->name.' Database - '.date("d-m-Y"), function($excel) use($event,$delegates) {
				    //$excel->setTitle('Our new awesome title');
				    $excel->sheet('New sheet', function($sheet) use($delegates,$event) {
					    $sheet->loadView('export.delegate-db')->with('delegates',$delegates)->with('event',$event);
				    })->setTitle($event->name.' Database');
			    })->download('csv');
		    }
	    }
	    else {
		    return redirect()->back()->withError('You have selected an Invalid Event');
	    }
    }

    public function DelegateDbCustomDownload (Request $request, $event_id) {
    	$data = array();
	    $event = Event::whereId($event_id)
	                  ->with('poster','tickets','tickets.delegates','tpackages','programmes','news','activities')
	                  ->with('sponsor','organizer','meta','delegations','fields','hotels','hotels.hotel','hotels.delegate','hotels.delegate.type')
	                  ->first();
	    if ($event) {
//	    	$delegates = Invitations::where('event_id','=',$event_id)->active()
//                       ->with('delegateType','delegatePrice','hotel','transfer','transfer.package','profile')
//                       ->with('activity','delegation','inflight','outflight','payments','refunds')
//                       ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
//                       //->where('id','=',9)
//                       ->get();
	    	$delegates = Invitations::query();

		    //dd($request);
	    	if ($request->has('delegations') && count($request->get('delegations')) >= 1 ) {
				//delegations[]
			    if( in_array('all',$request->get('delegations')) ){	   }   else {
					//$delegates1 = $delegates->whereIn('delegation_id', $request->get('delegations'));
				    //$delegates = $delegates1->all();
				    $delegates = $delegates->whereIn('delegation_id', $request->get('delegations'));
			    }
		    }

		    if ($request->has('tickets') && count($request->get('tickets')) >= 1 ) {
				//tickets[]
			    if( in_array('all',$request->get('tickets')) ){	   }   else {
					//$delegates2 = $delegates->whereIn('delegation.id', $request->get('tickets'));
				    //$delegates = $delegates2->all();
				    $delegates = $delegates->whereIn('delegate_id', $request->get('tickets'));
			    }
		    }

		    if ($request->has('title') && count($request->get('title')) >= 1 ) {
				//title[]
			    if( in_array('all',$request->get('title')) ){    }   else {
					//$delegates3 = $delegates->whereIn('profile.title', $request->get('title'));
				    //$delegates = $delegates3->all();
					$delegates = $delegates->whereHas('profile', function ($query) use($request) {
						$query->whereIn('delegate_profile.title', $request->get('title'));
					});
			    }
		    }

		    if ($request->has('Hotels') && count($request->get('Hotels')) >= 1 ) {
				//Hotels[]
			    if( in_array('all',$request->get('Hotels')) ){    }    else {
					//$delegates4 = $delegates->whereIn('hotel.hotel_id', $request->get('Hotels'));
				    //$delegates = $delegates4->all();
				    $delegates = $delegates->whereHas('hotel', function ($query) use($request) {
					    $query->whereIn('invitation_acc.hotel_id', $request->get('Hotels'));
				    });
			    }
		    }

		    // if ($request->has('full') && count($request->get('full')) >= 1 ) {
			  //   //activities[]
			  //   if ( in_array( 'all', $request->get( 'full' ) ) ) {   } else {
				//     //$delegates5 = $delegates->whereIn( 'activity.id', $request->get( 'activities' ) );
				//     //$delegates  = $delegates5->all();
				//     $delegates = $delegates->whereHas('activity', function ($query) use($request) {
				// 	    $query->whereIn('invitation_act.activity_id', $request->get('activities'));
				//     });
			  //   }
		    // }
		    //dd($delegates);

		    if ($request->has('tpkgs') && count($request->get('tpkgs')) >= 1 ) {
				//tpkgs[]
			    if( in_array('all',$request->get('tpkgs')) ){    }    else {
					//$delegates6 = $delegates->whereIn('transfer.tpackage_id', $request->get('tpkgs'));
				    //$delegates = $delegates6->all();
				    $delegates = $delegates->whereHas('transfer', function ($query) use($request) {
					    $query->whereIn('invitation_tpackage.tpackage_id', $request->get('tpkgs'));
				    });
			    }
		    }

		    if ($request->has('ppass') && count($request->get('ppass')) >= 1 ) {
			    //ppass[]
			    if( in_array('all',$request->get('ppass')) ){    }    else {
				    $delegates = $delegates->whereIn('vehicle_pass', $request->get('ppass'));
				    //$delegates = $delegates7->all();
			    }
		    }
		    //dd($request);
		    $delegates = $delegates->where('event_id','=',$event_id)//->active()
                       ->with('delegateType','delegatePrice','hotel','transfer','transfer.package','profile')
                       ->with('activity','delegation','inflight','outflight','payments','refunds')
                       ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
                       ->get();
	    	//dd($delegates);

		    if ($request->has('file') && $request->get('file') == 'pdf') {
			    Excel::create( $event->name.' Database - '.$request->get('report_type').' - '.date("d-m-Y"), function($excel) use($event,$delegates,$request) {
				    //$excel->setTitle('Our new awesome title');
				    $excel->sheet('New sheet', function($sheet) use($delegates,$event,$request) {
				    	if ( $request->get('report_type') == 'full') {
						    $sheet->loadView('export.delegate-db')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'hotels') {
						    $sheet->loadView('export.delegate-db-hotels')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'tpkgs') {
						    $sheet->loadView('export.delegate-db-tpkgs')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'flight') {
						    $sheet->loadView('export.delegate-db-flight')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'owing') {
						    $sheet->loadView('export.delegate-db-owing')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'other') {
						    $sheet->loadView('export.delegate-db-other')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    //$sheet->loadView('export.delegate-db')->with('delegates',$delegates)->with('event',$event);
				    })->setTitle($event->name.' Database');
			    })->download('pdf');
		    }
		    else {
			    Excel::create( $event->name.' Database- '.$request->get('report_type').' - '.date("d-m-Y"), function($excel) use($event,$delegates,$request) {
				    //$excel->setTitle('Our new awesome title');
				    $excel->sheet('New sheet', function($sheet) use($delegates,$event,$request) {
					    if ( $request->get('report_type') == 'full') {
						    $sheet->loadView('export.delegate-db')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'hotels') {
						    $sheet->loadView('export.delegate-db-hotels')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'tpkgs') {
						    $sheet->loadView('export.delegate-db-tpkgs')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'flight') {
						    $sheet->loadView('export.delegate-db-flight')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'owing') {
						    $sheet->loadView('export.delegate-db-owing')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    if ( $request->get('report_type') == 'other') {
						    $sheet->loadView('export.delegate-db-other')->with('request',$request)->with('delegates',$delegates)->with('event',$event);
					    }
					    //$sheet->loadView('export.delegate-db')->with('delegates',$delegates)->with('event',$event);
				    })->setTitle($event->name.' Database');
			    })->download('csv');
		    }
	    }
	    else {
		    return redirect()->back()->withError('You have selected an Invalid Event');
	    }
    }

    public function ListDelegationPayments (Request $request, $delegation_id) {
        if ($delegation_id) {
            $list = Payments::where('delegation_id','=',$delegation_id)->where('payment_group','=',1);
        } else {
            $list = Payments::where('payment_group','=',1);
        }
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='" . route('org.edit.event', $list->id) . "'  > Enable | Disable </a></li>
                    <li><a href='" . route('org.view.overview', $list->id) . "'  > Add Refund </a></li>
                    <li><a href='" . route('org.view.overview', $list->id) . "'  > Print Reciept </a></li>
                    </ul>
                </div>";
            })
            ->rawColumns(['tools','posters'])
            ->make(true);
    }

    public function ListDelegationRefunds (Request $request, $delegation_id) {
        if ($delegation_id) {
            $list = Refunds::where('delegation_id','=',$delegation_id)->where('refund_group','=',1);
        } else {
            $list = Refunds::where('refund_group','=',1);
        }
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='" . route('org.edit.event', $list->id) . "'  > Enable | Disable </a></li>
                    <li><a href='" . route('org.view.overview', $list->id) . "'  > Print Reciept </a></li>
                    </ul>
                </div>";
            })
            ->rawColumns(['tools','posters'])
            ->make(true);
    }

    public function ListDelegationOverPayments (Request $request, $delegation_id) {
        if ($delegation_id) {
            $list = Overpayment::where('delegation_id','=',$delegation_id)
                ->where('overpayment_group','=',1)->with('payment');
        } else {
            $list = Overpayment::where('overpayment_group','=',1)->with('payment');
        }
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='" . route('org.edit.event', $list->id) . "'  > Enable | Disable </a></li>
                    <li><a href='" . route('org.view.overview', $list->id) . "'  > Print Reciept </a></li>
                    </ul>
                </div>";
            })
            ->rawColumns(['tools','posters'])
            ->make(true);
    }

    public function CheckEvent($event) {

    }

    public function ListEvents (Request $request, $status) {
    if ($status == '1') {
        $list = Event::where('is_deleted','=', 0)
            ->Where('end', '>', Carbon::now())
            ->Where('is_published', '=', 0)
            ->with('poster','delegations','delegations.invitations','delegations.countryname')
            ;
    } elseif ($status == '2') {
        $list = Event::where('is_deleted','=', 0)
            ->Where('end', '<', Carbon::now())
            ->Where('is_published', '=', 0)
            ->with('poster','delegations','delegations.invitations','delegations.countryname');
    } elseif ($status == '3') {
        $list = Event::where('is_deleted','=', 0)
            //->Where('end', '>', Carbon::now())
            ->Where('is_published', '=', 1)
            ->with('poster','delegations','delegations.invitations','delegations.countryname');
    } else {
        $list = Event::where('is_deleted','=', 1)
            ->Where('end', '>', Carbon::now())
            ->Where('is_published', '=', 1)
            ->with('poster','delegations','delegations.invitations','delegations.countryname');
    }
    return Datatables::of($list)
        ->editColumn('posters', function ($list) {
            if (count($list->poster) > 0 ) {
                return '<img style="width: 50px !important" src="' . url($list->poster->first()->image_path) . '" />';
            } else {
                return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
            }
        })
        ->addColumn('tools', function ($list) {
            $lst = null ;
            if ($list->is_deleted == 0) {
                $lst .= "
                    <li><a href='#' data-url='" . route('org.edit.delete.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to delete this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>
                ";
            }
            else {
                $lst .= "
                    <li><a href='#' data-url='" . route('org.edit.delete.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to undelete this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> un-Delete </a></li>
                ";
            }
            if ($list->is_published == 0 ) {
                $lst .= "
                    <li><a href='#' data-url='" . route('org.edit.publish.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to unpublish this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Unpublish </a></li>
                ";
            }
            else {
                $lst .= "
                    <li><a href='#' data-url='" . route('org.edit.publish.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to publish this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Publish </a></li>
                ";
            }
                //<li><a href='" . route('org.edit.event', $list->id) . "'  > Manage Event </a></li>
            return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='" . route('org.view.overview', $list->id) . "'  >Manage Event </a></li>
                    <li><a target='_blank' href='" . route('org.view.event', $list->slug) . "'  > View Event </a></li>
                    <li><a href='" . route('org.events.delegations', $list->id) . "'  > Event Delegations </a></li>
                    <li><a href='" . route('org.events.delegations.uploads', $list->id) . "'  > Delegations Uploads </a></li>
                    <li><a href='" . route('org.events.export.database', $list->id) . "'> Delegate Database </a></li>
                    <li><a href='" . route('org.events.custom.fields', $list->id) . "'  > Custom Fields </a></li>
                    ".$lst ."
                    </ul>
                </div>";
        })
        ->rawColumns(['tools','posters'])
        ->make(true);
}

    public function EventListNew (Request $request) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                'event-name'  => 'required',
                'theme'       => 'required',
                'venue'       => 'required',
                'lat'         => 'required',
                'lng'         => 'required',
                'start'       => 'required',
                'end'         => 'required',
                'description' => 'required',
                'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|',

              ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {

                $event = New Event();
                $input = $request->except('_token');
                $input['name'] = ($request->get('event-name'));
                $input['start'] = Carbon::parse($request->get('start'));
                $input['end'] = Carbon::parse($request->get('end'));
                $input['venue'] = $request->get("formatted_address") != null || $request->get("formatted_address") == '' ? $request->get("formatted_address") : $request->get("venue") ;
                $input['use_coordinates'] = 1;
                $input['coordinates'] = implode(',', [ $input['lat'], $input['lng'] ]);
                $input['user_id'] = auth()->user()->id;
                if($request->hasFile('logo')){
                  $image= $request->file('logo');
                  $filename = time() . '.' . $image->getClientOriginalExtension();
                  $try = Image::make($image)->save( public_path('uploads/event-logos/' . $filename ) );

                  if($try){
                    $input['logo']= $filename;
                  }else{}
                }
                if(!is_null($request->file('event_programme'))){
                  $file= $request->file('event_programme');
                  // var_dump($file);die;
                  $pdfname = $file->getClientOriginalName();
                  $pdf = $request->file('event_programme')->move('uploads/files/', $pdfname);

                  if ($pdf ) {
                      $input['event_programme']= $pdfname;
                  } else { $input['event_programme'] = null; }
                }
                $updated = $event->fill($input)->save();
                if ($updated) {
                    //\LogActivity::addToLog('created a new event.');
                    return redirect()->route('org.edit.event.meta',[$event->id])->withSuccess('You have created a new event, please new event meta data such as videos.');
                }
                else {
                    return redirect()->back()
                        ->withError('The Error inserting data record to the database.');
                }
            }
        }
        return view('organizer.event-make-new');
    }

    public function EventListManageMeta (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $meta_types = EventsMetaData::active()->get();
            return view('organizer.event-manage-meta',compact('event','meta_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventManageProgram (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $meta_types = EventsMetaData::active()->get();
            return view('organizer.event-manage-programme',compact('event','meta_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventTicketManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::where('event_id','=', $id)->active()->get();
            return view('organizer.event-manage-tickets',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventImagesManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::active()->get();
            return view('organizer.event-manage-poster',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventHotelsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::where('event_id','=', $id)->active()->get();
            return view('organizer.event-manage-hotels',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }
    public function EventTentsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $tents = Tent::where('event_id','=',$id)->get();
            return view('organizer.event-manage-tents',compact('event','delegate_types','tents'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }


    public function EventTermsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $terms = TermsConditions::where('event_id','=',$id)->get();
            return view('organizer.event-manage-terms-conditions',compact('event','terms'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventDisclaimersManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $disclaimers = EventDisclaimer::where('event_id','=',$id)->get();
            return view('organizer.event-manage-disclaimer',compact('event','disclaimers'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }


    public function EventDisclaimerNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',
    //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $disclaimers = new EventDisclaimer();
                    $disclaimers->event_id = $id;
                    $disclaimers->description = $request->input('description');
                    $disclaimers->created_by = auth()->user()->id;
                    $disclaimers->save();

                    return redirect()->back()->withSuccess('You created a new Tent for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }


    public function EventMediasManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $medias = EventMedia::where('event_id','=',$id)->get();
            return view('organizer.event-manage-media',compact('event','medias'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }


    public function EventMediaNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',
    //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $medias = new EventMedia();
                    $medias->event_id = $id;
                    $medias->description = $request->input('description');

                    $medias->save();

                    return redirect()->back()->withSuccess('You created a new Tent for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }
    public function EventActivitiesManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::where('event_id','=',$id)->active()->get();
            return view('organizer.event-manage-activities',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventTransferManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::where('event_id','=',$id)->active()->get();
            return view('organizer.event-manage-transport',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventSponsorsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::active()->get();
            return view('organizer.event-manage-sponsors',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventOrganizersManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::active()->get();
            return view('organizer.event-manage-organizers',compact('event','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventNewsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $delegate_types = DelegatesType::active()->get();
            $newscat = NewsCategories::active()->get();
            return view('organizer.event-manage-news',compact('event','delegate_types','newscat'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }


    public function EventBookingFormsManage (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {

            }
            $bookingForms = BookingForm::where('event_id','=',$id)->get();
            return view('organizer.event-manage-bookingForm',compact('event','bookingForms'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventMetaEdit (Request $request, $meta_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'field_name' => 'required',
                    'content_title' => 'required',
                    'content_description' => 'required'
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = EventsMeta::where('id','=',$meta_id)->where('event_id','=',$event_id)->first();
                if ($meta && $event) {
                    $meta->content_title = $request->get('content_title');
                    $meta->content_description = $request->get('content_description');
                    $meta->field_name = $request->get('field_name');
                    $meta->field_title = $request->get('field_name');
                    $meta->save();
                    return redirect()->back()->withSuccess('You edited the meta information');
                }
                return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventTicketEdit (Request $request, $meta_id, $event_id) {
  // dd($meta_id);
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'delegate_id' => 'required',
                    'price' => 'required',
                    'notes' => 'required',
                    'valid_from' => 'required',
                    'valid_to' => 'required',
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {

                $event = Event::where('id','=',$event_id)->first();
                $meta = EventsTickets::where('delegate_id','=',$meta_id)->where('event_id','=',$event_id)->first();
                 // dd($meta);

                if ($meta && $event) {
                    $meta->price = $request->get('price');
                    $meta->notes = $request->get('notes');
                    $meta->valid_from = Carbon::parse($request->get('valid_from'));
                    $meta->valid_to = Carbon::parse($request->get('valid_to'));
                    $meta->save();
                    return redirect()->back()->withSuccess('You edited the Ticket information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Ticket and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventTicketDelete (Request $request, $meta_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = EventsTickets::where('delegate_id','=',$meta_id)->where('event_id','=',$event_id)->first();
                if ($meta && $event) {
                    $meta->delete();
                    return redirect()->back()->withSuccess('You Deleted the Ticket information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Ticket and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventEditProgram (Request $request, $prog_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'title' => 'required',
                    'p_type' => 'required',
                    'description' => 'required',
                    'start' => 'required',
                    'end' => 'required',
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = EventProgramme::where('id','=',$prog_id)->where('event_id','=',$event_id)->first();
                if ($meta && $event) {
                    $meta->title = $request->get('title');
                    $meta->p_type = $request->get('p_type');
                    $meta->description = $request->get('description');
                    $meta->start =  Carbon::parse($request->get('start'));
                    $meta->end =  Carbon::parse($request->get('end'));
                    $meta->save();
                    return redirect()->back()->withSuccess('You edited the Event Programme information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Programme and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventHotelNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'description' => 'required',
                    'website' => 'required',
                    'contact_person' => 'required',
                    'contact_phone' => 'required',
                    'contact_email' => 'required',
//                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $hotel = new EventHotel();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $hotel->fill($input)->save();
                    //assoc gotel to an event
                    $hotel_pivot = new EventHotelPivot();
                    $hotel_pivot->fill(['coupon' => $input['coupon'],'event_id' => $id,'hotel_id' => $hotel->id,'created_by'=>auth()->user()->id])
                    ->save();
                    //assoc hotel to a delegate
                    //DB::table('events_hotels_pivot')->insert(['event_id' => $id,'hotel_id' => $hotel->id]);
                    if(!empty($request->get('delegate_id'))){
                      foreach ($request->get('delegate_id') as $did) {
                          DB::table('delegates_hotels_pivot')->insert(['hotel_pivot' => $hotel_pivot->id, 'event_id' =>$id,'hotel_id' =>$hotel->id,'delegate_id'=>$did,'created_by'=>auth()->user()->id]);
                      }
                    }

                    return redirect()->back()->withSuccess('You created a new Hotel for this event');
                }

            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function EventTentNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'description' => 'required',
                    'price' => 'required',
  //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $tent = new Tent();
                    $tent->name = $request->input('name');
                    $tent->event_id = $id;
                    $tent->description = $request->input('description');
                    $tent->price = $request->input('price');
                    $tent->created_by = auth()->user()->id;
                    $tent->save();
                    //assoc gotel to an event

                    //assoc hotel to a delegate
                    //DB::table('events_hotels_pivot')->insert(['event_id' => $id,'hotel_id' => $hotel->id]);

                    return redirect()->back()->withSuccess('You created a new Tent for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function EventBookingFormNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
              // var_dump($request->input('hotel'));die;
                $validate = Validator::make($request->except('_token'), [
                    'upload_section' => 'required',
                    // 'description' => 'required',
                    // 'price' => 'required',
  //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $bookingForm = new BookingForm();
                    $bookingForm->event_id = $id;
                    $bookingForm->hotels = $request->input('hotel');
                    $bookingForm->tents = $request->input('tent');
                    $bookingForm->flights = $request->input('flight');
                    $bookingForm->transfers = $request->input('transfer');
                    $bookingForm->activities = $request->input('activity');
                    $bookingForm->custom_fields = $request->input('custom_field');
                    $bookingForm->media = $request->input('media');
                    $bookingForm->terms_conditions = $request->input('term_condition');
                    $bookingForm->health_insurance = $request->input('health_insurance');
                    $bookingForm->emergency_contacts = $request->input('emergency_contact');
                    $bookingForm->upload_section = $request->input('upload_section');
                    $bookingForm->created_by = auth()->user()->id;
                    $bookingForm->save();
                    //assoc gotel to an event

                    //assoc hotel to a delegate
                    //DB::table('events_hotels_pivot')->insert(['event_id' => $id,'hotel_id' => $hotel->id]);

                    return redirect()->back()->withSuccess('You created a new Booking Form Setting for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }
    public function EventTermNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',
  //                    'address' => 'required',
                ]);

                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //new hotel
                    $terms = new TermsConditions();
                    $terms->event_id = $id;
                    $terms->description = $request->input('description');
                    $terms->created_by = auth()->user()->id;
                    $terms->save();
                    //assoc gotel to an event

                    //assoc hotel to a delegate
                    //DB::table('events_hotels_pivot')->insert(['event_id' => $id,'hotel_id' => $hotel->id]);

                    return redirect()->back()->withSuccess('You created a new Tent for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }
    public function EventHotelsEdit (Request $request, $hotel_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'description' => 'required',
                    'website' => 'required',
                    'contact_person' => 'required',
                    'contact_phone' => 'required',
                    'contact_email' => 'required',
                    'order_custom' => 'required',
                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = EventHotelPivot::where('hotel_id','=',$hotel_id)->where('event_id','=',$event_id)->first();
                $hotel = EventHotel::where('id','=',$hotel_id)->first();
                $del_hotel = EventHotelDelegatePivot::where('hotel_id','=',$hotel_id)->where('event_id','=',$event_id)->get();

                if ($meta && $event) {
                    //update hotel info
                    $hotel->name = $request->get('name');
                    $hotel->website = $request->get('website');
                    $hotel->description = $request->get('description');
                    $hotel->contact_person = $request->get('contact_person');
                    $hotel->contact_phone = $request->get('contact_phone');
                    $hotel->contact_email = $request->get('contact_email');
                    $hotel->order_custom = $request->get('order_custom');
                    $hotel->coupon = $request->get('coupon');
                    //$hotel->address = $request->get('addredd');
                    $hotel->save();

                    $meta->coupon = $request->get('coupon');
                    $meta->save();

                    //insert new del assoc
                    $hotel_ids = $del_hotel->pluck('id')->toArray();
                    EventHotelDelegatePivot::destroy($hotel_ids);

                    foreach ($request->get('delegate_id') as $did) {
                        DB::table('delegates_hotels_pivot')->insert(['hotel_pivot' => $meta->id, 'event_id' =>$event_id,'hotel_id' =>$hotel_id,'delegate_id'=>$did,'created_by'=>auth()->user()->id]);
                    }
                    return redirect()->back()->withSuccess('You edited the Event Hotel information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventTentsEdit (Request $request, $tent_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'description' => 'required',
                    'price' => 'required',
                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $tent = Tent::where('id','=',$tent_id)->where('event_id','=',$event_id)->first();

                if ($tent && $event) {
                    //update hotel info
                    $tent->name = $request->get('name');
                    $tent->description = $request->get('description');
                    $tent->price = $request->get('price');
                    $tent->status = $request->get('status');
                    //$tent->address = $request->get('addredd');
                    $tent->save();


                    //insert new del assoc
                    // $hotel_ids = $del_hotel->pluck('id')->toArray();
                    // EventHotelDelegatePivot::destroy($hotel_ids);
                    //
                    // foreach ($request->get('delegate_id') as $did) {
                    //     DB::table('delegates_hotels_pivot')->insert(['hotel_pivot' => $meta->id, 'event_id' =>$event_id,'hotel_id' =>$hotel_id,'delegate_id'=>$did,'created_by'=>auth()->user()->id]);
                    //}
                    return redirect()->back()->withSuccess('You edited the Event Tent information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventTermsEdit (Request $request, $term_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',

                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $term = TermsConditions::where('id','=',$term_id)->where('event_id','=',$event_id)->first();

                if ($term && $event) {
                    //update hotel info
                    $term->description = $request->get('description');
                    $term->status = $request->get('status');
                    //$term->address = $request->get('addredd');
                    $term->save();


                    //insert new del assoc
                    // $hotel_ids = $del_hotel->pluck('id')->toArray();
                    // EventHotelDelegatePivot::destroy($hotel_ids);
                    //
                    // foreach ($request->get('delegate_id') as $did) {
                    //     DB::table('delegates_hotels_pivot')->insert(['hotel_pivot' => $meta->id, 'event_id' =>$event_id,'hotel_id' =>$hotel_id,'delegate_id'=>$did,'created_by'=>auth()->user()->id]);
                    //}
                    return redirect()->back()->withSuccess('You edited the Event Tent information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventDisclaimersEdit (Request $request, $disclaimer_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',

                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $disclaimer = EventDisclaimer::where('id','=',$disclaimer_id)->where('event_id','=',$event_id)->first();

                if ($disclaimer && $event) {
                    //update hotel info
                    $disclaimer->description = $request->get('description');
                    $disclaimer->status = $request->get('status');
                    //$disclaimer->address = $request->get('addredd');
                    $disclaimer->save();

                    return redirect()->back()->withSuccess('You edited the Event Tent information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EventMediasEdit (Request $request, $media_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'description' => 'required',

                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $media = EventMedia::where('id','=',$media_id)->where('event_id','=',$event_id)->first();

                if ($media && $event) {
                    //update hotel info
                    $media->description = $request->get('description');
                    $media->save();

                    return redirect()->back()->withSuccess('You edited the Event Tent information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }



    public function EventBookingFormsEdit (Request $request, $form_id, $event_id) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'upload_section' => 'required',
                    // 'description' => 'required',
                    // 'price' => 'required',
                ]
            );
            //dd($request);
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $bookingForm = BookingForm::where('id','=',$form_id)->where('event_id','=',$event_id)->first();

                if ($bookingForm && $event) {
                    //update hotel info
                    $bookingForm->event_id = $event_id;
                    $bookingForm->hotels = $request->input('edit_hotel');
                    $bookingForm->tents = $request->input('edit_tent');
                    $bookingForm->flights = $request->input('edit_flight');
                    $bookingForm->transfers = $request->input('edit_transfer');
                    $bookingForm->activities = $request->input('edit_activity');
                    $bookingForm->custom_fields = $request->input('edit_custom_field');
                    $bookingForm->media = $request->input('edit_media');
                    $bookingForm->terms_conditions = $request->input('edit_term_condition');
                    $bookingForm->health_insurance = $request->input('edit_health_insurance');
                    $bookingForm->emergency_contacts = $request->input('edit_emergency_contact');
                    $bookingForm->upload_section = $request->input('upload_section');
                    $bookingForm->created_by = auth()->user()->id;
                    $bookingForm->save();


                    return redirect()->back()->withSuccess('You edited the Booking Form information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Hotel and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }


    public function EventMetaDelete (Request $request, $meta_id, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = EventsMeta::where('id','=',$meta_id)->where('event_id','=',$event_id)->first();
        if ($meta && $event) {
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the meta information');
        }
        return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
    }

    public function EventImagesDelete (Request $request, $meta_id, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = EventPosters::where('id','=',$meta_id)->where('event_id','=',$event_id)->first();
        if ($meta && $event) {
	        if(file_exists(public_path().'/'.$meta->image_path)){
		        unlink(public_path().'/'.$meta->image_path);
	        }else{
		        //echo 'file not found';
	        }
            //unlink(public_path().$meta->image_path);
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the Poster information');
        }
        return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
    }

    public function DelOtherImages (Request $request, $id, $event_id, $rid, $type ) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = SysImages::where('id','=',$id)->first();
	        if ($meta && $event) {
		        if(file_exists(public_path().'/'.$meta->image_path )){
		            unlink(public_path().'/'.$meta->image_path );
		        } else {
			        //echo 'file not found';
		        }
            //unlink(public_path().'/'.$meta->image_path);
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the Image information');
        }
        return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
    }

    public function EventSponsorDelete (Request $request, $id, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = EventSponsor::where('id','=',$id)->first();
        if ($meta && $event) {
            //unlink(public_path().'/'.$meta->logo);
	        if(file_exists(public_path().'/'.$meta->logo ) ){
	            unlink(public_path().'/'.$meta->logo );
	        } else{
			        //echo 'file not found';
	        }
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the Sponsor information');
        }
        return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
    }

    public function EventOrganizerDelete (Request $request, $id, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = EventOrganizer::where('id','=',$id)->first();
        if ($meta && $event) {
            //unlink(public_path().'/'.$meta->logo);
	        if(file_exists(public_path().'/'.$meta->logo )){
		        unlink(public_path().'/'.$meta->logo );
	        }else{
		        //echo 'file not found';
	        }
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the Organizer information');
        }
        return redirect()->back()->withError('Invalid request - Invalid meta data and event identifier');
    }

    public function EventDeleteProgram (Request $request, $prog_id, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        $meta = EventProgramme::where('id','=',$prog_id)->where('event_id','=',$event_id)->first();
        if ($meta && $event) {
            $meta->delete();
            return redirect()->back()->withSuccess('You deleted the Event Programme information');
        }
        return redirect()->back()->withError('Invalid request - Invalid Event Programme data and event identifier');
    }

    public function EventDelete (Request $request, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        if ($event) {
            if ($event->is_deleted == 0) {
                $event->is_deleted = 1;
                $event->is_published = 1;
                $event->status = 'deleted';
                $event->save();
                return redirect()->back()->withSuccess('You deleted the event');
            }
            else {
                $event->is_deleted = 0;
                $event->is_published = 0;
                $event->status = 'active';
                $event->save();
                return redirect()->back()->withSuccess('You un-deleted the event ');
            }
        }
        return redirect()->back()->withError('Invalid request - Invalid event identifier');
    }

    public function EventPublish (Request $request, $event_id) {
        $event = Event::where('id','=',$event_id)->first();
        if ($event) {
            if ($event->is_published == 0) {
                //$event->is_deleted = 1;
                $event->is_published = 1;
                $event->status = 'unpublished';
                $event->save();
                return redirect()->back()->withSuccess('You unpublished the event');
            }
            else {
                //$event->is_deleted = 0;
                $event->is_published = 0;
                $event->status = 'active';
                $event->save();
                return redirect()->back()->withSuccess('You published the event ');
            }
        }
        return redirect()->back()->withError('Invalid request - Invalid  event identifier');
    }

    public function ListEventsMeta (Request $request, $id) {
        $list = EventsMeta::where('event_id','=',$id);
        return Datatables::of($list)
            ->editColumn('status', function ($list) {
                return $list->is_processed == 0 ? "Active" : "In-active";
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                   <li> <a href='#' data-type='" . $list->field_name . "' data-url='" . route('org.edit.meta',[$list->id,$list->event_id]) . "' data-title='" . $list->content_title . "' data-desc='" . htmlentities($list->content_description, ENT_QUOTES) . "' data-toggle=\"modal\" data-target=\"#editMeta\"> Edit</a> </li>
                   <li> <a href='#' data-url='".route('org.del.meta',[$list->id, $list->event_id])."' class='confirm-this' data-msg='Are you sure you want to delete this item, Action is not Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Delete </a> </li>
                </ul>
                </div>";
            })
            ->rawColumns(['tools','data'])
            ->make(true);
    }

    public function ListEventsTickets (Request $request, $id) {
        $list = EventsTickets::with('delegates');
        return Datatables::of($list)
            ->editColumn('status', function ($list) {
                return $list->is_processed == 0 ? "Active" : "In-active";
            })
            ->addColumn('validity', function ($list) {
                return \App\Logic\NthNumber::addOrdinalNumberSuffix(date('d',strtotime($list->valid_from)))  . " - ". \App\Logic\NthNumber::addOrdinalNumberSuffix(date('d',strtotime($list->valid_to))) .' '.date('M',strtotime($list->valid_to)) .' '. date('Y',strtotime($list->valid_to)) ;
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                   <li><a href='#' data-url='" . route('org.edit.tickets.event', [$list->id,$list->event_id]) . "' data-title='".$list->name."' data-delegate='".$list->delegate_id."' data-price='".$list->price."' data-desc='".htmlentities($list->notes)."' data-start_date='".$list->valid_from."' data-end_date='".$list->valid_to."'  data-toggle=\"modal\" data-target=\"#editTickets\"> Edit </a></li>
                   <li><a href='#' data-url='" . route('org.delete.tickets.event', [$list->id,$list->event_id]) . "'  data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>

                </ul>
                </div>";
            })
            ->rawColumns(['tools','notes'])
            ->make(true);
    }

    public function EventsPostersEdit (Request $request, $id, $event_id ) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'sort' => 'required',
                    'is_main' => 'required',
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = EventPosters::where('id','=',$id)->where('event_id','=',$event_id)->first();
                if ($meta && $event) {
                    $meta->sort = $request->get('sort');
                    $meta->is_main = $request->get('is_main');
                    $meta->save();
                    return redirect()->back()->withSuccess('You edited the Event Poster information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Poster and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EditOtherImages (Request $request,$id, $event_id, $rid, $type ) {
        if ($request->isMethod('post') ) {
            $validate = Validator::make($request->except('_token'), [
                    'sort' => 'required',
                    'is_main' => 'required',
                ]
            );
            if ($validate->fails()) {
                return redirect()->back()
                    ->withError('The data you submitted has some errors, see highlighted in red .')
                    ->withErrors($validate)
                    ->withInput();
            } else {
                $event = Event::where('id','=',$event_id)->first();
                $meta = SysImages::where('id','=',$id)->first();
                if ($meta && $event) {
                    $meta->sort = $request->get('sort');
                    $meta->is_main = $request->get('is_main');
                    $meta->status = $request->get('status');
                    $meta->save();
                    return redirect()->back()->withSuccess('You edited the Event Poster information');
                }
                return redirect()->back()->withError('Invalid request - Invalid Event Poster and event identifier');
            }
            return redirect()->back()->withError('Invalid request - Only post requests are accepted');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function ListEventsPosters (Request $request, $id) {
         $list = EventPosters::where('event_id','=',$id);
        return Datatables::of($list)
            ->editColumn('image_path', function ($list) {
                if ( \File::exists(public_path().$list->image_path)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->image_path) . '"> <img style="width: 50px !important" src="' . url($list->image_path) . '" /> </a>';

                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->editColumn('is_main', function ($list) {
                return $list->is_main == 1 ? 'Yes' : 'No';
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.poster.event', [$list->id,$list->event_id]) . "' data-image='".url($list->image_path)."' data-sort='".$list->sort."' data-is_main='".$list->is_main."' data-toggle=\"modal\" data-target=\"#editImages\"> Edit </a></li>
                    <li><a href='#' data-url='" . route('org.delete.poster.event', [$list->id,$list->event_id]) . "'  data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>

                </ul>
                </div>";
            })
            ->rawColumns(['tools','image_path'])
            ->make(true);
    }

    public function ListEventsSponsors (Request $request, $id) {
         $list = EventSponsor::where('event_id','=',$id);
        return Datatables::of($list)
            ->editColumn('logo', function ($list) {
                if (File::exists(public_path().'/'.$list->logo)) {
                    return '<img style="width: 50px !important" src="' . url($list->logo) . '" />';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='".route('org.edit.sponsor',[$list->id,$list->event_id])."'
                    data-name='".$list->name."' data-ordering='".$list->ordering."' data-logo='".url($list->logo)."' data-desc='".htmlentities($list->brief, ENT_QUOTES)."'
                    data-website='".$list->website."' data-toggle=\"modal\" data-target=\"#editSponsors\" > Manage Sponsor </a></li>
                    <li> <li><a href='#' data-url='" . route('org.del.sponsor', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to disable this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Delete </a></li> </li>
                </ul>
                </div>";
            })
            ->rawColumns(['tools','logo','brief','image_path'])
            ->make(true);
    }

    public function ListEventsNews (Request $request, $id) {
        $list = EventNews::where('event_id','=',$id);
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.news.event', $list->id) . "' data-type='".$list->category."' data-order_custom='".$list->order_custom."' data-desc='".htmlentities($list->text, ENT_QUOTES)."'  data-toggle=\"modal\" data-target=\"#editNewsDetails\"> Edit </a></li>
                    <li><a href='#' data-url='" . route('org.delete.news.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>
                </ul>
                </div>";
            })
            ->rawColumns(['tools','text'])
            ->make(true);
    }

    public function ListEventsOrganizers (Request $request, $id) {
         $list = EventOrganizer::where('event_id','=',$id);
        return Datatables::of($list)
            ->editColumn('logo', function ($list) {
                if (File::exists(public_path().'/'.$list->logo)) {
                    return '<img style="width: 50px !important" src="' . url($list->logo) . '" />';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                     <li><a href='#' data-url='".route('org.edit.organizer',[$list->id,$list->event_id])."'
                    data-name='".$list->name."' data-logo='".url($list->logo)."' data-desc='".htmlentities($list->brief, ENT_QUOTES)."'
                    data-website='".$list->website."' data-toggle=\"modal\" data-target=\"#editSponsors\" > Manage Sponsor </a></li>
                    <li> <li><a href='#' data-url='" . route('org.delete.organizer', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to disable this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Delete </a></li> </li>

                </ul>
                </div>";
            })
            ->rawColumns(['tools','logo','brief','image_path'])
            ->make(true);
    }

    public function ListOtherImages (Request $request, $eventid, $rid, $type) {
         $list = SysImages::where('r_id','=',$rid)->where('r_type','=',$type);
        return Datatables::of($list)
            ->editColumn('image_path', function ($list) {
                if ( \File::exists(public_path().'/'.$list->image_path)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->image_path) . '"> <img style="width: 50px !important" src="' . url($list->image_path) . '" /> </a>';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->editColumn('is_main', function ($list) {
                return $list->is_main == 1 ? 'Yes' : 'No';
            })
            ->addColumn('tools', function  ($list) use($eventid, $rid, $type) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.image.sys', [$list->id,$eventid, $rid, $type]) . "' data-image='".url($list->image_path)."' data-sort='".$list->sort."' data-is_main='".$list->is_main."' data-status='".$list->status."' data-toggle=\"modal\" data-target=\"#editImages\"> Edit </a></li>
                    <li><a href='#' data-url='" . route('org.delete.image.sys', [$list->id,$eventid, $rid, $type]) . "'  data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>
                </ul>
                </div>";
            })
            ->rawColumns(['tools','image_path'])
            ->make(true);
    }

    public function ListEventsProg (Request $request, $event) {
        $list = EventProgramme::where('event_id','=', $event);
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.event.prog', [$list->id,$list->event_id]) . "' data-title='".$list->title."' data-type='".$list->p_type."' data-desc='".htmlentities($list->description, ENT_QUOTES)."' data-start_date='".$list->start."' data-end_date='".$list->end."'  data-toggle=\"modal\" data-target=\"#editProgramme\"> Edit </a></li>
                    <li><a href='#' data-url='" . route('org.delete.event.prog', [$list->id,$list->event_id]) . "'  class='confirm-this' data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>

                </ul>
                </div>";
            })
            ->rawColumns(['tools','description'])
            ->make(true);
    }

    public function EventListEdit (Request $request, $id) {
        $event = Event::whereId($id)->with('map')->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'event-name'  => 'required',
                    'theme'       => 'required',
                    'venue'       => 'required',
                    'lat'         => 'required',
                    'lng'         => 'required',
                    'start'       => 'required',
                    'end'         => 'required',
                    'description' => 'required',
                    'logo'        => 'image|mimes:jpeg,png,jpg,gif,svg',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate)->withInput();
                } else {
                    $input = $request->except('_token');
                    $input['name'] = ($request->get('event-name'));
                    $input['start'] = Carbon::parse($request->get('start'));
                    $input['end'] = Carbon::parse($request->get('end'));
                    $input['venue'] = $request->get("formatted_address") != null || $request->get("formatted_address") == '' ? $request->get("formatted_address") : $request->get("venue") ;
                    $input['use_coordinates'] = 1;
                    $input['coordinates'] = implode(',', [ $input['lat'], $input['lng'] ]);
                    $input['user_id'] = auth()->user()->id;
                    $input['is_published'] = 0 ;
                    $input['is_deleted'] = 0 ;
                    //$updated = $event->fill($input)->save();
                    //upload folder
                    if (!File::exists('uploads/posters')) {
                        File::makeDirectory('uploads/posters');
                    }
                    //upload
                    if (!is_null($request->file('file_attachment'))) {
                        $extension = $request->file('file_attachment')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999) . '.' . $extension;
                        $upload = $request->file('file_attachment')->move('uploads/posters/', $fileName);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['file_attachment'] = 'uploads/posters/'.$fileName;
                        } else { $input['file_attachment'] = null; }
                    }
                    if($request->hasFile('logo')){
                      $image= $request->file('logo');
                      $filename = time() . '.' . $image->getClientOriginalExtension();
                      $try = Image::make($image)->save( public_path('uploads/event-logos/' . $filename ) );

                      if($try){
                        $input['logo']= $filename;
                      }else{}
                    }
                    if(!is_null($request->file('event_programme'))){
                      $file= $request->file('event_programme');
                      // var_dump($file);die;
                      $pdfname = $file->getClientOriginalName();
                      $pdf = $request->file('event_programme')->move('uploads/files/', $pdfname);

                      if ($pdf ) {
                          $input['event_programme']= $pdfname;
                      } else {  }
                    }
                    //$event_to_update = Events::find($event->id);
                    $updated = $event->fill($input)->update();
                    //\LogActivity::addToLog('edited an event.');
                    return redirect()->back()->withSuccess('Event Details Updated.');
                }
            }
            $meta_types = EventsMetaData::active()->get();
            $delegate_types = DelegatesType::active()->get();
            return view('organizer.event-make-edit',compact('event','meta_types','delegate_types'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventTicketNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'delegate_id'        => 'required',
                    'valid_from'       => 'required',
                    'valid_to'       => 'required',
                    'price'         => 'required',
                    'notes' => 'required',
                    //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                ]);
                if ($validate->fails()) {
//                    return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $delegate = DelegatesType::where('id','=', $request->get('delegate_id'))->first();
                    $ticket_exists =  EventsTickets::where('delegate_id','=',$request->get('delegate_id'))->where('event_id','=',$id)->first();
                    if ( $ticket_exists ) {
                        return redirect()->back()->withError('Invalid request, the ticket '.$delegate->name. ' already exists for the this event');
                    }
                    else {
                        $ticket = new EventsTickets();
                        $input = $request->except('_token');
                        $start = Carbon::parse($request->get('valid_from'));
                        $end = Carbon::parse($request->get('valid_to'));
                        $input['valid_from'] = $start;
                        $input['valid_to'] = $end;
                        $input['name'] = $delegate->name;
                        $input['event_id'] = $id;
                        $input['created_by'] = auth()->user()->id;
                        $ticket->fill($input)->save();
                        //\LogActivity::addToLog('created a new event ticket.');
                        return redirect()->back()->withSuccess('Event Details Updated.');//'#tab_meta_tickets'
                        //return redirect()->back()->withSuccess('You created a new Event Ticket');
                    }
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventProgNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'title' => 'required',
                    'start' => 'required',
                    'end' => 'required',
                    'description' => 'required',
                    'p_type' => 'required',
                ]);

                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $programme = new EventProgramme();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['event_id'] = $id;
                    $input['start'] = Carbon::parse($request->get('start'));;
                    $input['end'] = Carbon::parse($request->get('end'));
                    $programme->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Programme for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function EventNewsNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'category' => 'required',
                    'text' => 'required',
                ]);

                if ($validate->fails()) {
                    return redirect()->route('org.edit.event', [$id,'#tab_news'])
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {

                    $news = new EventNews();
                    $input = $request->except('_token');
                    $input['event_id'] = $id;
                    $input['created_by'] = auth()->user()->id;
                    $news->fill($input)->save();

                    return redirect()->route('org.edit.event', [$id,'#tab_news'])->withSuccess('You created a new News Article for this event');
                }
            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function EventNewsEdit (Request $request, $id) {
        $news = EventNews::whereId($id)->first();
            if ($news) {
                if ($request->isMethod('post')) {
                    $validate = Validator::make($request->except('_token'), [
                        'category' => 'required',
                        'text' => 'required',
                        'order_custom' => 'required',
                    ]);

                    if ($validate->fails()) {
                        return redirect()->back()
                            ->withError('The data you submitted has some errors, see highlighted in red .')
                            ->withErrors($validate)->withInput();
                    } else {
                        //$news = new EventNews();
                        $input = $request->except('_token');
                        //$input['event_id'] = $id;
                        //$input['created_by'] = auth()->user()->id;
                        $news->fill($input)->save();
                        return redirect()->back()->withSuccess('You edited edit News Article for this event');
                    }
                }
            }
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function EventNewsDel (Request $request, $id) {
        $news = EventNews::whereId($id)->first();
            if ($news) {
                $news->delete();
                return redirect()->back()->withSuccess('You deleted an Event news item '. $news->category);
            }
            return redirect()->route('org.events')->withError('You have selected an Invalid Event news item');
    }

    public function EventMapNew (Request $request, $id) {
        $event = Event::whereId($id)->with('map')->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'lat' => 'required',
                    'lng' => 'required',
                    'country' => 'required',
                    'formatted_address' => 'required',
                ]);

                if ($validate->fails()) {
//                    return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->route('org.edit.event', [$id,'#tab_5_3'])
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    if ($event->map) {
                        $map = EventsMap::where('id','=',$event->map->id)->first(); //new EventsMap();
                        $input = $request->except('_token');
                        $input['event_id'] = $id;
                        $input['created_by'] = auth()->user()->id;
                        $map->fill($input)->update();
                    }
                    else {
                        $map = new EventsMap();
                        $input = $request->except('_token');
                        $input['event_id'] = $id;
                        $input['created_by'] = auth()->user()->id;
                        $map->fill($input)->save();
                    }

                    return redirect()->route('org.edit.event', [$id,'#tab_5_3'])->withSuccess('You created a new Map for this event');
                }

            }
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event');
    }

    public function ListEventsHotels (Request $request, $event=null) {
        if ($event == null) {
            $list = EventHotelPivot::with('hotel','images','event','delegate','delegate.type');
            $datatables =  Datatables::of($list);
            if ($request->get('event_id')) {
                //$datatables->having('count', $request->get('operator'), $request->get('post')); // having count search
                $datatables->where('event_id', '=', $request->get('event_id'));
            }
        }
        else {
            $list = EventHotelPivot::where('event_id','=', $event)->with('hotel','images','event','delegate','delegate.type');
            $datatables =  Datatables::of($list);
        }
        return $datatables
            //<li><a href='#'  data-url='" . route('org.delete.event.hotel', [$list->hotel_id,$list->event_id]) . "' data-msg='Are you sure you want to detach this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Detach hotel from Event </a></li>

            ->addColumn('tools', function ($list) use($event) {
                        return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#'  data-url='" . route('org.edit.event.hotel', [$list->hotel_id,$list->event_id]) . "' data-name='".$list->hotel->name."' data-coupon='".$list->coupon."'
                    data-desc='".htmlentities($list->hotel->description, ENT_QUOTES)."' data-website='".$list->hotel->website."' data-person='".$list->hotel->contact_person."'
                    data-phone='".$list->hotel->contact_phone."' data-email='".$list->hotel->contact_email."' data-delegate='".implode(',',$list->delegate->pluck('delegate_id')->flatten()->toArray())."'
                    data-order_custom='".$list->hotel->order_custom."' data-toggle=\"modal\" data-target=\"#editHotels\" > Manage Hotel </a></li>
                    <li><a href='" . route('org.new.event.bulk.img', [$list->event_id,$list->id,'event_hotel'])."'  > Add Images </a></li>
                </ul>
                </div>";
            })
            ->addColumn('delegates', function ($list) {
                $str = null;
                foreach ($list->delegate as $ls ) {
                   // dd($ls->type->name);
                    $str .= "<tr> <td> ".$ls->type->name." </td> </tr>";
                   // dd($str);
                }
                //dd($str);
                return " <table class='table table-bordered'> <tr><th> Delegate Type</th></tr> ".$str." </table>";
            })
            ->editColumn('available', function ($list) {
                return $list->available == 0 ? 'Yes' : 'No';
            })
            ->editColumn('all-images', function ($list) {
                return count($list->images). ' - pictures';
            })
            ->addColumn('contacts', function ($list) {
                $str2 = null;
                $str2 .= "<tr> <td> ".$list->hotel->contact_person." </td> </tr>";
                $str2 .= "<tr> <td> ".$list->hotel->contact_email." </td> </tr>";
                $str2 .= "<tr> <td> ".$list->hotel->contact_phone." </td> </tr>";
                return " <table class='table table-bordered'> <tr><th> Brief </th></tr> ".$str2." </table>";
            })
            ->rawColumns(['tools','delegates','contacts'])
            ->removeColumn('event.description')
            ->orderColumn('hotel.order_custom','-hotel.order_custom $1')
            ->make(true);
    }



    public function EnDisEventActivities (Request $request, $id, $event_id) {
        $act = EventActivity::where('id','=',$id)->with('images')->first();
        $event = Event::where('id','=',$event_id)->first();
        if ($request->isMethod('post') ) {
            if ($act && $event) {
                if ($act->status == 'active') {
                    $act->status = 'inactive';
                }
                else {
                    $act->status = 'inactive';
                }
                $act->save();
                return redirect()->back()->withSuccess('You edited the Event Activity information');
            }
            return redirect()->back()->withError('Invalid request - Invalid Event Poster and event identifier');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function DeleteEventActivities (Request $request, $id, $event_id) {
        $act = EventActivity::where('id','=',$id)->with('images')->first();
        $event = Event::where('id','=',$event_id)->first();
        if ($request->isMethod('post') ) {
            if ($act && $event) {
            	$sel_act = InvitationActivity::where('activity_id','=',$id)->delete();
                $act->delete();
                return redirect()->back()->withSuccess('You Deleted the Event Activity information');
            }
            return redirect()->back()->withError('Invalid request - Invalid Event Poster and event identifier');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function EnDisEventPackages (Request $request, $id, $event_id) {
        $act = EventTransport::where('id','=',$id)->with('images')->first();
        $event = Event::where('id','=',$event_id)->first();
        if ($request->isMethod('post') ) {
            if ($act && $event) {
                if ($act->status == 'active') {
                    $act->status = 'inactive';
                }
                else {
                    $act->status = 'inactive';
                }
                $act->save();
                return redirect()->back()->withSuccess('You edited the Event Activity information');
            }
            return redirect()->back()->withError('Invalid request - Invalid Event Poster and event identifier');
        }
        return redirect()->back()->withError('Invalid request - Only post requests are accepted');
    }

    public function ListEventsAct (Request $request, $event=null) {
        //dd($event);
        if ($event == null) {
            $list = EventActivity::with('images','event');
            $datatables =  Datatables::of($list);
            if ($request->get('event_id')) {
                //$datatables->having('count', $request->get('operator'), $request->get('post')); // having count search
                $datatables->where('event_id', '=', $request->get('event_id'));
            }
            return $datatables
                ->addColumn('tools', function ($list) use($event) {
                    $lt = null;
                    if ($list->status == 'active') {
                        $lt .= "<li><a href='#' data-url='" . route('org.event.dis.act', [$list->id,$list->event_id]) . "'
data-msg='Are you sure you want to disable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" >  Disable Activity </a></li>";
                    }
                    else {
                        $lt .= "<li><a href='#' data-url='" . route('org.event.dis.act', [$list->id,$list->event_id]) . "'
data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Enable Activity </a></li>";
                    }
                    return "
                        <div class='btn-group pull-right'>
                        <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                        aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                        </button>
                        <ul class='dropdown-menu pull-right' role='menu'>
                            <li><a href='#' data-url='".route('org.edit.act',[$list->id,$list->event_id])."'
                            data-name='".$list->name."' data-desc='".htmlentities($list->description, ENT_QUOTES)."' data-location='".$list->location."'
                            data-start_date='".$list->valid_from."' data-end_date='".$list->valid_to."' data-cost='".$list->cost."'
                            data-delegate='".$list->delegate."'  data-toggle=\"modal\" data-target=\"#editActivities\" > Manage Activity </a></li>
                            ".$lt."
                            <li><a href='" . route('org.new.event.bulk.img', [$list->event_id,$list->id,'event_act'])."'  > Add Images </a></li>
                            <li><a href='#' data-url='" . route('org.event.delete.act', [$list->id,$list->event_id]) . "'
data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"  > Delete </a></li>
                        </ul>
                        </div>";
                })
                ->editColumn('img', function ($list) {
                    return count($list->images). ' - pictures';
                })
                ->rawColumns(['tools','description'])
                ->make(true);
        }
        else {
            $list = EventActivity::where('event_id','=',$event)->with('images');
            return Datatables::of($list)
                ->addColumn('tools', function ($list) use($event) {
                    $lt = null;
                    if ($list->status == 'active') {
                        $lt .= "<li><a href='#' data-url='" . route('org.event.dis.act', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Enable Activity </a></li>";
                    }
                    else {
                        $lt .= "<li><a href='#' data-url='" . route('org.event.dis.act', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to disable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Disable Activity </a></li>";
                    }
                    return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='".route('org.edit.act',[$list->id,$list->event_id])."'
                    data-name='".$list->name."' data-desc='".htmlentities($list->description, ENT_QUOTES)."' data-location='".$list->location."'
                    data-start_date='".$list->valid_from."' data-end_date='".$list->valid_to."' data-cost='".$list->cost."'
                    data-delegate='".$list->delegate."'  data-toggle=\"modal\" data-target=\"#editActivities\" > Manage Activity </a></li>
                    ".$lt."
                    <li><a href='" . route('org.new.event.bulk.img', [$list->event_id,$list->id,'event_act'])."'  > Add Images </a></li>
                	<li><a href='#' data-url='" . route('org.event.delete.act', [$list->id,$list->event_id]) . "'
data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"  > Delete </a></li>

                </ul>
                </div>";
                })
                ->editColumn('img', function ($list) {
                    return count($list->images). ' - pictures';
                })
                ->rawColumns(['tools','description'])
                ->make(true);
        }
    }

    public function ListEventsTrans (Request $request, $event) {
        $list = EventTransport::where('event_id','=',$event);
        return Datatables::of($list)
            ->addColumn('tools', function ($list) use($event) {
                $lt = null;
                if ($list->status == 'active') {
                    $lt .= "<li><a href='#' data-url='" . route('org.event.dis.package', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Enable Package </a></li>";
                }
                else {
                    $lt .= "<li><a href='#' data-url='" . route('org.event.dis.package', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to disable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Disable Package </a></li>";
                }
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='".route('org.edit.trans',[$list->id,$list->event_id])."'    data-name='".$list->name."' data-type='".$list->t_type."' data-desc='".htmlentities($list->description, ENT_QUOTES)."' data-location='".$list->location."' data-start_date='".$list->valid_from."' data-end_date='".$list->valid_to."' data-cost='".$list->cost."'
                    data-delegate='".$list->delegate."'  data-toggle=\"modal\" data-target=\"#editTransportPackages\" > Manage Package </a></li>
                    ".$lt."
                    <li><a href='" . route('org.new.event.bulk.img', [$list->event_id,$list->id,'event_trans'])."'  > Add Images </a></li>
                </ul>
                </div>";
            })
            ->editColumn('all-images', function ($list) {
                return count($list->images). ' - pictures';
            })
            ->rawColumns(['tools','description'])
            ->make(true);
    }

    public function uploadEventBulkImg (Request $request, $eventid, $hotelid,$imgtype) {
        $event = Event::whereId($eventid)->first();
        if ($imgtype == 'event_hotel') {
            $hotel = EventHotelPivot::where('event_id','=', $eventid)->where('hotel_id','=', $hotelid)->with('hotel','delegate','delegate.type')->first();
        }
        if ($imgtype == 'event_act') {
            $hotel = EventActivity::where('event_id','=', $eventid)->where('id','=',$hotelid)->first();
        }
        if ($imgtype == 'event_trans') {
            $hotel = EventTransport::where('event_id','=', $eventid)->where('id','=',$hotelid)->first();
        }
        if ($imgtype == 'event_tent'){
           $hotel = Tent::where('event_id','=',$eventid)->where('id','=',$hotelid)->first();
        }
        if ($event && $hotel) {
            if ($request->isMethod('post')) {
                //$photo = $request;
                $response = $this->image->upload($request->toArray(),$eventid, $hotelid,$imgtype);
                return $response;
            }
            return view('organizer.event-upload-bulk-images',compact('event','hotel','hotelid','imgtype'));
        }
        return redirect()->route('org.events')->withError('You have selected an Invalid Event or hotel');
    }

    public function uploadEventBulkPosters (Request $request, $eventid) {
        $event = Event::whereId($eventid)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                //$photo = $request;
                // $response = $this->image->uploadPosters($request->toArray(),$eventid);
                // return $response;

                return Plupload::receive('file', function  ($file) use($request,$id)
                {
                    $validate = Validator::make($request->except('_token'), [
                        'file' => 'dimensions:min_width=1920,min_height=445'
                    ]);
                    if ($validate->fails()) {
                        return 'failed, Your image must be 1920x445 and above in dimensions';
                    } else {
                        $filen = str_random(8) . '-' . $file->getClientOriginalName();
                        $upload = $file->move(public_path() . '/uploads/',$filen);
                        if ($upload ) {
                            $poster = new EventPosters();
                            //$poster->event_id = $id;
                            //$poster->image_path = '/uploads/'.$filen;
                            //$poster->created_by = auth()->user()->id;
                            $input = [];
                            $input['event_id'] = $id;
                            $input['image_path'] =  '/uploads/'.$filen;
                            $input['is_main'] = 0;
                            $input['created_by'] = auth()->user()->id;
                            $poster->fill($input)->save();
                            //$poster->save();
                            return 'OK';
                        }  else {
                            return 'failed';
                        }
                        return 'failed';
                    }
                    return 'failed';
                });
            }
            return redirect()->back()->withError('Invalid request');
        }
        return redirect()->back()->withError('Invalid request');
    }

    public function uploadEventImg (Request $request, $id) {
        return Plupload::receive('file', function  ($file) use($request,$id)
        {
            $validate = Validator::make($request->except('_token'), [
                'file' => 'dimensions:min_width=1920,min_height=445'
            ]);
            if ($validate->fails()) {
                return 'failed, Your image must be 1920x445 and above in dimensions';
            } else {
                $filen = str_random(8) . '-' . $file->getClientOriginalName();
                $upload = $file->move(public_path() . '/uploads/',$filen);
                if ($upload ) {
                    $poster = new EventPosters();
                    //$poster->event_id = $id;
                    //$poster->image_path = '/uploads/'.$filen;
                    //$poster->created_by = auth()->user()->id;
                    $input = [];
                    $input['event_id'] = $id;
                    $input['image_path'] =  '/uploads/'.$filen;
                    $input['is_main'] = 0;
                    $input['created_by'] = auth()->user()->id;
                    $poster->fill($input)->save();
                    //$poster->save();
                    return 'OK';
                }  else {
                    return 'failed';
                }
                return 'failed';
            }
            return 'failed';
        });
    }

    public function EventTicketBulkNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
//                    'delegate_id'        => 'required',
//                    'valid_from'       => 'required',
//                    'valid_to'       => 'required',
//                    'price'         => 'required',
//                    'notes' => 'required|min:50',
                    //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate)->withInput();
                } else {
                    foreach ($request->get('delegate_id') as $key => $ls ) {
                        $delegate = DelegatesType::where('id','=', $ls)->first();
                        $ticket_exists =  EventsTickets::where('delegate_id','=',$ls)->where('event_id','=',$id)->first();
                        if ( $ticket_exists ) {
                            return redirect()->back()->withError('Invalid request, the ticket '.$delegate->name. ' already exists for the this event');
                        }
                        else {
                            $ticket = new EventsTickets();
                            $input = [];
                            $input['valid_from'] = Carbon::parse($request->get('valid_from')[$key]);
                            $input['valid_to'] = Carbon::parse($request->get('valid_to')[$key]);
                            $input['notes'] = ($request->get('notes')[$key]);
                            $input['price'] = ($request->get('price')[$key]);
                            $input['name'] = $delegate->name;
                            $input['event_id'] = $id;
                            $input['delegate_id'] = $delegate->id;
                            $input['created_by'] = auth()->user()->id;
                            $ticket->fill($input)->save();
                            //return redirect()->back()->withSuccess('You created a new Event Ticket');
                        }
                    }
                    return redirect()->route('org.edit.event', [$id,'#tab_meta_tickets'])->withSuccess('Event tickets have been Updated.');//'#tab_meta_tickets'

                }
            }
            //return redirect()->back()->withError('Invalid request, use post to request');
            $tick_types_ids = EventsTickets::where('event_id','=', $id)->pluck('delegate_id');
            $delegate_types = DelegatesType::where('event_id','=', $id)->active()->whereNotIn('id',$tick_types_ids)->get();
            if ($delegate_types->count() <= 0 ) {
                return redirect()->back()->withInfo('All Delegate types have a matching ticket');
            }
            return view('organizer.event-ticket-bulk',compact('delegate_types','event'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventMetaNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
//                    'delegate_id'        => 'required',
//                    'valid_from'       => 'required',
//                    'valid_to'       => 'required',
//                    'price'         => 'required',
//                    'notes' => 'required',
                    //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                ]);
                if ($validate->fails()) {
                    //return redirect()->back()->withErrors($validate)->withInput();
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $ticket = new EventsMeta();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['event_id'] = $id;
                    $input['field_title'] = $request->get('field_name');
                    $ticket->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Meta.');//'#tab_meta'
                    //return redirect()->back()->withSuccess('You created a new Event Meta');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventActNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'delegate_id'        => 'required',
                    'valid_from'       => 'required',
                    'valid_to'       => 'required',
                    'name'       => 'required',
                    'location'       => 'required',
                    'cost'         => 'required',
                    'description' => 'required',//|min:50
                    //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $ticket = new EventActivity();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['valid_from'] = Carbon::parse($request->get('valid_from'));
                    $input['valid_to'] = Carbon::parse($request->get('valid_to'));
                    $input['event_id'] = $id;
                    $input['delegate'] =  implode (",", $request->get('delegate_id'));;
                    $ticket->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Activity.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventActEdit (Request $request, $id, $event_id) {
        $ticket = EventActivity::where('id','=',$id)->first();
        $event = Event::whereId($event_id)->first();
        if ($event && $ticket) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'delegate_id'        => 'required',
                    'valid_from'       => 'required',
                    'valid_to'       => 'required',
                    'name'       => 'required',
                    'location'       => 'required',
                    'cost'         => 'required',
                    'description' => 'required'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['valid_from'] = Carbon::parse($request->get('valid_from'));
                    $input['valid_to'] = Carbon::parse($request->get('valid_to'));
                    //$input['event_id'] = $id;
                    $input['delegate'] =  implode (",", $request->get('delegate_id'));;
                    $ticket->fill($input)->save();
                    return redirect()->back()->withSuccess('You edited a new Event Activity.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventTransNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'delegate_id'        => 'required',
                    'valid_from'       => 'required',
                    'valid_to'       => 'required',
                    'name'       => 'required',
                    'location'       => 'required',
                    'cost'         => 'required',
                    't_type'         => 'required',
                    't_mode'         =>'required',
                    'description' => 'required',//|min:50
                    //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $ticket = new EventTransport();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['valid_from'] = Carbon::parse($request->get('valid_from'));
                    $input['valid_to'] = Carbon::parse($request->get('valid_to'));
                    $input['event_id'] = $id;
                    $input['delegate'] =  implode (",", $request->get('delegate_id'));
                    $ticket->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Transfer package - '. $ticket->name);
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventTransEdit (Request $request, $id, $event_id) {
            $event = Event::whereId($event_id)->first();
            $pack = EventTransport::whereId($id)->first();
            if ($event && $pack) {
                if ($request->isMethod('post')) {
                    $validate = Validator::make($request->except('_token'), [
                        'delegate_id'        => 'required',
                        'valid_from'       => 'required',
                        'valid_to'       => 'required',
                        'name'       => 'required',
                        'location'       => 'required',
                        'cost'         => 'required',
                        't_type'         => 'required',
                        't_mode'         =>  'required',
                        'description' => 'required',//|min:50
                        //'file_attachment' => 'mimes:pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|max:5000'
                    ]);
                    if ($validate->fails()) {
                        return redirect()->back()
                            ->withError('The data you submitted has some errors, see highlighted in red .')
                            ->withErrors($validate)->withInput();
                    } else {
                        //$ticket = new EventTransport();
                        $input = $request->except('_token');
                        $input['created_by'] = auth()->user()->id;
                        $input['valid_from'] = Carbon::parse($request->get('valid_from'));
                        $input['valid_to'] = Carbon::parse($request->get('valid_to'));
                        $input['event_id'] = $event_id;
                        $input['delegate'] =  implode (",", $request->get('delegate_id'));
                        $pack->fill($input)->save();
                        return redirect()->back()->withSuccess('You edited a Event Transport.');
                    }
                }
                return redirect()->back()->withError('Invalid request, use post to request');
            }
            else {
                return redirect()->route('org.events')->withError('You have selected an Invalid Event');
            }
        }

    public function EventSponsorNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name'        => 'required',
                    'website'     => 'required',
                    'brief'       => 'required',
                    'file'        => 'mimes:png,jpg,bmp,jpeg',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $sponsor = new EventSponsor();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['event_id'] = $id;

                    //upload folder
                    if (!File::exists('uploads/logos')) {
                        File::makeDirectory('uploads/logos');
                    }
                    //upload
                    if (!is_null($request->file('file'))) {
                        $extension = $request->file('file')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999) . '.' . $extension;
                        $upload = $request->file('file')->move('uploads/logos/', $fileName);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['logo'] = 'uploads/logos/'.$fileName;
                        } else { $input['logo'] = null; }
                    }
                    $sponsor->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Sponsor.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventSponsorEdit (Request $request, $id, $event_id) {
        $event = Event::whereId($event_id)->first();
        $sponsor = EventSponsor::where('id','=',$id)->first();
        if ($event && $sponsor ) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name'        => 'required',
                    'website'     => 'required',
                    'brief'       => 'required',
                    'file'        => 'sometimes|nullable|mimes:png,jpg,bmp,jpeg',
                  //  max:5000|dimensions:min_width=200,min_height=200
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {

                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    //$input['event_id'] = $id;

                    //upload folder
                    if (!File::exists('uploads/logos')) {
                        File::makeDirectory('uploads/logos');
                    }
                    //upload
                    if (!is_null($request->file('file'))) {
                        $extension = $request->file('file')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999) . '.' . $extension;
                        $upload = $request->file('file')->move('uploads/logos/', $fileName);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['logo'] = 'uploads/logos/'.$fileName;
                        } //else { $input['logo'] = null; }
                    }
                    $sponsor->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Sponsor.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventOrganizerNew (Request $request, $id) {
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name'        => 'required',
                    'website'     => 'required',
                    'brief'       => 'required',
                    'file'        => 'mimes:png,jpg,bmp,jpeg',
                    //|max:5000|dimensions:min_width=227,min_height=227
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $ticket = new EventOrganizer();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['event_id'] = $id;

                    //upload folder
                    if (!File::exists('uploads/logos')) {
                        File::makeDirectory('uploads/logos');
                    }
                    //upload
                    if (!is_null($request->file('file'))) {
                        $extension = $request->file('file')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999) . '.' . $extension;
                        $upload = $request->file('file')->move('uploads/logos/', $fileName);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['logo'] = 'uploads/logos/'.$fileName;
                        } else { $input['logo'] = null; }
                    }
                    $ticket->fill($input)->save();
                    return redirect()->back()->withSuccess('You created a new Event Organizer.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventOrganizerEdit (Request $request, $id, $event_id) {
        $event = Event::whereId($event_id)->first();
        $org = EventOrganizer::whereId($id)->first();
        if ($event && $org) {
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'name'        => 'required',
                    'website'     => 'required',
                    'brief'       => 'required',
                    'file'        => 'sometimes|nullable|mimes:png,jpg,bmp,jpeg',
                    //|max:5000|dimensions:min_width=227,min_height=227
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    //$ticket = new EventOrganizer();
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    //$input['event_id'] = $id;

                    //upload folder
                    if (!File::exists('uploads/logos')) {
                        File::makeDirectory('uploads/logos');
                    }
                    //upload
                    if (!is_null($request->file('file'))) {
                        $extension = $request->file('file')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999) . '.' . $extension;
                        $upload = $request->file('file')->move('uploads/logos/', $fileName);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['logo'] = 'uploads/logos/'.$fileName;
                        } //else { $input['logo'] = null; }
                    }
                    $org->fill($input)->save();
                    return redirect()->back()->withSuccess('You edited an Event Organizer.');
                }
            }
            return redirect()->back()->withError('Invalid request, use post to request');
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function generateInvitationCode(){
        $code = 'SGS' . strtoupper(str_random('6'));
        return $code;
    }

    public function delegationPrint(Request $request , $id, $event_id){
        $delegation = Delegation::where('event_id','=', $event_id)
            ->where('id','=', $id)
            ->with('restrictions','invitations','invitations.delegateType',
                'invitations.delegatePrice','invitations.profile','countryname','event','delegationtype')
            ->first();
        if ($delegation){
            $document_name = $delegation->name.' Delegation';
            //$document_notes = $delegation->name.' Delegation';
            return view('print.print-delegations',compact('delegation','document_name'));
        }
        return redirect()->back()->withError('Invalid delegation was selected');
    }

    public function editEventDelegations(Request $request, $id, $event_id){
        $delegation = Delegation::where('id','=',$id)->where('event_id','=',$event_id)
            ->with('event')
            ->first();
        if ($delegation){
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'delegation_name' => 'required',
                    'delegation_type' => 'required',
                    'primary_name' => 'required',
                    'primary_email' => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $input = $request->except('_token');
                    $input['created_by'] = auth()->user()->id;
                    $input['name'] = $request->get('delegation_name');
                    $input['contact_name'] = $request->get('primary_name');
                    $input['contact_email'] = $request->get('primary_email');
                    $input['cc_contact_email'] = $request->get('cc_primary_email');
                    $input['type'] = $request->get('delegation_type');
                    $delegation->fill($input)->save();

                   $invitation = Invitations::where('delegation_id','=',$id)->where('event_id','=',$event_id)->first();
                   $input['delegate_id'] = $request->get('primary_badge');
                   $input['complimentary'] = $request->get('complimentary');
                   $invitation->fill($input)->save();
                    return redirect()->back()->withSuccess('you edited delegation - '. $delegation->name);
                }
            }
            return redirect()->back()->withError('invalid delegation id');
        }
        return redirect()->back()->withError('invalid delegation id');
    }

    public function ListEventsStatusFields (Request $request, $id, $event) {
        $event = Event::whereId($event)->first();
        $field = EventCustomFields::where('id','=',$id)->first();
        if ($event && $field) {
            if ($request->isMethod('post')) {
                if ($field->status == 0 ) {
                    $field->status = 1;
                }
                else {
                    $field->status = 0;
                }
                $field->save();
                return redirect()->back()->withSuceess('You edited the '.($field->status==0 ? 'enabled' : 'disabled' ).' Field');
            }
        }
        return redirect()->back()->withError('You selected an invalid Event');
    }

    public  function ListEventsFields (Request $request) {
        $list = EventCustomFields::query();
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                $lt = null;
                if ($list->status <> 0 ) {
                    $lt .= "<li><a href='#' data-url='" . route('org.event.dis.field', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to enable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Enable Field </a></li>";
                }
                else {
                    $lt .= "<li><a href='#' data-url='" . route('org.event.dis.field', [$list->id,$list->event_id]) . "' data-msg='Are you sure you want to disable this item, Action is Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\" > Disable Field </a></li>";
                }
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='".route('org.edit.field',[$list->id,$list->event_id])."'
                    data-name='".$list->field_title."'  data-desc='".htmlentities($list->field_description, ENT_QUOTES)."' data-delegate='".$list->delegate_type_id."'  data-toggle=\"modal\" data-target=\"#editCustomField\" > Manage Fields </a></li>
                    ".$lt."      </ul>
                </div>";
            })
            ->rawColumns(['tools','description'])
            ->make(true);
    }

    public function ListEventsEditFields(Request $request, $id ,$event ){
        $event = Event::whereId($event)->first();
        $field = EventCustomFields::where('id','=',$id)->first();

        if ($event && $field) {
            if ($request->isMethod('post')) {
                //dd($request);
                $validate = Validator::make($request->except('_token'), [
                    'name' => 'required',
                    'description' => 'required',
                    'delegate_id' => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $field->field_title = $request->get('name');
                    $field->field_description = $request->get('description');
                    $field->delegate_type_id = implode(',', $request->get('delegate_id'));
                    $field->save();
                    return redirect()->back()->withSuccess('You edited the selected Field');
                }
            }
        }
        return redirect()->back()->withError('You selected an invalid Event');
    }

    public function newEventFields(Request $request, $id ){
        $event = Event::whereId($id)->first();
        if ($event) {
            if ($request->isMethod('post')) {
                //dd($request);
                if ( count($request->get('group-b')) > 0 ) {
                    foreach ($request->get('group-b') as $key => $value) {
                        if ($value > 0) {$new_field = new EventCustomFields();
                            $new_field->event_id = $id  ;
                            $new_field->created_by = auth()->user()->id  ;
                            $new_field->field_title = $request->get('group-b')[$key]['name'];
                            $new_field->field_description = $request->get('group-b')[$key]['desc'];
                            $new_field->field_type = $request->get('group-b')[$key]['field_type'];
                            $new_field->delegate_type_id= implode(',', $request->get('group-b')[$key]['delegate_id']);
                            $new_field->save();
                        }
                    }
                    return redirect()->back()->withSuccess('You did added new fields to the event');
                }
                else {
                    return redirect()->back()->withError('You did not enter any new fields');
                }
            }
            //render view
            $delegate = DelegatesType::where('event_id','=',$id)->active()->get();
            return view('organizer.event-new-extra-fields', compact('event','delegate'));
        }
        return redirect()->back()->withError('Invalid event selected');
    }

    public function newEventDelegations(Request $request, $id ) {
        //dd($request);
        $event = Event::whereId($id)->first();
	    $titles = Titles::active()->get();
        if ($event) {
            if ($request->isMethod('post')) {
                //dd($request->get('complimentary'));
                $validate = Validator::make($request->except('_token'), [
                    //'delegation_name'     => 'required',
                    // 'country'             => 'required',
                    'delegation_type'     => 'required',
                    'primary_fname'        => 'required',
                    'primary_sname'        => 'required',
                    'primary_email'       => 'required',
                    'primary_badge'       => 'required',
                    //'primary_name'       => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    // $delegation_exists =  Delegation::where('country','=',$request->get('countryname'))->where('event_id','=',$id)->first();
                    // if ( $delegation_exists ) {
                    //     return redirect()->back()->withError('Invalid request, the delegation '.$delegation_exists->country. ' already exists for the this event');
                    // }
                    // else {
                        //$password = 'SGS' . strtoupper(str_random('4'));
                        $code = self::generateInvitationCode();//'SGS-' . strtoupper(str_random('6'));
                        if($request->get('delegation_type') == 6){
                           $tp = DelegatesType::where('id','=',$request->get('primary_badge'))->first();
                        }else{
                          $tp = DelegationsType::where('id','=',$request->get('delegation_type'))->first();
                        }
                        if(!empty($request->get('country'))){
                          $naming = $tp->name.' ('.$request->get('country').')';
                        } else{
                             $naming = $tp->name;

                        }

						//$ct
                        $delegation = new Delegation();
                        $input = $request->except('_token');
                        $input['created_by'] = auth()->user()->id;
                        $input['name'] = $naming; //$request->get('delegation_name');
                        $input['contact_name'] = ucfirst($request->get('primary_fname')).' '.ucfirst($request->get('primary_sname'));
                        $input['contact_email'] = $request->get('primary_email');
                        $input['cc_contact_email'] = $request->get('cc_primary_email');
                        $input['type'] = $request->get('delegation_type');
                        $input['event_id'] = $id;
                        $delegation->fill($input)->save();

                        //make primary user and profile
                        $primary_user = self::makeDelegateUser($request->get('primary_email'),['fname'=>ucfirst($request->get('primary_fname')),'sname'=>ucfirst($request->get('primary_sname'))],$code);
                        $primary_profile = self::makeDelegateProfile('na',$request->get('cc_primary_email'),$request->get('primary_email'),['fname'=>ucfirst($request->get('primary_fname')),'sname'=>ucfirst($request->get('primary_sname')),'language'=>ucfirst($request->get('primary_language'))],$code,$primary_user);
                        // makeInvitation($event,$data,$code,$profile,$user,$delegate_id,$delegation)
                        $primary_invitation = self::makeInvitation($event,$request,$code,$primary_profile,$primary_user,$request->get('primary_badge'),$delegation,$request->get('complimentary'));

                        //create invitations for other delegates
                        if ($request->get('delegation_type') == 2) {
                            //skip any other requests
                        }
                        else {
                            //loop through the other delegates
                            //dd($request->get('del_badge'));
                            if ( count($request->get('group-b')) > 0 ) {
                                foreach ($request->get('group-b') as $key => $value) {
                                    if ($value > 0 && $value['del_fname'] != '' && $value['del_sname'] != '') {
                                        $code = self::generateInvitationCode();
                                        //$nm = isset($request->get('group-b')[$key]['del_name']) ? $request->get('group-b')[$key]['del_name'] : 'delegate '.$key;
                                        $fnm = isset($request->get('group-b')[$key]['del_fname']) ? $request->get('group-b')[$key]['del_fname'] : 'delegate';
                                        $snm = isset($request->get('group-b')[$key]['del_sname']) ? $request->get('group-b')[$key]['del_sname'] : $key;
                                        $em = isset($request->get('group-b')[$key]['del_email']) ? $request->get('group-b')[$key]['del_email'] : 'delegate@sfg.org';
                                        $lg = isset($request->get('group-b')[$key]['language']) ? $request->get('group-b')[$key]['language'] : '';
                                        $cc_em = isset($request->get('group-b')[$key]['cc_del_email']) ? $request->get('group-b')[$key]['cc_del_email'] : 'delegate@sfg.org';
                                        $del_user = self::makeDelegateUser($em,['fname' => $fnm,'sname'=>$snm],$code);
                                        $del_profile = self::makeDelegateProfile('na',$cc_em,$em,['fname'=> $fnm, 'sname' => $snm,'language'=>ucfirst($request->get('primary_langauage')) ],$code,$del_user);
                                        $del_innvitation = self::makeInvitation($event,$request,$code,$del_profile,$primary_user,$request->get('group-b')[$key]['del_badge'],$delegation,$request->get('complimentary'));
                                    }
                                }
                            }
                        }
                        return redirect()->route('org.delegation.send',[$delegation->id])
                            ->withSuccess('You created a delegation '.$delegation->name. ', in this screen send out to the primary recipient');

                    //}
                    return redirect()->back()->withSuccess('You created a new Event Delegation.');
                }
            }
            //return redirect()->back()->withError('Invalid request, use post to request');
            $country = Countries::get();
            $delegate = DelegatesType::where('event_id','=',$id)->active()->get();
            $delegation_type = DelegationsType::active()->get();
            return view('organizer.event-new-delegation', compact('event','country','delegate','delegation_type','titles'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function EventDelegationView(Request $request, $id) {
        $delegation = Delegation::where('id','=', $id)->
        with('restrictions','invitations','invitations.profile','delegationtype','delegationtype.upload',
            'invitations.delegateType','countryname','event','primaryprofile')
            ->first();
          $emailtemplates = EmailTemplate::all();
        //dd($delegation);
        if ($delegation){
            $event = $delegation->event;
            //$code = $delegation->invitations-
             if ($request->isMethod('post')) {
              $inv = $delegation->invitations->pluck('delegateType');
                //dd($inv);
               $count =  $inv->groupBy('name')->map(function ($people) {
                     return $people->count();
                 });
              $event_id = $request->get('event_id');
              $invitations = Invitations::where('delegation_id','=', $id)->where('event_id','=',$event_id)
                  ->with('delegateType','profile','delegation','delegation.countryname.details')->get();

                  if($invitations){
                    if(!is_null($request->file('attachment'))){
                      foreach($request->file('attachment') as $attachment){
                      $file= $attachment;

                      $pdfname = $file->getClientOriginalName();
                      $pdf = $attachment->move('uploads/files/attachments/', $pdfname);
                      if ($pdf ) {
                          $attachments[]= $pdfname;
                      }
                    }
                    }

                     //var_dump($attachments);die;
                    foreach($invitations as $invitation){
                      $delegations = Delegation::where('id','=', $invitation->delegation_id)->
                      with('restrictions','invitations','invitations.profile','invitations.delegateType','countryname','event')->first();
                          $journeys = 'paul@farwell-consultants.com';
                          $name= $invitation->profile->fname.' '.$invitation->profile->surname;
                          $eventdate = date("d",strtotime($event->start)). '-'. date("d",strtotime($event->end)).' '.date("F",strtotime($event->end));
                          $invitation_information = '<ul><li>'.Lang::get("trans.Name").':'.$invitation->profile->fname.' '.$invitation->profile->surname.'</li>
                                                      <li>'.Lang::get("trans.unique registration code").': '. $invitation->code.'</li>
                                                      <li>'.Lang::get("trans.Username").': '. $invitation->profile->email.'</li>
                                                      <li>'.Lang::get("trans.Password").': '. $invitation->code.'</li>
                                                      <li>'.Lang::get("trans.Badge").': '. ucfirst($invitation->delegateType->color).'</li>
                                                      <li>'.Lang::get("trans.Category").': '.$invitation->delegateType->name.'</li><ul>';
                           $attendance_category = '<ul><li> '.$invitation->delegateType->name .' </li></ul>';
                           $link = '<a href="'.route('org.view.event',$event->slug).'" target="blank" style="font-size: 20px; font-family: "Lato-Bold"; color: #045061; text-decoration: none;  text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #ffd52d; display: inline-block;">Registration Website </a>';
                           $rawlink = '<a href="'.route('org.view.event',$event->slug).'" target="blank">'.route('org.view.event',$event->slug).'</a>';

                          $econtent = EmailTemplate::find($request->get('emailtemplate'));

                          $replace = array('{name}','{eventname}','{eventdate}','{eventlocation}','{eventlink}','{rawlink}','{attendance_category}','{invitation_information}');
                          $new = array($name, $event->name,$eventdate,$event->venue, $link,$rawlink, $attendance_category,$invitation_information );
                          $content = str_replace($replace, $new, $econtent->content);


                      // $data = ['delegations' => $delegations ,'event'=>$delegations->event,'invitation'=>$invitation->profile,'count'=>$count,'invitations'=>$invitation];
                        $data = ['logo'=> $event->logo,'content'=>$content,'eventname'=>$event->name];
                        if(empty($attachments)){
                          $attachments = '';
                        }else{
                          $attachments = $attachments;
                        }
                        if($request->get('emailtemplate') == 2){
                      Mail::send('email.bulk-user-team-many', $data, function ($message) use ($request,$delegations,$invitation,$journeys,$attachments,$econtent) {
                               $message->from('invites@wildlifeeconomy.com', $econtent->name);
                               $message->to($delegations->contact_email, $delegations->name );
                               $message->cc($delegations->cc_contact_email);
                              if (!empty($attachments)) {
                                foreach($attachments as $attachment){
                                   $message->attach(public_path().'/uploads/files/attachments/'.$attachment);
                                }

                             }
                            //   $message->cc('kangethepaul53@gmail.com');
                              //   if ($delegations->cc_contact_email && (filter_var($delegations->cc_contact_email,FILTER_VALIDATE_EMAIL) )) {
                              //   $message->cc($delegations->cc_contact_email,$delegations->contact_email);
                              // }
                                $message->subject(ucwords( strtolower($delegations->event->name) ) . ' Registration - Now Open');
                                foreach ( $delegations->delegationtype->upload as $list ) {
                                     if (\File::exists(public_path().'/'.$list->file)) {
                                        $message->attach(public_path().'/'.$list->file);
                                     }
                                }
                            }
                            );
                          }
                          elseif($request->get('emailtemplate') == 3){
                            Mail::send('email.contact', $data, function ($message) use ($request,$delegations,$invitation,$journeys,$attachments,$econtent) {
                                     $message->from('invites@wildlifeeconomy.com', $econtent->name);
                                     $message->to($delegations->contact_email, $delegations->name );
                                    if (!empty($attachments)) {
                                     $message->attach(public_path().'/uploads/files/attachments/'.$attachments);
                                   }
                                      $message->subject(ucwords( strtolower($delegations->event->name) ));
                                      foreach ( $delegations->delegationtype->upload as $list ) {
                                           if (\File::exists(public_path().'/'.$list->file)) {
                                              $message->attach(public_path().'/'.$list->file);
                                           }
                                      }
                                  }
                                  );


                          }
                    }

                  }


                 return redirect()->route('org.events.delegations',$event->id)->withSuccess('We have sent out the invitation emails');
            }
           return view('organizer.event-delegation-send',compact('event','delegation','emailtemplates'));
       }
        return redirect()->back()->withError('Invalid delegation selected');
    }


    public function EventDelegateSend(Request $request, $id) {
        $delegateType = DelegatesType::where('id','=', $id)->with('invitations','invitations.profile')->first();
        $emailtemplates = EmailTemplate::all();
        if ($delegateType){




          return view('organizer.delegate-send',compact('delegateType','emailtemplates'));
        }

        return redirect()->back()->withError('Invalid delegation selected');
    }







    public function EventDelegationConfirmation($id,$code){

    var_dump($code);die;
    }

    public function newDelegationDeposit (Request $request, $delegation_id,$event_id) {
        $delegation = Delegation::where('id','=',$delegation_id)->where('event_id','=',$event_id)
            ->with('restrictions','invitations','countryname','event','delegationtype')
            ->with('invitations.profile','invitations.delegateType','invitations.transfer','invitations.activity')
            ->with('invitations.payments')
            ->first();
        if ($delegation){
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'pay_date'   => 'required',
                    'amount'     => 'required',
                    'description' => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $over_payment = new Overpayment();
                    $over_payment->delegation_id =$delegation_id;
                    $over_payment->payment_id	=  0;
                    $over_payment->payment_date	= $request->get('pay_date');
                    $over_payment->amount	= $request->get('amount');
                    $over_payment->description	= $request->get('description');
                    $over_payment->status	= 0;
                    $over_payment->refund_id = 0;
                    $over_payment->created_by = 1;
                    $over_payment->overpayment_group = 1;
                    $over_payment->save();
                }
                return redirect()->back()->withSuccess('You have added an a new deposit to the '.$delegation->name. ' delegation');
            }
            return redirect()->back()->withError('You have sent an invalid request');
        }
        return redirect()->back()->withError('You have selected an invalid delegation');
    }

    public function newEventInvitationPayment (Request $request, $delegation_id, $event_id) {

        $delegation = Delegation::where('id','=',$delegation_id)->where('event_id','=',$event_id)
            ->with('restrictions','invitations','countryname','event','delegationtype')
            ->with('invitations.profile','invitations.delegateType','invitations.transfer','invitations.activity')
            ->with('invitations.payments')
            ->with('overpayments')
            ->first();
        if ($delegation){
            if ($request->isMethod('post')) {
                //dd($request);
                $new_del_payment = new Payments();
                $new_del_payment->payment_date = $request->get('payment_date');
                $new_del_payment->event_id = $event_id;
                $new_del_payment->invitation_id = '0';
                $new_del_payment->delegation_id = $delegation_id;
                $new_del_payment->payment_group = 1;
                $new_del_payment->pay_method = $request->get('payment_method');
                $new_del_payment->reciept_number = $request->get('reference_number');
                $new_del_payment->details = $request->get('description');
                $new_del_payment->amount = $request->get('payment_amount');
                $new_del_payment->created_by = 1;
                $new_del_payment->save();

                //insert overpayment
                if ($request->get('balance') > 0 ){
                    $over_payment = new Overpayment();
                    $over_payment->delegation_id =$delegation_id;
                    $over_payment->payment_id	= $new_del_payment->id;
                    $over_payment->amount	= $request->get('balance');
                    $over_payment->status	= 0;
                    $over_payment->refund_id = 0;
                    $over_payment->created_by = 1;
                    $over_payment->save();
                }

                //use prepaid account
                if ($request->get('prepaid_amount') > 0 ) {
                    $over_payment = new Overpayment();
                    $over_payment->delegation_id =$delegation_id;
                    $over_payment->payment_id	= $new_del_payment->id;
                    $over_payment->amount	= -$request->get('prepaid_amount');
                    $over_payment->status	= 0;
                    $over_payment->refund_id = 0;
                    $over_payment->created_by = 1;
                    $over_payment->save();
                }

                //update each invitation
                foreach ($request->get('code') as $key => $list) {
                    $new_del_payment = new Payments();
                    $new_del_payment->payment_date = $request->get('payment_date');
                    $new_del_payment->event_id = $event_id;
                    $new_del_payment->invitation_id = $request->get('invitation_id')[$key];
                    $new_del_payment->delegation_id = $delegation_id;
                    $new_del_payment->payment_group = 0;
                    $new_del_payment->parent_id = $new_del_payment->id;
                    $new_del_payment->pay_method = $request->get('payment_method');
                    $new_del_payment->reciept_number = $request->get('reference_number');
                    $new_del_payment->details = $request->get('description');
                    $new_del_payment->amount = $request->get('amount')[$key];
                    $new_del_payment->created_by = 1;
                    $new_del_payment->save();
                }
                //
                return redirect()->back()->withSuccess('You created a payment for delegation '.$delegation->name. ' with reference '.$request->get('reference_number'));
            }
            return view ('organizer.delegation-payment',compact('payment_method','delegation'));
        }

    }

    public function listEventDelegation (Request $request, $id=null) {
        if ($id == null) {
          $list = Delegation::with('restrictions','invitations','countryname','delegationtype','event');
              $datatables =  Datatables::of($list);
          if ($request->get('event_id')) {
              //$datatables->having('count', $request->get('operator'), $request->get('post')); // having count search
              $datatables->where('event_id', '=', $request->get('event_id'));
          }
        }
        else {
          $list = Delegation::where('event_id','=', $id)
              ->with('restrictions','invitations','countryname','event','delegationtype','invitations.profile');
              $datatables =  Datatables::of($list);
        }
        // <li><a href='".route('org.events.inv.resend',[listid,listevent_id])."'  > RSVP | Confirm for Event </a></li>
        return $datatables
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>

                    <li><a href='".route('org.delegation.send',[$list->id])."'  > Send Email </a></li>
                    <li><a href='#' data-url='" . route('org.events.delegations.edit', [$list->id,$list->event_id]) . "'
                            data-name='".$list->name."' data-country='".$list->country."' data-type='".$list->type."'
                            data-contact_email='".$list->contact_email."' data-cc_contact_email='".$list->cc_contact_email."' data-contact_name='".$list->contact_name."'
                            data-toggle=\"modal\" data-target=\"#editDelegation\"
                     > Edit </a></li>
                    <li><a href='#' data-url='" . route('org.events.delegations.status', [$list->id,$list->event_id]) . "'
                        class='confirm-this' data-msg='Are you sure you want to change status of this item, Action is Reversible ?'
                        data-toggle=\"modal\" data-target=\"#confirmModal\"
                     > Cancel | Activate </a></li>
                    <li><a href='" . route('org.events.inv.send', [$list->id,$list->event_id]) . "'  > Add Invitations </a></li>
                    <li><a href='" . route('org.events.inv.payment', [$list->id,$list->event_id]) . "'  > Manage Payments </a></li>
                    <li><a href='" . route('org.events.inv.print', [$list->id,$list->event_id]) . "' target='_blank' > Print Delegation </a></li>
                </ul>
                </div>";
            })
            ->addColumn('count_invitations', function ($list) {
                return $list->invitations->count();
            })
            ->addColumn('count_restrictions', function ($list) {
                return $list->restrictions->count();
            })
            ->rawColumns(['tools','description'])
            ->make(true);
    }

    public function listEventDelegationUploads (Request $request, $id) {
        $list = EventDelegationUploads::where('event_id','=',$id)->with('delegationtype');
        return Datatables::of($list)
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.edit.upload.delegation.event', $list->id) . "'
                    data-name='".$list->name."' data-desc='".htmlentities($list->description, ENT_QUOTES)."' data-delegation_id='".$list->delegation_id."'
                    data-oldfile='".$list->file."'  data-toggle=\"modal\" data-target=\"#editUploadDetails\"> Edit </a></li>
                    <li><a href='#' data-url='" . route('org.delete.upload.delegation.event', $list->id) . "'  class='confirm-this' data-msg='Are you sure you want to delete this item, Action is NOT Reversible ?'  data-toggle=\"modal\" data-target=\"#confirmModal\"> Delete </a></li>
                </ul>
                </div>";
            })
            ->rawColumns(['tools','description'])
            ->make(true);
    }

    public function listEventDelegationInvitations (Request $request, $id=null) {
             if($id == null){
                    $list = Invitations::with('event','delegateType','profile','delegation','delegation.countryname.details');
                   $datatables = Datatables::of($list);
                   if($request->get('event_id')){
                              // dd($request->get('event_id'));
                      $datatables->where('event_id', '=', $request->get('event_id'));
                    }
           }
           else{
             $list = Invitations::where('delegation_id','=', $id)
                    ->with('delegateType','profile','delegation','delegation.countryname.details') ;
                   $datatables = Datatables::of($list);
           }
        return $datatables
            //<li><a href='" . route('org.events.inv.edit.profile', [$list->id,$list->event_id]) . "'  > Edit Profile </a></li>
            //<li><a href='" . route('org.events.inv.edit.service', [$list->id,$list->event_id]) . "'  > Edit Services </a></li>
            //<li><a href='" . route('org.events.inv.manage.all', [$list->id,$list->event_id]) . "'  target='_blank' > View Invitation </a></li>
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.events.inv.status', [$list->id,$list->event_id]) . "'
                        class='confirm-this' data-msg='Are you sure you want to change status of this item, Action is Reversible ?'
                        data-toggle=\"modal\" data-target=\"#confirmModal\"
                     > Cancel | Activate </a></li>
                    <li><a href='" . route('org.events.inv.resend', [$list->id,$list->event_id]) . "'  > Resend Invitation </a></li>
                    <li><a href='" . route('org.events.inv.print.card', [$list->id,$list->event_id]) . "' target='_blank'  > Print Card </a></li>
                    <li><a href='" . route('org.events.inv.print.all', [$list->id,$list->event_id]) . "' target='_blank'   > View Details </a></li>
                    <li><a href='#' data-url='" . route('org.events.inv.delete', [$list->id,$list->event_id]) . "'
                        class='confirm-this' data-msg='Are you sure you want to DELETE invitation, Action is NOT Reversible ?'
                        data-toggle=\"modal\" data-target=\"#confirmModal\"
                     > Delete </a></li>
                    </ul>
                </div>";
            })
            ->addColumn('passport_photo', function ($list) {
                if ( File::exists($list->profile->passport_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' .url($list->profile->passport_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->passport_image) . '" /> </a>';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('id_photo', function ($list) {
                if ( File::exists($list->profile->national_id_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->profile->national_id_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->national_id_image) . '" /> </a> ';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->rawColumns(['tools','passport_photo','id_photo'])
            ->make(true);
    }

    public function SalesAccountsNewPayment (Request $request , $id, $event_id) {
        $list = Invitations::where('id','=',$id)
            ->with('payments','event','delegateType','profile','delegation','delegation.countryname.details')->first() ;
        if ($list){
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'pay_method'        => 'required',
                    'reciept_number'    => 'required',
                    'amount'            => 'required',
                    'details'       => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $payment = new Payments();
                    $input = $request->except('_token');
                    $input['invitation_id'] = $id;
                    $input['event_id'] = $event_id;
                    $input['created_by'] = auth()->user()->id;
                    $payment->fill($input)->save();
                    return redirect()->back()->withSuccess('You added a new payment');
                }
            }
            return redirect()->back()->withError('Invalid request or account is invalid');
        }
        return redirect()->back()->withError('Invalid request or account is invalid');
    }

    public function SalesAccountsNewRefund (Request $request , $id, $inv, $event_id) {
        $list = Invitations::where('id','=',$inv)
            ->with('payments','event','delegateType','profile','delegation','delegation.countryname.details')->first() ;
        if ($list){
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'pay_method'        => 'required',
                    'reciept_number'    => 'required',
                    'amount'            => 'required',
                    'details'       => 'required',
                ]);
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)->withInput();
                } else {
                    $payment = new Refunds();
                    $input = $request->except('_token');
                    $input['invitation_id'] = $inv;
                    $input['payout_method'] = $request->get('pay_method');
                    $input['event_id'] = $event_id;
                    $input['payment_id'] = $id;
                    $input['created_by'] = auth()->user()->id;
                    $payment->fill($input)->save();
                    return redirect()->back()->withSuccess('You added a new Refund');
                }
            }
            return redirect()->back()->withError('Invalid request or account is invalid');
        }
        return redirect()->back()->withError('Invalid request or account is invalid');
    }

    public function SalesAccountsPrintReceipt (Request $request, $id , $inv, $event_id) {
        $payment = Payments::where('id','=',$id)
            ->with('invitation','invitation.profile','invitation.delegateType','invitation.delegation','refunds','event')
            ->first();
        if ($payment){
            $document_name = 'Reciept - '.$payment->reciept_number;
            return view('print.print-receipt', compact('payment','document_name'));
        }
        return redirect()->back()->withError('invalid payment number or invalid request');
    }

    public function SalesAccountsSendReceipt (Request $request, $id , $inv, $event_id) {
        $payment = Payments::where('id','=',$id)
            ->with('invitation','invitation.profile','invitation.delegateType','invitation.delegation','refunds','event')
            ->first();
        if ($payment){
            $document_name = 'Reciept - '.$payment->reciept_number;
            //return view('print.print-receipt', compact('payment','document_name'));
            $data = [ 'payment' => $payment, 'document_name'=> $document_name];
            Mail::send('print.print-receipt', $data, function ($message) use ($request,$payment) {
                $message->from('invites@wildlifeeconomy.com', 'Space for Giants');
                $message->to($payment->invitation->profile->email, $payment->invitation->profile->fname .' '.$payment->invitation->profile->surname );
                //$message->cc($delegation->profile->email, $delegation->profile->name );
                $message->subject('Payment Receipt -- '.$payment->receipt_number);
            }
            );
            return redirect()->back()->withSuccess('We have sent receipt');
        }
        return redirect()->back()->withError('invalid payment number or invalid request');
    }

    public function SalesAccountsLists (Request $request, $id=null) {
        $list = Invitations::with('payments','refunds','event','delegateType','profile','delegation','delegation.countryname.details')   ;
        return Datatables::of($list)
            //<li><a href='" . route('org.events.inv.edit.profile', [$list->id,$list->event_id]) . "'  > Edit Profile </a></li>
            //<li><a href='" . route('org.events.inv.edit.service', [$list->id,$list->event_id]) . "'  > Edit Services </a></li>
            //<li><a href='" . route('org.events.inv.manage.all', [$list->id,$list->event_id]) . "'  target='_blank' > View Invitation </a></li>
            //<li><a href='#' data-url='" . route('org.sales.accounts.new.refund', [$list->id,$list->event_id]) . "'
            //  data-toggle=\"modal\" data-target=\"#addRefund\"
            //  > Add Refund </a></li>
            ->addColumn('tools', function ($list) {
                return "
                <div class='btn-group pull-right'>
                <button type='button' class='btn green btn-sm btn-outline dropdown-toggle' data-toggle='dropdown'
                aria-expanded='true'> Actions <i class='fa fa-angle-down'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li><a href='#' data-url='" . route('org.sales.accounts.new.payment', [$list->id,$list->event_id]) . "'
                        data-toggle=\"modal\" data-target=\"#addPayment\"
                     > Add Payment </a></li>
                     <li><a href='#' data-url='" . route('org.events.inv.status', [$list->id,$list->event_id]) . "'
                        class='confirm-this' data-msg='Are you sure you want to change status of this item, Action is Reversible ?'
                        data-toggle=\"modal\" data-target=\"#confirmModal\"
                     > Cancel | Activate </a></li>
                    <li><a href='" . route('org.events.inv.print.card', [$list->id,$list->event_id]) . "' target='_blank'  > Print Card </a></li>
                    <li><a href='" . route('org.events.inv.print.all', [$list->id,$list->event_id]) . "' target='_blank'   > Print All - Activity </a></li>
                    <li><a href='" . route('org.events.statement.print.all', [$list->id,$list->event_id]) . "' target='_blank'   > Print Statement </a></li>
                    </ul>
                </div>";
            })
            ->addColumn('passport_photo', function ($list) {
                if ( File::exists($list->profile->passport_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' .url($list->profile->passport_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->passport_image) . '" /> </a>';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->addColumn('id_photo', function ($list) {
                if ( File::exists($list->profile->national_id_image)) {
                    return '<a href="#" data-toggle="modal" data-target="#myImgModal" data-img="' . url($list->profile->national_id_image) . '">
                    <img style="width: 50px !important" src="' . url($list->profile->national_id_image) . '" /> </a> ';
                } else {
                    return '<img style="width: 50px !important" src="' . url('uploads/default-poster.jpg') . '" >';
                }
            })
            ->rawColumns(['tools','passport_photo','id_photo'])
            ->make(true);
    }

    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }

        return $randomString;
    }

    public function makeDelegateUser ($email,$details,$code){
        $user_exists = User::where('email','=',$email)->first();
        if ($user_exists) {
            //$user_exists->password = Hash::make($code);
            //$user_exists->save();
            $user = $user_exists;
        }
        else {
            $user = New User();
            $user->name = $details['fname'] .' '.$details['sname'] ;
            $user->email = $email;
            $user->correspondence_email = $email;
            $user->password =  bcrypt($code);
            $user->remember_token = self::generateRandomString(10);
            $user->current_team_id = 1;
            $user->save();

            $new_role = New RoleUsers();
            $new_role->role_id = 4;
            $new_role->user_id = $user->id;
            $new_role->save();
        }
        return $user;
    }

    public function makeDelegateProfile ($title='na',$cc_email,$email,$details,$code,$user){
        $delegate_profile_exists = DelegateProfile::where('email','=',$email)->first();
        if ($delegate_profile_exists) {
            $delegate_profile = $delegate_profile_exists;
        }
        else {
            //list($fname,$lname) = explode(' ',$details['name']);//array_pad(explode(' ',$details['name']),$details['name'],null);
            $delegate_profile = New DelegateProfile();
            //$delegate_profile->title = $request->get('title');
            $delegate_profile->fname = $details['fname'];
            $delegate_profile->surname = $details['sname'];
            $delegate_profile->language = $details['language'];
            $delegate_profile->created_by = auth()->user()->id;
            $delegate_profile->user_id = $user->id ;
            if ($title != 'na' || $title != '') {
	            $delegate_profile->title = $title ;
            }
            $delegate_profile->email = $user->email; //request->get('	email') ;
            $delegate_profile->cc_email = $cc_email; //request->get('	email') ;
            $delegate_profile->save();
        }
        return $delegate_profile;
    }

    public function makeInvitation($event,$data,$code,$profile,$user,$delegate_id,$delegation,$complimentary) {
        //dd($delegate_id);

        $invitation_exists = Invitations::where('delegation_id','=',$delegation->id)
            ->where('delegate_profile','=',$profile->id)->first();
        if ($invitation_exists){
            $invitation = $invitation_exists;
        }
        else {
            $invitation = New Invitations();
            $invitation->event_id = $event->id;
            $invitation->delegation_id = $delegation->id;
            $invitation->code = $code;
            $invitation->delegate_profile = $profile->id;
            $invitation->delegate_id = $delegate_id;
            $invitation->created_by = auth()->user()->id;
            $invitation->complimentary = $complimentary;

            $invitation->save();
        }
        return $invitation;
    }

    public function makeBulkInvitations($event,$delegation,$data) {
        $password = 'SGS' . strtoupper(str_random('4'));
        $code = 'SGS_' . strtoupper(str_random('6'));

        $primary_user = self::makeDelegateUser($data->get('primary_email'),['name'=> $data->get('primary_name')],$code);
        $primary_profile = self::makeDelegateProfile($data->get('primary_email'),['name'=> $data->get('primary_name')]);

        if ($data->get('type') == '1'){

        }
        else {

        }
    }

    public function newEventInvitationMake (Request $request, $id, $eventid) {
        $event = Event::where('Id','=',$eventid)->first();
        $delegation = Delegation::where('id','=', $id)->with('restrictions','invitations','countryname','event')->first();
        if ($delegation) {
            if ($request->isMethod('post')) {
                // //dd($request);
                $password = 'SGS' . strtoupper(str_random('4'));
                $code = 'SGS-' . strtoupper(str_random('6'));

                //create a user
                $user_exists = User::where('email','=',$request->get('email'))->first();
                if ($user_exists) {
                    $user = $user_exists;
                }
                else {
                    $user = New User();
                    $user->name = ucfirst($request->get('fname')).' '.ucfirst($request->get('surname'));
                    $user->email = ($request->get('email') == NULL ? $request->get('surname').'delegate@sfg.org' : $request->get('email'));
                    $user->password = Hash::make($code);
                    $user->remember_token = self::generateRandomString(10);
                    $user->current_team_id = 1;
                    $user->save();

                    $new_role = New RoleUsers();
                    $new_role->role_id = 4;
                    $new_role->user_id = $user->id;
                    $new_role->save();
                }

                //create a delegates profile
                $delegate_profile_exists = DelegateProfile::where('email','=',$request->get('email'))->first();
                if ($delegate_profile_exists) {
                    $delegate_profile = $delegate_profile_exists;
                }
                else {
                    $delegate_profile = New DelegateProfile();
                    $delegate_profile->title = ($request->get('title') == NULL ? 'na': $request->get('title'));
                    $delegate_profile->fname = ucfirst($request->get('fname'));
                    $delegate_profile->surname = ucfirst($request->get('surname'));
                    $delegate_profile->created_by = auth()->user()->id;
                    $delegate_profile->user_id = $user->id ;
                    $delegate_profile->email = $user->email; //request->get('	email') ;
                    $delegate_profile->cc_email = ($request->get('cc_email') == NULL ? 'na': $request->get('cc_email'));
                    $delegate_profile->save();
                }

                //creates invitation
                $invitation_exists = Invitations::where('delegation_id','=',$id)->where('delegate_profile','=',$delegate_profile->id)->first();
                if ($invitation_exists){
                    $invitation = $invitation_exists;
                }
                else {
                    $invitation = New Invitations();
                    $invitation->event_id = $eventid;
                    $invitation->delegation_id = $id;
                    $invitation->code = $code;
                    $invitation->delegate_profile = $delegate_profile->id;
                    $invitation->delegate_id = $request->get('delegate_id');
                    $invitation->created_by = auth()->user()->id;
                    $invitation->save();
                }

                //send emails
                if ($request->get('save') == 'save&send') {
                    //send invitation code
                    $inv = $delegation->invitations->pluck('delegateType');
                      //dd($inv);
                     $count =  $inv->groupBy('name')->map(function ($people) {
                           return $people->count();
                       });
                       if(!is_null($request->file('attachment'))){
                         foreach($request->file('attachment') as $attachment){
                         $file= $attachment;

                         $pdfname = $file->getClientOriginalName();
                         $pdf = $attachment->move('uploads/files/attachments/', $pdfname);
                         if ($pdf ) {
                             $attachments[]= $pdfname;
                         }
                         }
                       }
                       $name= $delegate_profile->fname.' '.$delegate_profile->surname;
                       $eventdate = date("d",strtotime($event->start)). '-'. date("d",strtotime($event->end)).' '.date("F",strtotime($event->end));
                       $invitation_information = '<ul><li>Name:'.$delegate_profile->fname.' '.$delegate_profile->surname.'</li>
                                                   <li>Unique registration code: '. $invitation->code.'</li>
                                                   <li>Username: '. $delegate_profile->email.'</li>
                                                   <li>Password: '. $invitation->code.'</li>
                                                   <li>Badge: '. ucfirst($invitation->delegateType->color).'</li>
                                                   <li>Category: '.$invitation->delegateType->name.'</li><ul>';
                        $attendance_category = '<ul><li> '.$invitation->delegateType->name .' </li></ul>';
                       $econtent = EmailTemplate::find($request->get('emailtemplate'));
                       $link = "<a href='{{ route('org.view.event',$event->slug)}}' target='_blank'style='font-size: 20px; font-family: 'Lato-Bold'; color: #045061; text-decoration: none;  text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #ffd52d; display: inline-block;'> Registration Website </a>";

                       $replace = array('{name}','{eventname}','{eventdate}','{eventlocation}','{eventlink}','{attendance_category}','{invitation_information}');
                       $new = array($name, $event->name,$eventdate,$event->venue, $link, $attendance_category,$invitation_information );

                       $content = str_replace($replace, $new, $content->content);

                       // $data = ['delegations' => $delegation ,'event'=>$delegation->event,'invitation'=>$invitation->profile,'count'=>$count,'invitations'=>$invitation];
                    $data = [
                        'logo' => $event->logo,
                        'content'=> $content,
                        'eventname'=>$event->name,
                     ];
                     if(empty($attachments)){
                       $attachments = '';
                     }else{
                       $attachments = $attachments;
                     }
                    Mail::send('email.bulk-user-team-one', $data, function ($message) use ($request,$delegation,$invitation,$delegate_profile, $attachments,$econtent) {
                             $message->from('invites@wildlifeeconomy.com', $econtent->name);
                             $words = explode('@',$delegate_profile->email);
                            $e =implode(' ',array_slice($words,1));
                           if($e ==='sfg.org'){
                             if($delegate_profile->cc_email && (filter_var($delegate_profile->cc_email,FILTER_VALIDATE_EMAIL))){
                              $message->to($delegate_profile->cc_email, $delegation->name );
                            }

                          }elseif($e !='sfg.org'){
                             if($delegate_profile->email && (filter_var($delegate_profile->email,FILTER_VALIDATE_EMAIL))){
                            $message->to($delegate_profile->email, $delegation->name );
                               }
                           }
                           else{
                              $message->to($delegation->contact_email, $delegation->name );
                           }
                            // $message->cc('invites@wildlifeeconomy.com');
                            //   if ($delegation->cc_contact_email && (filter_var($delegation->cc_contact_email,FILTER_VALIDATE_EMAIL) )) {
                            //   $message->cc($delegation->cc_contact_email,$delegation->contact_email);
                            // }
                            if (!empty($attachments)) {
                              foreach($attachments as $attachment){
                                 $message->attach(public_path().'/uploads/files/attachments/'.$attachment);
                              }
                            }
                              $message->subject(ucwords( strtolower($delegation->event->name) ) . ' Registration - Now Open');
                              foreach ( $delegation->delegationtype->upload as $list ) {
                                   if (\File::exists(public_path().'/'.$list->file)) {
                                      $message->attach(public_path().'/'.$list->file);
                                   }
                              }
                          }
                          );


                }
                 return redirect()->back()->withSuccess('You have created a new invitation for a delegate - '. ucfirst($request->get('fname')).' '.ucfirst($request->get('surname')));
            }
            $delegate_types = DelegatesType::where('event_id','=',$eventid)->active()->get(); //dd($delegate_types);
            $titles = Titles::all();
            $emailtemplates = EmailTemplate::all();
            return view('organizer.event-new-invitation', compact('event','delegation','delegate_types','id','eventid','titles','emailtemplates'));
        }
        else {
            return redirect()->route('org.events')->withError('You have selected an Invalid Event');
        }
    }

    public function invitationStatusChange (Request $request, $id, $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegation','delegation.countryname.details')->first();
         if ($invitation){
                $invitation->status = $invitation->status == 0 ? 1:0;
                $invitation->save();
                return redirect()->back()->withSuccess('You '.($invitation->status == 0 ? 'Enabled' : 'Canceled' ).' the selected the Invitation');
         }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function EventInvitationResend (Request $request, $id, $event_id) {
      // var_dump($id);
      // var_dump($event_id);
         $invitations = Invitations::where('delegation_id','=', $id)->where('event_id','=',$event_id)
             ->with('delegateType','profile','delegation','delegation.countryname.details')->get();
             // var_dump($invitation);
              if($invitations){
                foreach($invitations as $invitation){
                    // var_dump($invitation->profile->email);
                    $delegations = Delegation::where('id','=', $invitation->delegation_id)->
                    with('restrictions','invitations','invitations.profile','invitations.delegateType','countryname','event')->get();
                    foreach($delegations as $delegation){
                      $data = [ 'delegation' => $delegation , 'email' => $invitation->profile->email ];
                      $email = $invitation->profile->email;

                      Mail::send('email.invitation', $data, function ($message) use ($request,$delegation,$email) {
                          $message->from('invites@wildlifeeconomy.com', 'Space for Giants');
                          $message->to($email, $delegation->name );
                           // $message->cc('invites@wildlifeeconomy.com' );
                          $message->subject( ucwords('Invitation To'.strtolower( $delegation->event->name) ));
                          // foreach ( $delegation->type->upload as $list ) {
                          //     if (\File::exists(public_path().'/'.$list->file)) {
                          //         $message->attach(public_path().'/'.$list->file);
                          //     }
                          // }
                      }
                      );

                    }
                     return redirect()->back()->withSuccess('We have resent the invitation information to the hand of the delegation');
                }

              }else{
                  return redirect()->back()->withError('Invalid invitation was selected');
              }
        // if ($invitation){
        //     if ($request->isMethod('post') ) {
        //         $delegation = Delegation::where('id','=', $invitation->delegation_id)->
        //         with('restrictions','invitations','invitations.profile','type','type.upload',
        //             'invitations.delegateType','invitations.delegateType.upload','countryname','event')
        //             ->first();
        //         $data = [ 'delegation' => $delegation , 'email' => $invitation->profile->email ];
        //         Mail::send('email.bulk-user-acc-one', $data, function ($message) use ($request,$delegation) {
        //             $message->from('summit@thegiantsclub.org', 'Space for Giants Events');
        //             $message->to($delegation->contact_email, $delegation->name );
        //             $message->cc($delegation->profile->email, $delegation->profile->name );
        //             $message->subject( ucwords(strtolower( $delegation->event->name) ). ' Registration - Now Open');
        //             foreach ( $delegation->type->upload as $list ) {
        //                 if (\File::exists(public_path().'/'.$list->file)) {
        //                     $message->attach(public_path().'/'.$list->file);
        //                 }
        //             }
        //         }
        //         );
        //         return redirect()->back()->withSuccess('We have resent the invitation information to the hand of the delegation');
        //     }
        // }
        // return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function EventInvitationPrintCard (Request $request, $id, $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegation','delegation.countryname.details')->first();
        if ($invitation){
                $delegation = Delegation::where('id','=', $invitation->delegation_id)->
                with('restrictions','invitations','invitations.profile','delegationtype',
                    'invitations.delegateType','countryname','event')
                    ->first();
                $document_name = $invitation->delegateType->name;
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->AddPageByArray(array(
                    'orientation' => 'P',
                    'condition' => '',
                    'resetpagenum' => '',
                    'pagenumstyle' => '',
                    'suppress' => '',
                    'mgl' => '2',
                    'mgr' => '2',
                    'mgt' => '2',
                    'mgb' => '2',
                    'mgh' => '2',
                    'mgf' => '2',
                    'ohname' => '',
                    'ehname' => '',
                    'ofname' => '',
                    'efname' => '',
                    'ohvalue' => 0,
                    'ehvalue' => 0,
                    'ofvalue' => 0,
                    'efvalue' => 0,
                    'pagesel' => '',
                    'newformat' => array(152.4,101.6),
                ));
                return view('print.print-invite-card',compact('invitation','document_name'));
                $mpdf->WriteHTML( View::make('print.print-invite-card', ['invitation'=>$invitation,'document_name' => $document_name]));
                $mpdf->Output();

        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function EventInvitationPrintAll (Request $request, $id, $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegatePrice','delegation','delegation.countryname.details')
            ->with('hotel','hotel.hotel')
            ->with('transfer','transfer.package')
            ->with('activity','activity.activity')->
            first();
        //dd($invitation);
        if ($invitation){
                $delegation = Delegation::where('id','=', $invitation->delegation_id)->
                with('restrictions','invitations','invitations.profile','delegationtype',
                    'invitations.delegateType','countryname','event')
                    ->first();
            $document_notes = null;
            $document_name = 'Summary of Invitation';
            $document_notes .= '<strong> Technical Requirements</strong>' .'</br>'. $invitation->tech_requirements .'</br>';
            $document_notes .= '<strong> Food Requirements</strong>' .'</br>'. $invitation->food_requirements .'</br>';
            // $document_notes .= '<strong> Vehicle Pass </strong>' .'</br>'. $invitation->vehicle_pass .'</br>';
            // $document_notes .= '<strong> Vehicle Booked </strong>' .'</br>'. $invitation->vehicle_details .'</br>';
            return view('print.print-invite-all', compact('delegation','invitation','document_notes','document_name'));
        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function EventInvitationPrintStatement (Request $request, $id, $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegatePrice','delegation','delegation.countryname.details')
            ->with('hotel','hotel.hotel')
            ->with('transfer','transfer.package')
            ->with('activity','activity.activity')->
            first();
        //dd($invitation);
        if ($invitation){
                $delegation = Delegation::where('id','=', $invitation->delegation_id)->
                with('restrictions','invitations','invitations.profile','delegationtype',
                    'invitations.delegateType','countryname','event')
                    ->first();
            $document_notes = null;
            $document_name = 'Delegate Statement';
            return view('print.print-statement', compact('delegation','invitation','document_notes','document_name'));
        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function invitationEditProfile (Request $request, $id , $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegation','delegation.countryname.details')->first();
        if ($invitation){
            if ($request->isMethod('post') ) {
                $validate = Validator::make($request->except('_token'), [

                    ]
                );
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)
                        ->withInput();
                } else {

                }
            }
            return view('organizer.event-invitation-edit-profile', compact('invitation'));
        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

    public function invitationEditService (Request $request, $id , $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegation','delegation.countryname.details')->first();
        if ($invitation){
            if ($request->isMethod('post') ) {
                $validate = Validator::make($request->except('_token'), [

                    ]
                );
                if ($validate->fails()) {
                    return redirect()->back()
                        ->withError('The data you submitted has some errors, see highlighted in red .')
                        ->withErrors($validate)
                        ->withInput();
                } else {

                }
            }
            return view('organizer.event-invitation-edit-service', compact('invitation'));
        }
        return redirect()->back()->withError('Invalid invitation was selected');
    }

}
