<?php

namespace App\Http\Controllers;

use App\Models\CountriesAdvanced;
use App\Models\FlightAircrafts;
use App\Models\FlightAirlines;
use App\Models\FlightAirports;
use App\Models\FlightRoutes;
use App\Models\InvitationCustomData;
use App\Models\Titles;
use Closure;
//use function redirect;
use Yajra\Datatables\Datatables;
use Hash;
//use Auth;
use Validator;
use Mail;
use Log;
use Session;
use Input;
use File;
use Image;
use GuzzleHttp\Client;
use DB;
use Config;
use mPDF;
use View;
use PDF;
use Carbon\Carbon;
use Plupload;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Logic\Image\ImageRepository;
use Illuminate\Support\Facades\Auth;
use Response;

use app\Logic\NthNumber;
use App\Models\DelegateProfile;
use App\Models\Delegation;
use App\Models\DelegationMeta;
use App\Models\EventNews;
use App\Models\Invitations;
use App\User;
use Lang;


use App\Models\Event;
use App\Models\EventsMeta;
use App\Models\EventsMetaData;
use App\Models\EventsTickets;
use App\Models\DelegatesType;
use App\Models\EventHotelPivot;
use App\Models\EventsMap;
use App\Models\EventProgramme;
use App\Models\SysImages;
use App\Models\EventTransport;
use App\Models\EventActivity;
use App\Models\EventHotel;
use App\Models\EventOrganizer;
use App\Models\EventPosters;
use App\Models\EventSponsor;
use App\Models\Hotel;
use App\Models\Countries;
use App\Models\EventHotelDelegatePivot;
use App\Models\InvitationAccomodation;
use App\Models\InvitationActivity;
use App\Models\InvitationTransferPackages;
use App\Models\InvitationTransportReturn;
use App\Models\InvitationTentAccomodation;
use App\Models\InvitationFlightOut;
use App\Models\InvitationFlightIn;
use App\Models\Payments;
use App\Models\PaymentMethods;
use App\Models\Overpayment;
use App\Team;
use App\Models\TermsConditions;
use App\Models\EventDisclaimer;
use App\Models\Tent;
use App\Models\BookingForm;
use App\Models\EventMedia;
//use App\Models\Countries
//use App\Logic\NthNumber;

class DelegateController extends Controller
{
    protected $image;
    protected $nthnumber;
    private $notices;
    private $countries;
    protected $code;
    protected $user;
    protected $biz;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) use($imageRepository) {
            if(Auth::user()) {
                //dd(Auth::user());
                //View::share ( 'delegatecode', Auth::user()->code );
                $this->countries = Countries::with('details')->get();
                $this->biz = Team::where('id','=',1)->first();
                View::share ( 'inviteCode', Auth::user()->code );
                View::share ( 'code', Auth::user()->code );
                View::share ( 'countries', $this->countries );
                View::share ( 'business', $this->biz );
                $this->image = $imageRepository;
            }
            return $next($request);
        });
    }

    public function index() {
        return view('delegate.index');
    }

    public function logout(Request $request, $code=null, $event_id) {
        if (is_int($event_id)) {
            $event = Event::where('id','=',$event_id)->with('poster','sponsor','organizer','meta')->first();
        }
        else {
            $event = Event::where('slug','=',$event_id)->with('poster','sponsor','organizer','meta')->first();
        }
        Auth::logout();
        return redirect()->route('org.view.event',$event->slug)->withSuccess(Lang::get('trans.You have Logged Out'));

    }


    public function EventInvitationPrintAll (Request $request, $id, $event_id) {
        $invitation = Invitations::where('id','=', $id)->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegatePrice','delegation','delegation.countryname.details')
            ->with('hotel','hotel.hotel')
            ->with('transfer','transfer.package')
            ->with('activity','activity.activity')->
            first();
        //dd($invitation);
        if ($invitation){
            $delegation = Delegation::where('id','=', $invitation->delegation_id)->
            with('restrictions','invitations','invitations.profile','delegationtype',
                'invitations.delegateType','countryname','event')
                ->first();
            $document_notes = null;
            $document_name = 'Summary of Invitation';
            $document_notes .= '<strong> Technical Requirements</strong>' .'</br>'. $invitation->tech_requirements .'</br>';
            $document_notes .= '<strong> Food Requirements</strong>' .'</br>'. $invitation->food_requirements .'</br>';
            // $document_notes .= '<strong> Vehicle Pass </strong>' .'</br>'. $invitation->vehicle_pass .'</br>';
            // $document_notes .= '<strong> Vehicle Booked </strong>' .'</br>'. $invitation->vehicle_details .'</br>';
            return view('print.print-invite-all', compact('delegation','invitation','document_notes','document_name'));
        }
        return redirect()->back()->withError(Lang::get('trans.Invalid invitation was selected'));
    }

    public function SalesAccountsPrintReceipt (Request $request, $id ) {
        $invitation = Invitations::where('id','=', $id) //->where('event_id','=',$event_id)
            ->with('delegateType','profile','delegatePrice','delegation','delegation.countryname.details')
            ->with('hotel','hotel.hotel')
            ->with('transfer','transfer.package')
            ->with('activity','activity.activity')->
            first();
        $payment = Payments::where('invitation_id','=',$id)
            ->with('invitation','invitation.profile','invitation.delegateType','invitation.delegation','refunds','event')
            ->get();
        if ($payment){
            $document_name = 'All Payments';//.$payment->reciept_number;
            return view('print.print-receipt-many', compact('payment','document_name','invitation'));
        }
        return redirect()->back()->withError(Lang::get('trans.Invalid payment number or invalid request'));
    }

    public function getCustomFields ($request, $the_event)
    {
        $validate_items = [];
        $selected_fields = $request->except('_token');
        foreach ($selected_fields as $key => $value) {
            $single_item = explode('custom-', $key);
            if (count($single_item) > 1) {
                if ($value != '') {
                    $field_id = $single_item[1];
                    $validate_items [] = [ 'field_id' => $field_id, 'field_value' => $value ];
                }
            }
        }
        //Log::alert($validate_items);
        return collect($validate_items);
    }

    public function autocompleteAirports (Request $request) {
        $query = $request->get('term','');
        $products = FlightAirports::where('name','LIKE','%'.$query.'%')->get();
        $data = array();
        foreach ($products as $product) {
            $data[]=array('value' => $product->name ,'id'=> $product->code );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function autocompleteAircrafts (Request $request) {
        $query = $request->get('term','');
        $products = FlightAircrafts::where('name','LIKE','%'.$query.'%')->get();
        $data = array();
        foreach ($products as $product) {
            $data[]=array('value' => $product->name ,'id'=> $product->code );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function autocompleteAirlines (Request $request) {
        $query = $request->get('term','');
        $products = FlightAirlines::where('name','LIKE','%'.$query.'%')->get();
        $data = array();
        foreach ($products as $product) {
            $data[]=array('value' => $product->name ,'id'=> $product->code );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function autocompleteFlightNumber (Request $request) {
      // dd($request->get('term',''));
        $query = $request->get('term','');
        $products = FlightRoutes::where('flight_number','LIKE','%'.$query.'%')->get();
        $data = array();
        foreach ($products as $product) {
            $data[]=array('value' => $product->flight_number ,'id'=> $product->id );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function ManageHotelInvitation (Request $request, $code, $eventid) {
        if (is_int($eventid)) {
            $event = Event::where('id','=',$eventid)->with('poster','sponsor','organizer','meta','fields')->first();
        }
        else {
            $event = Event::where('slug','=',$eventid)->with('poster','sponsor','organizer','meta','fields')->first();
        }

        $invitation = Invitations::where('code','=',$code)
            ->with('delegateType','delegatePrice','hotel','transfer')
            ->with('activity','delegation','inflight','outflight','payments','refunds')
            ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
            ->first();

        if ($request->isMethod('post')) {
            $inv_hotel = InvitationAccomodation::updateOrCreate (
                [
                    'invitation_id' => $invitation->id,
                    //'hotel_id' => $request->get('hotel'),
                ],
                [
                    'invitation_id' => $invitation->id,
                    'hotel_id' => $request->get('hotel'),
                    'name' => $request->get('h_name','N/A'),
                    'arrival_date' =>  Carbon::parse($request->get('h_arrival')),
                    'departure_date' =>  Carbon::parse($request->get('h_departure')),
                    'created_by' => auth()->user()->id
                ]
            );
            return redirect()->back()->withSuccess ('You selected a hotel you will be staying at ');
        }
        return redirect()->back()->withError ('invalid request');
    }

    public function ManageInvitation ( Request $request, $code, $eventid)
    {
        if (is_int($eventid)) {
            $event = Event::where('id','=',$eventid)->with('poster','sponsor','organizer','meta','fields')->first();
        }
        else {
            $event = Event::where('slug','=',$eventid)->with('poster','sponsor','organizer','meta','fields')->first();
        }

        $invitation = Invitations::where('code','=',$code)
            ->with('delegateType','delegatePrice','hotel','transfer','profile')
            ->with('activity','delegation','inflight','outflight','payments','refunds')
            ->with('event','customdata','user','hotel.hotel','hotel.otherhotel')
            ->first();

        $titles = Titles::active()->pluck('name');

        //$delegate_profile = DelegateProfile::where('id','=',$invitation->delegate_profile)->first();
        $delegate_types = DelegatesType::where('id','=',$invitation->delegate_id)->first();

        $hotel = EventHotelDelegatePivot::
            where('event_id','=',$event->id)
            ->where('delegate_id','=',$invitation->delegate_id)
            ->with('hotel','hotel.images')
            //->orderBy('hotel.order_custom')
            ->get();
        //dd($hotel);

        $activities = EventActivity::whereRaw("find_in_set('".$invitation->delegate_id."',event_activities.delegate)") //DB::table("event_activities")
            ->where('event_id','=',$event->id )
            ->with('images')
            ->get();

        $tpackages = DB::table("event_transport")
            ->whereRaw("find_in_set('".$invitation->delegate_id."',event_transport.delegate)")
            ->get();     //EventTransport::where('event_id','=',$event->id)->get();

        $codes = DB::table("countries_advanced")->pluck('id');
        //dd($invitation->activity);
        $tconditions = TermsConditions::where('event_id','=',$event->id)->get();
        $edisclaimer = EventDisclaimer::where('event_id','=',$event->id)->get();
        $emedia = EventMedia::where('event_id','=',$event->id)->get();
        $tents = Tent::where('event_id','=',$event->id)->get();
        $bookingForm = BookingForm::where('event_id','=',$event->id)->first();


        if ($request->isMethod('post')) {

        	//dd($request);

            //dd($request);
            $validate = Validator::make($request->except('_token'), [
                //'my_profile_pic' => 'dimensions:min_width=600,min_height=600',
                //'national_id_image' => 'dimensions:min_width=600,min_height=600'
            ]);
            if ($validate->fails()) {
                return redirect()->back()->withErrors($validate)->withInput();
            } else {

                //dd($request);

                $input = $request->except('_token');

                $settings = Config::get('settings.upload_folders');
                $folder = $settings['profile'];
                $folder2 = $settings['docs'];

                $custom_fields = self::getCustomFields($request, $event);

                if ($custom_fields->isEmpty()) {  }
                else {
                    foreach ($custom_fields as $ls) {
                        $custom_data = InvitationCustomData::updateOrCreate(
                            [
                                'invitation_id' => $invitation->id,
                                'delegate_id' => $invitation->profile->id,
                                'field_id' => $ls['field_id']
                            ],
                            [
                                'invitation_id' =>$invitation->id,
                                'delegate_id' => $invitation->profile->id,
                                'field_id' => $ls['field_id'],
                                'value' => $ls['field_value'],
                                'created_by' => auth()->user()->id
                            ]
                        );
                    }
                }

                if (!File::exists($folder)) {
                    File::makeDirectory($folder);
                }
                if (!File::exists($folder2)) {
                    File::makeDirectory($folder2);
                }

                // upload profile picture
                if (!is_null($request->file('my_profile_pic'))) {
                    $extension = $request->file('my_profile_pic')->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'.' . $extension;
                    $originalName = $fileName;//$request->file('file_attachment')->getClientOriginalName();
                    //$extension = $request->file('file_attachment')->getClientOriginalExtension();
                    $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
                    $filename = $this->image->sanitize($originalNameWithoutExt);
                    $allowed_filename = $this->image->createUniqueFilename( $filename, $extension );
                    //dd($allowed_filename);
                    $upload = $request->file('my_profile_pic')->move('uploads/profile/', $allowed_filename);
                    //$image->save($folder.'/'.$fileName); //
                    //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                    //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                    if ($upload) {
                        //$input['my_profile_pic'] = 'uploads/profile/'.$allowed_filename;
                        $invitation->profile->passport_image = 'uploads/profile/'.$allowed_filename;//$input['national_id_image']; //$request->get('national_id_image');
                    } //else { $input['my_profile_pic'] = null; }
                }
                //upload national id
                if (!is_null($request->file('national_id_image'))) {
                    //$extension = $request->file('national_id_image')->getClientOriginalExtension();
                    $extension = $request->file('national_id_image')->getClientOriginalExtension();
                    //$fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'.' . $extension;
                    $fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'-'.$request->get('national_id').'.' . $extension;
                    $originalName = $fileName;//$request->file('file_attachment')->getClientOriginalName();
                    //$extension = $request->file('file_attachment')->getClientOriginalExtension();
                    $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
                    $filename = $this->image->sanitize($originalNameWithoutExt);
                    $allowed_filename = $this->image->createUniqueFilename( $filename, $extension );

                    $upload = $request->file('national_id_image')->move('uploads/docs/', $allowed_filename);//$image->save($folder.'/'.$fileName); //
                    //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                    //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                    if ($upload) {
                        //$input['national_id_image'] = 'uploads/docs/'.$allowed_filename;
                        $invitation->profile->national_id_image = 'uploads/docs/'.$allowed_filename;//$input['my_profile_pic']; //$request->get('national_id_image');
                    } //else { $input['national_id_image'] = null; }
                }

                if ($request->get('nationality')){
	                $selected_country = CountriesAdvanced::where('demonym','=',$request->get('nationality'))->first();
                }
                 if($request->get('fname') || $request->get('surname')){

                   $user = User::where('id','=',$invitation->profile->user_id)->first();
                   $user->name = ucfirst($request->get('fname')).' '.ucfirst($request->get('surname'));
                   $user->save();
                 }

                //update profile
                $invitation->profile->organisation = $request->get('organisation');
                $invitation->profile->fname = ucfirst($request->get('fname'));
                $invitation->profile->surname = ucfirst($request->get('surname'));
                $invitation->profile->position = $request->get('position');
                $invitation->profile->country = $request->get('nationality') ? $selected_country->name : $invitation->delegation->countryname->name;//$invitation->delegation->countryname->name;//$request->get('country');
                $invitation->profile->nationality = $request->get('nationality',$invitation->delegation->countryname->details->demonym ) ; //or $invitation->delegation->countryname->details->name;
                $invitation->profile->national_id = $request->get('national_id');
                $invitation->profile->national_id_type = $request->get('national_id_type');
                $invitation->profile->residential = $request->get('residential');
                $invitation->profile->date_issue = $request->get('d_issue');
                $invitation->profile->date_expire = $request->get('d_expire');
//                if($input['national_id_image']) {
//                    $invitation->profile->national_id_image = $input['national_id_image']; //$request->get('national_id_image');
//                }
//                if ($input['my_profile_pic']) {
//                    $invitation->profile->passport_image = $input['my_profile_pic']; //$request->get('national_id_image');
//                }
                $invitation->profile->cell_phone = $request->get('cell_phone');//$request->get('cell_phone_code').'-'.
                $invitation->profile->cell_office = $request->get('office_phone');//$request->get('office_phone_code').'-'.
                $invitation->profile->cell_phone_code = $request->get('cell_phone_code');
                $invitation->profile->office_phone_code = $request->get('office_phone_code');
                $invitation->profile->title = $request->get('title');

                $invitation->profile->a_contact_name = $request->get('a_contact_name');
                $invitation->profile->a_contact_email = $request->get('a_contact_email');
                $invitation->profile->a_contact_phone_code = $request->get('a_contact_phone_code');
                $invitation->profile->a_contact_phone = $request->get('a_contact_phone');
                $invitation->profile->a_contact_office_phone = $request->get('a_contact_office_phone');

                $invitation->profile->membership_number = $request->get('membership_number');
                $invitation->profile->membership_email =  $request->get('membership_email');
                $invitation->profile->membership_phone_code =  $request->get('membership_phone_code');
                $invitation->profile->membership_telephone =  $request->get('membership_telephone');
                $invitation->profile->media =  $request->get('media');
                $invitation->profile->interview =  $request->get('interview');
                $invitation->profile->insurance_provider =  $request->get('insurance_provider');
                $invitation->profile->medical_conditions =  $request->get('medical_conditions');


                //$delegate_profile->tech_requirements = $request->get('tech_requirements');
                //$delegate_profile->food_requirements = $request->get('food_requirements');
                //$delegate_profile->vehicle_pass = $request->get('vehicle_pass');
                //$delegate_profile->vehicle_detials = $request->get('vehicle_registration');

                $invitation->profile->save();

                //update invitation
                $invitation->tech_requirements = $request->get('tech_requirements','N/A');
                $invitation->food_requirements = $request->get('food_requirements','N/A');
                $invitation->vehicle_pass = $request->get('vehicle_pass','No');
                $invitation->vehicle_details = $request->get('vehicle_registration','N/A');
                $invitation->flight_type = $request->get('flight-type');
                $invitation->save();



                //attach hotel
	            if ($request->get('hotel')) {
		            $inv_hotel = InvitationAccomodation::updateOrCreate (
			            [
				            'invitation_id' => $invitation->id,
				            //'hotel_id' => $request->get('hotel'),
			            ],
			            [
				            'invitation_id' => $invitation->id,
				            'hotel_id' => $request->get('hotel'),
				            'name' => $request->get('h_name','N/A'),
				            'arrival_date' =>  Carbon::parse($request->get('h_arrival')),
				            'departure_date' =>  Carbon::parse($request->get('h_departure')),
				            'created_by' => auth()->user()->id
			            ]
		            );
	            }

                //attach activities
                if ($request->get('activities')) {
                    // if (count($request->get('activities') > 0 )) {
                        //remove all acivities first
                        $sct = InvitationActivity::where('invitation_id','=',$invitation->id)->delete();
                        foreach ($request->get('activities') as $ls ){
                            if ($ls != '') {
                                $act = InvitationActivity::updateOrCreate(
                                    [
                                        'invitation_id' => $invitation->id,
                                        'activity_id' => $ls,
                                    ],
                                    [
                                        'invitation_id' => $invitation->id,
                                        'activity_id' => $ls,
                                        'created_by' => auth()->user()->id
                                    ]
                                );
                            }
                        //}
                    }
                }
                else {
                    $sct = InvitationActivity::where('invitation_id','=',$invitation->id)->delete();
                }

                if ($request->get('flight-type') == 'Scheduled Flight') {
                    //travel details
                    $inv_flight_in =  InvitationFlightIn::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'flight_type' => $request->get('flight-type','N/A'),
                            'flight_number' => $request->get('i_f_number','N/A'),
                            'flight_carrier' => $request->get('i_f_carrier','N/A'),
                            'airport_name' => $request->get('i_f_airport','N/A'),
                            'terminal' => $request->get('i_f_terminal','N/A'),
                            'arrival_date' => $request->get('i_f_date','N/A'),
                            'arrival_time' => $request->get('i_f_time','N/A'),
                            'transfer_req' => $request->get('i_f_transfer_req','N/A'),
                            'created_by' => auth()->user()->id
                    ]);

                    $inv_flight_out =  InvitationFlightOut::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'flight_type' => $request->get('flight-type','N/A'),
                            'flight_number'=> $request->get('o_f_number','N/A'),
                            'flight_carrier' => $request->get('o_f_carrier','N/A'),
                            'airport_name' => $request->get('o_f_airport','N/A'),
                            'terminal' => $request->get('o_f_terminal','N/A'),
                            'departure_date' => $request->get('o_f_date','N/A'),
                            'departure_time' => $request->get('o_f_time','N/A'),
                            'transfer_req' => $request->get('o_f_transfer_req','N/A'),
                            'created_by' => auth()->user()->id,
                    ]);
                }
                else {
                    //travel details
                    $inv_flight_in =  InvitationFlightIn::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'flight_type' => $request->get('flight-type','N/A'),
                            'flight_number'=> $request->get('c_i_f_number','N/A'),
                            'airport_name' => $request->get('c_i_f_airport','N/A'),
                            'flight_carrier' => $request->get('c_i_f_carrier','N/A'),
                            'terminal' => $request->get('c_i_f_terminal','N/A'),
                            'arrival_date' => $request->get('c_i_f_date','N/A'),
                            'arrival_time' => $request->get('c_i_f_time','N/A'),
                            'plane_reg' => $request->get('c_i_f_plane','N/A'),
                            'transfer_req' => $request->get('c_i_f_transfer_req','N/A'),
                            'created_by' => auth()->user()->id
                    ]);


                    $inv_flight_out =  InvitationFlightOut::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'flight_type' => $request->get('flight-type','N/A'),
                            'airport_name' => $request->get('c_o_f_airport','N/A'),
                            'flight_number'=> $request->get('c_o_f_number','N/A'),
                            'flight_carrier' => $request->get('c_o_f_carrier','N/A'),
                            'terminal' => $request->get('c_o_f_terminal','N/A'),
                            'departure_date' => $request->get('c_o_f_date','N/A'),
                            'departure_time' => $request->get('c_o_f_time','N/A'),
                            'plane_reg' => $request->get('c_o_f_plane','N/A'),
                            'transfer_req'=> $request->get('c_o_f_transfer_req','N/A'),
                            'created_by' => auth()->user()->id
                    ]);
                }

                //transfer package (1 package is allowed)
                if($request->get('flight_upgrade')){
                  $flight_upgrade = (float)$request->get('flight_upgrade');
                  $eventTransfer = EventTransport::where('cost','=', $flight_upgrade)->first();
                  // dd($eventTransfer->id);
                  $inv_tpackage = InvitationTransferPackages::updateOrCreate(
                         [
                            'invitation_id' => $invitation->id,
                            //'tpackage_id' => $request->get('transfer_packages'),
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'tpackage_id' => $eventTransfer->id,
                            'created_by' => auth()->user()->id
                    ]);
                }else if ($request->get('transfer_packages')) {
                    $inv_tpackage = InvitationTransferPackages::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                            //'tpackage_id' => $request->get('transfer_packages'),
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'tpackage_id' => $request->get('transfer_packages'),
                            'created_by' => auth()->user()->id
                    ]);

                }

                if ($request->get('transfer_packages_return')) {
                    $inv_tpackage_return = InvitationTransportReturn::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                            //'tpackage_id' => $request->get('transfer_packages'),
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'tpackage_id' => $request->get('transfer_packages_return'),
                            'created_by' => auth()->user()->id
                    ]);
                }

                if ($request->get('transfer_packages_return')) {
                    $inv_tpackage_return = InvitationTransportReturn::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                            //'tpackage_id' => $request->get('transfer_packages'),
                        ],
                        [
                            'invitation_id' => $invitation->id,
                            'tpackage_id' => $request->get('transfer_packages_return'),
                            'created_by' => auth()->user()->id
                    ]);
                }
                   if(is_null($request->get('tent_accomodation'))){

                   }else if ($request->get('tent_accomodation') == 0) {
                     $tent_input = (float)$request->get('tent_accomodation');
                     $tent = Tent::where('price','=', $tent_input)->first();
                      // dd($tent->id);
                    $inv_tent_accomodation = InvitationTentAccomodation::updateOrCreate(
                        [
                            'invitation_id' => $invitation->id,
                            //'tpackage_id' => $request->get('transfer_packages'),
                        ],
                        [
                            'event_id'=>$event->id,
                            'invitation_id' => $invitation->id,
                            'tent_id' => $tent->id,
                            'bed_type'=>$request->get('bed_type'),
                            'tent_sharing_name'=>$request->get('tent_sharing_name'),
                            'created_by' => auth()->user()->id
                    ]);
                }else if($request->get('tent_accomodation')){
                  $tent_input = (float)$request->get('tent_accomodation');
                  $tent = Tent::where('price','=', $tent_input)->first();
                   // dd($tent->id);
                 $inv_tent_accomodation = InvitationTentAccomodation::updateOrCreate(
                     [
                         'invitation_id' => $invitation->id,
                         //'tpackage_id' => $request->get('transfer_packages'),
                     ],
                     [
                         'event_id'=>$event->id,
                         'invitation_id' => $invitation->id,
                         'tent_id' => $tent->id,
                         'bed_type'=>$request->get('bed_type'),
                         'tent_sharing_name'=>$request->get('tent_sharing_name'),
                         'created_by' => auth()->user()->id
                 ]);

                }

                //check of invitation is complete
                $check = self::CheckInvitationComplete($code);
                if($check == true){
                  return redirect()->route('del.view.invitation',[$code,$eventid])->with('complete','Yes');
                }else{
                  return redirect()->route('del.view.invitation',[$code,$eventid])->withSuccess(Lang::get('trans.You have updated your profile'));
                }

            }
        }

        return view('delegate.delegate-profile',compact('titles','event','code','eventid','delegate_types','invitation','hotel','activities','tpackages','codes','tconditions','tents','bookingForm','edisclaimer','emedia'));
    }

    public function ViewInvitation ( Request $request, $code, $eventid)
        {
            if (is_int($eventid)) {
                $event = Event::where('id','=',$eventid)->with('poster','sponsor','organizer','meta')->first();
            }
            else {
                $event = Event::where('slug','=',$eventid)->with('poster','sponsor','organizer','meta')->first();
            }

            $invitation = Invitations::where('code','=',$code)
                ->with('delegateType','profile','delegation','delegation.countryname.details','delegatePrice')
                ->with('transfer','transfer.package','payments','refunds','activity')
                ->with('event','event.newsPayment')
                ->first();

            $delegate_profile = DelegateProfile::where('id','=',$invitation->delegate_profile)->first();

            $delegate_types = DelegatesType::where('id','=',$invitation->delegate_id)
                ->first();

            $hotel = InvitationAccomodation::
                 //where('event_id','=',$event->id)
                where('invitation_id','=',$invitation->id)
                ->with('hotel','hotel.images')
                ->first();

            $ticket = EventsTickets::where('delegate_id','=',$invitation->delegate_id)
                ->where('event_id','=',$event->id)
                ->first();

            $activities = \DB::table("event_activities")
                ->whereRaw("find_in_set('".$invitation->delegate_id."',event_activities.delegate)")
                ->get();

            //dd($invitation->delegate_id);

            $all_hotels = EventHotelDelegatePivot::
                where('event_id','=',$event->id)
                ->where('delegate_id','=',$invitation->delegate_id)
                ->with('hotel')
                ->get();

            //dd($all_hotels);

            $tpackages = EventTransport::where('event_id','=',$event->id)->first();
            $inv_flight_in = InvitationFlightIn::where('invitation_id','=',$invitation->id)->first();
            $inv_flight_out = InvitationFlightOut::where('invitation_id','=',$invitation->id)->first();
            $inv_act = InvitationActivity::where('invitation_id','=',$invitation->id)
                ->with('activity','activity.images')->get();

            //dd($inv_act->activity);
            //dd($inv_act->pluck('activity.cost'));
            if ($request->isMethod('post')) {
                $validate = Validator::make($request->except('_token'), [
                    'my_profile_pic' => 'dimensions:min_width=600,min_height=600',
                    'national_id_image' => 'dimensions:min_width=600,min_height=600'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate)->withInput();
                } else {
                    $input = $request->except('_token');
                    $settings = Config::get('settings.upload_folders');
                    $folder = $settings['profile'];
                    $folder2 = $settings['docs'];
                    if (!File::exists($folder)) {
                        File::makeDirectory($folder);
                    }
                    if (!File::exists($folder2)) {
                        File::makeDirectory($folder2);
                    }
                    // upload profile picture
                    if (!is_null($request->file('my_profile_pic'))) {
                        $extension = $request->file('file_attachment')->getClientOriginalExtension();
                        $fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'.' . $extension;
                        $originalName = $fileName;//$request->file('file_attachment')->getClientOriginalName();
                        //$extension = $request->file('file_attachment')->getClientOriginalExtension();
                        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
                        $filename = $this->image->sanitize($originalNameWithoutExt);
                        $allowed_filename = $this->image->createUniqueFilename( $filename, $extension );

                        $upload = $request->file('my_profile_pic')->move('uploads/profile/', $allowed_filename);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['my_profile_pic'] = 'uploads/profile/'.$allowed_filename;
                        } else { $input['my_profile_pic'] = null; }
                    }
                    //upload national id
                    if (!is_null($request->file('national_id_image'))) {
                        //$extension = $request->file('national_id_image')->getClientOriginalExtension();
                        $extension = $request->file('national_id_image')->getClientOriginalExtension();
                        //$fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'.' . $extension;
                        $fileName = rand(11111, 99999).'-'.$request->get('fname').'-'.$request->get('surname').'-'.$request->get('national_id').'.' . $extension;
                        $originalName = $fileName;//$request->file('file_attachment')->getClientOriginalName();
                        //$extension = $request->file('file_attachment')->getClientOriginalExtension();
                        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
                        $filename = $this->image->sanitize($originalNameWithoutExt);
                        $allowed_filename = $this->image->createUniqueFilename( $filename, $extension );

                        $upload = $request->file('national_id_image')->move('uploads/docs/', $allowed_filename);//$image->save($folder.'/'.$fileName); //
                        //$uniqueFileName = uniqid().$request->file('file_attachment')->getClientOriginalName().'.'.$request->file('file_attachment')->getClientOriginalExtension();
                        //$upload = $request->file('file_attachment')->move('uploads/posters/'. $uniqueFileName);
                        if ($upload) {
                            $input['national_id_image'] = 'uploads/docs/'.$allowed_filename;
                        } else { $input['national_id_image'] = null; }
                    }

                    //update profile
                    $delegate_profile->organisation = $request->get('organisation');
                    $delegate_profile->position = $request->get('position');
                    $delegate_profile->country = $request->get('country');
                    $delegate_profile->nationality = $request->get('nationality');
                    $delegate_profile->national_id = $request->get('national_id');
                    $delegate_profile->national_id_type = $request->get('national_id_type');
                    $delegate_profile->national_id_type = $request->get('national_id_type');
                    $delegate_profile->national_id_image = $input['national_id_image']; //$request->get('national_id_image');
                    $delegate_profile->passport_image = $input['my_profile_pic']; //$request->get('national_id_image');
                    $delegate_profile->cell_phone = $request->get('cell_phone');
                    $delegate_profile->cell_office = $request->get('office_phone');

                    $delegate_profile->a_contact_name = $request->get('a_contact_name');
                    $delegate_profile->a_contact_email = $request->get('a_contact_email');
                    $delegate_profile->a_contact_phone = $request->get('a_contact_phone');
                    $delegate_profile->a_contact_office_phone = $request->get('a_contact_office_phone');

                    $delegate_profile->tech_requirements = $request->get('a_contact_office_phone');
                    $delegate_profile->food_requirements = $request->get('a_contact_office_phone');

                    $delegate_profile->vehicle_pass = $request->get('vehicle_pass');
                    $delegate_profile->vehicle_detials = $request->get('vehicle_registration');

                    $delegate_profile->save();

                    //attach hotel
                    $inv_hotel = New InvitationAccomodation();
                    $inv_hotel->invitation_id = $invitation->id;
                    $inv_hotel->hotel_id = $request->get('hotel');
                    $inv_hotel->arrival_date = $request->get('h_arrival');
                    $inv_hotel->departure_date = $request->get('h_departure');
                    $inv_hotel->created_by = auth()->user()->id;
                    $inv_hotel->save();

                    //attach activities
                    $inv_act = New InvitationActivity();
                    $inv_act->invitation_id = $invitation->id;
                    $inv_act->activity_id = $request->get('hotel');
                    //$inv_act->arrival_date = $request->get('h_arrival');
                    //$inv_act->departure_date = $request->get('h_departure');
                    $inv_act->created_by = auth()->user()->id;
                    $inv_act->save();

                    //travel details
                    $inv_flight_in =  New InvitationFlightIn();
                    $inv_flight_in->invitation_id = $invitation->id;
                    $inv_flight_in->flight_type = $request->get('flight_type');
                    $inv_flight_in->flight_number = $request->get('i_f_number');
                    $inv_flight_in->flight_carrier = $request->get('i_f_carrier');
                    $inv_flight_in->airport_name = $request->get('i_f_airport');
                    $inv_flight_in->terminal = $request->get('i_f_terminal');
                    $inv_flight_in->arrival_date = $request->get('i_f_date');
                    $inv_flight_in->arrival_date = $request->get('i_f_time');
                    $inv_flight_in->plane_reg = $request->get('i_f_plane');
                    $inv_flight_in->transfer_req = $request->get('i_f_transfer_req');
                    $inv_flight_in->save();

                    $inv_flight_out =  New InvitationFlightOut();
                    $inv_flight_out->invitation_id = $invitation->id;
                    $inv_flight_out->flight_type = $request->get('flight_type');
                    $inv_flight_out->flight_number = $request->get('o_f_number');
                    $inv_flight_out->flight_carrier = $request->get('o_f_carrier');
                    $inv_flight_out->airport_name = $request->get('o_f_airport');
                    $inv_flight_out->terminal = $request->get('o_f_terminal');
                    $inv_flight_out->departure_date = $request->get('o_f_date');
                    $inv_flight_out->departure_date = $request->get('o_f_time');
                    $inv_flight_out->plane_reg = $request->get('o_f_plane');
                    $inv_flight_out->transfer_req = $request->get('o_f_transfer_req');
                    $inv_flight_out->save();

                    //transfer package
                    $inv_tpackage = New InvitationTransferPackages();
                    $inv_act->invitation_id = $invitation->id;
                    $inv_act->activity_id = $request->get('transfer_packages');
                    //$inv_act->arrival_date = $request->get('h_arrival');
                    //$inv_act->departure_date = $request->get('h_departure');
                    $inv_act->created_by = auth()->user()->id;
                    $inv_act->save();
                    //
                    return redirect()->route('del.view.invitation',[$code,$eventid])->withSuccess(Lang::get('trans.You have updated your profile'));
                }
            }

            return view('delegate.delegate-profile-view',compact('event','code',
                'eventid','delegate_types','invitation','hotel','activities','tpackages'
                ,'inv_flight_in','inv_flight_out','inv_act','ticket','all_hotels'
            ));
        }

    public function ViewEventSponsors(Request $request, $id,$code)
    {
        $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta')->first();
        if ($event) {
            return view('front.sponsors', compact('event','code'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function ViewEventEssentialInfo (Request $request, $id, $code)
    {
        //$event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta','newsFull')->first();
        $event = Event::where('slug','=',$id)->with('activities','activities.images','poster','hotels','hotels.hotel','hotels.delegate','hotels.delegate.type','sponsor','organizer','meta','news','newsFull')->first();
        if ($event) {
            //dd($event);
            return view('front.essential-info', compact('event','code'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function Eventindex (Request $request, $id)
    {
        if (is_int($id)) {
            $event = Event::where('id','=',$id)->with('poster','sponsor','organizer','meta','programmes','newsGeneral')->first();
        }
        else {
            $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta','programmes','newsGeneral')->first();
        }
        if ($event) {
            //dd($event);
            return view('front.index', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function AboutEventindex (Request $request, $id)
    {
        if (is_int($id)) {
            $event = Event::where('id','=',$id)->with('poster','sponsor','organizer','meta','programmes','newsGeneral')->first();
        }
        else {
            $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta','programmes','newsGeneral')->first();
        }
        if ($event) {
            //dd($event);
            return view('front.index-about', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function CheckInvitationComplete($code)
    {
        $invitation = Invitations::where('email_sent','=',0)
            ->where("code","=", $code)
            ->with('profile','event')
            ->first();
        if ($invitation) {
            app('App\Console\Commands\SendThankMsg')->SendThankyouMail($invitation);
            return true;
        }
        return false;
    }
    public function getCallingCodeList(Request $request)
        {
          // '+'.str_replace('"]','', str_replace('["','',$invitation->delegation->countryname->details->callingcodes)))
            $states = DB::table("countries_advanced")
                        ->where("demonym",$request->country_id)
                        ->pluck("callingcodes","callingcodes");

             // $states = '+'.str_replace('"]','', str_replace('["','',$states));
              // var_dump($states);die;
            return response()->json($states);
        }

        public function getTerms(){

            $file = public_path()."/uploads/Terms_and_Conditions.pdf";
            $headers = array('Content-Type: application/pdf',);
            return Response::download($file, 'Terms_and_Conditions.pdf',$headers);
        }
}
