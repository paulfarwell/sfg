<?php

namespace App\Http\Controllers;

use App\Models\Delegation;
use App\Models\Refunds;
use App\User;

//use function collect;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;


//use function var_dump;
use Yajra\Datatables\Datatables;
use Hash;
use Auth;
use Validator;
use Mail;
use Log;
use Session;
use Input;
use File;
use Image;
use GuzzleHttp\Client;
use DB;
use Config;
use mPDF;
use View;
use PDF;
use Carbon\Carbon;
use Plupload;

use App\Models\Event;
use App\Models\Invitations;
use App\Models\Payments;
use App\Models\EventsTickets;
use App\Models\InvitationActivity;

class ReportsController extends Controller
{
    public static function EventSalesStatsMonth($event_id,$type='normal'){
        $records = Payments::select(DB::raw("DATE_FORMAT(created_at, '%m-%Y') period"),DB::raw('sum(amount) as `sales`'))
            ->where('event_id','=',$event_id)->single()->active()
            ->groupBy(DB::raw('DATE_FORMAT(created_at,\'%Y-%m\')'))
            //->sum('amount')
            ->get();
        if ($type == 'json') {
            return response()->json($records,200);
        }
        return $records;
    }

    public static function EventSalesStatsTicket($event_id,$type='normal'){
        $records = Invitations::select(DB::raw('count(invitations.delegate_id) as `qty`'),'event_tickets.price','delegates_types.name')
            ->join('event_tickets', 'invitations.delegate_id', '=', 'event_tickets.delegate_id')
            ->join('delegates_types', 'event_tickets.delegate_id', '=', 'delegates_types.id')
            ->where('invitations.event_id','=',$event_id)
            ->where('invitations.status','=',0)
            ->groupBy('invitations.delegate_id')
            ->get();

        if ($type == 'json') {
            return response()->json($records,200);
        }
        return $records;
    }

    public static function EventSalesStatsAct($event_id,$type='normal') {
        $records = DB::table('invitation_act')->select(DB::raw('count(invitation_act.id) as `qty`'),'event_activities.cost','event_activities.name')
            ->join('invitations', 'invitation_act.invitation_id', '=', 'invitations.id')
            ->join('event_activities', 'invitation_act.activity_id', '=', 'event_activities.id')
            ->where('invitations.status','=',0) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->where('invitations.event_id','=',$event_id) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            //->selectRaw('SUM(event_activities.cost) AS act_value')
            ->groupBy('invitation_act.activity_id')
            ->get();
        if ($type == 'json') {
            return response()->json($records,200);
        }
        return $records;
    }

    public static function EventSalesStatsTrans($event_id,$type='normal') {
        $records =  DB::table('invitation_tpackage')->select(DB::raw('count(invitation_tpackage.id) as `qty`'),'event_transport.cost','event_transport.name')
            ->join('invitations', 'invitation_tpackage.invitation_id', '=', 'invitations.id')
            ->join('event_transport', 'invitation_tpackage.tpackage_id', '=', 'event_transport.id')
            ->where('invitations.status','=',0) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->where('invitations.event_id','=',$event_id) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            //->selectRaw('SUM(event_transport.cost) AS trans_value')
            ->groupBy('invitation_tpackage.tpackage_id')
            ->get();
        if ($type == 'json') {
            return response()->json($records,200);
        }
        return $records;
    }

    public static function DashboardStats ($type='normal') {
        $sales = Payments::Active()->Single()->sum('amount');
        $inv = Invitations::Active()->count();
        $users = User::Active()->count();
        $events = Event::Active()->count();
        if ($type == 'json') {
            return response()->json(['sales' => $sales, 'invitations' => $inv, 'events' => $events , 'users' => $users ],200);
        }
        return ['sales' => $sales, 'invitations' => $inv, 'events' => $events , 'users' => $users ];
    }

    public static function DashboardEventStats ($event_id, $type='normal') {
        $sales = Payments::Active()->Single()->where('event_id','=',$event_id)->sum('amount');
        $refunds = Refunds::where('event_id','=',$event_id)->active()->sum('amount');
        $sales_act = DB::table('invitation_act')  //InvitationActivity::
            ->join('invitations', 'invitation_act.invitation_id', '=', 'invitations.id')
            ->join('event_activities', 'invitation_act.activity_id', '=', 'event_activities.id')
            ->where('invitations.status','=',0) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->where('invitations.event_id','=',$event_id) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->selectRaw('SUM(event_activities.cost) AS act_value')
            ->get();
        $sales_tran =  DB::table('invitation_tpackage')
            ->join('invitations', 'invitation_tpackage.invitation_id', '=', 'invitations.id')
            ->join('event_transport', 'invitation_tpackage.tpackage_id', '=', 'event_transport.id')
            ->where('invitations.status','=',0) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->where('invitations.event_id','=',$event_id) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
            ->selectRaw('SUM(event_transport.cost) AS trans_value')
            ->get();
//        $sales_tickets = DB::table('invitations')
//            ->join('event_tickets', 'invitations.delegate_id', '=', 'event_tickets.delegate_id')
//            ->join('delegates_types', 'event_tickets.delegate_id', '=', 'delegates_types.id')
//            ->where('invitations.status','=',0) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
//            ->where('invitations.event_id','=',$event_id) //WHERE (`invitations`.`status` = 0) AND (`invitations`.`event_id` = 1)
//            ->selectRaw('SUM(event_tickets.price) AS tickets_value')
//            ->get();
        $all_tickets = self::EventSalesStatsTicket($event_id);
        $sales_tickets = $all_tickets->sum(function ($item) {
            return $item['qty'] * $item['price'];
        });
        //dd($sales_tickets);
        $total_orders =  $sales_act[0]->act_value + $sales_tran[0]->trans_value + $sales_tickets  ;
        $owed =  $total_orders - ($sales + $refunds)  ;
        $delegates = Invitations::Active()->where('event_id','=',$event_id)->count();//;
        $delegations = Delegation::Active()->where('event_id','=',$event_id)->count();//;
        if ($type == 'json') {
            return response()->json([
                'sales' => $sales ,
                'total_orders' => $total_orders,
                'refunds' => $refunds,
                'owed' => $owed ,
                'delegates' => $delegates,
                'delegations' => $delegations,
                'sales_act' => $sales_act[0]->act_value,
                'sales_tran' => $sales_tran[0]->trans_value
            ],200);
        }
        return [
            'sales' => $sales,
            'total_orders' => $total_orders,
            'refunds' => $refunds,
            'owed' => $owed,
            'delegates' => $delegates,
            'delegations' => $delegations ,
            'sales_act' => $sales_act[0]->act_value,
            'sales_tran' => $sales_tran[0]->trans_value
        ];
    }

}
