<?php

namespace App\Http\Controllers;

use App\Models\DelegateProfile;
use App\Models\Invitations;
use App\User;
use Illuminate\Http\Request;
//use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\EventHotel;
//use App\Http\Requests;

use Yajra\Datatables\Datatables;
use Auth;
use Validator;
use Mail;
use Log;
use Session;
use Input;
use File;
use Image;
use GuzzleHttp\Client;
use DB;
use Config;
use mPDF;
use View;
use PDF;
use Carbon\Carbon;
use Hash;
use Response;
use Lang;

use App\Models\Event;
use App\Models\EventsMeta;
use App\Models\EventsMetaData;
use App\Models\EventsTickets;
use App\Models\DelegatesType;
use App\Models\EventHotelPivot;
use App\PasswordReset;

use app\Logic\OurFunctions;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $ourfunctions;
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->$ourfunctions = OurFunctions::;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */



    public function index(Request $request, $id)
    {
        if (is_int($id)) {
            $event = Event::where('id','=',$id)->with('poster','sponsor','organizer','meta','programmes')->first();
        }
        else {
            $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta','programmes')->first();
        }
        if ($event) {
            //dd($event);
            return view('front.index', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function Aboutindex(Request $request, $id)
    {
        if (is_int($id)) {
            $event = Event::where('id','=',$id)->with('poster','sponsor','organizer','meta','programmes')->first();
        }
        else {
            $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta','programmes')->first();
        }
        if ($event) {
            //dd($event);
            return view('front.index-about', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function ViewEventSponsors(Request $request, $id)
    {
        $event = Event::where('slug','=',$id)->with('poster','sponsor','organizer','meta')->first();
        if ($event) {
            return view('front.sponsors', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }

    public function ViewEventEssentialInfo (Request $request, $id)
    {
        $event = Event::where('slug','=',$id)->with('activities','activities.images','poster','hotels','hotels.hotel','hotels.delegate','hotels.delegate.type','sponsor','organizer','meta','news','newsFull')->first();
        if ($event) {
            //dd($event);
            return view('front.essential-info', compact('event'));
        }
        return redirect()->back()->withError(Lang::get('trans.The event you selected does not exist'));
    }


    public function DelegateVerifyCode(Request $request, $eventid)
    {
      $code = $request->get('code');
      $event = Event::where('id','=',$eventid)->with('poster','sponsor','organizer','meta','news')->first();
      if($event){
        $invitation = Invitations::where('code','=',$code)
                                  ->where('event_id','=',$eventid)
                                    ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
                                  ->first();
              // dd($invitation);
        if($invitation == null){
                  return redirect()->back()->withError(Lang::get("trans.Invalid event invitation code"));
              }
          elseif($invitation->status === 0){
            Auth::logout();
            $authenticated = User::where(['email'=>$invitation->profile->email,'is_disabled'=>0]);

            if ($invitation && $authenticated) {
                //dd('auth in');
                if ($invitation->profile){
                  Auth::loginUsingId($invitation->profile->user_id, true);
                  //Auth::user()->setAttribute('InviteCode', $code);
                  Auth::user()->code = $code;
                  Auth::user()->confirmed_email = 1;
                  Auth::user()->save();

                  // if (Auth::user()->confirmed_email == 0) {
                  //     return redirect()->route('delegate.login.change.code',[$code,$eventid])
                  //         ->withSuccess("Login successful, please confirm your email, it's mandatory")->withEvent($event)->withInvitation($invitation);
                  // }
                  //check of changed pass word first time
                  if (Auth::user()->first_time_login == 0) {
                      $recovery = new PasswordReset();
                      $token = \App\Logic\OurFunctions::generateRandomString(16);
                      $recovery->email = Auth::user()->email;
                      $recovery->token = $token;
                      $saved = $recovery->save();
                      //dd(route('delegate.login.change.code',[$code,$event->slug,'reset_token'=>$token]));
                      return redirect()->route('delegate.login.change.code',[$code,$eventid,'reset_token'=>$token])
                          ->withSuccess(Lang::get("trans.Login successful, please change your password, it's mandatory"))->withEvent($event)->withInvitation($invitation);
                  }

                  if ($invitation->profile->completion < 100 ) {
                      return redirect()->route('del.view.invitation',[$code,$event->slug])
                          ->withSuccess(Lang::get("trans.Login successful"))->withEvent($event)->withInvitation($invitation);
                  }
                  else {
                      return redirect()->route('del.view.invitation',[$code,$event->slug])
                          ->withSuccess(Lang::get("trans.Login successful"))->withEvent($event)->withInvitation($invitation);
                  }

                }
                else {
                    Auth::logout();
                    return redirect()->route('org.view.event',$event->slug)
                        ->withError(Lang::get('trans.There seems to be problem with your profile, no associated delegate profile, contact support'));
                }
              }
              return redirect()->back()   //route('org.view.event',$event->slug)
                          ->withError(Lang::get('trans.There seems to be problem with your profile, no associated delegate profile, contact support'));
           // return redirect()->route('delegate.login',[$invitation->code,$eventid])->withSuccess('Your Invitation has been confirmed');;

         }elseif($invitation->status === 1){
           Auth::logout();
           return redirect()->route('login')->withInfo(Lang::get('trans.You already confirmed your account, Please Log in'));
         }

            return redirect()->back()->withError(Lang::get('Invalid event invitation code was selected'));
      }
      return redirect()->back()->withError(Lang::get('Invalid event ID was selected'));

    }

    public function forgot(){

        return view('auth.forgot');

    }

    public function ForgotPassword (Request $request) {
            Auth::logout();
        $user = User::where('email','=',$request->get('email'))
            ->first();
        if ( $user ) {
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'email' => 'email|required',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError(Lang::get('trans.Invalid Email format'))->withInput()->withErrors($validator);
                } else {
                    $recovery = new PasswordReset();
                    $token = \App\Logic\OurFunctions::generateRandomString(16);
                    $recovery->email = $request->get('email');
                    $recovery->token = $token;
                    $saved = $recovery->save();
                    if ($saved) {
                        // send this guy and email for password recovery
                        Mail::send('email.password-recovery', ['url' => route('reset.password', [$recovery->token]), 'name' => $user->name],
                            function ($message) use ($recovery) {
                                $message->from('no-reply@spaceforgiants.org', 'Space for Giants - Events Portal - support');
                                $message->to($recovery->email);
                                $message->subject('Password Recovery');
                            });
                        return redirect()->back()->withSuccess(Lang::get('trans.Please check your email'));
                    } else {
                        return redirect()->back()->withError(Lang::get('trans.Failed to reset password. Please try again'));
                    }
                }
            }
            return redirect()->back() //route('org.view.event',$event->slug)
            ->withError(Lang::get('trans.Invalid request, contact support'));
        }
        return redirect()->back() //route('org.view.event',$event->slug)
        ->withError(Lang::get('trans.The email you provided does not match to any of the existing accounts'));
    }

    public function ResetPassword (Request $request, $reset_token ) {
       $token = PasswordReset::where('token','=',$reset_token)->where('used','=',0)->first();
        if ($token) {
            $user = User::where('email','=',$token->email)->first();
            if ($user) {
            if ($request->isMethod('post')) {
                $passChange = PasswordReset::where('token', '=', $reset_token)->where('used', '=', 0)
                    ->first();
                if ($passChange){
                    $validator = Validator::make($request->except('_token'), [
                        'password1'    => 'required',
                        'password2' => 'required|same:password1',
                    ]);
                    if ($validator->fails()) {
                        return redirect()->back()->withError(Lang::get('trans.Please rectify the errors below'))->withInput()->withErrors($validator);
                    } else {
                        $user = User::where('email', '=', $passChange->email)->first();
                        if ($user) {
                            $updated = $user->update(['password' => Hash::make($request->get('password1'))]);
                            if ($updated) {
                                $passChange->update(['used' => 1]);
                                $user->first_time_login = 1;
                                $user->save();
                                //return redirect()->route('login')->withSuccess('Password Changed.');
                              return redirect()->route('login')->withSuccess(Lang::get('trans.Password has been changed'));
                            }
                            return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                        }
                        return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                    }
                }
                return redirect()->back() //route('org.view.event',$event->slug)
                ->withError(Lang::get('trans.There seems to be problem with your profile, invalid reset token, contact support'));
            }
                return view('auth.reset',compact('reset_token'));
            }
            else {
                return redirect()->back() //route('org.view.event',$event->slug)
                ->withError(Lang::get('trans.Invalid user account'));
            }
        }
        return redirect()->back() //route('org.view.event',$event->slug)
        ->withError(Lang::get('trans.The token is invalid or has expired'));
    }

    public function DelegateLoginRenew (Request $request, $code= null, $eventid) {
        $event = Event::whereId($eventid)->first();
        $invitation = Invitations::where('code','=',$code)->where('event_id','=',$eventid)
            ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
            ->first();
        if ($event && $invitation) {

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'email' => 'email|required',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError(Lang::get('trans.Invalid Email format'))->withInput()->withErrors($validator);
                } else {
                    $recovery = new PasswordReset();
                    $token = \App\Logic\OurFunctions::generateRandomString(16);
                    $recovery->email = $request->get('email');
                    $recovery->token = $token;
                    $saved = $recovery->save();
                    if ($saved) {
                        // send this guy and email for password recovery
                        Mail::send('email.password-recovery', ['event' => $event,'url' => route('delegate.login.reset.code', [$code,$eventid,$recovery->token]), 'name' => $invitation->profile->user_account->name],
                        function ($message) use ($recovery) {
                            $message->from('no-reply@spaceforgiants.org', 'Space for Giants - Events Portal - support');
                            $message->to($recovery->email);
                            $message->subject('Password Recovery');
                        });
                        return redirect()->back()->withSuccess(Lang::get('trans.Please check your email'));
                    } else {
                        return redirect()->back()->withError(Lang::get('trans.Failed to reset password. Please try again'));
                    }
                }
            }
            return redirect()->back() //route('org.view.event',$event->slug)
            ->withError(Lang::get('trans.Invalid request, contact support'));
        }
        return redirect()->back() //route('org.view.event',$event->slug)
        ->withError(Lang::get('trans.There seems to be problem with your profile,invalid invitation code or/and event selector, contact support'));
    }

    public function DelegateLoginReset (Request $request, $code= null, $eventid, $reset_token ) {
        $event = Event::whereId($eventid)->first();
        $invitation = Invitations::where('code','=',$code)->where('event_id','=',$eventid)
            ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
            ->first();
        //dd($invitation);
        if ($event && $invitation) {
            if ($request->isMethod('post')) {
                $passChange = PasswordReset::where('token', '=', $reset_token)->where('used', '=', 0)
                    ->first();
                if ($passChange){
                    $validator = Validator::make($request->except('_token'), [
                        'password1'    => 'required',
                        'password2' => 'required|same:password1',
                    ]);
                    if ($validator->fails()) {
                        return redirect()->back()->withError(Lang::get('trans.Please rectify the errors below'))->withInput()->withErrors($validator);
                    } else {
                        $user = User::where('email', '=', $passChange->email)->first();
                        if ($user) {
                            $updated = $user->update(['password' => Hash::make($request->get('password1'))]);
                            if ($updated) {
                                $passChange->update(['used' => 1]);
                                $user->first_time_login = 1;
                                $user->save();
                                //return redirect()->route('login')->withSuccess('Password Changed.');
                                return redirect()->route('delegate.login.with.code',[$invitation->code,$eventid])
                                    ->withSuccess(Lang::get('trans.Yey!, we changed you password, re-enter it to proceed'));
                            }
                            //return redirect()->route('login')->withInfo('Internal Error. Please try again');
                            return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                        }
                        return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                    }
                }
                return redirect()->back() //route('org.view.event',$event->slug)
                ->withError(Lang::get('trans.There seems to be problem with your profile, invalid reset token, contact support'));
            }
            return view('delegate.delgate-change-password',compact('code','eventid','event','invitation','reset_token'));
        }
        return redirect()->back() //route('org.view.event',$event->slug)
        ->withError(Lang::get('trans.There seems to be problem with your profile,invalid invitation code or/and event selector, contact support'));
    }

    public function DelegateLoginCode (Request $request, $code= null, $eventid)
    {
        $msg = null; $err = 0; //$code ;
        $event = Event::whereId($eventid)->first();
        $invitation = Invitations::where('code','=',$code)->where('event_id','=',$eventid)
            ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
            ->first();
        //dd($invitation);
        if ($event && $invitation) {
        	if (isset($invitation->profile->user_account)) {
        		$message = null;
        		$invitation->profile->user_account->first_time_login == 0 ? $message = 'Please enter the one-time password that was sent to your email' : $message = 'Please enter your password';
	        }
            if ($request->isMethod('post')) {
             // var_dump( $request->get('password'));die;
                //$password = User::where('password','=',hash)
                Auth::logout();
                 $authenticated = User::where(['email'=>$invitation->profile->email,'is_disabled'=>0])->first();
                 //auth()->attempt([
                //     'id'    => $invitation->profile->user_id,
                //     'password' =>$request->get('password'),
                //     'is_disabled' => 0,
                // ]);
                //authenticate
                if ($invitation && $authenticated) {
                    //dd('auth in');
                    if ($invitation->profile){
                        Auth::loginUsingId($invitation->profile->user_id, true);
                        //Auth::user()->setAttribute('InviteCode', $code);
                        Auth::user()->code = $code;
                        Auth::user()->save();
                        //check of changed pass word first time
                        if (Auth::user()->first_time_login == 0) {
                            $recovery = new PasswordReset();
                            $token = \App\Logic\OurFunctions::generateRandomString(16);
                            $recovery->email = Auth::user()->email;
                            $recovery->token = $token;
                            $saved = $recovery->save();
                            //dd(route('delegate.login.change.code',[$code,$event->slug,'reset_token'=>$token]));
                            return redirect()->route('delegate.login.change.code',[$code,$eventid,'reset_token'=>$token])
                                ->withSuccess(Lang::get('trans.Login successful, please change your password, its mandatory'))->withEvent($event)->withInvitation($invitation);
                        }
                        //check if email has been confirmed
                        if (Auth::user()->confirmed_email == 0) {
                            return redirect()->route('delegate.login.confirm.email',[$code,$eventid])
                                ->withSuccess(Lang::get('trans.Login successful, please confirm your email, its mandatory'))->withEvent($event)->withInvitation($invitation);
                        }
                        //go to the event profile
                        if ($invitation->profile->completion < 100 ) {
                            return redirect()->route('del.view.invitation',[$code,$event->slug])
                                ->withSuccess(Lang::get('trans.Login successful'))->withEvent($event)->withInvitation($invitation);
                        }
                        else {
                            return redirect()->route('del.view.invitation',[$code,$event->slug])
                                ->withSuccess(Lang::get('trans.Login successful'))->withEvent($event)->withInvitation($invitation);
                        }
                    }
                    else {
                        Auth::logout();
                        return redirect()->route('org.view.event',$event->slug)
                            ->withError(Lang::get('trans.There seems to be problem with your profile, no associated delegate profile, contact support'));
                    }
                }
                return redirect()->back()   //route('org.view.event',$event->slug)
                            ->withError(Lang::get('trans.There seems to be problem with your profile, user is not authenticated - invalid password , contact support'));
            }
            else {
                return view('delegate.delgate-verify-account',compact('code','eventid','event','message'));
            }
        }
        return redirect()->back() //route('org.view.event',$event->slug)
            ->withError(Lang::get('trans.There seems to be problem with your profile,invalid invitation code or/and event selector, contact support'));
    }

    public function DelegateLoginChangePassword (Request $request, $code= null, $eventid)
    {
        //dd($request->reset_token);
        $msg = null; $err = 0; //$code ;
        $reset_token = $request->get('reset_token') == '' ? $request->reset_token : $request->get('reset_token');
        $event = Event::whereId($eventid)->first();
        $invitation = Invitations::where('code','=',$code)->where('event_id','=',$eventid)
        ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
        ->first();
        if ($event && $invitation) {
            if ($request->isMethod('post')) {
                $passChange = PasswordReset::where('token', '=', $reset_token)->where('used', '=', 0)
                    ->first();
                if ($passChange){
                    $validator = Validator::make($request->except('_token'), [
                        'password'    => 'required',
                        'confirm_password' => 'required|same:password',
                    ]);
                    if ($validator->fails()) {
                        return redirect()->back()->withError(Lang::get('trans.Please rectify the errors below'))->withInput()->withErrors($validator);
                    } else {
                        $user = User::where('email', '=', $passChange->email);
                        if ($user) {
                            $updated = $user->update(['password' => Hash::make($request->get('password'))]);
                            if ($updated) {
                                $passChange->update(['used' => 1]);
                                Auth::user()->first_time_login = 1;
                                Auth::user()->save();

                                $invitation->status = 1;
                                $invitation->save();
                                //return redirect()->route('login')->withSuccess('Password Changed.');
                                return redirect()->route('del.view.invitation',[$invitation->code,$event->slug])
                                    ->withSuccess(Lang::get('trans.Login Successful'))->withEvent($event)->withInvitation($invitation);
                                // return redirect()->route('delegate.login.confirm.email',[$invitation->code,$eventid])
                                //     ->withSuccess('Yey!, we changed you password, confirm your email to proceed');
                            }
                            //return redirect()->route('login')->withInfo('Internal Error. Please try again');
                            return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                        }
                        return redirect()->back()->withError(Lang::get('trans.Internal Error. Please try again'));
                    }
                }
                return redirect()->back() //route('org.view.event',$event->slug)
                ->withError(Lang::get('trans.The reset token can only be used once, please proceed to login and reset your password or contact support'));
            }
            else {
                return view('delegate.delgate-change-password',compact('code','eventid','event','invitation','reset_token'));
            }
        }
        return redirect()->back() //route('org.view.event',$event->slug)
            ->withError(Lang::get('trans.There seems to be problem with your profile, contact support'));
    }

    public function DelegateLoginConfirmEmail (Request $request, $code= null, $eventid)
    {
        $msg = null; $err = 0; //$code ;
        $event = Event::whereId($eventid)->first();
        $invitation = Invitations::where('code','=',$code)->where('event_id','=',$eventid)
            ->with('profile','profile.user_account','delegateType','delegation','delegation.countryname.details')
            ->first();
        if ($event && $invitation) {
            if ($request->isMethod('post')) {
              $validator = Validator::make($request->except('_token'), [
                  'email'    => 'required',
                  'confirm_email' => 'required|same:email',
              ]);
              if ($validator->fails()) {
                  return redirect()->back()->withError(Lang::get('trans.Please rectify the errors below'))->withInput()->withErrors($validator);
              } else{
                if (Auth::user()->email == $request->get('email')) {
                    Auth::user()->confirmed_email = 1;
                    Auth::user()->correspondence_email = $request->get('confirm_email');
                    Auth::user()->save();

                    $recovery = new PasswordReset();
                    $token = \App\Logic\OurFunctions::generateRandomString(16);
                    $recovery->email = Auth::user()->email;
                    $recovery->token = $token;
                    $saved = $recovery->save();

                }
                else {
                  // var_dump($invitation->profile->email);die;
                    $validator = Validator::make($request->except('_token'), [
                        'email'       => 'required|email|unique:users'
                    ]);
                    if ($validator->fails()) {
                        return redirect()->back()->withInfo(Lang::get('trans.Please Correct the errors below'))->withInput()->withErrors($validator);
                    } else {
                        Auth::user()->confirmed_email = 1;
                        Auth::user()->correspondence_email = $request->get('confirm_email');
                        Auth::user()->email = $request->get('email');
                        Auth::user()->save();

                       $delegate_email = DelegateProfile::where('email','=', $invitation->profile->email)->first();
                       $delegate_email->email = $request->get('email');
                       $delegate_email->save();


                        $recovery = new PasswordReset();
                        $token = \App\Logic\OurFunctions::generateRandomString(16);
                        $recovery->email = Auth::user()->email;
                        $recovery->token = $token;
                        $saved = $recovery->save();

                    }
                }
              }
                 return redirect()->route('delegate.login.change.code',[$code,$eventid,'reset_token'=>$token])
                     ->withSuccess(Lang::get("trans.Login successful, please change your password, it's mandatory"))->withEvent($event)->withInvitation($invitation);
                //go to the event profile
                // return redirect()->route('del.view.invitation',[$code,$event->slug])
                //     ->withSuccess('Login Successful')->withEvent($event)->withInvitation($invitation);
            }
            else {
                return view('delegate.delgate-confirm-email',compact('code','eventid','event','invitation'));
            }
        }
        return redirect()->back() //->route('org.view.event',$event->slug)
            ->withError(Lang::get('trans.There seems to be problem with your profile, contact support'));
    }

    public function getDownload($id){
         $file = Event::where('id','=', $id)->first();
        $pdf = public_path()."/uploads/files/".$file->event_programme;
        $headers = array('Content-Type: application/pdf',);
        return Response::download($pdf, $file->event_programme,$headers);
    }

}
