<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\User;
use Validator;
use App\Team;
//use App\Http\Requests\Request;
//use Illuminate\Http\Request;
//use App\Http\Requests;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers; //, ThrottlesLogins;
    //use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'           => 'required|max:255',
            'company'        => 'required|max:255',
            'paybill_number' => 'sometimes|numeric|unique:paybillnumbers,number',
            'phone'          => 'required|regex:/[0-9]{9}/',
            'email'          => 'required|email|max:255|unique:users',
            'password'       => 'required|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {

        $user = User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $team = $this->createTeam($user, $data);
        $this->createPaybill($user, $data, $team);
        $user->generateKeys();

        return $user;
    }

    private function createTeam($user, $data)
    {
        $team = new Team();
        $team->owner_id = $user->id;
        $team->name = $data['company'];
        $team->save();

        $user->current_team_id = $team->id;
        $user->teams()->attach($team->id);
        $user->save();

        return $team;
    }

    private function createPaybill($user, $data, $team)
    {
//        $paybill = new PaybillNumber();
//        $paybill->name = $data['company'];
//        $paybill->number = $data['paybill_number'];
//        $paybill->user_id = $user->id;
//        $paybill->team_id = $team->id;
//        $paybill->save();

        return TRUE;
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function getRegister()
    {
        return redirect('login')->withError('Registration has been disabled, please contact Mookh (hello@mymookh.com) to create you an account');
    }

    public function showRegistrationForm()
    {
        return redirect('login')->withError('Registration has been disabled, please contact Mookh (hello@mymookh.com) to create you an account');
    }

    public function postRegister()
    {
        return redirect('login')->withError('Registration has been disabled, please contact Mookh (hello@mymookh.com) to create you an account');
    }


    public function logout(Request $request, $id=null)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        if ($id) {
           return redirect()->route('org.view.event',[$id]);
        }
        return redirect('/');
    }

}
