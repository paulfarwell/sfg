<?php

/**
 * Description of ShortenText
 *
 * @author Ansel Melly <ansel@anselmelly.com>
 * @date Aug 23, 2015
 * @link http://www.anselmelly.com
 */

namespace App\Http\Helpers;

class ShortenText {

    public static function shorten($string, $length) {
        // By default, an ellipsis will be appended to the end of the text.
        $suffix = '&hellip;';

        // Convert 'smart' punctuation to 'dumb' punctuation, strip the HTML tags,
        // and convert all tabs and line-break characters to single spaces.
        $short_desc = trim(str_replace(array("\r", "\n", "\t"), ' ', strip_tags($string)));

        // Cut the string to the requested length, and strip any extraneous spaces 
        // from the beginning and end.
        $desc = trim(substr($short_desc, 0, $length));

        // Find out what the last displayed character is in the shortened string
        $lastchar = substr($desc, -1, 1);

        // If the last character is a period, an exclamation point, or a question 
        // mark, clear out the appended text.
        if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?')
            $suffix = '';

        // Append the text.
        $desc .= $suffix;

        // Send the new description back to the page.
        return $desc;
    }

}
