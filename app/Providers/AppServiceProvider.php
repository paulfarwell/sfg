<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Models\Invitations;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

	    Invitations::deleting(function($inv)
	    {
		    // Delete all tricks that belong to this Invitation
//		    foreach ($inv->hotel as $trick) {
//			    $trick->delete();
//		    }
//		    isset($inv->transfer) ? $inv->transfer->delete() : null ;
//		    //foreach ($inv->transfer as $trick) {
//			    $trick->delete();
//		    //}
//		    foreach ($inv->inflight as $trick) {
//			    $trick->delete();
//		    }
//		    foreach ($inv->outflight as $trick) {
//			    $trick->delete();
//		    }
//		    foreach ($inv->payments as $trick) {
//			    $trick->delete();
//		    }
//		    foreach ($inv->refunds as $trick) {
//			    $trick->delete();
//		    }
//		    foreach ($inv->customdata as $trick) {
//			    $trick->delete();
//		    }
		    $inv->hotel()->delete();
		    $inv->transfer()->delete();
		    $inv->inflight()->delete();
		    $inv->outflight()->delete();
		    $inv->payments()->delete();
		    $inv->refunds()->delete();
		    $inv->customdata()->delete();
	    });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
        if ($this->app->environment() == 'local') {
            $this->app->register('Iber\Generator\ModelGeneratorProvider');
        }
    }
}
