<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 17/06/2017
 * Time: 17:43
 */

namespace App;
use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{
    protected $table = 'team';

    public $timestamps = true;

    protected $fillable = [
        'owner_id', 'name', 'city', 'state', 'postal',
        'website', 'email', 'phone', 'brief', 'logo'
    ];

    protected static $logAttributes = [
        'owner_id', 'name', 'city', 'state', 'postal',
        'website', 'email', 'phone', 'brief', 'logo'
    ];

    public function owner()
    {
        return $this->hasOne('App\User',  'owner_id','id');//, 'id', 'store'
    }

}