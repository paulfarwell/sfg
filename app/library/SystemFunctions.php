<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 14/09/2017
 * Time: 09:34
 */

namespace app\library;
use App\library\AfricasTalkingGateway;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Log;


class SystemFunctions
{
    function google_converter_currency($amount, $from, $to ){

        $url = "http://finance.google.com/finance/converter?a=$amount&from=$from&to=$to";

        $request = curl_init();
        $timeOut = 0;
        curl_setopt ($request, CURLOPT_URL, $url);
        curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
        $response = curl_exec($request);
        curl_close($request);

        $get = explode("<span class=bld>",$response);
        $get = explode("</span>",$get[1]);
        $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);

        //how to use
        //$rate = ($this->google_converter_currency(1, $event->currency,'USD'));
        //$amount_converted = round((($the_order->order_amount + ($this->convertion_percentage($the_order->order_amount)) ) * $rate), 2) ;

        return $converted_currency;
    }

    function convertion_percentage ($amount) {
        if ($amount <= 500){
            $inflate = 15/100 * $amount;
        }
        if ($amount > 500 && $amount <= 1000){
            $inflate = 12/100 * $amount;
        }
        if ($amount > 1000 && $amount <= 2500){
            $inflate = 10/100 * $amount;
        }
        if ($amount > 2500 && $amount <= 5000){
            $inflate = 8/100 * $amount;
        }
        if ($amount > 5000 && $amount <= 10000){
            $inflate = 7/100 * $amount;
        }
        if ($amount > 10000 ){
            $inflate = 6/100 * $amount;
        }
        return $inflate;
    }


    public function ValidateAndSendSms($numbers, $data, $type)
    {
        if (!$this->prepNumbers($numbers)) {
            return FALSE;
        }
        $recipients = $this->prepNumbers($numbers);
        $message = trans('messages.' . $type, $data);

        $result = self::sendMessage($recipients, $message);

        Log::alert('sending sms log : ' . json_encode($result));

        return $result;
    }

    public function prepNumbers($numbers)
    {
        $validNumbers = [];
        foreach ($numbers as $number) {
            if ($this->validatePhoneNumber($number)) {
                $validNumbers[] = $this->validatePhoneNumber($number);
            }
        }
        if (count($validNumbers) > 0) {
            Log::info(implode(",", $validNumbers));

            return implode(",", $validNumbers);
        }

        return FALSE;
    }

    public function sendMessage($recipients, $message)
    {
        $gateway = new AfricasTalkingGateway(config('services.africa.username'), config('services.africa.key'));
        try {
            //$results = $gateway->sendMessage($recipients, $message);
            $results = $gateway->sendMessage($recipients, $message,'MOOKH');
            $response = [];
            foreach ($results as $key => $result) {
                $response[ $key ]['number'] = $result->number;
                $response[ $key ]['status'] = $result->status;
            }
            return $response;
        } catch (Exception $e) {
            return ["Encountered an error while sending: " . $e->getMessage()];
        }

    }

    public function validatePhoneNumber($number)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $NumberProto = $phoneUtil->parse($number, "KE");
            $isValid = $phoneUtil->isValidNumber($NumberProto);
            if (!$isValid) {
                return FALSE;
            }
            $internationalNumber = $phoneUtil->format($NumberProto, PhoneNumberFormat::E164);

            return $internationalNumber;
            Log::alert($internationalNumber);
        } catch (NumberParseException $e) {
            return FALSE;
        }

    }

    public function validatePhoneNumber_NoPlus($number)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $NumberProto = $phoneUtil->parse($number, "KE");
            $isValid = $phoneUtil->isValidNumber($NumberProto);
            if (!$isValid) {
                return FALSE;
            }
            //return $NumberProto;
            $internationalNumber = $phoneUtil->format($NumberProto, PhoneNumberFormat::E164);
            return str_replace("+","",$internationalNumber);
            //Log::alert($internationalNumber);
        } catch (NumberParseException $e) {
            return FALSE;
        }

    }

}
