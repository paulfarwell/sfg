<?php
/**
 * Created by PhpStorm.
 * User: josin
 * Date: 18/08/2017
 * Time: 13:57
 */

namespace app\Helpers;

use Request;
use Spatie\Activitylog\Models\Activity;

trait DetailsLog
{
    public function setItems(){
       return  Activity::saving(function (Activity $activity) {
            $activity->properties->put('url', Request::fullUrl());
            $activity->properties->put('ip_address', Request::ip());
            $activity->properties->put('method', Request::method());
            $activity->properties->put('agent',Request::header('user-agent'));
        });
    }
}