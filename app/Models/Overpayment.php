<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class Overpayment extends Model
{
    protected $table = 'overpayment';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'payment_date', 'delegation_id', 'payment_id', 'amount', 'description',
        'status', 'refund_id', 'created_by','overpayment_group'
    ];

    protected static $logAttributes = [
        'payment_date', 'delegation_id', 'payment_id', 'amount', 'description',
        'status', 'refund_id', 'created_by','overpayment_group'
    ];

    protected $guarded = [];

    public function payment()
    {
        return $this->hasOne('App\Models\Payments', 'id', 'payment_id');//, 'id', 'store'
    }

    public function refunds()
    {
        return $this->HasMany('App\Models\Refunds', 'id', 'payment_id');//, 'id', 'store'
    }
}