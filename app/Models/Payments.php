<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use app\Helpers\DetailsLog;

/**
 * Class Payments
 */
class Payments extends Model
{
    protected $table = 'payments';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'payment_date', 'event_id', 'invitation_id', 'delegation_id',
        'parent_id', 'payment_group', 'pay_method', 'details', 'amount', 'reciept_number',
        'created_by', 'status'
    ];

    protected static $logAttributes = [
        'payment_date', 'event_id', 'invitation_id', 'delegation_id',
        'parent_id', 'payment_group', 'pay_method', 'details', 'amount', 'reciept_number',
        'created_by', 'status'
    ];

    protected $guarded = [];

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'id', 'invitation_id');//, 'id', 'store'
    }

    public function refunds()
    {
        return $this->HasMany('App\Models\Refunds', 'id', 'payment_id');//, 'id', 'store'
    }

    public function overpayment()
    {
        return $this->HasMany('App\Models\Overpayment', 'id', 'payment_id');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function scopeActive($query)
    {
        return $query->where('status','=' , 0);
    }

    public function scopeSingle($query)
    {
        return $query->where('payment_group','=' , 0);
    }

}