<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use Route;
//use Closure;

/**
 * Class DelegateProfile
 */
class DelegateProfile extends Model
{
    protected $table = 'delegate_profile';
    protected $appends = array('completion','full_name');
    //protected $appends = [];
    public $timestamps = true;
    use LogsActivity;

    //protected $appends = array('idurl','photourl');

    protected $fillable = [
        'title', 'fname', 'surname', 'organisation', 'position', 'country', 'nationality',
        'national_id', 'national_id_type', 'national_id_image', 'email','cc_email', 'cell_phone',
        'cell_office', 'passport_image', 'a_contact_name', 'a_contact_email', 'a_contact_phone',
        'a_contact_office_phone', 'created_by', 'user_id','membership_email','membership_number','membership_telephone',
        'membership_phone_code','media','interview','insurance_provider','medical_conditions','residential','language','date_expire','date_issue'
    ];

    protected static $logAttributes = [
        'title', 'fname', 'surname', 'organisation', 'position', 'country', 'nationality',
        'national_id', 'national_id_type', 'national_id_image', 'email','cc_email', 'cell_phone',
        'cell_office', 'passport_image', 'a_contact_name', 'a_contact_email', 'a_contact_phone',
        'a_contact_office_phone', 'created_by', 'user_id'
    ];

    protected $guarded = [];

    public function scopeFull($query) {
        return $query->where('completion','=',100);
    }

    function getFullNameAttribute() {
        return $this->fname . ' ' . $this->surname;
    }

    public function user_account()
    {
        return $this->hasOne('App\User', 'id', 'user_id');//, 'id', 'store'
    }

    public function invitations()
    {
        return $this->hasMany('App\Models\Invitations', 'delegate_profile', 'id');//, 'id', 'store'
    }

    public function getCompletionAttribute()
    {
        return $this->calculateCompletion();
    }

    public function calculateCompletion()
    {
      // dd($this);
        $completed = 0;
        $profileElements = [
          'title',
          'fname',
          'surname',
          'organisation',
          'position',
          'country',
          'nationality',
          'national_id',
          'national_id_type',
          'email',
          'cell_phone',
          'residential',
          // 'a_contact_name',
          // 'a_contact_email',
          // 'a_contact_phone',
          // 'membership_number',
          // 'membership_email',
          // 'membership_telephone',
          // 'insurance_provider'
          // 'passport_image'
        ];
        $total = count($profileElements);
        foreach($profileElements as $element) {

            $completed += empty($this->{$element}) ? 0 : 1;
        }
        // var_dump($completed);
        // var_dump($total);
        return round($completed / $total * 100); // 0.8 for 8/10 elements completed
    }

//    public function getPhotourlAttribute(){
//        // same thing as above for calling the url. Because $this->avatar will result in /storage/avatar/asduioajsdsd.png So it could lead to stripping some things of the string. Just wanted to give you the general idea.
//        //return route('org.files',str_replace('/','~',$this->passport_image));
//        return url($this->passport_image);
//        //,str_replace('/','#',$this->passport_image)); //url('files/' . str_replace('/','~',$this->passport_image));
//    }

//    public function getIdurlAttribute()
//    {
//        //return route('org.files',str_replace('/','~',$this->national_id_image));
//        return url($this->national_id_image);
//        //,str_replace('/','#',$this->national_id_image)); //url('files/' . str_replace('/','~',$this->national_id_image));
//    }

}
