<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventProgramme
 */
class EventProgramme extends Model
{
  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    protected $table = 'event_programme';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'title',
        'p_type',
        'event_id',
        'description',
        'start',
        'end',
        'created_by'
    ];

    protected static $logAttributes = [
        'title',
        'p_type',
        'event_id',
        'description',
        'start',
        'end',
        'created_by'
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages', 'id', 'r_id')->where('r_type','=','event_programme');//, 'id', 'store'
    }
}
