<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class PasswordReset
 */
class PasswordReset extends Model
{
    protected $table = 'password_resets';
    use LogsActivity;

    public $timestamps = true;

    protected $fillable = [
        'email',
        'token'
    ];

    protected static $logAttributes = [
        'email',
        'token'
    ];

    protected $guarded = [];

        
}