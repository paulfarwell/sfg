<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class invitationsActivity
 */
class InvitationFlightOut extends Model
{
    protected $table = 'invitation_flights_out';
    protected $appends = array('completion');
    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'invitation_id', 'flight_type', 'flight_number', 'flight_carrier',
        'airport_name', 'terminal', 'departure_date', 'departure_time',
        'created_by', 'plane_reg', 'transfer_req'
    ];

    protected static $logAttributes = [
        'invitation_id', 'flight_type', 'flight_number', 'flight_carrier',
        'airport_name', 'terminal', 'departure_date', 'departure_time',
        'created_by', 'plane_reg', 'transfer_req'
    ];

    protected $guarded = [];

//    public function profile()
//    {
//        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
//    }
//
//    public function delegateType()
//    {
//        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');//, 'id', 'store'
//    }
//
//    public function delegation()
//    {
//        return $this->hasOne('App\Models\Delegation', 'id', 'delegation_id');//, 'id', 'store'
//    }
//
//    public function event()
//    {
//        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
//    }
//
//    public function user()
//    {
//        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
//    }

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'id', 'invitation_id');//, '', 'store'
    }

    public function getCompletionAttribute()
    {
        return $this->calculateCompletion();
    }

    public function calculateCompletion()
    {
        $completed = 0;
        $profileElements = [
          'invitation_id',
          'flight_type',
          'flight_number',
          'flight_carrier',
          'airport_name',
          'terminal',
          'departure_date',
          'departure_time',
          'created_by',
          // 'plane_reg',
          'transfer_req'];
        $total = count($profileElements);
        foreach($profileElements as $element) {
            $completed += empty($this->{$element}) ? 0 : 1;
        }
        return round($completed / $total * 100,0); // 0.8 for 8/10 elements completed
    }

}
