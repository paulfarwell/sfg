<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class DelegationMeta
 */
class DelegationMeta extends Model
{
    protected $table = 'delegation_restrictions';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'delegation_id', 'data_type', 'delegate_id', 'allowed_count', 'created_by', 'status'
    ];

    protected static $logAttributes = [
        'delegation_id', 'data_type', 'delegate_id', 'allowed_count', 'created_by', 'status'
    ];

    protected $guarded = [];

        
}