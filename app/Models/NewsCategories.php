<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class NewsCategories extends Model
{

  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}
    protected $table = 'news_categories';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name','status','order_custom'
    ];

    protected static $logAttributes = [
        'name','status','order_custom'
    ];

    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status','Active')->orderBy('order_custom', 'asc');
    }
}
