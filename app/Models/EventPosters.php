<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventPosters
 */
class EventPosters extends Model
{
    protected $table = 'event_posters';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'image_path', 'is_main', 'sort','created_by'
    ];

    protected static $logAttributes = [
        'event_id', 'image_path', 'is_main', 'sort','created_by'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'envent_id', 'id');
    }
        
}