<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class SysImages
 */
class SysImages extends Model
{
    protected $table = 'system_images';
    protected $appends = array('image_urls');
    use LogsActivity;

    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp'
    ];

    public static $messages = [
        'file.mimes' => 'Uploaded file is not in image format',
        'file.required' => 'Image is required'
    ];

    protected static $logAttributes = [
        'r_id','filename',
        'r_type',
        'image_path',
        'is_main',
        'sort',
        'created_by',
    ];

    public $timestamps = true;

    protected $fillable = [
        'r_id','filename',
        'r_type',
        'image_path',
        'is_main',
        'sort',
        'created_by',
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function getImageUrlsAttribute()
    {
        return url($this->image_path);
    }

//    public function record()
//    {
//        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
//    }

}