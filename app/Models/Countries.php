<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use app\Helpers\DetailsLog;

/**
 * Class Countries
 */
class Countries extends Model
{
    protected $table = 'countries';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'code', 'tel_code', 'name', 'status'
    ];

    protected static $logAttributes = [
        'code', 'tel_code', 'name', 'status'
    ];

    protected $guarded = [];

    public function details()
    {
        return $this->hasOne('App\Models\CountriesAdvanced', 'code', 'code');//, 'id', 'store'
    }

}