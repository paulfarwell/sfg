<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventsTickets
 */
class EventsTickets extends Model
{
    public function __construct(array $attributes = array())
  {
    parent::__construct($attributes);
    if(\Config::get('app.locale') == 'en'){
        $this->setConnection('mysql');
      }
    elseif(\Config::get('app.locale') == 'fr'){
        $this->setConnection('tenant');
    }
  }

    protected $table = 'event_tickets';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'delegate_id', 'name', 'price',
        'ticket_number', 'minimum_level', 'email_low_level', 'is_season', 'usage_count',
        'valid_from', 'valid_to', 'notes',
        'no_complimentary', 'is_deleted', 'active', 'available_from',
        'available_to', 'pos', 'max_forsale','created_by'
    ];

    protected static $logAttributes = [
        'event_id', 'delegate_id', 'name', 'price',
        'ticket_number', 'minimum_level', 'email_low_level', 'is_season', 'usage_count',
        'valid_from', 'valid_to', 'notes',
        'no_complimentary', 'is_deleted', 'active', 'available_from',
        'available_to', 'pos', 'max_forsale','created_by'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'event_id', 'id');
    }

    public function delegates()
    {
        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');
    }

}
