<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventPoster
 */
class EventPoster extends Model
{
    protected $table = 'event_posters';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id',
        'directory',
        'file_name',
        'is_main'
    ];

    protected static $logAttributes = [
        'event_id',
        'directory',
        'file_name',
        'is_main'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'envent_id', 'id');
    }
        
}