<?php

namespace App; namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tent extends Model
{
    //
    protected $table ='tents';

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages',  'r_id','id')->where('r_type','=','event_tent');//, 'id', 'store'
    }

}
