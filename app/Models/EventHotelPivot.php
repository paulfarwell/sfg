<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventHotel
 */
class EventHotelPivot extends Model
{
    protected $table = 'events_hotels_pivot';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id',	'hotel_id',	'created_by','coupon'
    ];

    protected static $logAttributes = [
        'event_id',	'hotel_id',	'created_by','coupon'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');
    }

    public function hotel()
    {
        return $this->hasOne('App\Models\EventHotel', 'id', 'hotel_id');
    }

    public function delegate()
    {
        return $this->hasMany('App\Models\EventHotelDelegatePivot', 'hotel_pivot', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages', 'r_id', 'id')->where('r_type','=','event_hotel');//, 'id', 'store'
    }

}