<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Delegation
 */
class Delegation extends Model
{
    protected $table = 'delegation';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'name', 'country', 'created_by', 'type', 'contact_email','cc_contact_email', 'contact_name', 'status'
    ];

    protected static $logAttributes = [
        'event_id', 'name', 'country', 'created_by', 'type', 'contact_email', 'cc_contact_email', 'contact_name', 'status'
    ];

    protected $guarded = [];

    public function restrictions()
    {
        return $this->hasMany('App\Models\DelegationMeta', 'delegation_id', 'id');
    }

    public function invitations()
    {
        return $this->hasMany('App\Models\Invitations', 'delegation_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function primaryprofile()
    {
        return $this->hasOne('App\Models\DelegateProfile', 'email', 'contact_email');//, 'id', 'store'
    }

    public function primaryuser()
    {
        return $this->hasOne('App\User', 'contact_email', 'email');//, 'id', 'store'
    }

    public function delegationtype()
    {
        return $this->hasOne('App\Models\DelegationsType', 'id', 'type');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function countryname()
    {
        return $this->hasOne('App\Models\Countries', 'code', 'country');//, 'id', 'store'
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payments', 'delegation_id', 'id')
            ->where('payment_group','=',1)
            ->where('status','=',0);//, 'id', 'store'
    }

    public function overpayments()
    {
        return $this->hasMany('App\Models\Overpayment', 'delegation_id', 'id')
            ->where('overpayment_group','=',1)
            ->where('status','=',0);//, 'id', 'store'
    }

    public function scopeActive($query)
    {
        return $query->where('delegation.status','=' , 0);
    }

}