<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use app\Helpers\DetailsLog;

/**
 * Class Countries
 */
class FlightRoutes extends Model
{
    protected $table = 'flight_routes';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'flight_number', 'arrival', 'departure', 'created_by'
    ];

    protected static $logAttributes = [
        'flight_number', 'arrival', 'departure', 'created_by'
    ];

    protected $guarded = [];

    public function dept()
    {
        return $this->hasOne('App\Models\FlightAirports', 'code', 'departure');//, 'id', 'store'
    }

    public function arrv()
    {
        return $this->hasOne('App\Models\FlightAirports', 'code', 'arrival');//, 'id', 'store'
    }

}