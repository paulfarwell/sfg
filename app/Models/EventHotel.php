<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventHotel
 */
class EventHotel extends Model
{
  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    protected $table = 'event_hotels';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name',
        'description',
        'website',
        'contact_person',
        'contact_phone',
        'contact_email',
        'address',
        'longitude',
        'latitude',
        'available',
        'created_by','coupon','order_custom'
    ];

    protected static $logAttributes = [
        'name',
        'description',
        'website',
        'contact_person',
        'contact_phone',
        'contact_email',
        'address',
        'longitude',
        'latitude',
        'available',
        'created_by','coupon','order_custom'
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages',  'r_id','id')->where('r_type','=','event_hotel');//, 'id', 'store'
    }

}
