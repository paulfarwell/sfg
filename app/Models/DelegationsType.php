<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class DelegatesType
 */
class DelegationsType extends Model
{
    protected $table = 'delegations_type';

    public $timestamps = true;

    use LogsActivity;

    protected $fillable = [
        'name', 'internal_name', 'description', 'created_by', 'status'
    ];

    protected static $logAttributes = [
        'name', 'internal_name', 'description', 'created_by', 'status'
    ];

    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status',0);
    }

    public function upload()
    {
        return $this->hasMany('App\Models\EventDelegationUploads', 'delegation_id', 'id');//, 'id', 'store'
    }
}