<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Delegation
 */
class EventDelegationUploads extends Model
{
    protected $table = 'event_uploads_per_delegations';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name','description','event_id', 'delegation_id', 'file', 'created_by', 'status'
    ];

    protected static $logAttributes = [
        'name','description','event_id', 'delegation_id', 'file', 'created_by', 'status'
    ];

    protected $guarded = [];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function delegationtype()
    {
        return $this->hasOne('App\Models\DelegationsType', 'id', 'delegation_id');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }
}