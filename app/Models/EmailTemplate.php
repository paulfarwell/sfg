<?php

namespace App; namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    //
    public function __construct(array $attributes = array())
  {
    parent::__construct($attributes);
    if(\Config::get('app.locale') == 'en'){
        $this->setConnection('mysql');
      }
    elseif(\Config::get('app.locale') == 'fr'){
        $this->setConnection('tenant');
    }
  }

    protected $table ='email_template';

    protected $fillable = [
        'name','content', 'status',
    ];

}
