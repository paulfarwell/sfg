<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class EventCustomFields extends Model
{
  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    protected $table = 'event_custom_fields';
    protected $appends = array('delegates_names');

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'field_title', 'field_description', 'delegate_type_id','field_type', 'created_by', 'status'
    ];

    protected static $logAttributes = [
        'event_id', 'field_title', 'field_description', 'delegate_type_id','field_type', 'created_by', 'status'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function getDelegatesNamesAttribute()
    {
        return (\DB::table("event_custom_fields")
            ->select(\DB::raw("GROUP_CONCAT(delegates_types.name) as delegates_names"))
            ->where('event_custom_fields.id','=',$this->id)
            ->leftjoin("delegates_types",\DB::raw("FIND_IN_SET(delegates_types.id,event_custom_fields.delegate_type_id)"),">",\DB::raw("'0'"))
            ->groupBy('event_custom_fields.id')
            ->pluck('delegates_names'));
    }

}
