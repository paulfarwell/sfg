<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class invitationsActivity
 */
class InvitationTransferPackages extends Model
{
    protected $table = 'invitation_tpackage';
    public $timestamps = true;


    use LogsActivity;

    protected $fillable = [
        'invitation_id', 'tpackage_id', 'arrival_date', 'departure_date', 'created_by'
    ];

    protected static $logAttributes = [
        'invitation_id', 'tpackage_id', 'arrival_date', 'departure_date', 'created_by'
    ];

    protected $guarded = [];

//    public function profile()
//    {
//        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
//    }
//
//    public function delegateType()
//    {
//        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');//, 'id', 'store'
//    }
//
//    public function delegation()
//    {
//        return $this->hasOne('App\Models\Delegation', 'id', 'delegation_id');//, 'id', 'store'
//    }
//
//    public function event()
//    {
//        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
//    }
//
//    public function user()
//    {
//        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
//    }



    public function package()
    {
        return $this->hasOne('App\Models\EventTransport', 'id', 'tpackage_id');//, '', 'store'
    }

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'id', 'invitation_id');//, '', 'store'
    }

}