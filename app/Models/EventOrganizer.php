<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventOrganizer
 */
class EventOrganizer extends Model
{

  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    protected $table = 'event_organizers';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id',
        'name',
        'logo',
        'website','brief','created_by','status'
    ];

    protected static $logAttributes = [
        'event_id',
        'name',
        'logo',
        'website','brief','created_by','status'
    ];

    protected $guarded = [];


}
