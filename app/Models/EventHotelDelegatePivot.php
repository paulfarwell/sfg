<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventHotel
 */
class EventHotelDelegatePivot extends Model
{
  
    protected $table = 'delegates_hotels_pivot';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'hotel_id', 'delegate_id', 'created_by','hotel_pivot'
    ];

    protected static $logAttributes = [
        'event_id', 'hotel_id', 'delegate_id', 'created_by','hotel_pivot'
    ];

    protected $guarded = [];

    public function type()
    {
        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');
    }

    public function hotel()
    {
        return $this->hasOne('App\Models\EventHotel', 'id', 'hotel_id')->orderBy('order_custom');
    }

}
