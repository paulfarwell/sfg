<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\DelegatesType;
use LaravelCustomRelation\HasCustomRelations;
use DB;
/**
 * Class EventActivity
 */
class EventActivity extends Model
{

  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    use HasCustomRelations;

    protected $table = 'event_activities';
    protected $appends = array('delegates_names');

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id',
        'name',
        'description',
        'location',
        'valid_from',
        'valid_to',
        'cost',
        'delegate',
        'status','created_by'
    ];

    protected static $logAttributes = [
        'event_id',
        'name',
        'description',
        'location',
        'valid_from',
        'valid_to',
        'cost',
        'delegate',
        'status','created_by'
    ];

    protected $guarded = [];


    public function getDelegatesNamesAttribute()
    {
        return (\DB::table("event_activities")
            ->select(\DB::raw("GROUP_CONCAT(delegates_types.name) as delegates_names"))
            ->where('event_activities.id','=',$this->id)
            ->leftjoin("delegates_types",\DB::raw("FIND_IN_SET(delegates_types.id,event_activities.delegate)"),">",\DB::raw("'0'"))
            ->groupBy('event_activities.id')
            ->pluck('delegates_names'));
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages', 'r_id', 'id')->where('r_type','=','event_act');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');
    }


}
