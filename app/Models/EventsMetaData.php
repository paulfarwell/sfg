<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventsMetaData
 */
class EventsMetaData extends Model
{
    protected $table = 'events_meta_data';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'data'
    ];

    protected static $logAttributes = [
        'data'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'envent_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status',0);
    }
        
}