<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use App\Models\ActivityLog;

/**
 * Class EventsMeta
 */
class EventsMeta extends Model
{
    protected $table = 'events_meta';
    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'field_name', 'field_title', 'created_by', 'content_title', 'content_description'
    ];

    protected static $logAttributes = [
        'event_id', 'field_name', 'field_title', 'created_by', 'content_title', 'content_description'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasMany('App\Models\Event', 'envent_id', 'id');
    }


        
}