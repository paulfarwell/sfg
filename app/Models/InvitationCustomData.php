<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class InvitationCustomData extends Model
{
    protected $table = 'invitation_custom_fields';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'invitation_id', 'delegate_id', 'field_id', 'value', 'created_by'
    ];

    protected static $logAttributes = [
        'invitation_id', 'delegate_id', 'field_id', 'value', 'created_by'
    ];

    protected $guarded = [];

    public function invitation()
    {
        return $this->hasOne('App\Models\Event', 'id', 'invitation_id');//, 'id', 'store'
    }

    public function profile()
    {
        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_id');//, 'id', 'store'
    }

    public function field()
    {
        return $this->hasOne('App\Models\EventCustomFields', 'id', 'field_id');//, 'id', 'store'
    }
        
}