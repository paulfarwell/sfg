<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class PaymentMethods extends Model
{
    protected $table = 'payment_method';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name', 'description', 'created_by'
    ];

    protected static $logAttributes = [
        'name', 'description', 'created_by'
    ];

    protected $guarded = [];

        
}