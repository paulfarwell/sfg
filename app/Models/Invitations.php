<?php

namespace App\Models;

//use function at;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
Use DB;

/**
 * Class invitations
 */
class Invitations extends Model
{
    protected $table = 'invitations';
    protected $appends = array('completion','totalBill','owing');
    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'id', 'event_id', 'delegation_id', 'delegate_id', 'code', 'delegate_profile',
        'tech_requirements', 'food_requirements', 'vehicle_pass', 'vehicle_details',
        'created_by', 'created_at', 'updated_at','sms_sent','email_sent','status','flight_type'
    ];

    protected static $logAttributes = [
        'id', 'event_id', 'delegation_id', 'delegate_id', 'code', 'delegate_profile',
        'tech_requirements', 'food_requirements', 'vehicle_pass', 'vehicle_details',
        'created_by', 'created_at', 'updated_at','sms_sent','email_sent','status','flight_type'
    ];

    protected $guarded = [];


    public function profile()
    {
        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
    }

//    public function profileFull()
//    {
//        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
//    }
//
//    public function profileHalf()
//    {
//        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
//    }

    public function delegateType()
    {
        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');//, 'id', 'store'
    }

    public function delegatePrice()
    {
        return $this->hasOne('App\Models\EventsTickets', 'delegate_id', 'delegate_id');//, 'id', 'store'
    }

    public function hotel()
    {
        return $this->hasOne('App\Models\InvitationAccomodation', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function transfer()
    {
        return $this->hasOne('App\Models\InvitationTransferPackages', 'invitation_id', 'id');//, 'id', 'store'
    }
    public function transfer_return()
    {
        return $this->hasOne('App\Models\InvitationTransportReturn', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function activity ()
    {
        return $this->hasMany('App\Models\InvitationActivity', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function delegation()
    {
        return $this->hasOne('App\Models\Delegation', 'id', 'delegation_id');//, 'id', 'store'
    }

    public function inflight()
    {
        return $this->hasOne('App\Models\InvitationFlightIn', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function outflight()
    {
        return $this->hasOne('App\Models\InvitationFlightOut', 'invitation_id', 'id');//, 'id', 'store'
    }
    public function tent_accomodation()
    {
        return $this->hasOne('App\Models\InvitationTentAccomodation', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payments', 'invitation_id', 'id')
            ->where('payment_group','=',0)
            ->where('status','=',0);//, 'id', 'store'
    }

    public function refunds()
    {
        return $this->hasMany('App\Models\Refunds', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function customdata()
    {
        return $this->hasMany('App\Models\InvitationCustomData', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function mediadata()
    {
        return $this->hasMany('App\Models\InvitationMedia', 'invitation_id', 'id');//, 'id', 'store'
    }

    public  function getTotalBillAttribute(){
        return $this->totalBill();
    }

    public  function getOwingAttribute(){
        return $this->owing();
    }

    public function totalBill() {
        //$badge = $this->hasOne('App\Models\EventsTickets', 'delegate_id', 'delegate_id')->select('cost');
        //$act = $this->hasMany('App\Models\InvitationActivity', 'invitation_id', 'id')->select('activity.cost');
        //dd($badge);

         if($this->complimentary ==1){
           return 0;
         }else{
           return ( isset($this->delegatePrice) ? $this->delegatePrice->price : 0 )
           +
           ( isset($this->activity) ?  $this->activity->sum('activity.cost') : 0 )
           +
           ( isset($this->transfer) ? $this->transfer->package->cost : 0 )
           + (isset($this->transfer_return) ? $this->transfer_return->package_return->cost : 0 )
           + (isset($this->tent_accomodation) ? $this->tent_accomodation->tent->price :0 )
           ;

         }

    }

    public function owing () {
    	return $this->totalBill() - (isset($this->payments) ? $this->payments->sum('amount') : 0 );
    }

    public function getCompletionAttribute()
    {
        return $this->calculateCompletions();
    }

    public function calculateCompletion($id)
    {
      $form = DB::table('booking_form')->where('event_id','=',$id)->first();
      if($form->transfers == NULL){
        $profile = $this->profile->calculateCompletion() == 100 ? (20) :  (round( (($this->profile->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP));

        $hotel = $this->hotel()->count() > 0 ?  20 :  0;
        // $transfer = $this->transfer()->count() > 0 ?  14.3 :  0;
        // $activity = $this->transfer_return()->count() > 0 ?  14.3 :  0;
         $tent = $this->activity()->count() > 0 ?  20 :  0;
        //$inflight = $this->inflight()->count() > 0 ?  14.3 :  0;
        //$inflight = $this->inflight()->count() > 0 && $this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1));
        $inflight = $this->inflight()->count() > 0 ? ($this->inflight->calculateCompletion() == 100 ? (20) :  (round( (($this->inflight->calculateCompletion()*20)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
        //$outflight = $this->outflight()->count() > 0 ?  14.3 :  0;
        //$outflight = $this->outflight()->count() > 0 && $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1));
        $outflight = $this->outflight()->count() > 0 ? ( $this->outflight->calculateCompletion() == 100 ? (20) :  (round( (($this->outflight->calculateCompletion()*20)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
          return round(($profile+$hotel+$inflight+$outflight+$tent),0,PHP_ROUND_HALF_UP);
      }
      else if($form->tents == NUll){

        $profile = $this->profile->calculateCompletion() == 100 ? (14.3) :  (round( (($this->profile->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP));

        $hotel = $this->hotel()->count() > 0 ?  14.3 :  0;
        $transfer = $this->transfer()->count() > 0 ?  14.3 :  0;
        $activity = $this->transfer_return()->count() > 0 ?  14.3 :  0;
         $tent = $this->activity()->count() > 0 ?  14.3 :  0;
        //$inflight = $this->inflight()->count() > 0 ?  14.3 :  0;
        //$inflight = $this->inflight()->count() > 0 && $this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1));
        $inflight = $this->inflight()->count() > 0 ? ($this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
        //$outflight = $this->outflight()->count() > 0 ?  14.3 :  0;
        //$outflight = $this->outflight()->count() > 0 && $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1));
        $outflight = $this->outflight()->count() > 0 ? ( $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
          return round(($profile+$hotel+$transfer+$activity+$inflight+$outflight+$tent),0,PHP_ROUND_HALF_UP);
         // var_dump($profile);
         // var_dump($hotel);
         // var_dump($transfer);
         // var_dump($activity);
         // var_dump($inflight);
         // var_dump($outflight);
         // var_dump($tent);
         // die;
       }
       else{
         $profile = $this->profile->calculateCompletion() == 100 ? (14.3) :  (round( (($this->profile->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP));

         $hotel = $this->hotel()->count() > 0 ?  14.3 :  0;
         $transfer = $this->transfer()->count() > 0 ?  14.3 :  0;
         $activity = $this->transfer_return()->count() > 0 ?  14.3 :  0;
          $tent = $this->tent_accomodation()->count() > 0 ?  14.3 :  0;
         //$inflight = $this->inflight()->count() > 0 ?  14.3 :  0;
         //$inflight = $this->inflight()->count() > 0 && $this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1));
         $inflight = $this->inflight()->count() > 0 ? ($this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
         //$outflight = $this->outflight()->count() > 0 ?  14.3 :  0;
         //$outflight = $this->outflight()->count() > 0 && $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1));
         $outflight = $this->outflight()->count() > 0 ? ( $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
           return round(($profile+$hotel+$transfer+$activity+$inflight+$outflight+$tent),0,PHP_ROUND_HALF_UP);
       }
    }


    public function calculateCompletions()
    {

      $profile = $this->profile->calculateCompletion() == 100 ? (20) :  (round( (($this->profile->calculateCompletion()*14.3)/100) ,1,PHP_ROUND_HALF_UP));

      $hotel = $this->hotel()->count() > 0 ?  20 :  0;
      // $transfer = $this->transfer()->count() > 0 ?  14.3 :  0;
      // $activity = $this->transfer_return()->count() > 0 ?  14.3 :  0;
       $tent = $this->activity()->count() > 0 ?  20 :  0;
      //$inflight = $this->inflight()->count() > 0 ?  14.3 :  0;
      //$inflight = $this->inflight()->count() > 0 && $this->inflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->inflight->calculateCompletion()*14.3)/100) ,1));
      $inflight = $this->inflight()->count() > 0 ? ($this->inflight->calculateCompletion() == 100 ? (20) :  (round( (($this->inflight->calculateCompletion()*20)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
      //$outflight = $this->outflight()->count() > 0 ?  14.3 :  0;
      //$outflight = $this->outflight()->count() > 0 && $this->outflight->calculateCompletion() == 100 ? (14.3) :  (round( (($this->outflight->calculateCompletion()*14.3)/100) ,1));
      $outflight = $this->outflight()->count() > 0 ? ( $this->outflight->calculateCompletion() == 100 ? (20) :  (round( (($this->outflight->calculateCompletion()*20)/100) ,1,PHP_ROUND_HALF_UP)) ) : 0;
        return round(($profile+$hotel+$inflight+$outflight+$tent),0,PHP_ROUND_HALF_UP);
         // var_dump($profile);
         // var_dump($hotel);
         // var_dump($transfer);
         // var_dump($activity);
         // var_dump($inflight);
         // var_dump($outflight);
         // var_dump($tent);
         // die;

    }
    public function scopeActive($query)
    {
        return $query->where('invitations.status','=' , 0);
    }

}
