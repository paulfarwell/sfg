<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class Titles extends Model
{
    protected $table = 'titles';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name','status','order_custom'
    ];

    protected static $logAttributes = [
        'name','status','order_custom'
    ];

    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status','Active')->orderBy('order_custom', 'asc');
    }
        
}