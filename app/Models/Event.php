<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Event
 */
class Event extends Model
{
    use Sluggable;
    //use SoftDeletes;
    use LogsActivity;

    public function __construct(array $attributes = array())
  {
    parent::__construct($attributes);
    if(\Config::get('app.locale') == 'en'){
        $this->setConnection('mysql');
      }
    elseif(\Config::get('app.locale') == 'fr'){
        $this->setConnection('tenant');
    }
  }

    protected $table = 'events';
    //protected $dates = ['deleted_at'];

    public $timestamps = true;

    protected $fillable = [
        'name', 'venue', 'start', 'end', 'theme', 'description',
        'coordinates', 'use_coordinates', 'status', 'is_deleted',
        'is_published', 'user_id', 'file_attachment', 'custom_message', 'udf', 'slug','logo','event_programme'
    ];

    protected static $logAttributes = [
        'name', 'venue', 'start', 'end', 'theme', 'description',
        'coordinates', 'use_coordinates', 'status', 'is_deleted',
        'is_published', 'user_id', 'file_attachment', 'custom_message', 'udf', 'slug'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $guarded = [];


    public function poster()
    {
        return $this->hasMany('App\Models\EventPoster', 'event_id', 'id');
    }

    public function hotels()
    {
        return $this->hasMany('App\Models\EventHotelPivot', 'event_id', 'id');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\EventActivity', 'event_id', 'id');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\EventsTickets', 'event_id', 'id');
    }

    public function tpackages()
    {
        return $this->hasMany('App\Models\EventTransport', 'event_id', 'id');
    }

    public function programmes()
    {
        return $this->hasMany('App\Models\EventProgramme', 'event_id', 'id');
    }

    public function news()
    {
        return $this->hasMany('App\Models\EventNews', 'event_id', 'id')
            ->where('category', '=', 'General Information')
            ->orWhere('category', '=', 'Accreditation Procedure')->orderBy('order_custom');
    }

    public function newsGeneral()
    {
        return $this->hasMany('App\Models\EventNews', 'event_id', 'id')
            ->where('category', '=', 'General Information')->orderBy('order_custom');
            //->orWhere('category', '=', 'Accreditation Procedure');
    }

    public function newsFull()
    {
        return $this->hasMany('App\Models\EventNews', 'event_id', 'id')->orderBy('order_custom') ;
    }

    public function newsPayment()
    {
        return $this->hasMany('App\Models\EventNews', 'event_id', 'id')
            ->where('category', '=', 'Fees & Payment');
       ;
    }

    public function sponsor()
    {
        return $this->hasMany('App\Models\EventSponsor', 'event_id', 'id')->orderBy('ordering');
    }

    public function organizer()
    {
        return $this->hasMany('App\Models\EventOrganizer', 'event_id', 'id');
    }

    public function meta()
    {
        return $this->hasMany('App\Models\EventsMeta', 'event_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');//, 'id', 'store'
    }

    public function map()
    {
        return $this->hasOne('App\Models\EventsMap', 'event_id', 'id');//, 'id', 'store'
    }

    public function scopeActive($query)
    {
        return $query->where('status', '=', 'active')->where('is_deleted', '=', '0');
    }

    public function delegations()
    {
        return $this->hasMany('App\Models\Delegation', 'event_id', 'id');//, 'id', 'store'
    }

    public function fields()
    {
        return $this->hasMany('App\Models\EventCustomFields', 'event_id', 'id');//, 'id', 'store'
    }

    public function terms_conditions()
    {
        return $this->hasMany('App\Models\TermsConditions', 'event_id', 'id');//, 'id', 'store'
    }
    public function tents()
    {
        return $this->hasMany('App\Models\Tent', 'event_id', 'id');//, 'id', 'store'
    }


}
