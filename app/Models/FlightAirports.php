<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use app\Helpers\DetailsLog;

/**
 * Class Countries
 */
class FlightAirports extends Model
{
    protected $table = 'flight_airports';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name', 'code', 'created_by'
    ];

    protected static $logAttributes = [
        'name', 'code', 'created_by'
    ];

    protected $guarded = [];

}