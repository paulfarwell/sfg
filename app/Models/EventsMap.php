<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventsMap
 */
class EventsMap extends Model
{
    protected $table = 'events_map';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'event_id', 'name', 'point_of_interest', 'lat', 'lng', 'location', 'location_type',
        'formatted_address', 'bounds', 'viewport', 'route', 'street_number', 'postal_code',
        'locality', 'sublocality', 'country', 'country_short', 'administrative_area_level_1',
        'place_id', 'id', 'reference', 'url', 'website', 'created_by'
    ];

    protected static $logAttributes = [
        'event_id', 'name', 'point_of_interest', 'lat', 'lng', 'location', 'location_type',
        'formatted_address', 'bounds', 'viewport', 'route', 'street_number', 'postal_code',
        'locality', 'sublocality', 'country', 'country_short', 'administrative_area_level_1',
        'place_id', 'id', 'reference', 'url', 'website', 'created_by'
    ];

    protected $guarded = [];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'envent_id', 'id');
    }
        
}