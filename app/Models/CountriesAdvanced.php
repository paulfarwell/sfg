<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Countries
 */
class CountriesAdvanced extends Model
{
    protected $table = 'countries_advanced';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name', 'topleveldomain', 'alpha2code', 'alpha3code', 'callingcodes',
        'capital', 'altspellings', 'region', 'subregion', 'population', 'latlng',
        'demonym', 'area', 'gini', 'timezones', 'borders', 'nativename', 'numericcode',
        'currencies', 'languages', 'translations', 'flag', 'regionalblocs', 'code'
    ];

    protected static $logAttributes = [
        'name', 'topleveldomain', 'alpha2code', 'alpha3code', 'callingcodes',
        'capital', 'altspellings', 'region', 'subregion', 'population', 'latlng',
        'demonym', 'area', 'gini', 'timezones', 'borders', 'nativename', 'numericcode',
        'currencies', 'languages', 'translations', 'flag', 'regionalblocs', 'code'
    ];

    protected $guarded = [];



}