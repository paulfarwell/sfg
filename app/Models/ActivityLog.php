<?php

namespace App\Models;

//use function explode;
use Request;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Collection;
//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\Builder;
//use Illuminate\Database\Eloquent\Relations\MorphTo;
//use app\Helpers\DetailsLog;
use Spatie\Activitylog\Models\Activity as Activity ;
//use function strtotime;

//use Activity;

/**
 * Class ActivityLog
 */
class ActivityLog extends Activity
{
    //const IP = Request::ip();
    //protected $table = 'activity_log';
    protected $appends = array('TableName');
    public $timestamps = true;

    protected $fillable = [
        'log_name', 'description', 'subject_id', 'subject_type', 'causer_id',
        'causer_type', 'text', 'properties', 'ip_address', 'url', 'method', 'agent'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setOthers( ) {
        $this->attributes['url'] = Request::fullUrl();
        $this->attributes['ip_address'] =Request::ip();
        $this->attributes['method'] = Request::method();
        $this->attributes['agent'] = Request::header('user-agent');
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model)
        {
            $model->setOthers();
        });
        static::updating(function($model){
            $model->setOthers();
        });
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'causer_id');
    }

    protected function getTableNameAttribute() {
        return last( explode("\\", $this->subject_type) );
    }

//    protected function getCreatedAtAttribute($value) {
//        $this->attributes['created_at'] = date('d-m-Y',strtotime($value));
//    }
    
}