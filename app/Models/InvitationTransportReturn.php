<?php

namespace App; namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class InvitationTransportReturn extends Model
{
    //
    protected $table = 'invitation_tpackage_return';
    public $timestamps = true;


    use LogsActivity;

    protected $fillable = [
        'invitation_id', 'tpackage_id', 'arrival_date', 'departure_date', 'created_by'
    ];

    protected static $logAttributes = [
        'invitation_id', 'tpackage_id', 'arrival_date', 'departure_date', 'created_by'
    ];

    public function package_return()
    {
        return $this->hasOne('App\Models\EventTransport', 'id', 'tpackage_id');//, '', 'store'
    }

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'id', 'invitation_id');//, '', 'store'
    }

}
