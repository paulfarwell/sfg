<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
//use app\Helpers\DetailsLog;

/**
 * Class Refunds
 */
class Refunds extends Model
{
    protected $table = 'refunds';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'payment_date', 'invitation_id', 'payment_id', 'event_id', 'details','delegation_id',
        'amount', 'reciept_number', 'payout_method', 'created_by', 'reason', 'refund_group', 'status',
    ];

    protected static $logAttributes = [
        'payment_date', 'invitation_id', 'payment_id', 'event_id', 'details','delegation_id',
        'amount', 'reciept_number', 'payout_method', 'created_by', 'reason', 'refund_group', 'status',
    ];

    protected $guarded = [];

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'invitation_id', 'id');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'event_id', 'id');//, 'id', 'store'
    }

    public function scopeActive($query)
    {
        return $query->where('status','=' , 0);
    }

}