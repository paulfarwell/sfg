<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Hotel
 */
class Hotel extends Model
{
    protected $table = 'event_hotels';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name',
        'description',
        'website',
        'contact_person',
        'contact_phone',
        'contact_email',
        'address',
        'longitude',
        'latitude',
        'available',
        'updated','coupon'
    ];

    protected static $logAttributes = [
        'name',
        'description',
        'website',
        'contact_person',
        'contact_phone',
        'contact_email',
        'address',
        'longitude',
        'latitude',
        'available',
        'updated','coupon'
    ];

    protected $guarded = [];

        
}