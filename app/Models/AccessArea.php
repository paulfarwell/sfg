<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class AccessArea
 */
class AccessArea extends Model
{
    protected $table = 'access_areas';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'area',
        'description',
        'delegate_id',
        'created_by'
    ];

    protected static $logAttributes = [
        'area',
        'description',
        'delegate_id',
        'created_by'
    ];

    protected $guarded = [];

        
}