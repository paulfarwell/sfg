<?php

namespace App; namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventDisclaimer extends Model
{
  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}
    //
      protected $table = 'event_disclaimer';

      protected $fillable = [
          'description','event_id', 'status',
      ];

      public function event()
      {
          return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
      }
}
