<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class EventNews
 */
class EventNews extends Model
{

  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}

    protected $table = 'event_news';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'title',
        'category',
        'text','event_id',
        'created_by',
        'status',
        'created_by','order_custom'
    ];

    protected static $logAttributes = [
        'title',
        'category',
        'text','event_id',
        'created_by',
        'status',
        'created_by','order_custom'
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
    }

    public function images()
    {
        return $this->hasMany('App\Models\SysImages', 'id', 'r_id')->where('r_type','=','event_news');//, 'id', 'store'
    }

//    public function limited ($value) {
//        return $this->directory();
//    }

    public function scopeLimited($query)
    {
        return $query->where('category', '=', 'General Information')->orWhere('category', '=', 'Accreditation Procedure');
    }

    public function scopeGeneral($query)
    {
        return $query->where('category', '=', 'General Information');
    }

}
