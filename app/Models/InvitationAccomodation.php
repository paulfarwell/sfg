<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class invitationsAccomodation
 */
class InvitationAccomodation extends Model
{
    protected $table = 'invitation_acc';
    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'invitation_id', 'hotel_id', 'arrival_date', 'departure_date', 'created_by','name'
    ];

    protected static $logAttributes = [
        'invitation_id', 'hotel_id', 'arrival_date', 'departure_date', 'created_by', 'name'
    ];

    protected $guarded = [];

//    public function profile()
//    {
//        return $this->hasOne('App\Models\DelegateProfile', 'id', 'delegate_profile');//, 'id', 'store'
//    }
//
//    public function delegateType()
//    {
//        return $this->hasOne('App\Models\DelegatesType', 'id', 'delegate_id');//, 'id', 'store'
//    }
//
//    public function delegation()
//    {
//        return $this->hasOne('App\Models\Delegation', 'id', 'delegation_id');//, 'id', 'store'
//    }
//
//    public function event()
//    {
//        return $this->hasOne('App\Models\Event', 'id', 'event_id');//, 'id', 'store'
//    }
//
//    public function user()
//    {
//        return $this->hasOne('App\User', 'id', 'created_by');//, 'id', 'store'
//    }

    public function hotel()
    {
        return $this->hasOne('App\Models\EventHotel', 'id', 'hotel_id');//, 'id', 'store'
    }

    public function otherhotel()
    {
        return $this->hasOne('App\Models\InvitationAccomodationDetails', 'invitation_id', 'invitation_id');//, 'id', 'store'
    }

}