<?php

namespace App; namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class InvitationTentAccomodation extends Model
{
    //
    protected $table = 'invitation_tent_accomodation';
    public $timestamps = true;


    use LogsActivity;

    protected $fillable = [
        'event_id','invitation_id', 'tent_id', 'bed_type', 'tent_sharing_name', 'created_by'
    ];

    protected static $logAttributes = [
        'event_id','invitation_id', 'tent_id', 'bed_type', 'tent_sharing_name', 'created_by'
    ];

    public function tent()
    {
        return $this->hasOne('App\Models\Tent', 'id', 'tent_id');//, '', 'store'
    }

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitations', 'id', 'invitation_id');//, '', 'store'
    }
}
