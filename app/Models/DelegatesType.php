<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Builder;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class DelegatesType
 */
class DelegatesType extends Model
{

  public function __construct(array $attributes = array())
{
  parent::__construct($attributes);
  if(\Config::get('app.locale') == 'en'){
      $this->setConnection('mysql');
    }
  elseif(\Config::get('app.locale') == 'fr'){
      $this->setConnection('tenant');
  }
}
    protected $table = 'delegates_types';

    public $timestamps = true;
    use LogsActivity;

    protected $fillable = [
        'name', 'description', 'color', 'color_hexcode', 'color_hexcode_sec','ordering',
        'parent_group', 'status', 'created_by', 'per_delegation', 'image_path',
    ];

    protected static $logAttributes = [
        'name', 'description', 'color', 'color_hexcode', 'color_hexcode_sec','ordering',
        'parent_group', 'status', 'created_by', 'per_delegation', 'image_path',
    ];

	protected static function boot() {
		parent::boot();
		static::addGlobalScope('order', function (Builder $builder) {
			$builder->orderBy('ordering', 'asc');
		});
	}

    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status',0);
    }

    public function invitations()
    {
        return $this->hasMany('App\Models\Invitations', 'delegate_id', 'id');//, 'id', 'store'
    }

}
