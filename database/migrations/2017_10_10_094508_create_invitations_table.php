<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvitationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('event_id');
			$table->integer('delegation_id');
			$table->integer('delegate_id');
			$table->string('code');
			$table->integer('delegate_profile');
			$table->text('tech_requirements', 16777215)->nullable();
			$table->text('food_requirements', 16777215)->nullable();
			$table->string('vehicle_pass', 5)->nullable()->default('No');
			$table->text('vehicle_details', 16777215)->nullable();
			$table->string('flight_type')->nullable();
			$table->integer('created_by');
			$table->integer('sms_sent')->default(0);
			$table->integer('email_sent')->default(0);
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitations');
	}

}
