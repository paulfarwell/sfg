<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsMetaDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events_meta_data', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('data', 16777215);
			$table->boolean('status')->default(0);
			$table->integer('social_media')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events_meta_data');
	}

}
