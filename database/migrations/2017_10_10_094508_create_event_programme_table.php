<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventProgrammeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_programme', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('event_id');
			$table->string('p_type');
			$table->string('title');
			$table->text('description', 16777215);
			$table->dateTime('start');
			$table->dateTime('end');
			$table->integer('sort')->default(0);
			$table->integer('created_by');
			$table->string('status')->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_programme');
	}

}
