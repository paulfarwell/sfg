<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTransportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_transport', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('event_id');
			$table->string('t_type');
			$table->string('name');
			$table->text('description', 16777215);
			$table->string('location');
			$table->dateTime('valid_from');
			$table->dateTime('valid_to');
			$table->decimal('cost', 10, 0);
			$table->text('delegate', 16777215);
			$table->string('status')->default('active');
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_transport');
	}

}
