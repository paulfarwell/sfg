<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLaradropFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laradrop_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parent_id')->nullable()->index();
			$table->integer('lft')->nullable()->index();
			$table->integer('rgt')->nullable()->index();
			$table->integer('depth')->nullable();
			$table->string('type', 191)->nullable();
			$table->smallInteger('has_thumbnail')->default(0);
			$table->text('meta', 65535)->nullable();
			$table->string('filename', 191)->nullable();
			$table->string('alias', 191)->nullable();
			$table->string('public_resource_url', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laradrop_files');
	}

}
