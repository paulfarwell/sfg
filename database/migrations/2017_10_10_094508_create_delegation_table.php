<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDelegationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegation', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('event_id');
			$table->string('name');
			$table->string('country');
			$table->integer('created_by');
			$table->integer('type')->nullable();
			$table->string('contact_email')->nullable();
			$table->string('cc_contact_email')->nullable();
			$table->string('contact_name')->nullable();
			$table->integer('status')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegation');
	}

}
