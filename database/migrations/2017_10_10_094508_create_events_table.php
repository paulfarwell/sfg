<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('venue')->nullable();
			$table->dateTime('start')->nullable();
			$table->dateTime('end')->nullable();
			$table->text('theme', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->timestamps();
			$table->string('coordinates')->nullable()->default('0.0103211,37.0579145');
			$table->boolean('use_coordinates')->nullable()->default(1);
			$table->string('status', 50)->default('active');
			$table->integer('is_deleted')->nullable()->default(0);
			$table->integer('is_published')->nullable()->default(0);
			$table->integer('user_id');
			$table->text('file_attachment', 65535)->nullable();
			$table->text('custom_message', 65535)->nullable();
			$table->text('udf', 65535)->nullable();
			$table->string('slug')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
