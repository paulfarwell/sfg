<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDelegateProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegate_profile', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title')->nullable();
			$table->string('fname');
			$table->string('surname');
			$table->string('organisation')->nullable();
			$table->string('position')->nullable();
			$table->string('country')->nullable();
			$table->string('nationality')->nullable();
			$table->string('national_id')->nullable();
			$table->string('national_id_type')->nullable();
			$table->text('national_id_image', 16777215)->nullable();
			$table->string('email')->nullable();
			$table->string('cc_email')->nullable();
			$table->string('cell_phone')->nullable();
			$table->string('cell_phone_code', 45)->nullable();
			$table->string('cell_office')->nullable();
			$table->string('office_phone_code', 45)->nullable();
			$table->text('passport_image', 16777215)->nullable();
			$table->string('a_contact_name')->nullable();
			$table->string('a_contact_email')->nullable();
			$table->string('a_contact_phone')->nullable();
			$table->string('a_contact_office_phone')->nullable();
			$table->integer('created_by');
			$table->integer('user_id');
			$table->timestamps();


		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegate_profile');
	}

}
