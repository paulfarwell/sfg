<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsuranceproviderName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('delegate_profile', function($table) {

        $table->string('insurance_provider')->after('media')->nullable();
        $table->string('medical_conditions')->after('insurance_provider')->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('delegate_profile', function($table) {
        $table->dropColumn('insurance_provider');
        $table->dropColumn('medical_conditions');

    });
    }
}
