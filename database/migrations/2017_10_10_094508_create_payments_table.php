<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('payment_date')->nullable();
			$table->integer('event_id');
			$table->integer('invitation_id')->nullable()->default(0);
			$table->integer('delegation_id')->nullable()->default(0);
			$table->integer('parent_id')->nullable()->default(0);
			$table->integer('payment_group')->nullable()->default(0);
			$table->string('pay_method');
			$table->text('details', 65535);
			$table->float('amount', 10, 0);
			$table->string('reciept_number');
			$table->integer('created_by');
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
