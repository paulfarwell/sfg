<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvitationActTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitation_act', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('invitation_id');
			$table->integer('activity_id');
			$table->dateTime('arrival_date')->nullable();
			$table->dateTime('departure_date')->nullable();
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitation_act');
	}

}
