<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesAdvancedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries_advanced', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->string('topleveldomain')->nullable();
			$table->string('alpha2code')->nullable();
			$table->string('alpha3code')->nullable();
			$table->string('callingcodes')->nullable();
			$table->string('capital')->nullable();
			$table->string('altspellings')->nullable();
			$table->string('region')->nullable();
			$table->string('subregion')->nullable();
			$table->string('population')->nullable();
			$table->string('latlng')->nullable();
			$table->string('demonym')->nullable();
			$table->string('area')->nullable();
			$table->string('gini')->nullable();
			$table->string('timezones')->nullable();
			$table->string('borders')->nullable();
			$table->string('nativename')->nullable();
			$table->string('numericcode')->nullable();
			$table->string('currencies')->nullable();
			$table->string('languages')->nullable();
			$table->string('translations')->nullable();
			$table->string('flag')->nullable();
			$table->string('regionalblocs')->nullable();
			$table->timestamps();
			$table->string('code')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries_advanced');
	}

}
