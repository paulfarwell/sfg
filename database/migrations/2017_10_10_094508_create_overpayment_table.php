<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOverpaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('overpayment', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('payment_date')->nullable();
			$table->string('reciept_number')->nullable();
			$table->integer('delegation_id');
			$table->integer('payment_id');
			$table->float('amount', 10, 0);
			$table->text('description', 16777215)->nullable();
			$table->integer('status')->default(0);
			$table->integer('refund_id')->default(0);
			$table->integer('overpayment_group')->nullable()->default(0);
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('overpayment');
	}

}
