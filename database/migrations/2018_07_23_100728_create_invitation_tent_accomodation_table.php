<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationTentAccomodationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_tent_accomodation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('invitation_id');
            $table->integer('tent_id');
            $table->string('bed_type')->nullable();
            $table->string('tent_sharing_name')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_tent_accomodation');
    }
}
