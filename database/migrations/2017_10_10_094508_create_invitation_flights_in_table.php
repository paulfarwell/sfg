<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvitationFlightsInTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitation_flights_in', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('invitation_id');
			$table->string('flight_type');
			$table->string('flight_number')->nullable();
			$table->string('flight_carrier')->nullable();
			$table->string('airport_name')->nullable();
			$table->string('terminal')->nullable();
			$table->string('arrival_date')->nullable();
			$table->string('arrival_time')->nullable();
			$table->integer('created_by');
			$table->string('plane_reg')->nullable();
			$table->string('transfer_req', 4)->nullable()->default('No');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitation_flights_in');
	}

}
