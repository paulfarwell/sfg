<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_hotels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->text('description', 16777215);
			$table->string('website');
			$table->string('contact_person');
			$table->string('contact_phone');
			$table->string('contact_email');
			$table->text('coupon', 65535)->nullable();
			$table->string('address')->default('NA');
			$table->string('longitude')->default('0');
			$table->string('latitude')->default('0');
			$table->integer('available')->default(0);
			$table->integer('order_custom')->default(0);
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_hotels');
	}

}
