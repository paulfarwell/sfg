<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('r_id')->unsigned();
			$table->string('r_type');
			$table->text('filename', 65535);
			$table->text('image_path', 65535)->nullable();
			$table->boolean('is_main')->default(0);
			$table->integer('sort')->nullable()->default(0);
			$table->string('status')->default('active');
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('system_images');
	}

}
