<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsuranceProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('delegate_profile', function($table) {
        $table->string('membership_number')->after('a_contact_office_phone')->nullable();
        $table->string('membership_email')->after('membership_number')->nullable();
        $table->string('membership_phone_code')->after('membership_email')->nullable();
        $table->string('membership_telephone')->after('membership_phone_code')->nullable();
        $table->string('media')->after('membership_telephone')->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('delegate_profile', function($table) {
        $table->dropColumn('membership_number');
        $table->dropColumn('membership_email');
        $table->dropColumn('membership_phone_code');
        $table->dropColumn('membership_telephone');
        $table->dropColumn('media');
    });
    }
}
