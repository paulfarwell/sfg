<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_tickets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->integer('delegate_id');
			$table->string('name')->nullable()->default('NULL');
			$table->float('price', 10, 0);
			$table->integer('ticket_number')->unsigned()->default(200);
			$table->integer('minimum_level')->unsigned()->default(5);
			$table->boolean('email_low_level')->nullable()->default(0);
			$table->boolean('is_season')->default(0);
			$table->integer('usage_count')->default(1);
			$table->timestamp('valid_from')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('valid_to');
			$table->text('notes', 65535);
			$table->timestamps();
			$table->integer('no_complimentary')->default(0);
			$table->integer('is_deleted')->default(0);
			$table->boolean('active')->default(1);
			$table->dateTime('available_from')->nullable();
			$table->dateTime('available_to')->nullable();
			$table->boolean('pos')->default(1);
			$table->integer('max_forsale')->default(10);
			$table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_tickets');
	}

}
