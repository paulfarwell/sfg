<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('log_name')->nullable()->index('logname');
			$table->text('description')->nullable();
			$table->integer('subject_id')->nullable();
			$table->string('subject_type')->nullable();
			$table->integer('causer_id')->nullable();
			$table->string('causer_type')->nullable();
			$table->text('text')->nullable();
			$table->text('properties')->nullable();
			$table->string('ip_address', 64)->nullable();
			$table->text('url', 65535)->nullable();
			$table->string('method')->nullable();
			$table->text('agent', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_log');
	}

}
