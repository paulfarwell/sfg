<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('refunds', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('payment_date')->nullable();
			$table->integer('invitation_id');
			$table->integer('delegation_id')->nullable()->default(0);
			$table->integer('payment_id');
			$table->integer('event_id');
			$table->text('details', 65535);
			$table->float('amount', 10, 0);
			$table->string('reciept_number');
			$table->string('payout_method');
			$table->integer('created_by');
			$table->text('reason', 65535)->nullable();
			$table->integer('refund_group')->default(0);
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('refunds');
	}

}
