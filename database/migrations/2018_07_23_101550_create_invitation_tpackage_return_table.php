<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationTpackageReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_tpackage_return', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invitation_id');
      			$table->integer('tpackage_id');
      			$table->dateTime('arrival_date')->nullable();
      			$table->dateTime('departure_date')->nullable();
      			$table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_tpackage_return');
    }
}
