<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDelegatesTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegates_types', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('description');
			$table->string('color');
			$table->string('color_hexcode');
			$table->string('color_hexcode_sec')->nullable();
			$table->integer('parent_group')->default(0);
			$table->integer('status')->default(0);
			$table->integer('created_by')->default(1);
			$table->integer('per_delegation')->default(0);
			$table->text('image_path', 65535)->nullable();
			$table->text('ordering')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegates_types');
	}

}
