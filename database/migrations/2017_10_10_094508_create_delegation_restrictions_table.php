<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDelegationRestrictionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegation_restrictions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('delegation_id');
			$table->string('data_type')->nullable();
			$table->integer('delegate_id')->nullable();
			$table->integer('allowed_count')->nullable();
			$table->integer('created_by');
			$table->string('status')->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegation_restrictions');
	}

}
