<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvitationCustomFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitation_custom_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('invitation_id');
			$table->integer('delegate_id');
			$table->integer('field_id');
			$table->text('value', 16777215);
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitation_custom_fields');
	}

}
