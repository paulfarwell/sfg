<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsMapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events_map', function(Blueprint $table)
		{
			$table->integer('_id', true);
			$table->integer('event_id');
			$table->string('name')->nullable();
			$table->string('point_of_interest')->nullable();
			$table->string('lat')->nullable();
			$table->string('lng')->nullable();
			$table->string('location')->nullable();
			$table->string('location_type')->nullable();
			$table->string('formatted_address')->nullable();
			$table->string('bounds')->nullable();
			$table->string('viewport')->nullable();
			$table->string('route')->nullable();
			$table->string('street_number')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('locality')->nullable();
			$table->string('sublocality')->nullable();
			$table->string('country')->nullable();
			$table->string('country_short')->nullable();
			$table->string('administrative_area_level_1')->nullable();
			$table->string('place_id')->nullable();
			$table->string('id')->nullable();
			$table->text('reference', 16777215)->nullable();
			$table->text('url', 16777215)->nullable();
			$table->string('website')->nullable();
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events_map');
	}

}
