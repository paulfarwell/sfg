<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDelegationsTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegations_type', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('internal_name');
			$table->text('description', 65535)->nullable();
			$table->integer('created_by')->default(1);
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegations_type');
	}

}
