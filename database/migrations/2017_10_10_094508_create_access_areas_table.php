<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessAreasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('access_areas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('area');
			$table->string('description');
			$table->text('delegate_id', 16777215);
			$table->timestamps();
			$table->integer('created_by')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_areas');
	}

}
