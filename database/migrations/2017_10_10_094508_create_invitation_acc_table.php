<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvitationAccTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitation_acc', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('invitation_id');
			$table->integer('hotel_id');
			$table->string('name')->nullable();
			$table->dateTime('arrival_date')->nullable();
			$table->dateTime('departure_date')->nullable();
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitation_acc');
	}

}
