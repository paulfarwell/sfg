<?php

use Illuminate\Database\Seeder;

class DelegationsTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('delegations_type')->delete();
        
        \DB::table('delegations_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Head of State Delegation',
                'internal_name' => 'Head of State Delegation',
                'description' => '1. Head of State 2. National Government Ministers',
                'created_by' => 1,
                'status' => 0,
                'created_at' => '2017-08-22 20:48:46',
                'updated_at' => '2017-09-11 12:45:35',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Individual Delegation',
                'internal_name' => 'Individual Delegation',
                'description' => '1. Individual invitees 2. Their support team',
                'created_by' => 1,
                'status' => 0,
                'created_at' => '2017-08-22 20:48:46',
                'updated_at' => '2017-09-11 12:47:59',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Event Team & Suppliers',
                'internal_name' => 'Event Team & Suppliers',
                'description' => '1. Event team members 2. External suppliers',
                'created_by' => 1,
                'status' => 0,
                'created_at' => '2017-08-22 20:49:40',
                'updated_at' => '2017-09-11 12:48:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Media',
                'internal_name' => 'Media',
                'description' => NULL,
                'created_by' => 1,
                'status' => 0,
                'created_at' => '2017-08-22 20:49:40',
                'updated_at' => '2017-08-22 20:49:40',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Performer',
                'internal_name' => 'Performer',
                'description' => 'Performers at events',
                'created_by' => 1,
                'status' => 1,
                'created_at' => '2017-08-22 20:50:06',
                'updated_at' => '2017-09-17 23:52:39',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'test 34',
                'internal_name' => 'test 34',
                'description' => 'test 45',
                'created_by' => 1,
                'status' => 1,
                'created_at' => '2017-08-22 23:55:30',
                'updated_at' => '2017-09-11 10:11:41',
            ),
        ));
        
        
    }
}