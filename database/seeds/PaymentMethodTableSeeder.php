<?php

use Illuminate\Database\Seeder;

class PaymentMethodTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_method')->delete();
        
        \DB::table('payment_method')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'RTGS',
                'description' => NULL,
                'created_by' => 1,
                'created_at' => '2017-09-07 07:34:14',
                'updated_at' => '2017-09-07 07:34:14',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Cash',
                'description' => NULL,
                'created_by' => 1,
                'created_at' => '2017-09-07 07:34:14',
                'updated_at' => '2017-09-07 07:34:14',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Paypal',
                'description' => NULL,
                'created_by' => 1,
                'created_at' => '2017-09-07 07:34:39',
                'updated_at' => '2017-09-07 07:34:39',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Credit/Debit Card',
                'description' => NULL,
                'created_by' => 1,
                'created_at' => '2017-09-07 07:34:39',
                'updated_at' => '2017-09-07 07:34:39',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Visa',
                'description' => NULL,
                'created_by' => 1,
                'created_at' => '2017-09-07 07:34:55',
                'updated_at' => '2017-09-07 07:34:55',
            ),
        ));
        
        
    }
}