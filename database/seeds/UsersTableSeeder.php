<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Josiah Ngige',
                'email' => 'josina08@gmail.com',
                'password' => '$2y$10$9rZMe9RC/KwgEE0X1yC3d.8dQPaTbc/SeAyI4ej/HatzbtbtzpWSO',
                'remember_token' => 'ascP3TrREIs3pnwqE1zBXiI5UbabHLmycqPSfXdOZnd4NEpJEw1jd29feiJ0',
                'created_at' => '2017-06-19 00:00:00',
                'updated_at' => '2017-09-27 09:00:15',
                'current_team_id' => 1,
                'is_disabled' => 0,
                'code' => NULL,
                'first_time_login' => 1,
                'confirmed_email' => 0,
                'correspondence_email' => NULL,
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'JOSIAH NGIGE',
                'email' => 'josiahngige@yahoo.com',
                'password' => '$2y$10$IdsgZ.JpnLdD2MXMKbpRIOMYtfC/gDSt5tvHapMRvUaz87nLff9Nu',
                'remember_token' => 'CdCQYFJTyQhhftyLF9U8A2cKE1lZbgdmCkfTjCegmagHdkToT5uProRxr1A5',
                'created_at' => '2017-07-24 21:40:44',
                'updated_at' => '2017-08-23 17:50:53',
                'current_team_id' => 1,
                'is_disabled' => 0,
                'code' => 'SGS-XHXYPH',
                'first_time_login' => 1,
                'confirmed_email' => 1,
                'correspondence_email' => 'josiahngige@yahoo.com',
            ),
        ));
        
        
    }
}