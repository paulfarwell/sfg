<?php

use Illuminate\Database\Seeder;

class TitlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('titles')->delete();
        
        \DB::table('titles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Mr.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:53:40',
                'updated_at' => '2017-10-02 10:53:40',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Mrs.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:53:40',
                'updated_at' => '2017-10-02 10:53:40',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ms.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:53:40',
                'updated_at' => '2017-10-02 10:53:40',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Dr.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:53:40',
                'updated_at' => '2017-10-02 10:53:40',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Prof.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:53:40',
                'updated_at' => '2017-10-02 10:53:40',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'His Excellency',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:54:52',
                'updated_at' => '2017-10-02 10:54:52',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Her Excellency',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:54:52',
                'updated_at' => '2017-10-02 10:54:52',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Hon.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:54:52',
                'updated_at' => '2017-10-02 10:54:52',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'His Excellency Hon.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:54:52',
                'updated_at' => '2017-10-02 10:54:52',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Her Excellency Hon.',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 10:54:52',
                'updated_at' => '2017-10-02 10:54:52',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Elcapitan',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 11:27:25',
                'updated_at' => '2017-10-02 12:56:10',
            ),
        ));
        
        
    }
}