<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'access to all pages',
                'created_at' => '2017-06-17 00:00:00',
                'updated_at' => '2017-06-17 00:00:00',
                'special' => 'all-access',
                'status' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'support',
                'slug' => 'support',
                'description' => 'support staff limited access',
                'created_at' => '2017-06-17 00:00:00',
                'updated_at' => '2017-06-17 00:00:00',
                'special' => NULL,
                'status' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'accountant',
                'slug' => 'accountant',
                'description' => 'support staff limited access',
                'created_at' => '2017-06-17 00:00:00',
                'updated_at' => '2017-06-17 00:00:00',
                'special' => NULL,
                'status' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'delegate',
                'slug' => 'delegate',
                'description' => 'delegate access only',
                'created_at' => '2017-06-17 00:00:00',
                'updated_at' => '2017-06-17 00:00:00',
                'special' => NULL,
                'status' => 0,
            ),
        ));
        
        
    }
}