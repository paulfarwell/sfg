<?php

use Illuminate\Database\Seeder;

class NewsCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news_categories')->delete();
        
        \DB::table('news_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Accreditation Procedure',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'General Information',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Accommodation',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Travel',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Ground Transportation',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Media',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Wifi',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Security',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Firearms and Drones',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Medical and Health Services',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:07:57',
                'updated_at' => '2017-09-29 11:07:57',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Pre and Post Summit Safari Packages',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:08:10',
                'updated_at' => '2017-09-29 11:08:10',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Payment of Registration Fees',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-09-29 11:08:10',
                'updated_at' => '2017-09-29 11:08:10',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Activities',
                'status' => 'Active',
                'order_custom' => 0,
                'created_at' => '2017-10-02 11:26:21',
                'updated_at' => '2017-10-02 11:26:21',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Programme',
                'status' => 'Active',
                'order_custom' => 10,
                'created_at' => '2017-10-02 12:29:00',
                'updated_at' => '2017-10-02 12:29:00',
            ),
        ));
        
        
    }
}